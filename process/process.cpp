#include <algorithm>
#include <cstring>
#include <fstream>
#include <string>
#include <unordered_map>
#include <vector>
#include <mutex>

#include "process.h"
#include "hle/hle.h"
#include "loader/sce_elf.h"
#if defined(_WIN64) || defined(__CYGWIN__)
#include <windows.h>
#include <immintrin.h>
#else
#include <sys/mman.h>
#endif

namespace process {

static char game_path[0x100]{};

constexpr uint32_t TLS_MODULE_ID_MAIN = 1;

static std::vector< void* > tls_slots;
static std::vector< loader::sce_elf* > loaded_images;
static std::unordered_map< std::string, std::string > nid_names;

static void* proc_param{};

static exports exports {};
static std::vector<std::string> loaded_libraries, init_order;
static std::vector<std::string> m_messageList(10000);

static const std::unordered_map<std::string, std::vector<std::string>> prx_names {
        {"libSceNpScore", {"/sce_module/libSceNpScoreRanking.prx", "/system/common/lib/libSceNpScoreRanking.sprx"}},
        {"libSceAppContentUtil", {"/system/common/lib/libSceAppContent.sprx"} },
        {"libSceDipsw", {"/system/priv/lib/libSceDipsw.sprx"}},
        {"libGameCustomDataDialog", {"/system/common/lib/libSceGameCustomDataDialog.sprx"}},
        {"libSceJson", {"/system/common/lib/libSceJson2.sprx"}},

};

static const std::vector<std::string> skip_init_proc {
    "eboot.bin",
#ifdef __APPLE__
    "libkernel",
#endif
    "libSceSysmodule",
    "libSceGnmDriver",
    "libSceSystemService",
    "libSceHmd",
    "libSceCommonDialog",
    "libSceNpManager",
    "libSceNpScore",
    "libSceNpTrophy",
    "libSceAppContentUtil",
    "libSceAudioIn",
    "libSceDipsw",
};

thread_local void* t_fsbase = nullptr;
std::mutex m_mutex;
union DTV {
    void* pointer;
    uintptr_t counter;
};
struct TCB
{
    void* segbase;
    void* dtv;
};
struct TLSBlock {
    // TLS segment address in loaded module.
    void* address = nullptr;
    // initialized TLS data size
    uint32_t initSize = 0;
    // total TLS data size (initialized + uninitialized)
    uint32_t totalSize = 0;
    // alignment
    uint32_t align = 0;
    // tls index == module id
    uint32_t index = 0;
    // Dynamically-loaded modules here donЎЇt mean any dynamic shared objects,
    // they only refer to the shared objects that are loaded by explicitly calling dlopen
    bool isDynamic = false;
    // TLS image offset at TCB block
    uint32_t offset = 0;
};
std::vector<std::pair<TLSBlock, std::vector<uint8_t>>> m_TLSImages;

bool load_prx(const char* path, const char* lib_name);
void initialize_tls();

PS4API void ps4_exit() {
    LOG_DEBUG("ps4 exit callback");
    exit(0);
}

void load_aero_lib() {
    std::ifstream ifs("aerolib.csv");
    if(!ifs.good()) {
        return;
    }
    std::string a, b;
    while(ifs >> a >> b) {
        //printf("%s, %s", a.c_str(), b.c_str());
        nid_names[a] = b;
    }
    ifs.close();
}

void free_images() {
    //free all memory
    for (auto& elf : loaded_images) {
        elf->free_memory();
    }
}

void* get_tls(uint64_t slot) {
    if (slot >= tls_slots.size()) {
        return nullptr;
    }
    return tls_slots[slot];
}

void* get_proc_param(){
    return proc_param;
}
void setup_tls(loader::sce_elf& sce_elf) {
    if (sce_elf.has_phdr(PT_TLS)) {
        auto tls_phdr = sce_elf.get_phdr(PT_TLS);
        if (tls_phdr.memsz == 0) {
            return;
        }
        void *tls = sce_elf.set_tls_slot(tls_slots.size());
        tls_slots.emplace_back(tls);
    }
}

void test_lib() {
//    auto malloc_ = (PS4API void*(*)(size_t))get_export("libc", "gQX+4GDQjpM");
//    void* d  = malloc_(0x10000);
//    if(!d) {
//        std:printf("cannot malloc!\n");
//        return;
//    }
//    auto free_ = (PS4API void(*)(void*))get_export("libc", "tIhsqj0qsFE");
//    free_(d);
//
    LOG_DEBUG("----test----");
    auto new_ = call_native2<void*>("libSceLibcInternal", "gQX+4GDQjpM", 100);// "fJnpuVVBbKk");
    printf("malloc %p\n", new_);

    int rc = call_native("libSceSaveData", "TywrFKCoLGY");
    printf("init %u\n", rc);

//    auto init = (PS4API int(*)())get_export("libSceSaveData", "TywrFKCoLGY");
//    int n = init();
    printf("init %u\n", rc);
}

void get_resource_path(const char* ps4name, char filename[255]) {
    std::string s{game_path};
    if (ps4name[0] != '/') {
        s += '/';
    }
    s += ps4name;
    std::strcpy(filename, s.c_str());
}

void init_loaded_modules() {
    for (const auto& lib_name: init_order ) {
        const auto lib = std::find_if(loaded_images.begin(), loaded_images.end(), [lib_name](const auto elf) { return elf->get_name() == lib_name; } );
        if ( (lib != loaded_images.end()) && (std::find(skip_init_proc.begin(), skip_init_proc.end(), (*lib)->get_name()) == skip_init_proc.end())) {
            LOG_DEBUG("(%s): _init_proc() start.", (*lib)->get_name());
            auto ptr = (PS4API int (*)(int64_t p0, int64_t p1, void *p2)) (*lib)->init_proc();
            int rc = ptr(0, 0, nullptr);
            LOG_DEBUG("(%s): _init_proc() end. rc = %d", (*lib)->get_name(), rc);
        }
    }
}

start_func load_app(const char* path) {
    m_messageList.reserve(10000);
    load_aero_lib();
    std::strcpy(game_path, path);
    char app_name[0x100] {};
    std::strcpy(app_name, path);
    strcat(app_name, "/app0/eboot.bin");

    auto sce_elf = new loader::sce_elf("eboot.bin");
    if (!sce_elf->load(app_name) ) {
        delete sce_elf;
        return nullptr;
    }
    sce_elf->process_dynamic();
    for (const auto lib: sce_elf->import_libraries()) {
        load_prx(path, lib.name);
    }
    sce_elf->map_memory();
    proc_param = sce_elf->get_proc_param();
    LOG_DEBUG("loaded eboot  at address: %p, %s", sce_elf->get_image(), app_name);
    loaded_images.insert(loaded_images.begin(), sce_elf);
    setup_tls(*sce_elf);
    sce_elf->resolve_imports();
    sce_elf->apply_relocations();
    sce_elf->apply_protection();
    initialize_tls();
    init_loaded_modules();

    //test_lib();

    return reinterpret_cast<start_func>(sce_elf->entry_point());
}

bool load_hle(const char* lib_name) {
    export_funcs& e = exports[lib_name];
    return hle::load_hle_library(lib_name, e);
}

std::vector<std::string> get_known_names(const char* lib_name) {
    const auto it = prx_names.find(lib_name);
    if (it != prx_names.end()) {
        return it->second;
    }
    std::vector<std::string> names;
    std::string name = std::string("/app0/sce_module/") + std::string(lib_name) + ".prx";
    names.emplace_back(name);
    name = std::string("/system/common/lib/") + std::string(lib_name) + ".sprx";
    names.emplace_back(name);
    return std::move(names);
}

bool load_native_lib(const char* path, const char* lib_name) {
    const auto names = get_known_names(lib_name);
    loader::sce_elf* sce_elf{};
    for (const auto& name: names) {
        std::string prx_name = std::string(path) + name;
        sce_elf = new loader::sce_elf(lib_name);
        if(!sce_elf->load(prx_name.c_str())) {
            delete sce_elf; sce_elf = nullptr;
            continue;
        }

        sce_elf->process_dynamic();
        sce_elf->map_memory();
        loaded_images.emplace_back(sce_elf);
        setup_tls(*sce_elf);
        sce_elf->export_symbols();
        loaded_libraries.emplace_back(std::string(lib_name));
        for (const auto lib: sce_elf->import_libraries()) {
            if ( !::strcmp(lib.name, lib_name)) {
                continue;// don't import self
            }
            if(!load_prx(path, lib.name)) {
                return false;
            }
        }
        sce_elf->resolve_imports();
        sce_elf->apply_relocations();
        sce_elf->apply_protection();

        if ( std::find(init_order.begin(), init_order.end(), std::string(lib_name)) == init_order.end() ) {
            init_order.emplace_back(lib_name);
        }

        LOG_DEBUG("loaded native at address: %p, size: 0x%lx, '%s', '%s'", (void*)sce_elf->get_image(), sce_elf->get_image_size(), lib_name, name.c_str());

        return true;
    }
    LOG_DEBUG("can't load native library: %s", lib_name);
    return false;
}

bool load_prx(const char* path, const char* lib_name) {
    const auto lib = std::find(loaded_libraries.begin(), loaded_libraries.end(), lib_name);
    if (lib != loaded_libraries.end()) {
        return true;
    }
    bool rc = false;
    if (load_hle(lib_name)) {
        loaded_libraries.emplace_back(std::string(lib_name));
        rc = true; // at least hle library loaded
    }
    return rc | load_native_lib(path, lib_name);
}

const std::vector<uint8_t> funcTemplate = {
        0x48, 0x83, 0xEC, 0x48,			// sub rsp, 0x48
        0x48, 0x89, 0x4C, 0x24, 0x08,	// rcx
        0x48, 0x89, 0x54, 0x24, 0x10,	// rdx
        0x4C, 0x89, 0x44, 0x24, 0x18,	// r8
        0x4C, 0x89, 0x4C, 0x24, 0x20,	// r9
        0x4C, 0x89, 0x54, 0x24, 0x28,	// r10
        0x4C, 0x89, 0x5C, 0x24, 0x30,	// r11

        0x48, 0xB9, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, // movabs rcx, _str
        0x48, 0xB8, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, // movabs rax, _logFunc
        0xFF, 0xD0, // call rax

        // restore argument registers
        0x48, 0x8B, 0x4C, 0x24, 0x08,
        0x48, 0x8B, 0x54, 0x24, 0x10,
        0x4C, 0x8B, 0x44, 0x24, 0x18,
        0x4C, 0x8B, 0x4C, 0x24, 0x20,
        0x4C, 0x8B, 0x54, 0x24, 0x28,
        0x4C, 0x8B, 0x5C, 0x24, 0x30,
        0x48, 0x83, 0xC4, 0x48, // add rsp,0x48

        0x48, 0xB8, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, 0xAA, //mov rax, _dest
        0xFF, 0xE0 // jmp rax
};

static void logFunc(const char *log) {
    LOG_DEBUG("%s", log);
}

template <typename T>
void patch(void* memory, size_t offset, T value) {
    auto ptr = reinterpret_cast<T *>((uint8_t*)memory + offset);
    *ptr     = value;
}

void patchLogString(void* memory, const char *logString) {
    patch(memory, 0x24, reinterpret_cast<uint64_t>(logString));
}

void patchLogFunction(void* memory, void (*logFunctionPtr)(const char *))
{
    patch(memory, 0x2e, reinterpret_cast<uint64_t>(logFunctionPtr));
}

void patchDestPointer(void* memory, const void *dest) {
    patch(memory, 0x5c, reinterpret_cast<uint64_t>(dest));
}

//#define LOG_EXPORTS_CALL

export_func generate(std::string const &message, void* dest) {
#ifdef LOG_EXPORTS_CALL
    auto index = m_messageList.size();
    m_messageList.push_back(message);

    auto &msg   = m_messageList.at(index);
    auto msgPtr = msg.c_str();

    void* funcMem = nullptr;
#if defined(_WIN64) || defined(__CYGWIN__)
    funcMem = _aligned_malloc(4096, 0x10);
    if (funcMem == nullptr) {
        return nullptr;
    }
    DWORD oldProtect;
    VirtualProtect((LPVOID)funcMem, 4096, PAGE_EXECUTE_READWRITE, &oldProtect);
#else
     int rc = posix_memalign(&funcMem, 4096, 0x10);
     rc = ::mprotect(funcMem, 4096, PROT_READ | PROT_WRITE | PROT_EXEC);
#endif
    memcpy(funcMem, funcTemplate.data(), funcTemplate.size());

    patchLogString(funcMem, msgPtr);
    patchLogFunction(funcMem, logFunc);
    patchDestPointer(funcMem, dest);

    return (export_func)funcMem;
#else
    return (export_func)dest;
#endif
}

void add_export_func(const char* lib_name, const char* func_name, export_func func) {
    export_funcs& e = exports[lib_name];

    std::string f {func_name};
    if (nid_names.count(func_name) != 0) {
        f = nid_names[func_name];
    }
    char buf[0x200];
    std::snprintf(buf, 0x200, "call func : '%s' from library: '%s'", f.c_str(), lib_name);

    if (e.count(func_name) == 0) {
        e[func_name] = std::make_pair(nullptr, generate(buf, (void*)func));
    } else {
        //LOG_DEBUG("already hle: %s", func_name);
        auto& p = e[func_name];
        p.second = generate(buf, (void*)func);
    }
}

export_func get_export(const char* lib_name, const char* name, bool force_native) {
    const auto& library = exports.find(lib_name);
    if (library == exports.end()) {
        return nullptr;
    }

    const auto& addr = library->second.find(name);
    if (addr == library->second.end()) {
        return nullptr;
    }
    const auto p = addr->second;
    if (force_native) {
        return p.second;
    }
    return (p.first != nullptr) ? p.first : p.second;
}

template <typename T, typename U = T>
inline T alignRound(T size, U align) {
    return (size + align - 1) & ~(align - 1);
}

size_t calculateStaticTLSSize() {
    size_t size = 0;
    for (const auto& imgPair : m_TLSImages) {
        if (!imgPair.first.isDynamic) {
            size += alignRound(imgPair.first.totalSize, imgPair.first.align);
        }
    }
    return size;
}

void* allocateTLS() {
    std::lock_guard<std::mutex> lock(m_mutex);

    TCB* tcbSegbase = nullptr;

    size_t imageSize = calculateStaticTLSSize();
    if (!imageSize) {
        return reinterpret_cast<void*>(tcbSegbase);
    }

    uint32_t moduleCount = m_TLSImages.size();
    auto tlsAndTCB   = reinterpret_cast<uint8_t*>(calloc(1, imageSize + sizeof(TCB)));
    DTV* dtv             = reinterpret_cast<DTV*>(calloc(1, (moduleCount + 2) * sizeof(DTV)));

    dtv[0].counter = moduleCount;
    dtv[1].counter = moduleCount;

    tcbSegbase          = reinterpret_cast<TCB*>(tlsAndTCB + imageSize);
    tcbSegbase->segbase = tcbSegbase;
    tcbSegbase->dtv     = dtv;

    for (const auto& imgPair : m_TLSImages)
    {
        void* dst = reinterpret_cast<uint8_t*>(tcbSegbase) - imgPair.first.offset;
        // copy tls image backup to new allocated memory bound to current thread.
        std::memcpy(dst, imgPair.second.data(), imgPair.second.size());
        // update dtv array
        dtv[imgPair.first.index + 1].pointer = dst;
    }
    return reinterpret_cast<void*>(tcbSegbase);
}

void* read_fs_register(int32_t offset) {
    if (!t_fsbase)
    {
        t_fsbase = allocateTLS();
    }
    return reinterpret_cast<uint8_t*>(t_fsbase) + offset;
}

void backup_tls_image(std::vector<uint8_t>& image, const TLSBlock& block)
{
    size_t algnedSize = alignRound(block.totalSize, block.align);
    image.resize(algnedSize);
    memcpy(image.data(), block.address, block.initSize);

    uint32_t uninitSize = algnedSize - block.initSize;
    memset(image.data() + block.initSize, 0, uninitSize);
}

void allocate_tls_offset(TLSBlock& block) {
    uint32_t offset = 0;
    if (block.index == TLS_MODULE_ID_MAIN)
    {
        offset = alignRound(block.totalSize, block.align);
    }
    else
    {
        uint32_t lastTLSOffset = calculateStaticTLSSize();
        offset                 = lastTLSOffset + alignRound(block.totalSize, block.align);
    }
    block.offset = offset;
}

void register_tls_block(const TLSBlock& block) {

    if (!block.address || !block.totalSize) {
        return;
    }
    std::vector<uint8_t> tlsImage;
    backup_tls_image(tlsImage, block);
    TLSBlock newBlock = block;
    allocate_tls_offset(newBlock);
    m_TLSImages.emplace_back(newBlock, std::move(tlsImage));
}

void initialize_tls() {
    uint32_t tlsIndex = TLS_MODULE_ID_MAIN;
    for (auto const &mod : loaded_images) {
        void *pTls     = nullptr;
        uint64_t initSize  = 0;
        uint64_t totalSize = 0;
        uint32_t align     = 0;
        bool retVal        = mod->get_tls_info(&pTls, &initSize, &totalSize, &align);
        if (!retVal || pTls == nullptr) {
            LOG_DEBUG("no TLS info for module:%s", mod->get_name());
            continue;
        }
        TLSBlock block;
        block.address   = pTls;
        block.initSize  = initSize;
        block.totalSize = totalSize;
        block.align     = align;
        block.index     = tlsIndex;
        block.isDynamic = false;
        block.offset    = 0;
        register_tls_block(block);

        ++tlsIndex;
    }
}

}
