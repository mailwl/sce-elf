#pragma once

#include "common.h"

namespace process {
start_func load_app(const char* path);
void add_export_func(const char* lib_name, const char* name, export_func func);
export_func get_export(const char* lib_name, const char* name, bool force_native = false);
PS4API void ps4_exit();
void* get_tls(uint64_t slot);
void* get_proc_param();
void free_images();
void get_resource_path(const char* ps4name, char filename[255]);
void* read_fs_register(int32_t offset);
}

