#include "ipmi.h"
#include <unordered_map>
namespace kernel {

static std::unordered_map<uint32_t, IpmiClient*> handles;

IpmiClient::IpmiClient() {
    handle++;
}
IpmiClient::~IpmiClient() {}
int IpmiClient::Initialize(void* arg1, const std::string& name, void* arg3) {
    return 0;
}
int IpmiClient::PrepareConnect(uint32_t event_flag_count) {
    return 0;
}
int IpmiClient::Connect(uint64_t* session_key, uint32_t* unknown, uint32_t* session_id, uint32_t* result) {
    return 0;
}

int ipmimgr_call(uint32_t op, uint32_t handle, uint32_t* result, void* args_buffer, size_t args_size, uint64_t cookie) {
    errno = 0;
    if (args_size > 64) {
        return 0x800E0001;
    }
    switch (static_cast<kernel::ipmimgr_op>(op)) {
    default:
        return 0;
    case kernel::ipmimgr_op::TryGetMessage: {
        *result = 0;
        return 0;
    }
    case kernel::ipmimgr_op::ConnectWithWaitServer: {
        *result = 0;
        return 0;
    }
    case kernel::ipmimgr_op::CreateClient: {
        struct op_args {
            void* arg1;
            const char* name;
            void* arg3;
        };
        auto args = static_cast<op_args*>(args_buffer);
        LOG_DEBUG("ipmimgr_call create: %s", args->name);
        auto ipmi_client = ::new kernel::IpmiClient();
        auto init_result = ipmi_client->Initialize(args->arg1, std::string(args->name), args->arg3);
        if (init_result != 0) {
            ::delete ipmi_client;
            return init_result;
        }
        handles[ipmi_client->handle] = ipmi_client;
        *result = ipmi_client->handle;
        return 0;
    }

    case kernel::ipmimgr_op::DestroyClient: {
        handles.erase(handle);
        *result = 0;
        return 0;
    }

    case kernel::ipmimgr_op::Trace: {
        if (!args_buffer || args_size < 64) {
            return -1;
        }
        struct trace_args {
            uint32_t client_handle;
            uint32_t unknown_04;
            char name[25];
            uint32_t unknown_24;
            uint32_t unknown_28;
            uint32_t unknown_2C;
            uint32_t unknown_30;
            uint32_t unknown_34;
            uint32_t unknown_38;
            uint32_t unknown_3C;
        };
        auto args = static_cast<trace_args*>(args_buffer);
        LOG_DEBUG("ipmi trace(%u): client handle=%d %u name='%s' %u %u %u %u %u %u %u\n", handle, args->client_handle,
                  args->unknown_04, args->name, args->unknown_24, args->unknown_28, args->unknown_2C, args->unknown_30,
                  args->unknown_34, args->unknown_38, args->unknown_3C);
        return 0;
    }

    case kernel::ipmimgr_op::__u529: { // prepare connect?
        struct op_data {
            uint32_t pid;
            uint32_t unknown_004;
            uint32_t unknown_008;
            uint32_t unknown_00C;
            uint64_t unknown_010;
            uint64_t unknown_018;
            uint32_t unknown_020;
            uint32_t unknown_024;
            uint64_t unknown_028;
            uint64_t unknown_030;
            uint32_t event_flag_count;
            uint32_t unknown_03C;
            uint32_t unknown_040;
            uint32_t unknown_044;
            uint64_t unknown_048[32]; // just to reduce the complexity of the struct for now
            uint64_t unknown_148;
            uint32_t unknown_150;
            uint32_t unknown_154;
            uint32_t unknown_158;
            uint32_t client_handle;
        };
        struct op_args {
            op_data* data;
            uint64_t arg2;
            size_t data_size;
            uint64_t arg4;
        };
        auto args = static_cast<op_args*>(args_buffer);
        auto ipmi_client = handles[handle];
        if (!ipmi_client) {
            return -1;
        }
        auto prepare_result = ipmi_client->PrepareConnect(args->data->event_flag_count);
        if (prepare_result != 0) {
            return prepare_result;
            return false;
        }
        return 0;
    }

    case kernel::ipmimgr_op::__u531: // connect?
    {
        struct op_args {
            uint64_t* session_key;
            uint32_t* unknown;
            uint32_t* session_id;
            uint32_t* result;
        };
        auto args = static_cast<op_args*>(args_buffer);
        auto ipmi_client = handles[handle];
        if (!ipmi_client) {
            return -1;
        }
        auto connect_result = ipmi_client->Connect(args->session_key, args->unknown, args->session_id, args->result);
        if (connect_result != 0) {
            return connect_result;
        }
        *result = 0;
        return true;
    }

    case kernel::ipmimgr_op::__u561:
    case kernel::ipmimgr_op::__u563: {
        *result = 0;
        return 0;
    }
    }
    return 0;
}
} // namespace kernel
