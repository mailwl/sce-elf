#pragma once

#include "common.h"

namespace memory {
int alloc_direct(uint64_t searchStart, uint64_t searchEnd, size_t len, size_t alignment, int memoryType,
          uint64_t* physAddrOut);
int map_direct(void** addr, size_t len, int prot, int flags, sceoff_t directMemoryStart, size_t alignment);
int protect_direct();
int free_direct(sceoff_t start, size_t len);
size_t size_direct();
} // namespace direct_memory
