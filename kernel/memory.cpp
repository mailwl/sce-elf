#include "memory.h"
#include <vector>

#if defined(_WIN64) || defined(__CYGWIN__)
#else
#include <sys/mman.h>
#endif

namespace memory {

static uint64_t direct = 0x400000;
static size_t direct_size =  0x180000000u;
static std::vector<void*> direct_memorys;
int alloc_direct(uint64_t searchStart, uint64_t searchEnd, size_t len, size_t alignment, int memoryType,
            uint64_t* physAddrOut) {
    void* memory = nullptr;
#if defined(_WIN64) || defined(__CYGWIN__)
    // TODO: virtual alloc
    memory = _aligned_malloc(len, alignment);
#else
    memory = ::mmap((void*)direct, len, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
#endif
    direct_memorys.emplace_back(memory);
    *physAddrOut = (uint64_t)memory;
    direct_size -= len;
    direct += len;
    return 0;
}

int map_direct(void** addr, size_t len, int prot, int flags, sceoff_t directMemoryStart, size_t alignment) {
    auto memory = (uint8_t*)(*addr);
    if (memory == nullptr) {
        *addr = (void*)directMemoryStart;
    } else {
        // TODO: check if it right
        if (directMemoryStart) {
            *addr = (void*)directMemoryStart;
        }
    }
    return 0;
}

int protect_direct() {
    return 0;
}

int free_direct(sceoff_t start, size_t len) {
#if defined(_WIN64) || defined(__CYGWIN__)
    //::free((void*)start);
#else
    //::free((void*)start);
#endif
}
size_t size_direct() {
    return direct_size;
}
}
