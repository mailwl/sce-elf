#pragma once

#include "common.h"

namespace kernel {

enum class ipmimgr_op : uint32_t {
    CreateServer = 0,
    DestroyServer = 1,
    CreateClient = 2,
    DestroyClient = 3,
    CreateSession = 4,
    DestroySession = 5,
    Trace = 16,
    ReceivePacket = 513,
    __u514 = 514,
    __u529 = 529, // connect related
    __u530 = 530, // connect related
    __u531 = 531, // connect related
    __u546 = 546,
    __u547 = 547,
    __u561 = 561, // InvokeSyncMethod
    __u563 = 563,
    InvokeAsyncMethod = 577,
    TryGetResult = 579,
    GetMessage_ = 593,
    TryGetMessage = 594,
    SendMessage_ = 595,
    TrySendMessage = 596,
    EmptyMessageQueue = 597,
    __u609 = 609,
    __u784 = 784,
    ConnectWithWaitServer = 1024,
    GetUserData = 1126,
};

class IpmiClient {
public:
    IpmiClient();
    virtual ~IpmiClient();
    int Initialize(void* arg1, const std::string& name, void* arg3);
    int PrepareConnect(uint32_t event_flag_count);
    int Connect(uint64_t* session_key, uint32_t* unknown, uint32_t* session_id, uint32_t* result);
    static inline uint32_t handle{0};
};
int ipmimgr_call(uint32_t op, uint32_t handle, uint32_t* result, void* args_buffer, size_t args_size, uint64_t cookie);
}