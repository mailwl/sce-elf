#pragma once

#if defined(_WIN64) || defined(__CYGWIN__)
#else
#include <signal.h>
#endif

enum class LogLevel : int {
    kTrace,
    kDebug,
    kFixme,
    kWarning,
    kError,
    kSceTrace,
    kSceGraphic,
};

namespace logger {

void init_logging();

void log_print(LogLevel nLevel, const char* szFunction, const char* szSourcePath, int nLine, const char* szFormat, ...);
#define _LOG_PRINT_(level, format, ...) logger::log_print(level, __FUNCTION__, __FILE__, __LINE__, format, ##__VA_ARGS__)
#define LOG_DEBUG(format, ...)	_LOG_PRINT_(LogLevel::kDebug, format, ##__VA_ARGS__);
#define LOG_TRACE(format , ...) _LOG_PRINT_(LogLevel::kFixme, format, ##__VA_ARGS__);
#if defined(_WIN64)
#define UNIMPLEMENTED_FUNC() _LOG_PRINT_(LogLevel::kDebug, "Not Implemented!"); __debugbreak()
#else
#define UNIMPLEMENTED_FUNC() _LOG_PRINT_(LogLevel::kDebug, "Not Implemented!"); raise(SIGTRAP)
#endif
} // namespace logger


