#include "logger.h"

#include <spdlog/spdlog.h>
#include <spdlog/sinks/msvc_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/basic_file_sink.h>

namespace logger {
#define LOG_STR_BUFFER_LEN 2000
static std::unique_ptr<spdlog::logger> g_logger;

void init_logging() {
    auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
    console_sink->set_level(spdlog::level::trace); // message generating filter
    console_sink->set_pattern("[%t][%^%l%$]%v");

    auto file_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>("sce-elf.log", true);
    file_sink->set_level(spdlog::level::trace);
    file_sink->set_pattern("[%t][%^%l%$]%v");

    g_logger.reset(new spdlog::logger("sce-elf", { console_sink, file_sink }));
    g_logger->set_level(spdlog::level::trace); // message showing filter
}

void log_print(LogLevel nLevel, const char* szFunction, const char* szSourcePath, int nLine, const char* szFormat, ...) {
    va_list stArgList;
    va_start(stArgList, szFormat);
    char szTempStr[LOG_STR_BUFFER_LEN + 1] = { 0 };
    std::vsnprintf(szTempStr, LOG_STR_BUFFER_LEN, szFormat, stArgList);
    va_end(stArgList);


    switch (nLevel)
    {
        case LogLevel::kDebug:
            g_logger->debug("{}({}): {}, {}", szFunction, nLine, szTempStr, szSourcePath);
            break;
        case LogLevel::kTrace:
            g_logger->trace("{}({}): {}", szFunction, nLine, szTempStr);
            break;
        case LogLevel::kFixme:
            g_logger->warn("<FIXME>{}({}): {}", szFunction, nLine, szTempStr);
            break;
        case LogLevel::kWarning:
            g_logger->warn("{}({}): {}", szFunction, nLine, szTempStr);
            break;
        case LogLevel::kError:
            g_logger->error("{}({}): {}", szFunction, nLine, szTempStr);
            break;
        case LogLevel::kSceTrace:
            g_logger->trace("<SCE>{}({}): {}", szFunction, nLine, szTempStr);
            break;
        case LogLevel::kSceGraphic:
            g_logger->trace("<GRAPH>{}({}): {}", szFunction, nLine, szTempStr);
            break;
    }
}

}

