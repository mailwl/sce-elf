cmake_minimum_required(VERSION 3.10)
project(sce_elf)

set(CMAKE_CXX_STANDARD 17)

#option(ZYDIS_BUILD_TOOLS "" OFF)
#option(ZYDIS_BUILD_EXAMPLES "" OFF)
#add_subdirectory("external/zydis")


find_package(SDL2 REQUIRED)
include_directories(${SDL2_INCLUDE_DIRS})

find_package(Vulkan REQUIRED)

include_directories(external external/zydis/src)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
include_directories(hle)

add_executable(sce_elf
        main.cpp common.h
        process/process.cpp process/process.h
        loader/sce_elf.cpp loader/sce_elf.h

        external/zydis/src/Decoder.c external/zydis/src/DecoderData.c
        external/zydis/src/Register.c external/zydis/src/SharedData.c

        hle/hle.cpp hle/hle.h
        hle/libkernel/libkernel_pthreads.cpp hle/libkernel/libkernel_pthreads.h
        hle/libkernel/libkernel.cpp hle/libkernel/libkernel.h
        hle/libSceGnmDriver/libSceGnmDriver.cpp hle/libSceGnmDriver/libSceGnmDriver.h
        hle/libSceFios2/libSceFios2.cpp hle/libSceFios2/libSceFios2.h
        hle/libSceFiber/libSceFiber.cpp hle/libSceFiber/libSceFiber.h
        hle/libSceNet/libSceNet.cpp hle/libSceNet/libSceNet.h
        hle/libSceNetCtl/libSceNetCtl.cpp hle/libSceNetCtl/libSceNetCtl.h
        hle/libSceSysmodule/libSceSysmodule.cpp hle/libSceSysmodule/libSceSysmodule.h
        hle/libSceVideoOut/libSceVideoOut.cpp hle/libSceVideoOut/libSceVideoOut.h
        hle/libSceMouse/libSceMouse.cpp hle/libSceMouse/libSceMouse.h
        hle/libScePad/libScePad.cpp hle/libScePad/libScePad.h
        hle/libSceUserService/libSceUserService.cpp hle/libSceUserService/libSceUserService.h
        hle/libSceIme/libSceIme.cpp hle/libSceIme/libSceIme.h
        hle/libSceAudioIn/libSceAudioIn.cpp hle/libSceAudioIn/libSceAudioIn.h
        hle/libSceAudioOut/libSceAudioOut.cpp hle/libSceAudioOut/libSceAudioOut.h
        hle/libSceAjm/libSceAjm.cpp hle/libSceAjm/libSceAjm.h
        hle/libSceNpCommon/libSceNpCommon.cpp hle/libSceNpCommon/libSceNpCommon.h
        hle/libSceNpTrophy/libSceNpTrophy.cpp hle/libSceNpTrophy/libSceNpTrophy.h
        hle/libSceNpTus/libSceNpTus.cpp hle/libSceNpTus/libSceNpTus.h
        hle/libSceNpUtility/libSceNpUtility.cpp hle/libSceNpUtility/libSceNpUtility.h
        hle/libSceNpAuth/libSceNpAuth.cpp hle/libSceNpAuth/libSceNpAuth.h
        hle/libSceNpCommerce/libSceNpCommerce.cpp hle/libSceNpCommerce/libSceNpCommerce.h
        hle/libSceNpManager/libSceNpManager.cpp hle/libSceNpManager/libSceNpManager.h
        hle/libSceNpMatching2/libSceNpMatching2.cpp hle/libSceNpMatching2/libSceNpMatching2.h
        hle/libSceNpWebApi/libSceNpWebApi.cpp hle/libSceNpWebApi/libSceNpWebApi.h
        hle/libSceNpSignaling/libSceNpSignaling.cpp hle/libSceNpSignaling/libSceNpSignaling.h
        hle/libSceNpParty/libSceNpParty.cpp hle/libSceNpParty/libSceNpParty.h
        hle/libSceNpScore/libSceNpScore.cpp hle/libSceNpScore/libSceNpScore.h
        hle/libSceNpSnsFacebookDialog/libSceNpSnsFacebookDialog.cpp hle/libSceNpSnsFacebookDialog/libSceNpSnsFacebookDialog.h
        hle/libSceHttp/libSceHttp.cpp hle/libSceHttp/libSceHttp.h
        hle/libSceSsl/libSceSsl.cpp hle/libSceSsl/libSceSsl.h
        hle/libSceInvitationDialog/libSceInvitationDialog.cpp hle/libSceInvitationDialog/libSceInvitationDialog.h
        hle/libGameCustomDataDialog/libGameCustomDataDialog.cpp hle/libGameCustomDataDialog/libGameCustomDataDialog.h
        hle/libSceRtc/libSceRtc.cpp hle/libSceRtc/libSceRtc.h
        hle/libSceAppContentUtil/libSceAppContentUtil.cpp hle/libSceAppContentUtil/libSceAppContentUtil.h
        hle/libSceVideoRecording/libSceVideoRecording.cpp hle/libSceVideoRecording/libSceVideoRecording.h
        hle/libSceJpegEnc/libSceJpegEnc.cpp hle/libSceJpegEnc/libSceJpegEnc.h
        hle/libScePngEnc/libScePngEnc.cpp hle/libScePngEnc/libScePngEnc.h
        hle/libSceSystemService/libSceSystemService.cpp hle/libSceSystemService/libSceSystemService.h
        hle/libSceVoice/libSceVoice.cpp hle/libSceVoice/libSceVoice.h
        hle/libSceCommonDialog/libSceCommonDialog.cpp hle/libSceCommonDialog/libSceCommonDialog.h
        hle/libScePngDec/libScePngDec.cpp hle/libScePngDec/libScePngDec.h hle/libScePngDec/lodepng.cpp hle/libScePngDec/lodepng.h
        hle/libSceJpegDec/libSceJpegDec.cpp hle/libSceJpegDec/libSceJpegDec.h
        hle/libSceVideodec/libSceVideodec.cpp hle/libSceVideodec/libSceVideodec.h
        hle/libSceSaveData/libSceSaveData.cpp hle/libSceSaveData/libSceSaveData.h
        hle/libSceNpProfileDialog/libSceNpProfileDialog.cpp hle/libSceNpProfileDialog/libSceNpProfileDialog.h
        hle/libSceSaveDataDialog/libSceSaveDataDialog.cpp hle/libSceSaveDataDialog/libSceSaveDataDialog.h
        hle/libSceMsgDialog/libSceMsgDialog.cpp hle/libSceMsgDialog/libSceMsgDialog.h
        hle/libSceRemoteplay/libSceRemoteplay.cpp hle/libSceRemoteplay/libSceRemoteplay.h
        hle/libScePlayGo/libScePlayGo.cpp hle/libScePlayGo/libScePlayGo.h
        hle/libSceDiscMap/libSceDiscMap.cpp hle/libSceDiscMap/libSceDiscMap.h
        hle/libc/libc.cpp hle/libc/libc.h
        hle/libSceLibcInternal/libSceLibcInternal.cpp hle/libSceLibcInternal/libSceLibcInternal.h
        hle/libSceJson/libSceJson.cpp hle/libSceJson/libSceJson.h
        hle/libSceNpFriendListDialog/libSceNpFriendListDialog.cpp hle/libSceNpFriendListDialog/libSceNpFriendListDialog.h
        hle/libSceImeDialog/libSceImeDialog.cpp hle/libSceImeDialog/libSceImeDialog.h
        hle/libSceErrorDialog/libSceErrorDialog.cpp hle/libSceErrorDialog/libSceErrorDialog.h
        hle/libSceAudiodec/libSceAudiodec.cpp hle/libSceAudiodec/libSceAudiodec.h
        hle/libSceNgs2/libSceNgs2.cpp hle/libSceNgs2/libSceNgs2.h
        hle/libSceVoiceQoS/libSceVoiceQoS.cpp hle/libSceVoiceQoS/libSceVoiceQoS.h
        hle/libSceGameLiveStreaming/libSceGameLiveStreaming.cpp hle/libSceGameLiveStreaming/libSceGameLiveStreaming.h
        hle/libSceZlib/libSceZlib.cpp hle/libSceZlib/libSceZlib.h
        hle/libSceFontFt/libSceFontFt.cpp hle/libSceFontFt/libSceFontFt.h
        hle/libSceFont/libSceFont.cpp hle/libSceFont/libSceFont.h
        hle/libSceAvPlayer/libSceAvPlayer.cpp hle/libSceAvPlayer/libSceAvPlayer.h
        hle/libSceMove/libSceMove.cpp hle/libSceMove/libSceMove.h
        hle/libSceCompanionHttpd/libSceCompanionHttpd.cpp hle/libSceCompanionHttpd/libSceCompanionHttpd.h
        hle/libSceWebBrowserDialog/libSceWebBrowserDialog.cpp hle/libSceWebBrowserDialog/libSceWebBrowserDialog.h
        hle/libSceScreenShot/libSceScreenShot.cpp hle/libSceScreenShot/libSceScreenShot.h
        hle/libSceUlt/libSceUlt.cpp hle/libSceUlt/libSceUlt.h
        hle/libSceXml/libSceXml.cpp hle/libSceXml/libSceXml.h
        hle/libSceRudp/libSceRudp.cpp hle/libSceRudp/libSceRudp.h
        hle/libSceCompanionUtil/libSceCompanionUtil.cpp hle/libSceCompanionUtil/libSceCompanionUtil.h
        hle/libSceUsbd/libSceUsbd.cpp hle/libSceUsbd/libSceUsbd.h
        hle/libScePigletv2VSH/libScePigletv2VSH.cpp hle/libScePigletv2VSH/libScePigletv2VSH.h
        hle/libSceMbus/libSceMbus.cpp hle/libSceMbus/libSceMbus.h
        hle/libSceIpmi/libSceIpmi.cpp hle/libSceIpmi/libSceIpmi.h
        hle/libSceAvSetting/libSceAvSetting.cpp hle/libSceAvSetting/libSceAvSetting.h
        hle/libSceDolbyVision/libSceDolbyVision.cpp hle/libSceDolbyVision/libSceDolbyVision.h
        hle/libSceDipsw/libSceDipsw.cpp hle/libSceDipsw/libSceDipsw.h
        hle/libSceRegMgr/libSceRegMgr.cpp hle/libSceRegMgr/libSceRegMgr.h
        hle/libSceSysUtil/libSceSysUtil.cpp hle/libSceSysUtil/libSceSysUtil.h
        hle/libSceSigninDialog/libSceSigninDialog.cpp hle/libSceSigninDialog/libSceSigninDialog.h
        hle/libSceSysCore/libSceSysCore.cpp hle/libSceSysCore/libSceSysCore.h
        hle/libSceHmd/libSceHmd.cpp hle/libSceHmd/libSceHmd.h
        hle/libSceFreeType/libSceFreeType.cpp hle/libSceFreeType/libSceFreeType.h
        hle/libSceRazorCpu/libSceRazorCpu.cpp hle/libSceRazorCpu/libSceRazorCpu.h
        hle/libSceNpToolkit/libSceNpToolkit.cpp hle/libSceNpToolkit/libSceNpToolkit.h
        hle/libSceAvcap/libSceAvcap.cpp hle/libSceAvcap/libSceAvcap.h
        hle/libSceVideoOutSecondary/libSceVideoOutSecondary.cpp hle/libSceVideoOutSecondary/libSceVideoOutSecondary.h
        hle/libSceVdecCore/libSceVdecCore.cpp hle/libSceVdecCore/libSceVdecCore.h
        hle/libSceVdecSavc/libSceVdecSavc.cpp hle/libSceVdecSavc/libSceVdecSavc.h
        hle/libSceVdecSavc2/libSceVdecSavc2.cpp hle/libSceVdecSavc2/libSceVdecSavc2.h
        hle/libSceVdecShevc/libSceVdecShevc.cpp hle/libSceVdecShevc/libSceVdecShevc.h
        hle/libSceImeBackend/libSceImeBackend.cpp hle/libSceImeBackend/libSceImeBackend.h
        hle/libSceSharePlay/libSceSharePlay.cpp hle/libSceSharePlay/libSceSharePlay.h
        hle/libScePlayGoDialog/libScePlayGoDialog.cpp hle/libScePlayGoDialog/libScePlayGoDialog.h
        hle/libSceAudio3d/libSceAudio3d.cpp hle/libSceAudio3d/libSceAudio3d.h
        hle/libSceAvPlayerStreaming/libSceAvPlayerStreaming.cpp hle/libSceAvPlayerStreaming/libSceAvPlayerStreaming.h
        hle/libSceVideodec2/libSceVideodec2.cpp hle/libSceVideodec2/libSceVideodec2.h
        hle/libSceVideoDecoderArbitration/libSceVideoDecoderArbitration.cpp hle/libSceVideoDecoderArbitration/libSceVideoDecoderArbitration.h
        hle/libSceVrTracker/libSceVrTracker.cpp hle/libSceVrTracker/libSceVrTracker.h
        hle/libSceCamera/libSceCamera.cpp hle/libSceCamera/libSceCamera.h
        hle/libSceRazorGpuThreadTrace/libSceRazorGpuThreadTrace.cpp hle/libSceRazorGpuThreadTrace/libSceRazorGpuThreadTrace.h

        hle/ulobjmgr/ulobjmgr.cpp hle/ulobjmgr/ulobjmgr.h

        logger/logger.cpp logger/logger.h
        kernel/memory.cpp kernel/memory.h
        kernel/ipmi.cpp kernel/ipmi.h
        )

set_target_properties(sce_elf PROPERTIES COMPILE_FLAGS "-g  -Wextra -Wundef -march=native -mfsgsbase -funsafe-math-optimizations -fno-math-errno -Wunused-parameter")

target_compile_definitions(sce_elf PRIVATE VK_USE_PLATFORM_XLIB_KHR)

IF(WIN32)
target_link_libraries(sce_elf PRIVATE ws2_32 )
endif()
target_link_libraries(sce_elf PRIVATE ${SDL2_LIBRARIES} pthread m)
