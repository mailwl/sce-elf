#pragma once

#include <cstdint>
#include <vector>
#include "common.h"

static constexpr uint32_t ELF_MAGIC = 0x464C457F;
static constexpr uint16_t ELF_MACHINE_X86_64 = 0x3E;

enum class sce_elf_type : uint16_t {
    ET_SCE_EXEC = 0xfe00,    // PS4 Executable
    ET_SCE_DYNEXEC = 0xfe10, // PS4 Main module
    ET_SCE_RELEXEC = 0xfe04, // PS4 Relocatable PRX
    ET_SCE_STUBLIB = 0xfe0c, // PS4 Stub library
    ET_SCE_DYNAMIC = 0xfe18, // PS4 Dynamic PRX
};

#define SHN_UNDEF 0

enum phdr_flags : uint32_t {
    PF_X = 0x1,               // Execute
    PF_W = 0x2,               // Write
    PF_W_X = 0x3,             // Execute+Write
    PF_R = 0x4,               // Read
    PF_R_X = 0x5,             // Read, execute
    PF_R_W = 0x6,             // Read, write
    PF_R_W_X = 0x7,           // Read, write, execute
    PF_MASKOS = 0x0ff00000,   // Unspecified
    PF_MASKPROC = 0xf0000000, // Unspecified
};

enum elf_phdr_type : uint32_t {
    PT_NULL,
    PT_LOAD,
    PT_DYNAMIC,
    PT_INTERP,
    PT_NOTE,
    PT_SHLIB,
    PT_PHDR,
    PT_TLS,
    PT_LOPROC = 0x70000000,
    PT_HIPROC = 0x7FFFFFFF,
    PT_SCE_DYNLIBDATA = 0x61000000,
    PT_SCE_PROCPARAM = 0x61000001,
    PT_SCE_MODULEPARAM = 0x61000002,
    PT_SCE_RELRO = 0x61000010,
    PT_SCE_COMMENT = 0x6FFFFF00,
    PT_SCE_LIBVERSION = 0x6FFFFF01,
    PT_GNU_EH_FRAME = 0x6474E550
};

enum elf_dyn_tag : uint64_t {
    DT_NULL = 0,
    DT_NEEDED = 1,
    DT_PLTRELSZ = 2,
    DT_PLTGOT = 3,
    DT_HASH = 4,
    DT_STRTAB = 5,
    DT_SYMTAB = 6,
    DT_RELA,
    DT_RELASZ,
    DT_RELAENT,
    DT_STRSZ,
    DT_SYMENT,
    DT_INIT,
    DT_FINI,
    DT_SONAME,
    DT_RPATH,
    DT_SYMBOLIC,
    DT_REL,
    DT_RELSZ,
    DT_RELENT,
    DT_PLTREL,
    DT_DEBUG,
    DT_TEXTREL,
    DT_JMPREL,
    DT_BIND_NOW,
    DT_INIT_ARRAY,
    DT_FINI_ARRAY,
    DT_INIT_ARRAYSZ,
    DT_FINI_ARRAYSZ,
    DT_RUNPATH,
    DT_FLAGS,
    DT_ENCODING,
    DT_PREINIT_ARRAY,
    DT_PREINIT_ARRAYSZ,
    DT_SCE_FINGERPRINT = 0x61000007,
    DT_SCE_ORIGFILENAME = 0x61000009,
    DT_SCE_MODULEINFO = 0x6100000d,
    DT_SCE_NEEDED_MODULE = 0x6100000f,
    DT_SCE_MODULEATTR = 0x61000011,
    DT_SCE_EXPLIB = 0x61000013,
    DT_SCE_IMPLIB = 0x61000015,
    DT_SCE_EXPORT_LIB_ATTR = 0x61000017,
    DT_SCE_IMPORT_LIB_ATTR = 0x61000019,
    DT_SCE_STUB_MODULE_NAME = 0x6100001d,
    DT_SCE_STUB_MODULE_VERSION = 0x6100001f,
    DT_SCE_STUB_LIBRARY_NAME = 0x61000021,
    DT_SCE_STUB_LIBRARY_VERSION = 0x61000023,
    DT_SCE_HASH = 0x61000025,
    DT_SCE_PLTGOT = 0x61000027,
    DT_SCE_JMPREL = 0x61000029,
    DT_SCE_PLTREL = 0x6100002b,
    DT_SCE_PLTRELSZ = 0x6100002d,
    DT_SCE_RELA = 0x6100002f,
    DT_SCE_RELASZ = 0x61000031,
    DT_SCE_RELAENT = 0x61000033,
    DT_SCE_STRTAB = 0x61000035,
    DT_SCE_STRSIZE = 0x61000037,
    DT_SCE_SYMTAB = 0x61000039,
    DT_SCE_SYMENT = 0x6100003b,
    DT_SCE_HASHSZ = 0x6100003d,
    DT_SCE_SYMTABSZ = 0x6100003f,

};

inline const char* tag_to_string(elf_dyn_tag type) {
#define AS_STR(idx)                                                                                                    \
    if (type == idx)                                                                                                   \
        return #idx;
    AS_STR(DT_NULL)
    AS_STR(DT_NEEDED)
    AS_STR(DT_PLTRELSZ)
    AS_STR(DT_PLTGOT)
    AS_STR(DT_HASH)
    AS_STR(DT_STRTAB)
    AS_STR(DT_SYMTAB)
    AS_STR(DT_RELA)
    AS_STR(DT_RELASZ)
    AS_STR(DT_RELAENT)
    AS_STR(DT_STRSZ)
    AS_STR(DT_SYMENT)
    AS_STR(DT_INIT)
    AS_STR(DT_FINI)
    AS_STR(DT_SONAME)
    AS_STR(DT_RPATH)
    AS_STR(DT_SYMBOLIC)
    AS_STR(DT_REL)
    AS_STR(DT_RELSZ)
    AS_STR(DT_RELENT)
    AS_STR(DT_PLTREL)
    AS_STR(DT_DEBUG)
    AS_STR(DT_TEXTREL)
    AS_STR(DT_JMPREL)
    AS_STR(DT_BIND_NOW)
    AS_STR(DT_INIT_ARRAY)
    AS_STR(DT_FINI_ARRAY)
    AS_STR(DT_INIT_ARRAYSZ)
    AS_STR(DT_FINI_ARRAYSZ)
    AS_STR(DT_RUNPATH)
    AS_STR(DT_FLAGS)
    AS_STR(DT_ENCODING)
    AS_STR(DT_PREINIT_ARRAY)
    AS_STR(DT_PREINIT_ARRAYSZ)
    AS_STR(DT_SCE_FINGERPRINT)
    AS_STR(DT_SCE_ORIGFILENAME)
    AS_STR(DT_SCE_MODULEINFO)
    AS_STR(DT_SCE_NEEDED_MODULE)
    AS_STR(DT_SCE_MODULEATTR)
    AS_STR(DT_SCE_EXPLIB)
    AS_STR(DT_SCE_IMPLIB)
    AS_STR(DT_SCE_EXPORT_LIB_ATTR)
    AS_STR(DT_SCE_IMPORT_LIB_ATTR)
    AS_STR(DT_SCE_STUB_MODULE_NAME)
    AS_STR(DT_SCE_STUB_MODULE_VERSION)
    AS_STR(DT_SCE_STUB_LIBRARY_NAME)
    AS_STR(DT_SCE_STUB_LIBRARY_VERSION)
    AS_STR(DT_SCE_HASH)
    AS_STR(DT_SCE_PLTGOT)
    AS_STR(DT_SCE_JMPREL)
    AS_STR(DT_SCE_PLTREL)
    AS_STR(DT_SCE_PLTRELSZ)
    AS_STR(DT_SCE_RELA)
    AS_STR(DT_SCE_RELASZ)
    AS_STR(DT_SCE_RELAENT)
    AS_STR(DT_SCE_STRTAB)
    AS_STR(DT_SCE_STRSIZE)
    AS_STR(DT_SCE_SYMTAB)
    AS_STR(DT_SCE_SYMENT)
    AS_STR(DT_SCE_HASHSZ)
    AS_STR(DT_SCE_SYMTABSZ)
#undef AS_STR
    return "Unknown";
}

struct elf_header_t {
    uint32_t magic;
    uint8_t ident[12]; // < actually 16
    sce_elf_type type; //< sony custom
    uint16_t machine;
    uint32_t version;
    uint64_t entry;
    uint64_t phoff; //< program header offset
    uint64_t shoff; //< does sometimes not exist
    uint32_t flags;
    uint16_t ehsize;
    uint16_t phentsize; //< size in bytes of entries in pg table
    uint16_t phnum;
    uint16_t shentsize;
    uint16_t shnum;
    uint16_t shstrndx;
};

struct elf_phdr_t {
    elf_phdr_type type; //< data info
    phdr_flags flags;   //< memory protection flags
    uint64_t offset;
    uint64_t vaddr;
    uint64_t paddr;
    uint64_t filesz;
    uint64_t memsz;
    uint64_t align;
};

inline const char* phdr_type_to_string(elf_phdr_type type) {
#define AS_STR(idx)                                                                                                    \
    if (type == idx)                                                                                                   \
        return #idx;
    AS_STR(PT_NULL)
    AS_STR(PT_LOAD)
    AS_STR(PT_DYNAMIC)
    AS_STR(PT_INTERP)
    AS_STR(PT_NOTE)
    AS_STR(PT_SHLIB)
    AS_STR(PT_PHDR)
    AS_STR(PT_TLS)
    AS_STR(PT_LOPROC)
    AS_STR(PT_HIPROC)
    AS_STR(PT_SCE_DYNLIBDATA)
    AS_STR(PT_SCE_PROCPARAM)
    AS_STR(PT_SCE_MODULEPARAM)
    AS_STR(PT_SCE_RELRO)
    AS_STR(PT_SCE_COMMENT)
    AS_STR(PT_SCE_LIBVERSION)
    AS_STR(PT_GNU_EH_FRAME)
#undef AS_STR
    return "Unknown";
}

struct elf_dyn_t {
    elf_dyn_tag tag;
    union {
        uint64_t value;
        uint64_t ptr;
    } un;
};

struct elf_rel_t {
    uint64_t offset;
    uint64_t info;
    int64_t addend;
};

struct elf_sym_t {
    uint32_t st_name;
    uint8_t st_info;
    uint8_t st_other;
    uint16_t st_shndx;
    uint64_t st_value;
    uint64_t st_size;
};

#define ELF64_R_SYM(i) ((i) >> 32u)
#define ELF64_R_TYPE(i) ((i)&0xffffffff)

/* Relocation types for AMD x86-64 architecture */
#define R_X86_64_NONE 0      /* No reloc */
#define R_X86_64_64 1        /* Direct 64 bit  */
#define R_X86_64_PC32 2      /* PC relative 32 bit signed */
#define R_X86_64_GOT32 3     /* 32 bit GOT entry */
#define R_X86_64_PLT32 4     /* 32 bit PLT address */
#define R_X86_64_COPY 5      /* Copy symbol at runtime */
#define R_X86_64_GLOB_DAT 6  /* Create GOT entry */
#define R_X86_64_JUMP_SLOT 7 /* Create PLT entry */
#define R_X86_64_RELATIVE 8  /* Adjust by program base */
#define R_X86_64_GOTPCREL 9  /* 32 bit signed pc relative offset to GOT */
#define R_X86_64_32 10       /* Direct 32 bit zero extended */
#define R_X86_64_32S 11      /* Direct 32 bit sign extended */
#define R_X86_64_16 12       /* Direct 16 bit zero extended */
#define R_X86_64_PC16 13     /* 16 bit sign extended pc relative */
#define R_X86_64_8 14        /* Direct 8 bit sign extended  */
#define R_X86_64_PC8 15      /* 8 bit sign extended pc relative */
#define R_X86_64_DTPMOD64 16 /* ID of module containing symbol */
#define R_X86_64_DTPOFF64 17 /* Offset in module's TLS block */
#define R_X86_64_TPOFF64 18  /* Offset in initial TLS block */
#define R_X86_64_TLSGD                                                                                                 \
    19 /* 32 bit signed PC relative offset                                                                             \
           to two GOT entries for GD symbol */
#define R_X86_64_TLSLD                                                                                                 \
    20                       /* 32 bit signed PC relative offset                                                       \
                                 to two GOT entries for LD symbol */
#define R_X86_64_DTPOFF32 21 /* Offset in TLS block */
#define R_X86_64_GOTTPOFF                                                                                              \
    22                              /* 32 bit signed PC relative offset                                                \
                                        to GOT entry for IE symbol */
#define R_X86_64_TPOFF32 23         /* Offset in initial TLS block */
#define R_X86_64_PC64 24            /* PC relative 64 bit */
#define R_X86_64_GOTOFF64 25        /* 64 bit offset to GOT */
#define R_X86_64_GOTPC32 26         /* 32 bit signed pc relative offset to GOT */
#define R_X86_64_GOT64 27           /* 64-bit GOT entry offset */
#define R_X86_64_GOTPCREL64 28      /* 64-bit PC relative offset to GOT entry */
#define R_X86_64_GOTPC64 29         /* 64-bit PC relative offset to GOT */
#define R_X86_64_GOTPLT64 30        /* like GOT64, says PLT entry needed */
#define R_X86_64_PLTOFF64 31        /* 64-bit GOT relative offset to PLT entry */
#define R_X86_64_SIZE32 32          /* Size of symbol plus 32-bit addend */
#define R_X86_64_SIZE64 33          /* Size of symbol plus 64-bit addend */
#define R_X86_64_GOTPC32_TLSDESC 34 /* GOT offset for TLS descriptor */
#define R_X86_64_TLSDESC_CALL 35    /* Marker for call through TLS descriptor */
#define R_X86_64_TLSDESC 36         /* TLS descriptor */
#define R_X86_64_IRELATIVE 37       /* Adjust indirectly by program base */
#define R_X86_64_RELATIVE64 38      /* 64bit adjust by program base */
#define R_X86_64_ORBIS_GOTPCREL_LOAD 40

#define ELF_ST_BIND(x) ((x) >> 4)
#define ELF_ST_TYPE(x) (((unsigned int)x) & 0xf)
#define ELF32_ST_BIND(x) ELF_ST_BIND(x)
#define ELF32_ST_TYPE(x) ELF_ST_TYPE(x)
#define ELF64_ST_BIND(x) ELF_ST_BIND(x)
#define ELF64_ST_TYPE(x) ELF_ST_TYPE(x)

#define STB_LOCAL 0
#define STB_GLOBAL 1
#define STB_WEAK 2

inline const char* binding_to_string(uint8_t type) {
#define AS_STR(idx)                                                                                                    \
    if (type == idx)                                                                                                   \
        return #idx;
    AS_STR(STB_LOCAL)
    AS_STR(STB_GLOBAL)
    AS_STR(STB_WEAK)
#undef AS_STR
    return "Unknown";
}

#define STT_NOTYPE 0
#define STT_OBJECT 1
#define STT_FUNC 2
#define STT_SECTION 3
#define STT_FILE 4
#define STT_COMMON 5
#define STT_TLS 6

inline const char* type_to_string(uint8_t type) {
#define AS_STR(idx)                                                                                                    \
    if (type == idx)                                                                                                   \
        return #idx;
    AS_STR(STT_NOTYPE)
    AS_STR(STT_OBJECT)
    AS_STR(STT_FUNC)
    AS_STR(STT_SECTION)
    AS_STR(STT_FILE)
    AS_STR(STT_COMMON)
    AS_STR(STT_TLS)
#undef AS_STR
    return "Unknown";
}
namespace loader {
class sce_elf {
public:
    struct ImportLib {
        const char* name{};
        const char* filename{};
        int32_t modid{};
    };

    explicit sce_elf(const char* elf_name);
    ~sce_elf() {
        //LOG_DEBUG("sce_elf::~sce_elf(), %s\n", elf_name);
    }
    bool load(const char* path);
    [[nodiscard]] const elf_phdr_t& get_phdr(elf_phdr_type type) const;
    void process_dynamic();
    void export_symbols() const;
    void map_memory();
    void resolve_imports();
    void apply_relocations();
    void apply_protection();
    void free_memory();
    [[nodiscard]] void* init_proc() const {
        return (image + _init_proc);
    }
    [[nodiscard]] void* term_proc() const {
        return (image + _term_proc);
    } // TODO call before  free
    [[nodiscard]] void* set_tls_slot(uint16_t slot);
    [[nodiscard]] std::vector<ImportLib> import_libraries() const {
        return implibs;
    }
    [[nodiscard]] void* entry_point() const {
        return image + elf_header.entry;
    }
    [[nodiscard]] bool has_phdr(elf_phdr_type type) const;
    [[nodiscard]] void* get_image() const {
        return image;
    };
    [[nodiscard]] size_t get_image_size();
    [[nodiscard]] const char* get_name() const { return elf_name; }
    [[nodiscard]] void* get_proc_param() const { return proc_param; }
    bool get_tls_info(void** tls, uint64_t* init_size, uint64_t* total_size, uint32_t* alignment) const;
private:
    bool decode_nid(const char* subset, size_t len, uint64_t& out) const;
    void alloc_memory();
    export_func find_export(const char* name) const;

    char elf_name[0x100]{};

    std::vector<char> elf_content{};
    elf_header_t elf_header{};
    std::vector<elf_phdr_t> program_headers{};
    elf_rel_t* jmpslots{};
    uint32_t num_jmp_slots{};

    elf_rel_t* rela{};
    uint32_t num_rela{};

    elf_sym_t* symbols{};
    uint32_t num_symbols{};

    uint64_t pltgot{};

    char* dynld_ptr{};

    char* strtab_ptr{};
    size_t strtab_size{};

    std::vector<ImportLib> implibs{};
    //std::vector<const char*> needed{};

    char* image{};
    ssize_t image_size{-1};

    uint16_t tls_slot{};

    uint64_t _init_proc{};
    uint64_t _term_proc{};

    void* proc_param;
};
} // namespace loader
