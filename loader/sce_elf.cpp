//
// Created by Vladimir Russkih on 16/09/2019.
//

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <map>
#include <string>
#include <memory.h>

#include "sce_elf.h"

#include "process/process.h"

#define ALIGN_MEMORY 0x10000000

#if defined(_WIN64) || defined(__CYGWIN__)
#include <windows.h>
DWORD convert_prot(uint32_t flags) {
    switch (flags & 0x7u) {
    case phdr_flags::PF_R:
        return PAGE_READONLY;
    case phdr_flags::PF_W:
    case phdr_flags::PF_R_W:
        return PAGE_READWRITE;
    case phdr_flags::PF_X:
        return PAGE_EXECUTE;
    case phdr_flags::PF_R_X:
        return PAGE_EXECUTE_READ;
    case phdr_flags::PF_R_W_X:
        return PAGE_EXECUTE_READWRITE;
    default:
        return PAGE_READWRITE;
    }
}
#else
#include <signal.h>
#include <sys/mman.h>
int convert_prot(uint32_t flags) {
    switch (flags & 0x7u) {
    case phdr_flags::PF_R:
        return PROT_READ;
    case phdr_flags::PF_W:
        return PROT_WRITE;
    case phdr_flags::PF_R_W:
        return (PROT_READ | PROT_WRITE);
    case phdr_flags::PF_X:
        return PROT_EXEC;
    case phdr_flags::PF_R_X:
        return (PROT_READ | PROT_EXEC);
    case phdr_flags::PF_R_W_X:
        return (PROT_READ | PROT_WRITE | PROT_EXEC);
    default:
        return (PROT_READ | PROT_WRITE);
    }
}
#endif

template <typename T, typename = std::enable_if_t<std::is_integral<T>::value>>
constexpr T align(const T& value, uint64_t align) {
    return static_cast<T>((value + (align - 1)) & ~(align - 1));
}

//#define STATIC_LOAD

#ifdef STATIC_LOAD
alignas(0x2000) static char dumb[0x36000];
alignas(0x2000) static char libc[0xf8000]{};
alignas(0x2000) static char libSceFios2[0x18c000]{};
alignas(0x2000) static char eboot_bin[0x33c000]{};
#endif

namespace loader {

sce_elf::sce_elf(const char* name) {
    std::strcpy(elf_name, name);
    //LOG_DEBUG("loading elf: %s\n", name);
}

bool sce_elf::load(const char* path) {
    // LOG_DEBUG("loading elf: %s\n", path);
    std::ifstream ifs;
    ifs.open(path, std::ios_base::in | std::ios_base::binary);
    if (!ifs.is_open()) {
        // LOG_DEBUG("unable open file %s\n", path);
        return false;
    }
    ifs.seekg(0, std::ios_base::end);
    auto size = (size_t)ifs.tellg();
    ifs.seekg(0, std::ios_base::beg);

    elf_content.resize(size);
    ifs.read(elf_content.data(), size);
    ifs.close();

    std::memcpy(&elf_header, elf_content.data(), sizeof(elf_header_t));

    if (elf_header.magic != ELF_MAGIC) {
        LOG_DEBUG("bad MAGIC\n");
        elf_content.resize(0);
        return false;
    }
    if (elf_header.machine != ELF_MACHINE_X86_64) {
        LOG_DEBUG("bad machine type\n");
        elf_content.resize(0);
        return false;
    }

    for (auto i = 0; i < elf_header.phnum; ++i) {
        elf_phdr_t& p = program_headers.emplace_back();
        std::memcpy(&p, elf_content.data() + elf_header.phoff + (i * elf_header.phentsize), elf_header.phentsize);
        //LOG_DEBUG("phdr %d, offset: %llx, vaddr: %llx -> type: %s, filesz: %llx, memsz: %llx\n", i, p.offset, p.vaddr,
        //            phdr_type_to_string(p.type), p.filesz, p.memsz);
    }
    return true;
}

const elf_phdr_t& sce_elf::get_phdr(elf_phdr_type type) const {
    auto p =
        std::find_if(program_headers.begin(), program_headers.end(), [&type](const auto& e) { return e.type == type; });
    if (p == program_headers.end()) {
        throw std::runtime_error("elf: no such program header");
    }
    return *p;
}

void sce_elf::process_dynamic() {
    auto dynamic = get_phdr(elf_phdr_type::PT_DYNAMIC);

    auto sce_dynlibdata = get_phdr(elf_phdr_type::PT_SCE_DYNLIBDATA);
    dynld_ptr = elf_content.data() + sce_dynlibdata.offset;

    auto d = reinterpret_cast<elf_dyn_t*>(elf_content.data() + dynamic.offset);
    if (d == nullptr) {
        throw std::runtime_error("elf error: no dynamics");
    }
    while (d->tag != DT_NULL) {
        //LOG_DEBUG("tag: 0x%08x (%s) = 0x%x\n", d->tag, tag_to_string(d->tag), d->un.value);
        switch (d->tag) {
        default:
            break;
        case DT_SCE_PLTGOT:
            pltgot = d->un.ptr;
            break;
        case DT_SCE_ORIGFILENAME:
            if (strtab_ptr && d->un.value < strtab_size) {
                //LOG_DEBUG("Filename: %s\n", (strtab_ptr + d->un.value));
            }
            break;
        case DT_INIT:
            _init_proc = d->un.ptr;
            break;
        case DT_INIT_ARRAY:
            break;
        case DT_FINI:
            _term_proc = d->un.ptr;
            break;
        case DT_SCE_JMPREL:
            jmpslots = (elf_rel_t*)(dynld_ptr + d->un.ptr);
            break;
        case DT_PLTRELSZ:
        case DT_SCE_PLTRELSZ:
            num_jmp_slots = static_cast<uint32_t>(d->un.value / sizeof(elf_rel_t));
            break;
        case DT_SCE_RELA:
            rela = (elf_rel_t*)(dynld_ptr + d->un.ptr);
            break;
        case DT_RELASZ:
            break;
        case DT_SCE_RELASZ:
            num_rela = static_cast<uint32_t>(d->un.value / sizeof(elf_rel_t));
            break;
        case DT_SCE_STRTAB:
            strtab_ptr = (char*)(dynld_ptr + d->un.ptr);
            break;
        case DT_STRSZ:
            break;
        case DT_SCE_STRSIZE:
            strtab_size = d->un.value;
            break;
        case DT_SCE_SYMTAB:
            symbols = (elf_sym_t*)(dynld_ptr + d->un.ptr);
            break;
        case DT_SCE_SYMTABSZ:
            num_symbols = static_cast<uint32_t>(d->un.value / sizeof(elf_sym_t));
            break;
            case DT_NEEDED:
                if ( strtab_ptr && d->un.value < strtab_size) {
                    // LOG_DEBUG("Needed: %s\n",  (strtab_ptr + d->un.value) );
                }
                break;
        case DT_SCE_NEEDED_MODULE: {
            auto& e = implibs.emplace_back();
            e.name = (const char*)(strtab_ptr + (d->un.value & 0xFFFFFFFF));
            e.modid = int32_t(d->un.value >> 48u);
            //LOG_DEBUG("DT_SCE_NEEDED_MODULE: %s, %d\n", e.name, e.modid);
            break;
        }
        }
        ++d;
    }
}

size_t sce_elf::get_image_size() {
    if (image_size > 0) {
        return image_size;
    }
    size_t size = 0;
    for (auto i = 0; i < elf_header.phnum; ++i) {
        auto phdr = program_headers[i];
        if (phdr.memsz) {
            size_t seg_size_aligned = align(phdr.memsz, phdr.align);
            size_t curr_max_size = phdr.vaddr + seg_size_aligned;
            if (curr_max_size > size) {
                size = curr_max_size;
            }
        }
    }
    image_size = size;
    return image_size;
}

#ifdef STATIC_LOAD
void sce_elf::alloc_memory() {
    if (!strcmp(elf_name, "libc")) {
        image = libc;
    } else if (!strcmp(elf_name, "libSceFios2")) {
        image = libSceFios2;
    } else if (!strcmp(elf_name, "eboot.bin")) {
        image = eboot_bin;
    } else {
        throw std::bad_alloc();
    }
    std::memset(image, 0x00, get_image_size());
}
#else
void sce_elf::alloc_memory() {
    auto size = get_image_size();
#ifdef __CYGWIN__
    int rc = ::posix_memalign((void**)&image, ALIGN_MEMORY, size);
    if (rc) {
        LOG_DEBUG("cannot alloc memory!\n");
    }
#elif defined(_WIN64)
    image = (char*)_aligned_malloc(size, ALIGN_MEMORY);
#else
    int rc = ::posix_memalign((void**)&image, ALIGN_MEMORY, size);
    if (rc) {
        LOG_DEBUG("cannot alloc memory!\n");
    }
#endif
    if (image == nullptr) {
        throw std::bad_alloc();
    }
    std::memset(image, 0x00, size);
}
#endif

void sce_elf::map_memory() {
    alloc_memory();
    for (auto i = 0; i < elf_header.phnum; ++i) {
        auto phdr = program_headers[i];
        if (phdr.type == PT_SCE_PROCPARAM) {
            proc_param = image + phdr.vaddr;
            continue;
        }
        if (phdr.memsz && phdr.filesz && phdr.type != PT_DYNAMIC) {
            if ((phdr.vaddr + phdr.filesz) > (size_t)image_size) {
                LOG_DEBUG("overload!");
            }
            //LOG_DEBUG("copy for lib: %s, vaddr: %x, offset: %x, file_size: %x, mem_size: %x, %s\n", elf_name,
            //            image + phdr.vaddr, phdr.offset, phdr.filesz, phdr.memsz, phdr_type_to_string(phdr.type));
            std::memcpy(image + phdr.vaddr, elf_content.data() + phdr.offset, phdr.filesz);
        }
    }
}

void sce_elf::export_symbols() const {
    for (uint32_t i = 0; i < num_symbols; ++i) {
        auto sym = symbols[i];

        auto st_binding = ELF64_ST_BIND(sym.st_info);
        auto st_type = ELF64_ST_TYPE(sym.st_info);
        const char* name = &strtab_ptr[sym.st_name];
        if (name[std::strlen(name)-2] == '#') {
            auto* ptr = &name[std::strlen(name)-1];
            uint64_t mod_id = 0;
            decode_nid(ptr, 1, mod_id);
            if (/*mod_id == 0*/ sym.st_shndx != SHN_UNDEF) {
//                if ( 0 == strcmp(this->elf_name, "libSceFreeType") ) {
//                    LOG_DEBUG("Export symbol: '%s', BINDING: %s, TYPE: %s, VALUE: %x", name,
//                              binding_to_string(st_binding), type_to_string(st_type), sym.st_value);
//                }
                // exported function. TODO: weak, tls???
                char encoded[12]{};
                strncpy(encoded, name, 11);
                process::add_export_func(elf_name, encoded, (void (*)())(image + sym.st_value));
            }
        }
    }
}

void unimplemented() {
    LOG_DEBUG("unimplemented func\n");
#if defined (__CYGWIN__)
    raise(SIGTRAP);
#elif defined(_WIN64)
    __debugbreak();
#else
    raise(SIGTRAP);
#endif
}

void sce_elf::resolve_imports() {
    for (uint32_t i = 0; i < num_jmp_slots; i++) {
        auto r = jmpslots[i];

        auto type = uint32_t(ELF64_R_TYPE(r.info));
        auto isym = uint32_t(ELF64_R_SYM(r.info));

        elf_sym_t sym = symbols[isym];

        if (type != R_X86_64_JUMP_SLOT) {
            LOG_DEBUG("Bad jmpslot %d\n", i);
            continue;
        }

        if (isym >= num_symbols || sym.st_name >= strtab_size) {
            LOG_DEBUG("Bad symbol idx %d for relocation %d\n", isym, i);
            continue;
        }
        auto st_binding = ELF64_ST_BIND(sym.st_info);
        auto st_type = ELF64_ST_TYPE(sym.st_info);

        const char* name = &strtab_ptr[sym.st_name];
        // LOG_DEBUG("SYMBOL binding '%s', BINDING: %s, TYPE: %s, offset: %x\n", name,
        //        binding_to_string(st_binding), type_to_string(st_type), r.offset);

        if (st_binding == STB_LOCAL) {
            *((uintptr_t*)(image + r.offset)) = (uintptr_t)(image + sym.st_value);
        } else if (st_binding == STB_WEAK) {
            //LOG_DEBUG("WEAK symbol: %s, offset: 0x%x\n", name, r.offset);
        } else {
            export_func addr = find_export(name);
            *((uintptr_t*)(image + r.offset)) = (addr == nullptr) ? (uintptr_t)unimplemented : (uintptr_t)addr;
        }
    }
}

void sce_elf::apply_relocations() {
    for (size_t i = 0; i < num_rela; i++) {
        auto& r = rela[i];

        auto isym = (uint32_t)ELF64_R_SYM(r.info);
        int32_t type = ELF64_R_TYPE(r.info);

        elf_sym_t& sym = symbols[isym];

        if (isym >= num_symbols) {
            LOG_DEBUG("[!] Invalid symbol index %d\n", isym);
            continue;
        }
        const char* name = &strtab_ptr[sym.st_name];

        switch (type) {
        case R_X86_64_NONE:
            break;
        case R_X86_64_64: {
            export_func addr = find_export(name);
            *((uintptr_t*)(image + r.offset)) =
                (addr == nullptr) ? (uintptr_t)unimplemented : ((uintptr_t)addr + r.addend);
            break;
        }
        case R_X86_64_RELATIVE: /* base + ofs*/
            *((uint64_t*)(image + r.offset)) = (uint64_t)(image + r.addend);
            break;
        case R_X86_64_GLOB_DAT: {
            export_func addr = find_export(name);
            *((uintptr_t*)(image + r.offset)) = (addr == nullptr) ? (uintptr_t)0 : ((uintptr_t)addr + r.addend);
            break;
        }
        case R_X86_64_PC32:
            *((uint32_t*)(image + r.offset)) =
                (uint32_t)(sym.st_value + r.addend - (uint64_t)((uint64_t*)(image + r.offset)));
            break;
        case R_X86_64_DTPMOD64:
            *((uint64_t*)(image + r.offset)) = tls_slot;
            break;
        case R_X86_64_TPOFF64:
        case R_X86_64_TPOFF32:
        default:
            LOG_DEBUG("unknown relocation type %d\n", type);
            break;
        }
    }
}

void sce_elf::apply_protection() {
    for (auto i = 0; i < elf_header.phnum; ++i) {
        auto phdr = program_headers[i];
        if (phdr.type == PT_LOAD) {
#if defined(_WIN64)
            DWORD dummy;
            int flag = convert_prot(phdr.flags);
            bool rc = VirtualProtect(image + phdr.vaddr, phdr.memsz, flag, &dummy);
            if (!rc) {
                LOG_DEBUG("Can't change memory protection!\n");
                return;
            }
#else
            int flag = convert_prot(phdr.flags);
            int rc = ::mprotect(image + phdr.vaddr, phdr.memsz, flag);
            if (rc) {
                auto t = strerror(rc);
                LOG_DEBUG("Can't change memory protection: %s\n", t);
                return;
            }
#endif
        }
    }
    elf_content.resize(0); // Free elf
}

void sce_elf::free_memory() {
#ifndef STATIC_LOAD

//#if defined(_WIN64)
    //::VirtualFree(image, image_size, MEM_RELEASE);
//#else
    //::free(image);
//#endif

#endif
}

bool sce_elf::decode_nid(const char* subset, size_t len, uint64_t& out) const {
    const char lookup[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-";
    for (size_t i = 0; i < len; i++) {
        auto pos = std::strchr(lookup, subset[i]);
        // invalid NID?
        if (!pos) {
            return false;
        }
        auto offset = static_cast<uint32_t>(pos - lookup);
        // max NID is 11
        if (i < 10) {
            out <<= 6;
            out |= offset;
        } else {
            out <<= 4;
            out |= (offset >> 2);
        }
    }
    return true;
}

bool sce_elf::has_phdr(elf_phdr_type type) const {
    for (auto phdr : program_headers) {
        if (phdr.type == type)
            return true;
    }
    return false;
}

void* sce_elf::set_tls_slot(uint16_t slot) {
    tls_slot = slot;
    auto tls_phdr = get_phdr(PT_TLS);
    void* tls_ptr = image + tls_phdr.vaddr;
    //LOG_DEBUG("TLS addr: %p\n", tls_ptr);
    return tls_ptr;
}

bool sce_elf::get_tls_info(void** tls, uint64_t* init_size, uint64_t* total_size, uint32_t* alignment) const {
    if (!has_phdr(PT_TLS)) {
        return false;
    }
    auto tls_phdr = get_phdr(PT_TLS);
    *tls = image + tls_phdr.vaddr;
    *init_size = tls_phdr.filesz;
    *total_size = align(tls_phdr.memsz, tls_phdr.align);
    *alignment = tls_phdr.align;
    return true;
}

export_func sce_elf::find_export(const char* name) const {
    if (name[std::strlen(name)-2] == '#') {
        auto* ptr = &name[std::strlen(name)-1];
        uint64_t mod_id = 0;
        decode_nid(ptr, 1, mod_id);
        char encoded[12]{};
        strncpy(encoded, name, 11);
        if (mod_id == 0) {
            // self module
            const auto addr = process::get_export(elf_name, encoded);
            if (addr == nullptr) {
                ::printf("fun(\"%s\", ); // %s\n", encoded, elf_name);
                return nullptr;
            }
            return addr;
        }
        for (auto& imp : implibs) {
            if (imp.modid == static_cast<int32_t>(mod_id)) {
                const auto addr = process::get_export(imp.name, encoded);
                if (addr == nullptr) {
                    ::printf("fun(\"%s\", ); // %s\n", encoded, imp.name);
                    return nullptr;
                }
                return addr;
            }
        }
    }
    ::printf("fun(\"%s\", ); // ??\n", name);
    return nullptr;
}
} // namespace loader
