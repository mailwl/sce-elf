#include "libSceNpUtility.h"

namespace libSceNpUtility {

PS4API int sceNpAppInfoIntAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAppInfoIntCheckAvailability() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAppInfoIntCheckAvailabilityAll() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAppInfoIntCheckServiceAvailability() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAppInfoIntCheckServiceAvailabilityAll() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAppInfoIntCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAppInfoIntDestroyRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAppLaunchLinkIntAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAppLaunchLinkIntCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAppLaunchLinkIntDestroyRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAppLaunchLinkIntGetCompatibleTitleIdList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAppLaunchLinkIntGetCompatibleTitleIdNum() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpBandwidthTestAbort() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpBandwidthTestGetStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpBandwidthTestInitStart() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpBandwidthTestShutdown() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpBandwidthTestShutdownInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLookupAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLookupCreateAsyncRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLookupCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLookupCreateTitleCtx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLookupCreateTitleCtxA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLookupDeleteRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLookupDeleteTitleCtx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLookupNpId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLookupPollAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLookupSetTimeout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLookupWaitAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpServiceChecker2IntAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpServiceChecker2IntCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpServiceChecker2IntDestroyRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpServiceChecker2IntGetServiceAvailability() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpServiceChecker2IntIsSetServiceType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpServiceCheckerIntAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpServiceCheckerIntCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpServiceCheckerIntDestroyRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpServiceCheckerIntGetAvailability() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpServiceCheckerIntGetAvailabilityList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpServiceCheckerIntIsCached() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTitleMetadataIntAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTitleMetadataIntCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTitleMetadataIntDeleteRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTitleMetadataIntGetInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilityInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilityTerm() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWordFilterAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWordFilterCensorComment() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWordFilterCreateAsyncRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWordFilterCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWordFilterCreateTitleCtx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWordFilterCreateTitleCtxA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWordFilterDeleteRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWordFilterDeleteTitleCtx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWordFilterPollAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWordFilterSanitizeComment() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWordFilterSetTimeout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWordFilterWaitAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("IEB+vgVoQbw", sceNpWordFilterCreateAsyncRequest);
    fun("-McDhX8tnWE", sceNpTitleMetadataIntGetInfo);
    fun("t0P5z5yuFPA", sceNpWordFilterDeleteTitleCtx);
    fun("hBsBswrAiGM", sceNpServiceChecker2IntDestroyRequest);
    fun("87ivWj5yKzg", sceNpWordFilterWaitAsync);
    fun("waeEzwwYfZY", sceNpServiceCheckerIntAbortRequest);
    fun("0MV72WO7V34", sceNpLookupSetTimeout);
    fun("BaihFa8LBw0", sceNpAppInfoIntCheckServiceAvailabilityAll);
    fun("9YhwG4DhwtU", sceNpAppLaunchLinkIntCreateRequest);
    fun("BYIZGKm6bO4", sceNpBandwidthTestGetStatus);
    fun("wLaxchvEEnk", sceNpLookupDeleteRequest);
    fun("jktww3yJXnc", sceNpBandwidthTestInitStart);
    fun("6p9jvljuvsw", sceNpWordFilterCreateTitleCtxA);
    fun("mtqDK9zkoIE", sceNpLookupDeleteTitleCtx);
    fun("eYz4v5Uek9U", sceNpLookupAbortRequest);
    fun("M5Jyo9TKYPI", sceNpUtilityTerm);
    fun("YLXt-vGw4Kg", sceNpServiceCheckerIntCreateRequest);
    fun("Y797Sw9-jqY", sceNpAppInfoIntAbortRequest);
    fun("wIX00Brskoc", sceNpServiceCheckerIntGetAvailability);
    fun("PSptxtJVbv4", sceNpBandwidthTestShutdownInt);
    fun("85ZWdzWYgas", sceNpServiceCheckerIntDestroyRequest);
    fun("AQV4A8YFx44", sceNpAppLaunchLinkIntAbortRequest);
    fun("pRgpBtHx8P4", sceNpAppInfoIntDestroyRequest);
    fun("vT9xhqPO6+0", sceNpLookupCreateTitleCtxA);
    fun("rAOOqDAxBIk", sceNpWordFilterAbortRequest);
    fun("1mfDBl40Dms", sceNpAppInfoIntCheckServiceAvailability);
    fun("PYFS1H70bDs", sceNpWordFilterDeleteRequest);
    fun("iQr9UxPHUFs", sceNpLookupCreateRequest);
    fun("8533Q+LU7EQ", sceNpLookupCreateTitleCtx);
    fun("iCq5xW5KQW4", sceNpWordFilterCreateRequest);
    fun("GB7Fhk5SUaA", sceNpAppLaunchLinkIntGetCompatibleTitleIdList);
    fun("rei4kjOSiyc", sceNpTitleMetadataIntAbortRequest);
    fun("tynva-9jrtI", sceNpTitleMetadataIntDeleteRequest);
    fun("ukBq62OPAYA", sceNpServiceChecker2IntIsSetServiceType);
    fun("aUgLCb3pSOo", sceNpServiceChecker2IntGetServiceAvailability);
    fun("1dMndqL-QgE", sceNpWordFilterCensorComment);
    fun("T6tnM1Uti4g", sceNpLookupNpId);
    fun("Fa4dVWgmffk", sceNpWordFilterSetTimeout);
    fun("W6iWw8aUQtA", sceNpUtilityInit);
    fun("X4elOoiAtB4", sceNpAppLaunchLinkIntGetCompatibleTitleIdNum);
    fun("r9BgI0PfJZg", sceNpWordFilterCreateTitleCtx);
    fun("V4EVrruHuy8", sceNpLookupPollAsync);
    fun("MjOFdwXYRKY", sceNpServiceCheckerIntGetAvailabilityList);
    fun("YX9dAus6baE", sceNpLookupWaitAsync);
    fun("Kq+ftR9LHlE", sceNpServiceChecker2IntAbortRequest);
    fun("cXpyESo49ko", sceNpAppInfoIntCreateRequest);
    fun("jXx0+2Wd1q8", sceNpAppInfoIntCheckAvailabilityAll);
    fun("pLr1fEQS1z8", sceNpBandwidthTestShutdown);
    fun("UUhI+IUMrcE", sceNpAppInfoIntCheckAvailability);
    fun("kvdMF48mB3Y", sceNpBandwidthTestAbort);
    fun("ur5SShyG0dk", sceNpWordFilterPollAsync);
    fun("az7fl9snOqw", sceNpServiceCheckerIntIsCached);
    fun("A1XQslLAA-Y", sceNpTitleMetadataIntCreateRequest);
    fun("Jj4mkpFO2gE", sceNpWordFilterSanitizeComment);
    fun("JA4+sS39GMs", sceNpLookupCreateAsyncRequest);
    fun("IG1Kd+k6U3s", sceNpServiceChecker2IntCreateRequest);
    fun("-8Wn4YKZLMM", sceNpAppLaunchLinkIntDestroyRequest);
#undef fun
}
}
