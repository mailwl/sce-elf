#include "libScePlayGo.h"
#include "process/process.h"
#define export(nid) process::get_export("libScePlayGo", nid, true)

namespace libScePlayGo {

typedef struct ScePlayGoInitParams {
    const void* bufAddr;
    uint32_t bufSize;
    uint32_t reserved;
} ScePlayGoInitParams;

typedef int32_t ScePlayGoHandle;

typedef uint16_t ScePlayGoChunkId;

typedef int8_t ScePlayGoLocus;
typedef enum ScePlayGoLocusValue {
    SCE_PLAYGO_LOCUS_NOT_DOWNLOADED = 0, // The chunk is not downloaded. Data in the chunk is inaccessible
    SCE_PLAYGO_LOCUS_LOCAL_SLOW = 2, // Slow local storage (Blu-ray Disc). Data in the chunk can be accessed directly
    SCE_PLAYGO_LOCUS_LOCAL_FAST = 3, // Fast local storage (HDD). Data in the chunk can be accessed directly
} ScePlayGoLocusValue;

typedef struct ScePlayGoToDo {
    ScePlayGoChunkId chunkId;
    ScePlayGoLocus locus;
    int8_t reserved;
} ScePlayGoToDo;

PS4API void scePlayGoPrefetch() {
    UNIMPLEMENTED_FUNC();
}
PS4API void scePlayGoGetEta() {
    UNIMPLEMENTED_FUNC();
}
PS4API void scePlayGoGetProgress() {
    UNIMPLEMENTED_FUNC();
}
PS4API void scePlayGoGetLanguageMask() {
    UNIMPLEMENTED_FUNC();
}
PS4API void scePlayGoSetLanguageMask() {
    UNIMPLEMENTED_FUNC();
}
PS4API void scePlayGoSetInstallSpeed() {
    UNIMPLEMENTED_FUNC();
}
PS4API int scePlayGoInitialize(const ScePlayGoInitParams* initParam) {
    LOG_DEBUG("calling native version, %p", initParam);
    typedef PS4API int (*scePlayGoInitialize_t)(const ScePlayGoInitParams*);
    auto func = (scePlayGoInitialize_t) export("ts6GlZOKRrE");
    int rc = 0;
    if (func) {
        rc = func(initParam);
    }
    return rc;
}
PS4API int scePlayGoOpen(ScePlayGoHandle* outHandle, const void* param) {
    //    LOG_DEBUG("%p, %p", outHandle, param);
    //    *outHandle = 111;
    //    return 0;
    LOG_DEBUG("calling native version, %p, %p", outHandle, param);
    typedef PS4API int (*scePlayGoOpen_t)(ScePlayGoHandle*, const void*);
    auto func = (scePlayGoOpen_t) export("M1Gma1ocrGE");
    int rc = 0;
    if (func) {
        rc = func(outHandle, param);
    }
    return rc;
}
PS4API void scePlayGoGetInstallSpeed() {
    UNIMPLEMENTED_FUNC();
}
PS4API int scePlayGoGetLocus(ScePlayGoHandle handle, const ScePlayGoChunkId* chunkIds, uint32_t numberOfEntries,
                             ScePlayGoLocus* outLoci) {
    LOG_DEBUG("");
    return 0;
}
PS4API int scePlayGoGetToDoList(ScePlayGoHandle handle, ScePlayGoToDo* outTodoList, uint32_t numberOfEntries,
                                uint32_t* outEntries) {
    LOG_DEBUG("");
    return 0;
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("-Q1-u1a7p0g", scePlayGoPrefetch);
    fun("v6EZ-YWRdMs", scePlayGoGetEta);
    fun("-RJWNMK3fC8", scePlayGoGetProgress);
    fun("3OMbYZBaa50", scePlayGoGetLanguageMask);
    fun("LosLlHOpNqQ", scePlayGoSetLanguageMask);
    fun("4AAcTU9R3XM", scePlayGoSetInstallSpeed);
    fun("ts6GlZOKRrE", scePlayGoInitialize);
    fun("M1Gma1ocrGE", scePlayGoOpen);
    fun("rvBSfTimejE", scePlayGoGetInstallSpeed);
    fun("uWIYLFkkwqk", scePlayGoGetLocus);
    fun("Nn7zKwnA5q0", scePlayGoGetToDoList);
#undef fun
}
}; // namespace libScePlayGo