#pragma once

#include "common.h"

typedef int64_t SceFiosTime;

typedef int32_t SceFiosHandle;
typedef SceFiosHandle SceFiosOp;
typedef uint8_t SceFiosOpEvent;

typedef int (*SceFiosOpCallback)(void* pContext, SceFiosOp op, SceFiosOpEvent event, int err);

typedef struct SceFiosOpAttr {
    SceFiosTime deadline;
    SceFiosOpCallback pCallback;
    void* pCallbackContext;
    int32_t priority : 8;
    uint32_t opflags : 24;
    uint32_t userTag;
    void* userPtr;
    void* pReserved;
} SceFiosOpAttr;

typedef struct SceFiosBuffer {
    void* pPtr;
    size_t length;
} SceFiosBuffer;
#define SCE_FIOS_BUFFER_INITIALIZER                                                                                    \
    { 0, 0 }

typedef int (*SceFiosVprintfCallback)(const char* fmt, va_list ap);
typedef void* (*SceFiosMemcpyCallback)(void* dst, const void* src, size_t len);

#define SCE_FIOS_THREAD_TYPES 3

typedef struct SceFiosParams {
    uint32_t initialized : 1;
    uint32_t paramsSize : 15;
    uint32_t pathMax : 16;
    uint32_t reserved1;
    uint32_t ioThreadCount;
    uint32_t threadsPerScheduler;
    uint32_t extraFlag1 : 1;
    uint32_t quiet : 1;
    uint32_t extraFlags : 30;
    uint32_t maxChunk;
    uint8_t maxDecompressorThreadCount;
    uint8_t schedulerAlgorithm;
    uint8_t reserved3;
    uint8_t reserved4;
    intptr_t reserved5;
    intptr_t reserved6;
    SceFiosBuffer opStorage;
    SceFiosBuffer fhStorage;
    SceFiosBuffer dhStorage;
    SceFiosBuffer chunkStorage;
    SceFiosVprintfCallback pVprintf;
    SceFiosMemcpyCallback pMemcpy;
    void* reserved7;
    int threadPriority[SCE_FIOS_THREAD_TYPES];
    int threadAffinity[SCE_FIOS_THREAD_TYPES];
    int threadStackSize[SCE_FIOS_THREAD_TYPES];
} SceFiosParams;

typedef enum SceFiosIOSchedulerAlgorithm {
    SCE_FIOS_IO_SCHED_DEADLINE = 0, ///< Use the deadline scheduler.
    SCE_FIOS_IO_SCHED_PRIORITY = 1, ///< Use the default priority I/O scheduler.
    SCE_FIOS_IO_SCHED_FIFO = 2,     ///< Use the FIFO I/O scheduler.
    SCE_FIOS_IO_SCHED_MAXIMUM = 4   ///< Maximum number of scheduler types.
} SceFiosIOSchedulerAlgorithm;
typedef enum SceFiosIOThreadCount {
    SCE_FIOS_IO_THREAD_COUNT_MIN = 1, ///< The minimum number of I/O threads allowed.

    SCE_FIOS_IO_THREAD_COUNT_MAX = 2
} SceFiosIOThreadCount;

typedef enum SceFiosSchedulerThreadCount {
    SCE_FIOS_SCHEDULER_THREAD_COUNT_MIN = 1, ///< The minimum number of non-callback scheduler threads allowed.
    SCE_FIOS_SCHEDULER_THREAD_COUNT_MAX =
        3, ///< The maximum number of non-callback scheduler threads allowed (for Windows and PlayStation&reg;4).
    SCE_FIOS_SCHEDULER_THREAD_COUNT_DEFAULT = 1 ///< The default number of non-callback scheduler threads.
} SceFiosSchedulerThreadCount;

#define SCE_FIOS_CHUNK_DEFAULT (512 * 1024)
#define SCE_FIOS_DECOMPRESSOR_THREAD_COUNT_DEFAULT 2
#define SCE_FIOS_IO_THREAD_DEFAULT_PRIORITY (260)
#define SCE_FIOS_DECOMPRESSOR_THREAD_DEFAULT_PRIORITY (700)
#define SCE_FIOS_CALLBACK_THREAD_DEFAULT_PRIORITY (260)
#define SCE_FIOS_THREAD_DEFAULT_AFFINITY 0
#define SCE_FIOS_IO_THREAD_DEFAULT_AFFINITY                                                                            \
    SCE_FIOS_THREAD_DEFAULT_AFFINITY ///< Default I/O thread affinity (PlayStation&reg;4).
#define SCE_FIOS_DECOMPRESSOR_THREAD_DEFAULT_AFFINITY                                                                  \
    SCE_FIOS_THREAD_DEFAULT_AFFINITY ///< Default decompressor thread affinity (PlayStation&reg;4).
#define SCE_FIOS_CALLBACK_THREAD_DEFAULT_AFFINITY                                                                      \
    SCE_FIOS_THREAD_DEFAULT_AFFINITY                     ///< Default callback thread affinity (PlayStation&reg;4).
#define SCE_FIOS_IO_THREAD_DEFAULT_STACKSIZE (80 * 1024) ///< Stack size for I/O threads (PlayStation&reg;4).
#define SCE_FIOS_DECOMPRESSOR_THREAD_DEFAULT_STACKSIZE                                                                 \
    (16 * 1024) ///< Stack size for decompressor threads (PlayStation&reg;4).
#define SCE_FIOS_CALLBACK_THREAD_DEFAULT_STACKSIZE (16 * 1024) ///< Stack size for callback threads (PlayStation&reg;4).

#define SCE_FIOS_PARAMS_INITIALIZER                                                                                    \
    {                                                                                                                  \
        0, sizeof(SceFiosParams), 0, 0, SCE_FIOS_IO_THREAD_COUNT_MAX, SCE_FIOS_SCHEDULER_THREAD_COUNT_DEFAULT,         \
            SCE_FIOS_IO_SCHED_PRIORITY, 0, 0, SCE_FIOS_CHUNK_DEFAULT, SCE_FIOS_DECOMPRESSOR_THREAD_COUNT_DEFAULT, 0,   \
            0, 0, 0, 0, SCE_FIOS_BUFFER_INITIALIZER, SCE_FIOS_BUFFER_INITIALIZER, SCE_FIOS_BUFFER_INITIALIZER,         \
            SCE_FIOS_BUFFER_INITIALIZER, NULL, NULL, NULL,                                                             \
            {SCE_FIOS_IO_THREAD_DEFAULT_PRIORITY, SCE_FIOS_DECOMPRESSOR_THREAD_DEFAULT_PRIORITY,                       \
             SCE_FIOS_CALLBACK_THREAD_DEFAULT_PRIORITY},                                                               \
            {SCE_FIOS_IO_THREAD_DEFAULT_AFFINITY, SCE_FIOS_DECOMPRESSOR_THREAD_DEFAULT_AFFINITY,                       \
             SCE_FIOS_CALLBACK_THREAD_DEFAULT_AFFINITY},                                                               \
        {                                                                                                              \
            SCE_FIOS_IO_THREAD_DEFAULT_STACKSIZE, SCE_FIOS_DECOMPRESSOR_THREAD_DEFAULT_STACKSIZE,                      \
                SCE_FIOS_CALLBACK_THREAD_DEFAULT_STACKSIZE                                                             \
        }                                                                                                              \
    }


#define SCE_FIOS_FH_SIZE	112
#define SCE_FIOS_DH_SIZE	104    ///< Size of DH data structure.

#define SCE_FIOS_DIVIDE_ROUNDING_UP(val,divisor)   (((val) + (divisor) - 1) / (divisor))

#define SCE_FIOS_OP_SIZE    256    ///< Size of OP data structure.
#define SCE_FIOS_CHUNK_SIZE	80     ///< Average estimated chunk size.

#define SCE_FIOS_ALIGN_UP(val, align) (((val) + ((align)-1)) & ~((align)-1))

#define SCE_FIOS_STORAGE_SIZE(num, size) (((num) * (size)) + SCE_FIOS_ALIGN_UP(SCE_FIOS_ALIGN_UP((num), 8) / 8, 8))

#define SCE_FIOS_CHUNK_STORAGE_SIZE(numChunks) \
	SCE_FIOS_STORAGE_SIZE(numChunks, SCE_FIOS_CHUNK_SIZE)

#define SCE_FIOS_DH_STORAGE_SIZE(numDHs, pathMax) SCE_FIOS_STORAGE_SIZE(numDHs, SCE_FIOS_DH_SIZE + pathMax)

#define SCE_FIOS_FH_STORAGE_SIZE(numFHs, pathMax) SCE_FIOS_STORAGE_SIZE(numFHs, SCE_FIOS_FH_SIZE + pathMax)

#define SCE_FIOS_OP_STORAGE_SIZE(numOps, pathMax) SCE_FIOS_STORAGE_SIZE(numOps, SCE_FIOS_OP_SIZE + pathMax)

namespace libSceFios2 {

void load(export_funcs& exp);
} // namespace libSceFios2
