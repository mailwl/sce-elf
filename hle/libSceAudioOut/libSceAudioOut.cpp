#include "libSceAudioOut.h"
#ifndef _WIN64
#include <unistd.h>
#endif // !_WIN64

namespace libSceAudioOut {

PS4API int sceAudioOutA3dControl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutA3dInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutChangeAppModuleState() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutClose() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutExConfigureOutputMode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutExGetSystemInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutGetHandleStatusInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutGetLastOutputTime() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutGetPortState() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutGetSystemState() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutInit() {
    LOG_DEBUG("");
    return 0;
}
PS4API int sceAudioOutMasteringGetState() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutMasteringInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutMasteringSetParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutMasteringTerm() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutMbusInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutOpen(SceUserServiceUserId userId, int type, int index, uint32_t len, uint32_t freq,
                               uint32_t param) {
    LOG_DEBUG("%s(%d, %d, %d, %u, %u, %u)", __FUNCTION__, userId, type, index, len, freq, param);
    return 1;
}
PS4API int sceAudioOutOpenEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int32_t sceAudioOutOutput(int32_t handle, const void* ptr) {
    LOG_DEBUG("(%d, %p)",handle, ptr);
#if defined(_WIN64)
    ::_sleep(100);
#else
    ::sleep(100);
#endif      
    return 0;
}
PS4API int sceAudioOutOutputs() {
    // UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutPtClose() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutPtOpen() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutSetConnections() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutSetDevConnection() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutSetHeadphoneOutMode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutSetMixLevelPadSpk(int32_t handle,
                                        int32_t mixLevel) {
    LOG_DEBUG("dummy");
    return 0;
}
PS4API int sceAudioOutSetMorpheusParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutSetPortConnections() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutSetPortStatuses() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutSetSparkParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutSetUsbVolume() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int32_t sceAudioOutSetVolume(int32_t handle, int32_t flag, int32_t* vol) {
    LOG_DEBUG("%s(%d, %x, %p)", __FUNCTION__, handle, flag, vol);
    return 0;
}
PS4API int sceAudioOutStartSharePlay() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutStopSharePlay() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioOutA3dExit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("RVWtUgoif5o", sceAudioOutMasteringTerm);
    fun("MapHTgeogbk", sceAudioOutPtClose);
    fun("ekNvsT22rsY", sceAudioOutOpen);
    fun("b+uAV89IlxE", sceAudioOutSetVolume);
    fun("n7KgxE8rOuE", sceAudioOutA3dInit);
    fun("teCyKKZPjME", sceAudioOutStopSharePlay);
    fun("O3FM2WXIJaI", sceAudioOutChangeAppModuleState);
    fun("wVwPU50pS1c", sceAudioOutSetMixLevelPadSpk);
    fun("r+qKw+ueD+Q", sceAudioOutMasteringGetState);
    fun("-LXhcGARw3k", sceAudioOutMbusInit);
    fun("r9KGqGpwTpg", sceAudioOutSetDevConnection);
    fun("o4OLQQqqA90", sceAudioOutSetConnections);
    fun("Gy0ReOgXW00", sceAudioOutSetPortConnections);
    fun("Iz9X7ISldhs", sceAudioOutA3dControl);
    fun("GrQ9s4IrNaQ", sceAudioOutGetPortState);
    fun("eeRsbeGYe20", sceAudioOutSetMorpheusParam);
    fun("Ptlts326pds", sceAudioOutGetLastOutputTime);
    fun("d3WL2uPE1eE", sceAudioOutSetSparkParam);
    fun("s1--uE9mBFw", sceAudioOutClose);
    fun("uo+eoPzdQ-s", sceAudioOutStartSharePlay);
    fun("08MKi2E-RcE", sceAudioOutSetHeadphoneOutMode);
    fun("oRBFflIrCg0", sceAudioOutSetPortStatuses);
    fun("X7Cfsiujm8Y", sceAudioOutSetUsbVolume);
    fun("Y3lXfCFEWFY", sceAudioOutGetHandleStatusInfo);
    fun("wZakRQsWGos", sceAudioOutExGetSystemInfo);
    fun("JfEPXVxhFqA", sceAudioOutInit);
    fun("R5hemoKKID8", sceAudioOutGetSystemState);
    fun("r1V9IFEE+Ts", sceAudioOutExConfigureOutputMode);
    fun("QOQtbeDqsT4", sceAudioOutOutput);
    fun("w3PdaSTSwGE", sceAudioOutOutputs);
    fun("4055yaUg3EY", sceAudioOutMasteringSetParam);
    fun("qLpSK75lXI4", sceAudioOutOpenEx);
    fun("xX4RLegarbg", sceAudioOutMasteringInit);
    fun("xyT8IUCL3CI", sceAudioOutPtOpen);
    fun("9RVIoocOVAo", sceAudioOutA3dExit); // libSceAudioOut
#undef fun
}
} // namespace libSceAudioOut
