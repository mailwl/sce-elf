#include "libSceRtc.h"

namespace libSceRtc {

typedef struct SceRtcTick {
    uint64_t tick;
} SceRtcTick;

PS4API int sceRtcCheckValid() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcCompareTick() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcConvertLocalTimeToUtc() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcConvertUtcToLocalTime() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcEnd() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcFormatRFC2822() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcFormatRFC2822LocalTime() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcFormatRFC3339() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcFormatRFC3339LocalTime() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcGetCurrentAdNetworkTick() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcGetCurrentClock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcGetCurrentClockLocalTime(SceRtcDateTime* pTime) {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcGetCurrentDebugNetworkTick() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcGetCurrentNetworkTick(SceRtcTick* pTick) {
    LOG_DEBUG("STUB %s(%p)\n", __FUNCTION__, pTick);
    pTick->tick = 0x1000000;
    return 0;
}
PS4API int sceRtcGetCurrentRawNetworkTick() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcGetCurrentTick(SceRtcTick* tick) {
    LOG_DEBUG("");
    tick->tick = 0x1000000;
    return 0;
}
PS4API int sceRtcGetDayOfWeek(int year, int month, int day) {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcGetDaysInMonth() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcGetDosTime() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcGetTick() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcGetTickResolution() {
    //UNIMPLEMENTED_FUNC();
    return 0x1000;
}
PS4API int sceRtcGetTime_t() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcGetWin32FileTime() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcIsLeapYear() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcParseDateTime() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcParseRFC3339() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcSetConf() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcSetCurrentAdNetworkTick() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcSetCurrentNetworkTick() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcSetCurrentTick() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcSetDosTime() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcSetTick() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcSetTime_t() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcSetWin32FileTime() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcTickAddDays() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcTickAddHours() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcTickAddMicroseconds() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcTickAddMinutes() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcTickAddMonths() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcTickAddSeconds() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcTickAddTicks() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcTickAddWeeks() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRtcTickAddYears() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("eiuobaF-hK4", sceRtcFormatRFC2822);
    fun("NxEI1KByvCI", sceRtcParseDateTime);
    fun("d4fHLCGmY80", sceRtcSetCurrentTick);
    fun("18B2NS1y9UU", sceRtcGetCurrentTick);
    //fun("CyIK-i4XdgQ", sceRtcGetDayOfWeek);
    fun("99bMGglFW3I", sceRtcParseRFC3339);
    fun("Ot1DE3gif84", sceRtcGetCurrentDebugNetworkTick);
    fun("qhDBtIo+auw", sceRtcSetCurrentNetworkTick);
    fun("AqVMssr52Rc", sceRtcTickAddTicks);
    fun("gI4t194c2W8", sceRtcTickAddWeeks);
    //fun("ueega6v3GUw", sceRtcSetTick);
    fun("WJ3rqFwymew", sceRtcFormatRFC3339);
    fun("aYPCd1cChyg", sceRtcSetDosTime);
    fun("M1TvFst-jrM", sceRtcConvertUtcToLocalTime);
    fun("LN3Zcb72Q0c", sceRtcGetCurrentAdNetworkTick);
    fun("fNaZ4DbzHAE", sceRtcCompareTick);
    fun("bDEVVP4bTjQ", sceRtcSetTime_t);
    fun("CL6y9q-XbuQ", sceRtcTickAddMonths);
    fun("n5JiAJXsbcs", sceRtcSetWin32FileTime);
    fun("fFLgmNUpChg", sceRtcSetConf);
    fun("8lfvnRMqwEM", sceRtcGetCurrentClock);
    fun("MDc5cd8HfCA", sceRtcTickAddHours);
    fun("8Yr143yEnRo", sceRtcConvertLocalTimeToUtc);
    fun("AxHBk3eat04", sceRtcFormatRFC2822LocalTime);
    fun("DwuHIlLGW8I", sceRtcFormatRFC3339LocalTime);
    //fun("mn-tf4QiFzk", sceRtcTickAddMinutes);
    fun("sV2tK+yOhBU", sceRtcSetCurrentAdNetworkTick);
    fun("LlodCMDbk3o", sceRtcInit);
    fun("BtqmpTRXHgk", sceRtcGetTime_t);
    fun("NR1J0N7L2xY", sceRtcTickAddDays);
    fun("HWxHOdbM-Pg", sceRtcGetCurrentRawNetworkTick);
    // fun("ZPD1YOKI+Kw", sceRtcGetCurrentClockLocalTime);
    fun("XPIiw58C+GM", sceRtcTickAddMicroseconds);
    fun("3O7Ln8AqJ1o", sceRtcGetDaysInMonth);
    fun("E7AR4o7Ny7E", sceRtcGetDosTime);
    //fun("zO9UL3qIINQ", sceRtcGetCurrentNetworkTick);
    fun("-5y2uJ62qS8", sceRtcTickAddYears);
    fun("jMNwqYr4R-k", sceRtcGetTickResolution);
    fun("07O525HgICs", sceRtcTickAddSeconds);
    fun("8SljQx6pDP8", sceRtcEnd);
    fun("Ug8pCwQvh0c", sceRtcIsLeapYear);
    fun("jfRO0uTjtzA", sceRtcGetWin32FileTime);
    fun("lPEBYdVX0XQ", sceRtcCheckValid);
    //fun("8w-H19ip48I", sceRtcGetTick);
#undef fun
}
} // namespace libSceRtc
