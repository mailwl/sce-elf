#pragma once

#include "common.h"

namespace libSceRtc {
typedef struct SceRtcDateTime {
    unsigned short year;
    unsigned short month;
    unsigned short day;
    unsigned short hour;
    unsigned short minute;
    unsigned short second;
    unsigned int microsecond;
} SceRtcDateTime;

void load(export_funcs& exp);
}; // namespace libSceRtc
