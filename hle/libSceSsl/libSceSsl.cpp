#include "libSceSsl.h"

namespace libSceSsl {
    PS4API int sceSslInit(size_t poolSize) {
        LOG_DEBUG("Unimplemented %s(%llx)\n",__FUNCTION__, poolSize);
        return 1;
    }
    PS4API int sceSslTerm() {
        //UNIMPLEMENTED_FUNC();
        return 0;
    }

    void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
        fun("hdpVEUDFW3s", sceSslInit); // libSceSsl
        fun("0K1yQ6Lv-Yc", sceSslTerm); // libSceSsl
#undef fun
    }
};