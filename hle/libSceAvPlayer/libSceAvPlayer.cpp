#include "libSceAvPlayer.h"

namespace libSceAvPlayer {

PS4API int sceAvPlayerAddSource() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerAddSourceEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerClose() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerCurrentTime() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerDisableStream() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerEnableStream() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerGetAudioData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerGetStreamInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerGetVideoData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerGetVideoDataEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerInitEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerIsActive() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerJumpToTime() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerPause() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerPostInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerPrintf() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerResume() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerSetAvSyncMode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerSetLogCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerSetLooping() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerSetTrickSpeed() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerStart() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerStop() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerStreamCount() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAvPlayerVprintf() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("ZC17w3vB5Lo", sceAvPlayerStop);
    fun("agig-iDRrTE", sceAvPlayerPrintf);
    fun("w5moABNwnRY", sceAvPlayerResume);
    fun("o3+RWnHViSg", sceAvPlayerGetVideoData);
    fun("d8FcbzfAdQw", sceAvPlayerGetStreamInfo);
    fun("XC9wM+xULz8", sceAvPlayerJumpToTime);
    fun("OVths0xGfho", sceAvPlayerSetLooping);
    fun("yN7Jhuv8g24", sceAvPlayerVprintf);
    fun("Wnp1OVcrZgk", sceAvPlayerGetAudioData);
    fun("wwM99gjFf1Y", sceAvPlayerCurrentTime);
    fun("ET4Gr-Uu07s", sceAvPlayerStart);
    fun("x8uvuFOPZhU", sceAvPlayerAddSourceEx);
    fun("eBTreZ84JFY", sceAvPlayerSetLogCallback);
    fun("o9eWRkSL+M4", sceAvPlayerInitEx);
    fun("9y5v+fGN4Wk", sceAvPlayerPause);
    fun("NkJwDzKmIlw", sceAvPlayerClose);
    fun("hdTyRzCXQeQ", sceAvPlayerStreamCount);
    fun("KMcEa+rHsIo", sceAvPlayerAddSource);
    fun("JdksQu8pNdQ", sceAvPlayerGetVideoDataEx);
    fun("av8Z++94rs0", sceAvPlayerSetTrickSpeed);
    fun("BOVKAzRmuTQ", sceAvPlayerDisableStream);
    fun("HD1YKVU26-M", sceAvPlayerPostInit);
    fun("UbQoYawOsfY", sceAvPlayerIsActive);
    fun("ODJK2sn9w4A", sceAvPlayerEnableStream);
    fun("aS66RI0gGgo", sceAvPlayerInit);
    fun("k-q+xOxdc3E", sceAvPlayerSetAvSyncMode);
#undef fun
}
}
