#include "libSceImeDialog.h"

namespace libSceImeDialog {

PS4API int sceImeDialogAbort() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceImeDialogForceClose() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceImeDialogGetCurrentStarState() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceImeDialogGetPanelPositionAndForm() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceImeDialogGetPanelSize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceImeDialogGetPanelSizeExtended() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceImeDialogGetResult() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceImeDialogGetStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceImeDialogInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceImeDialogInitInternal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceImeDialogInitInternal2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceImeDialogInitInternal3() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceImeDialogSetPanelPosition() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceImeDialogTerm() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("oBmw4xrmfKs", sceImeDialogAbort);
    fun("x01jxu+vxlc", sceImeDialogGetResult);
    fun("NUeBrN7hzf0", sceImeDialogInit);
    fun("8jqzzPioYl8", sceImeDialogGetPanelPositionAndForm);
    fun("wqsJvRXwl58", sceImeDialogGetPanelSize);
    fun("KR6QDasuKco", sceImeDialogInitInternal);
    fun("bX4H+sxPI-o", sceImeDialogForceClose);
    fun("oe92cnJQ9HE", sceImeDialogInitInternal2);
    fun("gyTyVn+bXMw", sceImeDialogTerm);
    fun("fy6ntM25pEc", sceImeDialogGetCurrentStarState);
    fun("-2WqB87KKGg", sceImeDialogSetPanelPosition);
    fun("IADmD4tScBY", sceImeDialogGetStatus);
    fun("IoKIpNf9EK0", sceImeDialogInitInternal3);
    fun("CRD+jSErEJQ", sceImeDialogGetPanelSizeExtended);
#undef fun
}
}
