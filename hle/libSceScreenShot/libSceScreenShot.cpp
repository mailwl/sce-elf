#include "libSceScreenShot.h"

namespace libSceScreenShot {

PS4API int _Z5dummyv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceScreenShotCapture() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceScreenShotDisable() {
    LOG_DEBUG("called.");
    return 0;
}
PS4API int sceScreenShotDisableNotification() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceScreenShotEnable() {
    LOG_DEBUG("");
    return 0;
}
PS4API int sceScreenShotEnableNotification() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceScreenShotGetAppInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceScreenShotIsDisabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceScreenShotIsVshScreenCaptureDisabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceScreenShotSetOverlayImage() {
    LOG_DEBUG("");
    return 0;
}
PS4API int sceScreenShotSetOverlayImageWithOrigin() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceScreenShotSetParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("73WQ4Jj0nJI", sceScreenShotSetOverlayImageWithOrigin);
    fun("2xxUtuC-RzE", sceScreenShotEnable);
    fun("JuMLLmmvRgk", sceScreenShotCapture);
    fun("ICNJ-1POs84", sceScreenShotIsVshScreenCaptureDisabled);
    fun("ysfza71rm9M", sceScreenShotDisableNotification);
    fun("ahHhOf+QNkQ", sceScreenShotSetOverlayImage);
    fun("AS45QoYHjc4", _Z5dummyv);
    fun("BDUaqlVdSAY", sceScreenShotEnableNotification);
    fun("G7KlmIYFIZc", sceScreenShotSetParam);
    fun("hNmK4SdhPT0", sceScreenShotGetAppInfo);
    fun("-SV-oTNGFQk", sceScreenShotIsDisabled);
    fun("tIYf0W5VTi8", sceScreenShotDisable);
#undef fun
}
}
