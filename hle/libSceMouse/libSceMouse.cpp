#include "libSceMouse.h"

namespace libSceMouse {
typedef struct SceMouseOpenParam {
    uint8_t behaviorFlag;
    uint8_t reserve[7];
} SceMouseOpenParam;

PS4API int sceMouseInit() {
    LOG_DEBUG("Unimplemented");
    return 0;
}
PS4API int sceMouseOpen(SceUserServiceUserId userId, int32_t type, int32_t index, SceMouseOpenParam* pParam) {
    LOG_DEBUG("Unimplemented");
    return 0x1234;
}
PS4API int sceMouseClose(int32_t handle) {
    LOG_DEBUG("Unimplemented");
    return 0;
}
PS4API void sceMouseRead() {
    UNIMPLEMENTED_FUNC();
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("Qs0wWulgl7U", sceMouseInit);  // libSceMouse
    fun("RaqxZIf6DvE", sceMouseOpen);  // libSceMouse
    fun("cAnT0Rw-IwU", sceMouseClose); // libSceMouse
    fun("x8qnXqh-tiM", sceMouseRead);  // libSceMouse
#undef fun
}
} // namespace libSceMouse