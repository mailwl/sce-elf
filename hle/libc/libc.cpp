#include "libc.h"

namespace libc {

PS4API int abort() {
    UNIMPLEMENTED_FUNC();
    return 0;
}

PS4API uint64_t strtol(const char * __restrict__ _Str,char ** __restrict__ _EndPtr,int _Radix) {
    uint64_t rc = ::strtol(_Str, _EndPtr, _Radix);
    printf("%s('%p', %p, %d) = %zd\n", __FUNCTION__, _Str, _EndPtr, _Radix, rc);
    return rc;
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("L1SBTkC+Cvw", abort);
    //fun("mXlxhmLNMPg", strtol);
#undef fun
}
} // namespace libc
