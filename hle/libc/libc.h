#pragma  once

#include "common.h"

namespace libc {
    void load(export_funcs& exp);
}
