#include "libSceSigninDialog.h"

namespace libSceSigninDialog {

PS4API int sceSigninDialogClose() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSigninDialogGetResult() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSigninDialogGetStatus() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSigninDialogInitialize() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSigninDialogOpen() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSigninDialogTerminate() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSigninDialogUpdateStatus() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("2m077aeC+PA", sceSigninDialogGetStatus);
    fun("nqG7rqnYw1U", sceSigninDialogGetResult);
    fun("JlpJVoRWv7U", sceSigninDialogOpen);
    fun("LXlmS6PvJdU", sceSigninDialogTerminate);
    fun("mlYGfmqE3fQ", sceSigninDialogInitialize);
    fun("M3OkENHcyiU", sceSigninDialogClose);
    fun("Bw31liTFT3A", sceSigninDialogUpdateStatus);
#undef fun
}
}
