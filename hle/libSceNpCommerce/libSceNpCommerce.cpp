#include "libSceNpCommerce.h"

namespace libSceNpCommerce {

PS4API int sceNpCommerceDialogClose() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCommerceDialogGetResult() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCommerceDialogGetStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCommerceDialogInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCommerceDialogInitializeInternal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCommerceDialogOpen() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCommerceDialogTerminate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCommerceDialogUpdateStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCommerceHidePsStoreIcon() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCommerceSetPsStoreIconLayout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCommerceShowPsStoreIcon() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("NU3ckGHMFXo", sceNpCommerceDialogClose);
    fun("uKTDW8hk-ts", sceNpCommerceSetPsStoreIconLayout);
    fun("m-I92Ab50W8", sceNpCommerceDialogTerminate);
    fun("DHmwsa6S8Tc", sceNpCommerceShowPsStoreIcon);
    fun("dsqCVsNM0Zg", sceNpCommerceHidePsStoreIcon);
    fun("CCbC+lqqvF0", sceNpCommerceDialogGetStatus);
    fun("DfSCDRA3EjY", sceNpCommerceDialogOpen);
    fun("LR5cwFMMCVE", sceNpCommerceDialogUpdateStatus);
    fun("9ZiLXAGG5rg", sceNpCommerceDialogInitializeInternal);
    fun("0aR2aWmQal4", sceNpCommerceDialogInitialize);
    fun("r42bWcQbtZY", sceNpCommerceDialogGetResult);
#undef fun
}
}
