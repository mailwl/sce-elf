#include "libSceInvitationDialog.h"

namespace libSceInvitationDialog {
    PS4API void sceInvitationDialogTerminate() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceInvitationDialogInitialize() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceInvitationDialogOpen() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceInvitationDialogUpdateStatus() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceInvitationDialogGetResult() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceInvitationDialogClose() {
        UNIMPLEMENTED_FUNC();
    }

    void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
        fun("B6HVJtDYxEE", sceInvitationDialogTerminate); // libSceInvitationDialog
        fun("XvA5KS56wcs", sceInvitationDialogInitialize); // libSceInvitationDialog
        fun("0zU0G+wiVLA", sceInvitationDialogOpen); // libSceInvitationDialog
        fun("9+g9iOq+7kg", sceInvitationDialogUpdateStatus); // libSceInvitationDialog
        fun("8XKR6wa64iQ", sceInvitationDialogGetResult); // libSceInvitationDialog
        fun("WWtCL5lzi7Y", sceInvitationDialogClose); // libSceInvitationDialog
#undef fun
    }
};