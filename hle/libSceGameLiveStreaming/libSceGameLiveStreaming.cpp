#include "libSceGameLiveStreaming.h"

namespace libSceGameLiveStreaming {

PS4API int sceGameLiveStreamingApplySocialFeedbackMessageFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingClearPresetSocialFeedbackCommands() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingClearSocialFeedbackMessages() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingClearSpoilerTag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingEnableLiveStreaming(const bool isEnableLiveStreaming) {
    LOG_DEBUG("enabled: %d", isEnableLiveStreaming);
    return 0;
}
PS4API int sceGameLiveStreamingEnableSocialFeedback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingGetCurrentBroadcastScreenLayout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingGetCurrentStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingGetCurrentStatus2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingGetProgramInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingGetSocialFeedbackMessages() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingGetSocialFeedbackMessagesCount() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingInitialize(const size_t heapSize) {
    LOG_DEBUG("(%zx)", heapSize);
    return 0;
}
PS4API int sceGameLiveStreamingLaunchLiveViewer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingPermitLiveStreaming() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingPermitServerSideRecording(const bool isPermitServerSideRecording) {
    LOG_DEBUG("(%d)", isPermitServerSideRecording);
    return 0;
}
PS4API int sceGameLiveStreamingPostSocialMessage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingSetCameraFrameSetting() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingSetGuardAreas() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingSetInvitationSessionId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingSetLinkCommentPreset() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingSetMaxBitrate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingSetMetadata() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingSetPresetSocialFeedbackCommands() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingSetPresetSocialFeedbackCommandsDescription() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingSetSpoilerTag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingSetStandbyScreenResource() {
    LOG_DEBUG("");
    return 0;
}
PS4API int sceGameLiveStreamingStartSocialFeedbackMessageFiltering() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingStopSocialFeedbackMessageFiltering() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceGameLiveStreamingTerminate() {
    LOG_DEBUG("()");
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("PMx7N4WqNdo", sceGameLiveStreamingGetSocialFeedbackMessages);
    fun("hUY-mSOyGL0", sceGameLiveStreamingSetMetadata);
    fun("kvYEw2lBndk", sceGameLiveStreamingInitialize);
    fun("3PSiwAzFISE", sceGameLiveStreamingSetCameraFrameSetting);
    fun("CoPMx369EqM", sceGameLiveStreamingGetCurrentStatus);
    fun("FcHBfHjFXkA", sceGameLiveStreamingClearPresetSocialFeedbackCommands);
    fun("yeQKjHETi40", sceGameLiveStreamingGetSocialFeedbackMessagesCount);
    fun("NqkTzemliC0", sceGameLiveStreamingApplySocialFeedbackMessageFilter);
    fun("q-kxuaF7URU", sceGameLiveStreamingSetMaxBitrate);
    fun("-EHnU68gExU", sceGameLiveStreamingPermitServerSideRecording);
    fun("aRSQNqbats4", sceGameLiveStreamingGetCurrentBroadcastScreenLayout);
    fun("ysWfX5PPbfc", sceGameLiveStreamingLaunchLiveViewer);
    fun("6c2zGtThFww", sceGameLiveStreamingClearSpoilerTag);
    fun("Y1WxX7dPMCw", sceGameLiveStreamingStartSocialFeedbackMessageFiltering);
    fun("wBOQWjbWMfU", sceGameLiveStreamingEnableSocialFeedback);
    fun("lZ2Sd0uEvpo", sceGameLiveStreamingClearSocialFeedbackMessages);
    fun("bYuGUBuIsaY", sceGameLiveStreamingStopSocialFeedbackMessageFiltering);
    fun("9yK6Fk8mKOQ", sceGameLiveStreamingTerminate);
    fun("hggKhPySVgI", sceGameLiveStreamingPostSocialMessage);
    fun("lK8dLBNp9OE", sceGameLiveStreamingGetCurrentStatus2);
    fun("ycodiP2I0xo", sceGameLiveStreamingSetPresetSocialFeedbackCommands);
    fun("QmQYwQ7OTJI", sceGameLiveStreamingSetInvitationSessionId);
    fun("K0QxEbD7q+c", sceGameLiveStreamingPermitLiveStreaming);
    fun("OIIm19xu+NM", sceGameLiveStreamingGetProgramInfo);
    fun("x6deXUpQbBo", sceGameLiveStreamingSetPresetSocialFeedbackCommandsDescription);
    fun("dWM80AX39o4", sceGameLiveStreamingEnableLiveStreaming);
    fun("Sb5bAXyUt5c", sceGameLiveStreamingSetLinkCommentPreset);
    fun("ZuX+zzz2DkA", sceGameLiveStreamingSetSpoilerTag);
    fun("MLvYI86FFAo", sceGameLiveStreamingSetStandbyScreenResource);
    fun("Gw6S4oqlY7E", sceGameLiveStreamingSetGuardAreas);
#undef fun
}
}
