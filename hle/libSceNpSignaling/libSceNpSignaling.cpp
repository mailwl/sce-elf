#include "libSceNpSignaling.h"

namespace libSceNpSignaling {
PS4API void sceNpSignalingGetConnectionStatus() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceNpSignalingGetPeerNetInfoResult() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceNpSignalingGetConnectionInfo() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceNpSignalingTerminateConnection() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceNpSignalingInitialize() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceNpSignalingCreateContext() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceNpSignalingDeleteContext() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceNpSignalingTerminate() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceNpSignalingActivateConnection() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceNpSignalingGetConnectionFromNpId() {
    UNIMPLEMENTED_FUNC();
}

PS4API int sceNpSignalingGetLocalNetInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("bD-JizUb3JM", sceNpSignalingGetConnectionStatus);   // libSceNpSignaling
    fun("2HajCEGgG4s", sceNpSignalingGetPeerNetInfoResult);  // libSceNpSignaling
    fun("AN3h0EBSX7A", sceNpSignalingGetConnectionInfo);     // libSceNpSignaling
    fun("b4qaXPzMJxo", sceNpSignalingTerminateConnection);   // libSceNpSignaling
    fun("3KOuC4RmZZU", sceNpSignalingInitialize);            // libSceNpSignaling
    fun("5yYjEdd4t8Y", sceNpSignalingCreateContext);         // libSceNpSignaling
    fun("hx+LIg-1koI", sceNpSignalingDeleteContext);         // libSceNpSignaling
    fun("NPhw0UXaNrk", sceNpSignalingTerminate);             // libSceNpSignaling
    fun("0UvTFeomAUM", sceNpSignalingActivateConnection);    // libSceNpSignaling
    fun("GQ0hqmzj0F4", sceNpSignalingGetConnectionFromNpId); // libSceNpSignaling
    fun("U8AQMlOFBc8", sceNpSignalingGetLocalNetInfo);       // libSceNpSignaling

#undef fun
}
} // namespace libSceNpSignaling
