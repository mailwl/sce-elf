#include "libSceNpSnsFacebookDialog.h"

namespace libSceNpSnsFacebookDialog {
    PS4API void sceNpSnsFacebookDialogTerminate() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceNpSnsFacebookDialogInitialize() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceNpSnsFacebookDialogOpen() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceNpSnsFacebookDialogUpdateStatus() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceNpSnsFacebookDialogGetResult() {
        UNIMPLEMENTED_FUNC();
    }

    void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
        fun("cdqOFyupRS4", sceNpSnsFacebookDialogTerminate); // libSceNpSnsFacebookDialog
        fun("tdQArULbNQk", sceNpSnsFacebookDialogInitialize); // libSceNpSnsFacebookDialog
        fun("Hi3onqHSN6M", sceNpSnsFacebookDialogOpen); // libSceNpSnsFacebookDialog
        fun("fjV7C8H0Y8k", sceNpSnsFacebookDialogUpdateStatus); // libSceNpSnsFacebookDialog
        fun("AifdcI1aKas", sceNpSnsFacebookDialogGetResult); // libSceNpSnsFacebookDialog
#undef fun
    }
};