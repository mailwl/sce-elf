#include "libSceErrorDialog.h"

namespace libSceErrorDialog {
PS4API int32_t sceErrorDialogInitialize() {
    return 0;
}
PS4API void sceErrorDialogOpen() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceErrorDialogTerminate() {
    LOG_DEBUG("dummy");
    return 0;
}
PS4API void sceErrorDialogUpdateStatus() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceErrorDialogClose() {
    UNIMPLEMENTED_FUNC();
}
typedef enum SceErrorDialogStatus {
    SCE_ERROR_DIALOG_STATUS_NONE			= 0,
    SCE_ERROR_DIALOG_STATUS_INITIALIZED		= 1,
    SCE_ERROR_DIALOG_STATUS_RUNNING			= 2,
    SCE_ERROR_DIALOG_STATUS_FINISHED		= 3
} SceErrorDialogStatus;
PS4API SceErrorDialogStatus sceErrorDialogGetStatus() {
    LOG_DEBUG("%s()\n", __FUNCTION__);
    return SCE_ERROR_DIALOG_STATUS_INITIALIZED;
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("I88KChlynSs", sceErrorDialogInitialize);
    fun("M2ZF-ClLhgY", sceErrorDialogOpen);
    fun("9XAxK2PMwk8", sceErrorDialogTerminate);
    fun("WWiGuh9XfgQ", sceErrorDialogUpdateStatus);
    fun("ekXHb1kDBl0", sceErrorDialogClose);
    fun("t2FvHRXzgqk", sceErrorDialogGetStatus);
#undef fun
}
}
