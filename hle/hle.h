#pragma once

#include "common.h"

namespace hle {
bool load_hle_library(const char* lib_name, export_funcs&);
}

