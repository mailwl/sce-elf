#include "libSceSysCore.h"

namespace libSceSysCore {

PS4API int _sceApplicationGetAppId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationAddProcess2() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationBlockingKill2() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationBlockingKill3() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationContinue() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationCrashSyscore() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationExitSpawn() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationExitSpawn2() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationFinalize() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationGetAppInfoByAppId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationGetCoredumpDirAndFileName() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationGetCoredumpState() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationGetProcs() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationGetShellCoreAppId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationInitialize() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationInitializeForShellCore() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationIsPrimaryProcess() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationIsResumable() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationIsSuspendable() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationKickCoredump() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationKickCoredump2() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationLocalProcessKill() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationLocalProcessKill2() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationLocalProcessResume() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationLocalProcessSuspend() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationNotifyCoredumpRequestEnd() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationNotifyCoredumpRequestProgress() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationNotifyVshMainOnStandby() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationNotifyVshReady() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationRaiseException() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationRaiseExceptionToLocalPid() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationResume() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSendDebugSpawnResult2() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSendResultOfDebuggerKillRequest() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSendResultOfDebuggerResumeRequest() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSendResultOfDebuggerSuspendRequest() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSendResultOfDebuggerTitleIdLaunchRequest() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSetApplicationFocus() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSetControllerFocus() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSetControllerFocusPermissionToSubProcess() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSetMemoryPstate() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSignalShellCoreHeartBeat() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSuspend() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSwitchToBaseMode() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSwitchToNeoMode() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSwitchToNeoMode2() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSystemReboot() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSystemShutdown2() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceApplicationSystemSuspend() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysCoreReceiveEvent() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("F0VSNgqS1+g", sceApplicationSystemReboot);
    fun("60RuiO+Dep4", sceApplicationSwitchToNeoMode);
    fun("e-ODUaoHXiY", _sceApplicationGetAppId);
    fun("GdIm4Jm2mm8", sceApplicationGetProcs);
    fun("UWQ3JZbb5bM", sceApplicationIsPrimaryProcess);
    fun("f+HdRtISAvw", sceApplicationGetCoredumpDirAndFileName);
    fun("IgqBZdkIvqM", sceApplicationRaiseException);
    fun("g8rs9gM3qAQ", sceApplicationSystemSuspend);
    fun("+7qLP7HjL+4", sceApplicationContinue);
    fun("S5pL+zLvJPI", sceApplicationSetApplicationFocus);
    fun("mzppT1RdIj0", sceApplicationAddProcess2);
    fun("8aPSCSE8msA", sceApplicationIsSuspendable);
    fun("Oj6EZyXtQkk", sceApplicationSwitchToBaseMode);
    fun("SfPuQO4vW9M", sceApplicationFinalize);
    fun("W+VrzBY+LBA", sceApplicationNotifyCoredumpRequestEnd);
    fun("os4QwIWwOJw", sceApplicationIsResumable);
    fun("DfD47ZODK8Y", sceApplicationGetCoredumpState);
    fun("z5e9ck9iUDc", sceApplicationCrashSyscore);
    fun("BVfIVVqL1DA", sceApplicationLocalProcessKill);
    fun("K62IGGSMrME", sceApplicationSetMemoryPstate);
    fun("C9ElcezDvIE", sceApplicationSuspend);
    fun("GPC-wPiKvKk", sceApplicationSendResultOfDebuggerKillRequest);
    fun("dqwCM7RWifY", sceApplicationNotifyVshReady);
    fun("bn3xT3DX1o8", sceApplicationSetControllerFocus);
    fun("aDloR-BDH7k", sceApplicationSendResultOfDebuggerSuspendRequest);
    fun("XFYItOxS6r0", sceApplicationInitialize);
    fun("slkr1eWM1o8", sceApplicationLocalProcessSuspend);
    fun("DReLgJ4LPj8", sceApplicationSendResultOfDebuggerTitleIdLaunchRequest);
    fun("f6zbafRlX3s", sceApplicationNotifyVshMainOnStandby);
    fun("BIqhORVcFnI", sceApplicationSwitchToNeoMode2);
    fun("n2oTCajh0fE", sceApplicationSendResultOfDebuggerResumeRequest);
    fun("++DyHhMUeQA", sceApplicationRaiseExceptionToLocalPid);
    fun("MW+T6ZCK7wo", sceApplicationSignalShellCoreHeartBeat);
    fun("y55QWr8yzrY", sceApplicationBlockingKill3);
    fun("9PCBPZcDU7Q", sceApplicationInitializeForShellCore);
    fun("AumF38kir6s", sceApplicationLocalProcessResume);
    fun("kMaBFIQNsSI", sceApplicationGetAppInfoByAppId);
    fun("+dLapxrRdJM", sceApplicationNotifyCoredumpRequestProgress);
    fun("b6KysZfpTwE", sceApplicationExitSpawn);
    fun("t6NwIzt2IE8", sceApplicationKickCoredump);
    fun("FvuQlkBE3Ng", sceApplicationBlockingKill2);
    fun("0Ygl1mtSLhg", sceApplicationExitSpawn2);
    fun("yvbO67OvrFc", sceApplicationGetShellCoreAppId);
    fun("Fv+LV+3CjIE", sceApplicationResume);
    fun("5DIzIUbaYkc", sceApplicationSendDebugSpawnResult2);
    fun("zrxj3h66ScY", sceApplicationKickCoredump2);
    fun("0YmjfSJ8X98", sceSysCoreReceiveEvent);
    fun("OITH4TZEdRY", sceApplicationLocalProcessKill2);
    fun("8X5O9jV-bFs", sceApplicationSetControllerFocusPermissionToSubProcess);
    fun("kP9AaRQ4bs0", sceApplicationSystemShutdown2);
#undef fun
}
}
