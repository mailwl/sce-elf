#include "libSceAjm.h"

namespace libSceAjm {

typedef unsigned int SceAjmContextId;
typedef unsigned int SceAjmCodecType;
PS4API int sceAjmBatchCancel() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmBatchErrorDump() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmBatchJobControlBufferRa() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmBatchJobInlineBuffer() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmBatchJobRunBufferRa() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmBatchJobRunSplitBufferRa() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmBatchStartBuffer() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmBatchWait() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmDecAt9ParseConfigData() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmDecMp3ParseFrame() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmFinalize() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmInitialize(int64_t iReserved, SceAjmContextId* const pContext) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, pContext);
    *pContext = 1;
    return 0;
}
PS4API int sceAjmInstanceCodecType() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmInstanceCreate() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmInstanceDestroy() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmInstanceExtend() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmInstanceSwitch() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmMemoryRegister() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmMemoryUnregister() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmModuleRegister(const SceAjmContextId uiContext, const SceAjmCodecType uiCodec, int64_t iReserved) {
    LOG_DEBUG("Unimplemented %s(%d, %d)", __FUNCTION__, uiContext, uiCodec);
    return 0;
}
PS4API int sceAjmModuleUnregister() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceAjmStrError() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
//    fun("ElslOCpOIns", sceAjmBatchJobRunBufferRa);
//    fun("AxhcqVv5AYU", sceAjmStrError);
//    fun("Wi7DtlLV+KI", sceAjmModuleUnregister);
//    fun("MHur6qCsUus", sceAjmFinalize);
//    fun("-qLsfDAywIY", sceAjmBatchWait);
//    fun("pIpGiaYkHkM", sceAjmMemoryUnregister);
//    fun("bkRHEYG6lEM", sceAjmMemoryRegister);
//    fun("AxoDrINp4J8", sceAjmInstanceCreate);
//    fun("1t3ixYNXyuc", sceAjmDecAt9ParseConfigData);
//    fun("dmDybN--Fn8", sceAjmBatchJobControlBufferRa);
//    fun("Q3dyFuwGn64", sceAjmModuleRegister);
//    fun("rgLjmfdXocI", sceAjmInstanceSwitch);
//    fun("7jdAXK+2fMo", sceAjmBatchJobRunSplitBufferRa);
//    fun("fFFkk0xfGWs", sceAjmBatchStartBuffer);
//    fun("stlghnic3Jc", sceAjmBatchJobInlineBuffer);
//    fun("WfAiBW8Wcek", sceAjmBatchErrorDump);
//    fun("diXjQNiMu-s", sceAjmInstanceCodecType);
//    fun("RbLbuKv8zho", sceAjmInstanceDestroy);
//    fun("YDFR0dDVGAg", sceAjmInstanceExtend);
//    fun("dl+4eHSzUu4", sceAjmInitialize);
//    fun("eDFeTyi+G3Y", sceAjmDecMp3ParseFrame);
//    fun("NVDXiUesSbA", sceAjmBatchCancel);
#undef fun
}
} // namespace libSceAjm
