#pragma once

#include "common.h"

namespace libSceNpManager {
    typedef enum SceNpReachabilityState {
        SCE_NP_REACHABILITY_STATE_UNAVAILABLE = 0,
        SCE_NP_REACHABILITY_STATE_AVAILABLE,
        SCE_NP_REACHABILITY_STATE_REACHABLE
    } SceNpReachabilityState;

    typedef void (*SceNpReachabilityStateCallback)(
            SceUserServiceUserId userId,
            SceNpReachabilityState state,
            void *userdata
    );
    void load(export_funcs& exp);
};
