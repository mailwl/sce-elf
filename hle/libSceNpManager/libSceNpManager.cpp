#include "libSceNpManager.h"

namespace libSceNpManager {

#define SCE_NP_TITLE_ID_LEN 12
typedef struct SceNpTitleId {
    char id[SCE_NP_TITLE_ID_LEN + 1];
    uint8_t padding[3];
} SceNpTitleId;

#define SCE_NP_TITLE_SECRET_SIZE (128)
typedef struct SceNpTitleSecret {
    uint8_t data[SCE_NP_TITLE_SECRET_SIZE];
} SceNpTitleSecret;

typedef int32_t SceNpPlusEventType;
#define SCE_NP_PLUS_EVENT_RECHECK_NEEDED 1

typedef PS4API void (*SceNpPlusEventCallback)(SceUserServiceUserId userId, SceNpPlusEventType event, void* userdata);

#define SCE_NP_COUNTRY_CODE_LENGTH 2
typedef struct SceNpCountryCode {
    char data[SCE_NP_COUNTRY_CODE_LENGTH];
    char term;
    char padding[1];
} SceNpCountryCode;

typedef struct SceNpAgeRestriction {
    SceNpCountryCode countryCode;
    int8_t age;
    uint8_t padding[3];
} SceNpAgeRestriction;

#define SCE_NP_NO_AGE_RESTRICTION (0)
typedef struct SceNpContentRestriction {
    size_t size;
    int8_t defaultAgeRestriction;
    char padding[3];
    int32_t ageRestrictionCount;
    const SceNpAgeRestriction* ageRestriction;
} SceNpContentRestriction;

#define SCE_NP_ONLINEID_MIN_LENGTH 3
#define SCE_NP_ONLINEID_MAX_LENGTH 16
typedef struct SceNpOnlineId {
    char data[SCE_NP_ONLINEID_MAX_LENGTH];
    char term;
    char dummy[3];
} SceNpOnlineId;

typedef struct SceNpId {
    SceNpOnlineId handle;
    uint8_t opt[8];
    uint8_t reserved[8];
} SceNpId;

typedef enum SceNpState {
    SCE_NP_STATE_UNKNOWN = 0,
    SCE_NP_STATE_SIGNED_OUT,
    SCE_NP_STATE_SIGNED_IN
} SceNpState;

PS4API int _sceNpIpcCreateMemoryFromKernel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceNpIpcCreateMemoryFromPool() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceNpIpcDestroyMemory() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceNpIpcFreeImpl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceNpIpcGetNpMemAllocator() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceNpIpcMallocImpl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceNpIpcReallocImpl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceNpManagerCreateMemoryFromKernel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceNpManagerCreateMemoryFromPool() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceNpManagerDestroyMemory() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceNpManagerFreeImpl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceNpManagerGetNpMemAllocator() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceNpManagerMallocImpl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceNpManagerReallocImpl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10NpOnlineId5ClearEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10NpOnlineIdC2ERKS1_() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10NpOnlineIdC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10NpOnlineIdD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10NpOnlineIdD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np11NpHttpTransD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np11NpHttpTransD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np12NpHttpClientD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np12NpHttpClientD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np13NpAccessTokenC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np13NpAccessTokenD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np13NpAccessTokenD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4User7GetUserEiPS1_() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4UserC2Ei() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4UserC2ERKS1_() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4UserC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4UserD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4UserD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZNK3sce2np4User9GetUserIdEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAsmClientGetNpComInfo2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAsmClientGetNpTitleToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAsmClientGetServiceIdInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAsmClientInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCheckCallback() {
    // UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCheckCallbackForLib() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCheckNpAvailability() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCheckNpAvailabilityA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCheckNpReachability() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCheckPlus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCreateAsyncRequest() {
    LOG_DEBUG("Unimplemented %s()\n", __FUNCTION__);
    return 0;
}
PS4API int sceNpCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpDeleteRequest(int reqId) {
    LOG_DEBUG("reqId: %d", reqId);
    return 0;
}
PS4API int sceNpGetAccountCountry(SceUserServiceUserId userId,
                                  SceNpCountryCode *pCountryCode) {
    LOG_DEBUG("dummy");
    pCountryCode->data[0] = 'r';
    pCountryCode->data[1] = 'u';
    pCountryCode->term = 0;
    return 0;
}
PS4API int sceNpGetAccountCountryA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetAccountDateOfBirth() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetAccountDateOfBirthA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetAccountIdA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetAccountLanguage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetAccountLanguageA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetGamePresenceStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetGamePresenceStatusA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetNpId(SceUserServiceUserId userId, SceNpId* npId) {
    return 0;
}
PS4API int sceNpGetNpReachabilityState() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetOnlineId() {
    LOG_DEBUG("Unimplemented %s()\n", __FUNCTION__);
    return 0;
}
PS4API int sceNpGetParentalControlInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetParentalControlInfoA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetState(SceUserServiceUserId userId,
                         SceNpState *state) {
    LOG_DEBUG("%d, %p", userId, state);
    *state = SCE_NP_STATE_SIGNED_IN;
    return 0;
}
PS4API int sceNpGetUserIdByAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetUserIdByOnlineId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpHasSignedUp() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpIdMapperAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpIdMapperAccountIdToNpId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpIdMapperAccountIdToOnlineId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpIdMapperCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpIdMapperDeleteRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpIdMapperNpIdToAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpIdMapperOnlineIdToAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpInGameMessageAbortHandle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpInGameMessageCreateHandle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpInGameMessageDeleteHandle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpInGameMessageInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpInGameMessagePrepare() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpInGameMessagePrepareA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpInGameMessageSendData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpInGameMessageSendDataA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpInGameMessageTerminate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpIntCheckPlus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntAddPlusMemberTypeCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntBindOfflineAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntCheckGameNpAvailability() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntCheckGameNpAvailabilityA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntClearParentalControlInfoSubAccount() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntClearUsedFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntCreateLoginContext() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntCreateLoginRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntDeleteLoginContext() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntDeleteRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetAccountCountry() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetAccountCountryA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetAccountDateOfBirth() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetAccountDateOfBirthA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetAccountLanguage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetAccountLanguageA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetAccountNpEnv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetAccountType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetActiveSigninState() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetAuthorizationCode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetAuthorizationCodeA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetAuthorizationCodeWithRedirectUri() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetAuthServerErrorFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetClientCredentialAccessToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetGameTitleBanInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetGameTitleToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetGameTitleTokenA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetGameVshToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetLastAccountLanguage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetMAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetNpEnv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetNpId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetNpIdSdk() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetOfflineAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetOnlineIdInternal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetOnlineIdSdk() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetParentalControlFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetParentalControlInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetParentalControlInfoA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetParentalControlInfoNB() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetPlusMemberType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetPlusMemberTypeNB() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetServerError() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetSigninState() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetTicket() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetUserIdByAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetUserIdByOfflineAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetUserIdByOnlineId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetUserList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetUserNum() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetVshToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntGetVshTokenA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntIsServerMaintenanceError() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntIsSubAccount() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntIsTemporarySignout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntIsUnregisteredClientError() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginAddJsonInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginBind() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginCheckSignin() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginGet2svInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginGetAccessToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginGetAccessTokenViaImplicitFlow() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginGetAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginGetAuthorizationCode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginGetDeviceCodeInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginGetEmail() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginGetOnlineId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginGetUserId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginParseJsonUserInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginResetSsoToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginSetAccountInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginSetSsoToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginValidateCredential() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginValidateKratosAuthCode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntLoginVerifyDeviceCode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntPfAuth() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntRemovePlusMemberTypeCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntRevalidatePassword() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntSetTimeout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntSignout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntTemporarySignout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntUnbindOfflineAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntUpdateVshToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntUpdateVshTokenA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerIntWebLoginRequired() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerPrxStartVsh() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerPrxStopVsh() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerUtilConvertJidToNpId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpManagerUtilConvertNpIdToJid() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpNotifyPlusFeature() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpPollAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpPushInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpPushIntBeginInactive() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpPushIntEndInactive() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpPushIntGetConnectionState() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpRegisterGamePresenceCallback() {
    LOG_DEBUG("Unimplemented %s()\n", __FUNCTION__);
    return 0;
}
PS4API int sceNpRegisterGamePresenceCallbackA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpRegisterNpReachabilityStateCallback(SceNpReachabilityStateCallback callback,
                                                    void *userdata) {
    LOG_DEBUG("Unimplemented (%p, %p)\n", callback, userdata);
    return 0;
}
PS4API int sceNpRegisterPlusEventCallback(SceNpPlusEventCallback callback, void* userdata) {
    LOG_DEBUG("Unimplemented (%p, %p)", callback, userdata);
    return 0;
}
PS4API int sceNpRegisterStateCallback() {
    LOG_DEBUG("Unimplemented %s()", __FUNCTION__);
    return 0;
}
PS4API int sceNpRegisterStateCallbackA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpServiceClientInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpServiceClientTerm() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSetContentRestriction(const SceNpContentRestriction* pRestriction) {
    return 0;
}
PS4API int sceNpSetGamePresenceOnline() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSetGamePresenceOnlineA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSetNpTitleId(const SceNpTitleId* titleId, const SceNpTitleSecret* titleSecret) {
    LOG_DEBUG("(%p, %p)", titleId, titleSecret);

    return 0;
}
PS4API int sceNpSetTimeout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUnregisterNpReachabilityStateCallback() {
    LOG_DEBUG("Unimplemented");
    return 0;
}
PS4API int sceNpUnregisterPlusEventCallback() {
    LOG_DEBUG("Unimplemented");
    return 0;
}
PS4API int sceNpUnregisterStateCallback() {
    LOG_DEBUG("Unimplemented");
    return 0;
}
PS4API int sceNpUnregisterStateCallbackA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWaitAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}

// SNS
PS4API int sceNpSnsFacebookAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSnsFacebookCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSnsFacebookDeleteRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSnsFacebookGetAccessToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSnsIntAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSnsIntCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSnsIntDeleteRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSnsIntFbGetGameAccessToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSnsIntFbGetGameAccessTokenAllowed() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSnsIntFbGetSystemAccessToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSnsIntFbGetTitleInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSnsIntLinkedStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSnsIntTest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSnsIntTwGetSystemAccessToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSnsIntYtGetAccessToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSnsIntYtRefreshMasterToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}

PS4API int sceNpIntGetNpTitleIdSecret() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int tpXVNSFwJRs() {
    // UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int UvDQq9_QMuI() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int hKTdrR1_dN0() {
    // UNIMPLEMENTED_FUNC();
    return 0;
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("0yDdrIgqpjE", sceNpManagerIntGetVshTokenA);
    fun("Hhmu86aYI1E", sceNpServiceClientTerm);
    fun("UAA2-ZTmgJc", sceNpManagerIntGetActiveSigninState);
    fun("A2CQ3kgSopQ", sceNpSetContentRestriction);
    fun("uVAfWmv+cc8", sceNpManagerIntGetTicket);
    fun("21FMz6O4B2E", sceNpIdMapperNpIdToAccountId);
    fun("wXfHhmzUjK4", sceNpManagerIntLoginGetOnlineId);
    fun("OZTedKNUeFU", sceNpManagerIntGetUserIdByAccountId);
    fun("38cfkczfN08", sceNpManagerIntLoginGetAccessTokenViaImplicitFlow);
    fun("ilwLM4zOmu4", sceNpGetParentalControlInfo);
    fun("EgmlHG93Tpw", sceNpManagerIntDeleteLoginContext);
    fun("6miba-pcQt8", sceNpManagerIntGetSigninState);
    fun("e6rTjFmcQjY", sceNpManagerIntGetAccountLanguageA);
    fun("HtpGVrVLOlA", sceNpAsmClientGetNpTitleToken);
    fun("BoD6du5+wxo", sceNpManagerIntGetAuthorizationCode);
    fun("Xg7dJekKeHM", sceNpManagerIntCheckGameNpAvailability);
    fun("kUsK6ZtqofM", _ZN3sce2np10NpOnlineIdD0Ev);
    fun("yqsFy9yg2rU", sceNpManagerIntLoginGetEmail);
    fun("sTtvF4QVhjg", sceNpManagerIntGetOnlineIdSdk);
    fun("XS-eY7KRqjQ", sceNpManagerIntGetAccountId);
    fun("X52vXnVvtpE", sceNpManagerIntGetParentalControlInfoNB);
    fun("yWMBHiRdEbk", sceNpManagerIntLoginGetUserId);
    fun("Y+hLqeLseRk", sceNpManagerIntLoginGetAuthorizationCode);
    fun("hw5KNqAAels", sceNpRegisterNpReachabilityStateCallback);
    fun("Oad3rvY-NJQ", sceNpHasSignedUp);
    fun("cRILAEvn+9M", sceNpUnregisterNpReachabilityStateCallback);
    fun("xZk+QcivrFE", sceNpManagerIntCreateRequest);
    fun("TyACAxDH3Uw", _sceNpIpcReallocImpl);
    fun("FqtDOHUuDNw", sceNpManagerIntGetParentalControlFlag);
    fun("uFJpaKNBAj4", sceNpRegisterGamePresenceCallback);
    fun("azEmYv5NqWo", sceNpManagerIntGetNpId);
    fun("v8+25H9WIX4", _sceNpManagerCreateMemoryFromPool);
    fun("e-ZuhGEoeC4", sceNpGetNpReachabilityState);
    fun("eiqMCt9UshI", sceNpCreateAsyncRequest);
    fun("TPMbgIxvog0", sceNpGetAccountLanguageA);
    fun("HkUgFhrpAD4", sceNpManagerIntGetAuthServerErrorFlag);
    fun("KO+11cgC7N0", sceNpSetGamePresenceOnline);
    fun("pHLjntY0psg", _sceNpIpcCreateMemoryFromKernel);
    fun("9P8qV9WtgKA", sceNpManagerPrxStopVsh);
    fun("ossvuXednsc", sceNpManagerIntIsSubAccount);
    fun("bMG3cVmUmuk", sceNpInGameMessageTerminate);
    fun("vrre3KW6OPg", sceNpManagerIntGetPlusMemberType);
    fun("XyvQv2qjUng", sceNpPushIntEndInactive);
    fun("eQH7nWPcAgc", sceNpGetState);
    fun("6n8NT1pHW9g", sceNpManagerIntCreateLoginContext);
    fun("3Zl8BePTh9Y", sceNpCheckCallback);
    fun("bwwspVgS4hQ", _ZN3sce2np4User7GetUserEiPS1_);
    fun("LGEIdgILQek", sceNpManagerPrxStartVsh);
    fun("gBeifc27nO4", _ZN3sce2np10NpOnlineIdC2Ev);
    fun("fHGhS3uP52k", _sceNpManagerCreateMemoryFromKernel);
    fun("6AcoqeEhs6E", sceNpManagerIntWebLoginRequired);
    fun("64D6V-ADQe0", sceNpManagerIntSignout);
    fun("VfRSmPmj8Q8", sceNpRegisterStateCallback);
    fun("zEZvGyjEhuk", sceNpIdMapperOnlineIdToAccountId);
    fun("qQJfO8HAiaY", sceNpRegisterStateCallbackA);
    fun("8Z2Jc5GvGDI", sceNpCheckNpAvailabilityA);
    fun("1S2urF24zNQ", sceNpPushIntBeginInactive);
    fun("-QglDeRr8D8", sceNpSetTimeout);
    fun("M3wFXbYQtAA", sceNpUnregisterStateCallbackA);
    fun("zXukItkUuko", sceNpManagerIntLoginValidateKratosAuthCode);
    fun("cOLn5A3ZoqU", sceNpAsmClientGetNpComInfo2);
    fun("TiC81-OKjpg", sceNpAsmClientGetServiceIdInfo);
    fun("Nhxy2NmQhbs", sceNpManagerIntGetUserIdByOfflineAccountId);
    fun("GImICnh+boA", sceNpRegisterPlusEventCallback);
    fun("Z8nyVQCGCVo", sceNpIdMapperDeleteRequest);
    fun("IPb1hd1wAGc", sceNpGetGamePresenceStatus);
    fun("HvNrMhlWBSk", sceNpManagerIntGetAccountNpEnv);
    fun("yX8qSFmkiyc", sceNpManagerIntClearParentalControlInfoSubAccount);
    fun("jCJEWuExbZg", sceNpManagerIntGetLastAccountLanguage);
    fun("yHl0pPA3rPQ", sceNpManagerIntLoginResetSsoToken);
    fun("ukEeOizCkIU", _sceNpManagerGetNpMemAllocator);
    fun("Oad+nopFTTA", sceNpManagerIntGetMAccountId);
    fun("gPux+0B5N9I", _ZN3sce2np10NpOnlineIdC2ERKS1_);
    fun("hyuye+88uPo", _sceNpIpcDestroyMemory);
    fun("TV3KKXZLUj4", sceNpIdMapperAccountIdToOnlineId);
    fun("97RAfJch+qE", sceNpManagerIntBindOfflineAccountId);
    fun("-QlrD62pWME", _ZN3sce2np10NpOnlineId5ClearEv);
    fun("KfGZg2y73oM", sceNpCheckNpReachability);
    fun("aU5QaUCW-Ik", sceNpManagerIntLoginAddJsonInfo);
    fun("p-o74CnoNzY", sceNpGetNpId);
    fun("V38nfJwXYhg", _sceNpIpcGetNpMemAllocator);
    fun("PZhz+vjp2CM", sceNpManagerIntSetTimeout);
    fun("KZ1Mj9yEGYc", sceNpGetAccountLanguage);
    fun("rbknaUjpqWo", sceNpGetAccountIdA);
    fun("s4UEa5iBJdc", sceNpInGameMessageCreateHandle);
    fun("fjJ4xXM+3Tw", sceNpManagerIntGetAccountCountryA);
    fun("7+uKCMe4SLk", sceNpManagerIntGetAccountCountry);
    fun("JT+t00a3TxA", sceNpGetAccountCountryA);
    fun("XRFchqddEVU", sceNpManagerIntGetPlusMemberTypeNB);
    fun("a8R9-75u4iM", sceNpGetAccountId);
    fun("9Uew6b9Pp8U", _ZN3sce2np12NpHttpClientD2Ev);
    fun("VgYczPGB5ss", sceNpGetUserIdByAccountId);
    fun("1DMXuE0CbGQ", sceNpManagerIntGetAuthorizationCodeA);
    fun("OcnSddPkQns", sceNpManagerIntGetGameTitleBanInfo);
    fun("i2KGykoRA-4", _ZN3sce2np4UserC2Ei);
    fun("XRpM9tQecCU", sceNpManagerIntLoginCheckSignin);
    fun("mKGqVK1SwFk", sceNpManagerIntGetVshToken);
    fun("VY8Xji9cAFA", _sceNpIpcFreeImpl);
    fun("HhKQodH164k", _ZN3sce2np4UserD0Ev);
    fun("etZ84Rf3Urw", sceNpManagerIntGetUserNum);
    fun("ggj9Qm4XDrU", sceNpManagerIntGetParentalControlInfoA);
    fun("NS1sEhoj-B0", sceNpManagerIntGetParentalControlInfo);
    fun("Vh1bhUG6mSs", sceNpInGameMessagePrepare);
    fun("CConkVwc7Dc", sceNpManagerIntGetAccountDateOfBirthA);
    fun("3FtD6y5Rk5Q", sceNpManagerIntUpdateVshToken);
    fun("HneC+SpeLwc", sceNpManagerIntDeleteRequest);
    fun("BTRVfOx7K1c", sceNpManagerIntGetNpEnv);
    fun("JELHf4xPufo", sceNpCheckCallbackForLib);
    fun("UdhQmx64-uM", _sceNpIpcCreateMemoryFromPool);
    fun("O-2TTjhWw10", sceNpPushIntGetConnectionState);
    fun("E6rzFwsDFwE", sceNpManagerIntAddPlusMemberTypeCallback);
    fun("PIYEFT1iG0Y", _sceNpManagerReallocImpl);
    fun("u9tBiSNnvn8", _ZN3sce2np13NpAccessTokenD2Ev);
    fun("TXzpCgPmXEQ", sceNpManagerIntGetClientCredentialAccessToken);
    fun("YvL0D8Vg6VM", _ZN3sce2np4UserC2ERKS1_);
    fun("GpLQDNKICac", sceNpCreateRequest);
    fun("xAdGRA3ucDg", sceNpManagerIntLoginGet2svInfo);
    fun("QZpXoz9wjbE", sceNpManagerIntClearUsedFlag);
    fun("DkN+WBclFps", sceNpPushInit);
    fun("GFhVUpRmbHE", sceNpInGameMessageInitialize);
    fun("PL10NiZ0XNA", sceNpManagerIntGetUserList);
    fun("atgHp5dQi5k", sceNpManagerIntIsTemporarySignout);
    fun("EXeJ80p01gs", sceNpManagerIntLoginGetDeviceCodeInfo);
    fun("oPO9U42YpgI", sceNpGetGamePresenceStatusA);
    fun("dvkqP9KUMfk", sceNpManagerIntLoginGetAccountId);
    fun("IG6ZoGSDaMk", sceNpManagerIntUnbindOfflineAccountId);
    fun("Yd7V7lM4bSA", _ZN3sce2np11NpHttpTransD0Ev);
    fun("F6E4ycq9Dbg", sceNpGetUserIdByOnlineId);
    fun("hmVLIi3pQDE", sceNpManagerIntLoginSetAccountInfo);
    fun("OzKvTvg3ZYU", sceNpAbortRequest);
    fun("BdykpTwq2bs", sceNpInGameMessageAbortHandle);
    fun("2VmnxS1aZG0", sceNpManagerIntGetAuthorizationCodeWithRedirectUri);
    fun("2rsFmlGWleQ", sceNpCheckNpAvailability);
    fun("d0IkWV+u25g", sceNpManagerIntPfAuth);
    fun("uqcPJLWL08M", sceNpPollAsync);
    fun("p0TfCdPEcsk", _sceNpManagerMallocImpl);
    fun("Ec63y59l9tw", sceNpSetNpTitleId);
    fun("C6xstRBFOio", sceNpManagerIntGetAccountLanguage);
    fun("GsWjzRU7AWA", sceNpIntCheckPlus);
    fun("S7QTn72PrDw", sceNpDeleteRequest);
    fun("FjbLZy95ts4", _ZN3sce2np12NpHttpClientD0Ev);
    fun("isNn0YyU83c", sceNpManagerIntCheckGameNpAvailabilityA);
    fun("dnyvPTam4Gc", sceNpManagerIntGetAccountDateOfBirth);
    fun("m9L3O6yst-U", sceNpGetParentalControlInfoA);
    fun("Gaxrp3EWY-M", sceNpNotifyPlusFeature);
    fun("SFZYbH7eOnk", _ZN3sce2np13NpAccessTokenD0Ev);
    fun("mjjTXh+NHWY", sceNpUnregisterStateCallback);
    fun("CdQg39qlfgY", sceNpManagerIntCreateLoginRequest);
    fun("wUT4cOK0bj0", sceNpManagerIntTemporarySignout);
    fun("4uhgVNAqiag", _sceNpManagerDestroyMemory);
    fun("xViqJdDgKl0", sceNpUnregisterPlusEventCallback);
    fun("ujtFwWJnv+E", sceNpManagerIntLoginVerifyDeviceCode);
    fun("O7ivIf9AIFI", sceNpManagerIntGetGameTitleTokenA);
    fun("70Swvw7h6ck", sceNpManagerIntGetOfflineAccountId);
    fun("k4M1w5Xstck", sceNpManagerIntRevalidatePassword);
    fun("iUjiTIiYnZk", sceNpManagerUtilConvertJidToNpId);
    fun("koU-Duc1F-0", sceNpManagerIntIsServerMaintenanceError);
    fun("qmZHHehEDog", sceNpManagerIntLoginValidateCredential);
    fun("btKQfNe1jBY", sceNpManagerIntGetGameTitleToken);
    fun("q3M7XzBKC3s", sceNpGetAccountDateOfBirthA);
    fun("KswxLxk4c1Y", sceNpRegisterGamePresenceCallbackA);
    fun("itBuc3IIDaY", _ZN3sce2np4UserD2Ev);
    fun("wZy5M6lzip0", sceNpAsmClientInitialize);
    fun("X-WHexCbxcI", sceNpManagerIntLoginSetSsoToken);
    fun("2lzWy2xmnhY", _ZN3sce2np13NpAccessTokenC2Ev);
    fun("8JX-S2ADen4", _sceNpManagerFreeImpl);
    fun("D5qJmwMlccI", _ZN3sce2np11NpHttpTransD2Ev);
    fun("VBZtcFn7+aY", _sceNpIpcMallocImpl);
    fun("td8GJFROaEA", _ZNK3sce2np4User9GetUserIdEv);
    fun("YcMKsqoMBtg", _ZN3sce2np10NpOnlineIdD2Ev);
    fun("jyi5p9XWUSs", sceNpWaitAsync);
    fun("uSLgWz8ohak", sceNpManagerIntGetUserIdByOnlineId);
    fun("ON7Sf5XEMmI", sceNpInGameMessageSendData);
    fun("9lz4fkS+eEk", sceNpManagerIntGetAccountType);
    fun("iDlso2ZrQfA", sceNpManagerIntGetServerError);
    fun("41CVMRinjWU", sceNpManagerIntGetNpIdSdk);
    fun("vfHBP2-WXcM", sceNpManagerIntGetGameVshToken);
    fun("PafRf+sxnwA", sceNpManagerIntRemovePlusMemberTypeCallback);
    fun("hIGX-h1cgvA", sceNpManagerUtilConvertNpIdToJid);
    fun("Ghz9iWDUtC4", sceNpGetAccountCountry);
    fun("+anuSx2avHQ", sceNpInGameMessageDeleteHandle);
    fun("jkQKWQTOu8g", sceNpManagerIntGetOnlineIdInternal);
    fun("jwOjEhWD6E4", sceNpManagerIntIsUnregisteredClientError);
    fun("C0gNCiRIi4U", sceNpSetGamePresenceOnlineA);
    fun("r6MyYJkryz8", sceNpCheckPlus);
    fun("IkL62FMpIpo", sceNpInGameMessagePrepareA);
    fun("JHOtNtQ-jmw", sceNpServiceClientInit);
    fun("uaCfG0TAPmg", sceNpManagerIntLoginParseJsonUserInfo);
    fun("8kM+eFzoBas", sceNpManagerIntUpdateVshTokenA);
    fun("-P0LG2EUFBE", sceNpManagerIntLoginGetAccessToken);
    fun("AUuzKQIwhXY", sceNpManagerIntAbortRequest);
    fun("fJuQuipzW10", sceNpIdMapperAbortRequest);
    fun("bzf8a7LxtCQ", sceNpManagerIntLoginBind);
    fun("PQDFxcnqxtw", sceNpInGameMessageSendDataA);
    fun("F-AkFa9cABI", _ZN3sce2np4UserC2Ev);
    fun("XDncXQIJUSk", sceNpGetOnlineId);
    fun("alNLle2vACg", sceNpIdMapperAccountIdToNpId);
    fun("8VBTeRf1ZwI", sceNpGetAccountDateOfBirth);
    fun("lCAYAK4kfkc", sceNpIdMapperCreateRequest);

    // SNS
    fun("uPj-CXHNEFE", sceNpSnsIntAbortRequest);
    fun("-LJbF9b-Who", sceNpSnsFacebookAbortRequest);
    fun("pTvAKV1iQkE", sceNpSnsFacebookDeleteRequest);
    fun("QW7NonbfeFk", sceNpSnsIntLinkedStatus);
    fun("zSR6kEWpr0c", sceNpSnsIntTest);
    fun("Wp+C91igTkE", sceNpSnsIntTwGetSystemAccessToken);
    fun("0xrhvJ8QANU", sceNpSnsIntYtGetAccessToken);
    fun("e6K3LE8qXsc", sceNpSnsIntFbGetSystemAccessToken);
    fun("uINq9QxFYqU", sceNpSnsIntFbGetGameAccessTokenAllowed);
    fun("VtyS8XLBqNE", sceNpSnsFacebookGetAccessToken);
    fun("qLqzbBxATrU", sceNpSnsIntDeleteRequest);
    fun("Q+mSQ2U6wWY", sceNpSnsIntYtRefreshMasterToken);
    fun("PDD7gmqbnKE", sceNpSnsIntFbGetTitleInfo);
    fun("OoTjfxl8-wI", sceNpSnsFacebookCreateRequest);
    fun("5zCW8dx4mKk", sceNpSnsIntFbGetGameAccessToken);
    fun("T+F4GKuY3oE", sceNpSnsIntCreateRequest);
    fun("LtYqw9M23hw", sceNpIntGetNpTitleIdSecret);
    fun("tpXVNSFwJRs", tpXVNSFwJRs);
    fun("UvDQq9+QMuI", UvDQq9_QMuI);
    fun("hKTdrR1+dN0", hKTdrR1_dN0);
#undef fun
}
} // namespace libSceNpManager
