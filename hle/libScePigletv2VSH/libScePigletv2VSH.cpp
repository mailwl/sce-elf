#include "libScePigletv2VSH.h"

namespace libScePigletv2VSH {

PS4API int _OrbisTextureImage2DCanvas() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglBindAPI() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglBindTexImage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglChooseConfig() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglCopyBuffers() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglCreateContext() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglCreatePbufferFromClientBuffer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglCreatePbufferSurface() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglCreatePixmapSurface() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglCreateWindowSurface() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglDestroyContext() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglDestroySurface() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglGetConfigAttrib() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglGetConfigs() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglGetCurrentContext() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglGetCurrentDisplay() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglGetCurrentSurface() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglGetDisplay() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglGetError() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglGetProcAddress() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglMakeCurrent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglQueryAPI() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglQueryContext() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglQueryString() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglQuerySurface() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglReleaseTexImage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglReleaseThread() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglSurfaceAttrib() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglSwapBuffers() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglSwapInterval() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglTerminate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglWaitClient() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglWaitGL() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int eglWaitNative() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glActiveTexture() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glAttachShader() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBeginQuery() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBeginQueryEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBeginTransformFeedback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBindAttribLocation() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBindBuffer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBindBufferBase() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBindBufferRange() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBindFramebuffer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBindRenderbuffer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBindSampler() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBindTexture() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBindTransformFeedback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBindVertexArray() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBlendColor() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBlendEquation() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBlendEquationSeparate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBlendFunc() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBlendFuncSeparate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBlitFramebuffer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBufferData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glBufferSubData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glCheckFramebufferStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glClear() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glClearBufferfi() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glClearBufferfv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glClearBufferiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glClearBufferuiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glClearColor() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glClearDepthf() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glClearStencil() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glClientWaitSync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glColorMask() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glCompileShader() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glCompressedTexImage2D() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glCompressedTexImage3D() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glCompressedTexSubImage2D() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glCompressedTexSubImage3D() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glCopyBufferSubData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glCopyTexImage2D() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glCopyTexSubImage2D() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glCopyTexSubImage3D() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glCreateProgram() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glCreateShader() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glCullFace() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDeleteBuffers() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDeleteFramebuffers() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDeleteProgram() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDeleteQueries() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDeleteQueriesEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDeleteRenderbuffers() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDeleteSamplers() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDeleteShader() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDeleteSync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDeleteTextures() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDeleteTransformFeedbacks() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDeleteVertexArrays() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDepthFunc() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDepthMask() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDepthRangef() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDetachShader() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDisable() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDisableVertexAttribArray() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDrawArrays() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDrawArraysInstanced() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDrawArraysInstancedEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDrawBuffers() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDrawElements() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDrawElementsInstanced() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDrawElementsInstancedEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glDrawRangeElements() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glEnable() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glEnableVertexAttribArray() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glEndQuery() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glEndQueryEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glEndTransformFeedback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glFenceSync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glFinish() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glFlush() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glFlushMappedBufferRange() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glFramebufferRenderbuffer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glFramebufferTexture2D() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glFramebufferTextureLayer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glFrontFace() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGenBuffers() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGenerateMipmap() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGenFramebuffers() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGenQueries() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGenQueriesEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGenRenderbuffers() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGenSamplers() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGenTextures() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGenTransformFeedbacks() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGenVertexArrays() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetActiveAttrib() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetActiveUniform() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetActiveUniformBlockiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetActiveUniformBlockName() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetActiveUniformsiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetAttachedShaders() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetAttribLocation() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetBooleanv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetBufferParameteri64v() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetBufferParameteriv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetBufferPointerv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetError() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetFloatv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetFragDataLocation() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetFramebufferAttachmentParameteriv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetInteger64i_v() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetInteger64v() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetIntegeri_v() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetIntegerv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetInternalformativ() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetProgramBinary() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetProgramInfoLog() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetProgramiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetQueryiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetQueryObjecti64vEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetQueryObjectivEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetQueryObjectui64vEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetQueryObjectuiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetQueryObjectuivEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetRenderbufferParameteriv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetSamplerParameterfv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetSamplerParameteriv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetShaderInfoLog() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetShaderiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetShaderPrecisionFormat() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetShaderSource() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetString() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetStringi() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetSynciv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetTexParameterfv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetTexParameteriv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetTransformFeedbackVarying() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetUniformBlockIndex() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetUniformfv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetUniformIndices() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetUniformiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetUniformLocation() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetUniformuiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetVertexAttribfv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetVertexAttribIiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetVertexAttribIuiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetVertexAttribiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glGetVertexAttribPointerv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glHint() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glInsertEventMarkerColorSCE() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glInsertEventMarkerEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glInvalidateFramebuffer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glInvalidateSubFramebuffer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glIsBuffer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glIsEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glIsFramebuffer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glIsProgram() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glIsQuery() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glIsQueryEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glIsRenderbuffer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glIsSampler() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glIsShader() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glIsSync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glIsTexture() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glIsTransformFeedback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glIsVertexArray() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glLineWidth() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glLinkProgram() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glMapBufferRange() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glPauseTransformFeedback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glPigletGetShaderBinarySCE() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glPixelStorei() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glPolygonOffset() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glPopGroupMarkerEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glProgramBinary() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glProgramParameteri() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glPushGroupMarkerColorSCE() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glPushGroupMarkerEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glQueryCounterEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glReadBuffer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glReadPixels() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glReleaseShaderCompiler() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glRenderbufferStorage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glRenderbufferStorageMultisample() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glResumeTransformFeedback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glSampleCoverage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glSamplerParameterf() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glSamplerParameterfv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glSamplerParameteri() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glSamplerParameteriv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glScissor() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glShaderBinary() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glShaderSource() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glStencilFunc() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glStencilFuncSeparate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glStencilMask() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glStencilMaskSeparate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glStencilOp() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glStencilOpSeparate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glTexImage2D() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glTexImage3D() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glTexParameterf() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glTexParameterfv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glTexParameteri() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glTexParameteriv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glTexStorage2D() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glTexStorage2DEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glTexStorage3D() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glTexSubImage2D() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glTexSubImage3D() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glTextureStorage2DEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glTransformFeedbackVaryings() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform1f() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform1fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform1i() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform1iv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform1ui() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform1uiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform2f() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform2fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform2i() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform2iv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform2ui() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform2uiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform3f() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform3fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform3i() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform3iv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform3ui() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform3uiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform4f() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform4fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform4i() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform4iv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform4ui() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniform4uiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniformBlockBinding() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniformMatrix2fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniformMatrix2x3fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniformMatrix2x4fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniformMatrix3fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniformMatrix3x2fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniformMatrix3x4fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniformMatrix4fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniformMatrix4x2fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUniformMatrix4x3fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUnmapBuffer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glUseProgram() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glValidateProgram() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttrib1f() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttrib1fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttrib2f() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttrib2fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttrib3f() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttrib3fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttrib4f() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttrib4fv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttribDivisor() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttribDivisorEXT() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttribI4i() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttribI4iv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttribI4ui() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttribI4uiv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttribIPointer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glVertexAttribPointer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glViewport() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int glWaitSync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePigletAllocateSystemMemoryEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePigletAllocateVideoMemoryEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePigletGetConfigurationVSH() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePigletGetInteger() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePigletGetShaderCacheConfiguration() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePigletGetUsageVSH() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePigletReleaseSystemMemoryEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePigletReleaseVideoMemoryEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePigletSetConfigurationVSH() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePigletSetShaderCacheConfiguration() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("d38bTmxILUQ", glDeleteQueriesEXT);
    fun("-HAzeV7vL-Q", glCheckFramebufferStatus);
    fun("Xe-vzCJeV3Y", glDrawElements);
    fun("oXkQEVitkCs", scePigletAllocateSystemMemoryEx);
    fun("mwSvDt0dHF8", eglQueryString);
    fun("WIGwUmVdp+Y", glGetQueryObjectui64vEXT);
    fun("1ELoOr5IBlM", glEndTransformFeedback);
    fun("H8Dp8lwh8QQ", glUniform4f);
    fun("idWUOMHlXf0", glViewport);
    fun("KZ0xIC-Ev2c", glCompressedTexImage2D);
    fun("WS+7Pxb5gNg", glUniform4iv);
    fun("XS1XH14cemc", glDrawArraysInstanced);
    fun("Yk0ofesYP1U", glVertexAttribPointer);
    fun("VXxylRE34KU", glPauseTransformFeedback);
    fun("KDPOldWSEtc", glGetActiveUniformBlockName);
    fun("LGiUQAdAxTQ", glTexStorage2D);
    fun("ON26vhraMMU", glLinkProgram);
    fun("3DtWT550eGM", glUniformMatrix2x3fv);
    fun("CqW1XfJDqNs", glGetQueryObjectivEXT);
    fun("PdWicphhWgg", glFenceSync);
    fun("rD1uDD2rEso", glUniformMatrix4x3fv);
    fun("5dfVNhCZFxE", glGetTransformFeedbackVarying);
    fun("7w09iXWXQZc", glUniformMatrix3fv);
    fun("yFJY7Te6ZRQ", glShaderBinary);
    fun("f8f0+hYyOb0", glBindVertexArray);
    fun("En8ghzCM0HM", glBlendFuncSeparate);
    fun("B-mNueVFgAw", glDepthRangef);
    fun("YWItHEulIAI", glUniform4i);
    fun("gTD5beqJ81U", glGetIntegeri_v);
    fun("I+2QnmhN3xc", glInvalidateFramebuffer);
    fun("En+dYUM8u7U", glDeleteVertexArrays);
    fun("HFIXBmlQmXI", glBlendEquation);
    fun("pwl29nuNnqI", glDisable);
    fun("nXwKrwF-wUM", scePigletSetShaderCacheConfiguration);
    fun("Untg89NcROM", glClearBufferiv);
    fun("y20mWUHGzj8", glActiveTexture);
    fun("4e-JjxDUlb8", glGetInteger64v);
    fun("vSCc5IpzQ-w", glVertexAttrib1f);
    fun("xf-XRFu56-8", eglWaitGL);
    fun("Pu5Yd9+FY9Y", eglGetError);
    fun("vWkaB9NZpGA", glBindFramebuffer);
    fun("CWnGG3DOIt8", glDeleteRenderbuffers);
    fun("+caGEFeVZd8", glInvalidateSubFramebuffer);
    fun("w2aPcrilqmg", glInsertEventMarkerEXT);
    fun("MnlqH3VAJxU", glCopyTexSubImage2D);
    fun("b-YdE60Z8tw", glUniformMatrix3x4fv);
    fun("KOJ4+zzhpAg", glClear);
    fun("xg5Kn1Zto4k", eglGetConfigs);
    fun("I9IXZ61WkD0", glBindTexture);
    fun("wGn8sX26Fds", glFlushMappedBufferRange);
    fun("lY7qdFGr0go", glBindAttribLocation);
    fun("r-OiMbqJuEQ", glGetSamplerParameteriv);
    fun("Kzbvd9G7ieE", glTexParameteri);
    fun("GpgGEmVl9Ck", eglQueryAPI);
    fun("5I0ZSvxhRmc", glDrawElementsInstanced);
    fun("7Nm1o42CbJ8", eglGetCurrentContext);
    fun("Pkhe5Qcq++0", glClearColor);
    fun("cYSjErZW5gE", scePigletGetShaderCacheConfiguration);
    fun("fbiTO9gjGSE", glSamplerParameterf);
    fun("U6+U2AYUe7Y", glEndQueryEXT);
    fun("CAypDWjJScU", glResumeTransformFeedback);
    fun("+mwnHP7Lou8", glGenTransformFeedbacks);
    fun("yyJshE4eV3U", glCopyTexImage2D);
    fun("DqJyaSqqTng", glUniform1iv);
    fun("tZ1lckB5o2A", glDeleteTextures);
    fun("ehXUKUtch8M", glTexParameterfv);
    fun("jQlQ4IR95YU", glTransformFeedbackVaryings);
    fun("hR59aDWzHPw", glUniform1i);
    fun("go0QBZdmPKk", glGetQueryObjectuivEXT);
    fun("rhmQ142bvvU", glVertexAttribI4uiv);
    fun("yyAMU7O65qY", glGetVertexAttribfv);
    fun("0cZxCTXvyPs", eglSurfaceAttrib);
    fun("l1PatT2jaok", glCopyBufferSubData);
    fun("Os1rxlZYt-Y", glGetActiveUniformsiv);
    fun("aOKKRhOYzyU", glUniform4ui);
    fun("3Ns42AD8UGI", glDrawElementsInstancedEXT);
    fun("9TB8RLYXg+g", eglDestroySurface);
    fun("fVpuh+UJ4Qo", glIsRenderbuffer);
    fun("68Xne9xAk7s", glReadBuffer);
    fun("dI1Y-XIAEqU", glDeleteSamplers);
    fun("wGe2stW-CTw", glGetFragDataLocation);
    fun("vdV5zDWJ9Q8", glUniform3iv);
    fun("beY9ZJv5-dU", glGetAttribLocation);
    fun("h0slGrL6pBs", glGenQueries);
    fun("5O92iRi6Hk4", eglGetConfigAttrib);
    fun("3yQipX6ypKM", glBlendEquationSeparate);
    fun("YdGQOl6bp50", glInsertEventMarkerColorSCE);
    fun("tPHxFS+55pY", glReadPixels);
    fun("6CQ7RFmBjnQ", eglQueryContext);
    fun("3QC1YQvFxEw", glPopGroupMarkerEXT);
    fun("ub4yceOLlbQ", glVertexAttrib4f);
    fun("Ok3Hu4DWmpU", glGetUniformIndices);
    fun("xiH4YdjKEYc", glTexStorage2DEXT);
    fun("5a4nrywbfZc", glBindTransformFeedback);
    fun("v3Qv1I3P7x4", glBindBufferRange);
    fun("Yo4iw4JYCTI", glGetShaderInfoLog);
    fun("nd-xdRd4+BU", glIsBuffer);
    fun("4vHxgXZ9HV8", glUniform1uiv);
    fun("M27ezF1iANs", glBlitFramebuffer);
    fun("wA0LYGRV2KQ", glEndQuery);
    fun("UMeDmpCpF40", glGetUniformLocation);
    fun("sDotUt1uEhA", scePigletAllocateVideoMemoryEx);
    fun("4Pd54jvwVhY", glFrontFace);
    fun("ey9N5U-C0hw", glBlendColor);
    fun("zyTMmUiR2tQ", glGetQueryObjecti64vEXT);
    fun("camy5cvmZII", glDrawRangeElements);
    fun("DRwo4pgCHKk", glGetShaderPrecisionFormat);
    fun("x8npgO9yaNE", glGetVertexAttribIiv);
    fun("1h8m1KuIR9o", eglSwapInterval);
    fun("Cu07x4fIYO0", glIsEnabled);
    fun("1hulS01us-4", glIsProgram);
    fun("GGIG9nj2zXk", eglCreatePbufferSurface);
    fun("g-C5shOvH1Y", glClearBufferfv);
    fun("sfRsq9Yq2Y8", glGetShaderiv);
    fun("9h3QG13-rQQ", glSamplerParameteriv);
    fun("zyebgmQUnWY", glCompileShader);
    fun("13Y3oJ7jRHQ", eglGetCurrentDisplay);
    fun("MyyhVXryovc", glGetUniformBlockIndex);
    fun("CWOI5J2vrQo", glGetSamplerParameterfv);
    fun("fo7GYqkkHBA", glPushGroupMarkerEXT);
    fun("p8hjICuQP74", glIsTexture);
    fun("xMlxiPxwrAw", glVertexAttribI4i);
    fun("52+a3D15aw4", glTexImage2D);
    fun("51YpII41z34", eglSwapBuffers);
    fun("4MR1gw+ffTs", glUniformMatrix3x2fv);
    fun("6u98-pOEZ7A", scePigletGetUsageVSH);
    fun("Gfzd1R86ep8", glGetFloatv);
    fun("oaqISo2f6rk", glUniformMatrix2fv);
    fun("Cyw+pgg2YcI", glVertexAttribIPointer);
    fun("2PuCkkEw9eE", glShaderSource);
    fun("5HIn4jxKwu8", glIsQueryEXT);
    fun("OOI5nqLihBw", glGetStringi);
    fun("0TyJ6AxRs5w", glCreateShader);
    fun("s5sv2KhvlxE", glClearDepthf);
    fun("n79wW64CUh8", glVertexAttribI4ui);
    fun("ZggLwqNsGIk", glGenRenderbuffers);
    fun("2w3bo8DF4vM", glBeginTransformFeedback);
    fun("D0n924iO0Yw", glGetBufferPointerv);
    fun("rP9VyAtSJ08", eglCopyBuffers);
    fun("rSW2hZt1Oo4", glVertexAttribDivisor);
    fun("Vov9fE6FD1c", glGetTexParameteriv);
    fun("9TZg6EG5ncM", glReleaseShaderCompiler);
    fun("J06J7hH+9bs", glClearBufferfi);
    fun("FyhlwO6-+sM", glDepthFunc);
    fun("LJ9eQ5Z7Wmo", glPolygonOffset);
    fun("zJ8P2q3ZflM", glTexImage3D);
    fun("Dn1NY1xs7Zw", glUniform1fv);
    fun("nmUsbuL3Rc0", glGenerateMipmap);
    fun("RqcWVDCXZcY", eglCreatePbufferFromClientBuffer);
    fun("Bc3rL5ELD-A", glGetAttachedShaders);
    fun("EdefUaKYEPE", eglBindTexImage);
    fun("g-XVEn-57y8", glIsShader);
    fun("Ads7t69vHBk", glIsQuery);
    fun("KGXoebAph80", glVertexAttribDivisorEXT);
    fun("3xsE3RDNnQw", glDeleteFramebuffers);
    fun("jCksK8VIzyE", glTexSubImage3D);
    fun("TgsW7eXEdpA", glGetBufferParameteriv);
    fun("RJtXkpCVfmA", glDetachShader);
    fun("jGo2URP56tE", glStencilMask);
    fun("qU9JeLiphww", glVertexAttrib3fv);
    fun("SOMZ080N0wA", glIsSampler);
    fun("q0iQ6QttHjQ", glCreateProgram);
    fun("v8-KyLvjWKw", glUniformMatrix4fv);
    fun("UkvXd2jt++I", glBeginQueryEXT);
    fun("xqbiS7PMrSE", glGetString);
    fun("ffv6I7TFVP4", glGetVertexAttribPointerv);
    fun("lGJTnZqchvA", glBindRenderbuffer);
    fun("5T6TphmcgVQ", glGetBufferParameteri64v);
    fun("TIdn+yGbJlo", glEnable);
    fun("NHxpE45ygfQ", glFramebufferTextureLayer);
    fun("Dm9kHKVv-GI", glUniform4uiv);
    fun("t8HI9sZ4TSM", glStencilOp);
    fun("j1qEC6CZgpQ", glGetSynciv);
    fun("LP5RvgCb3AA", eglGetCurrentSurface);
    fun("SR9U4-+eibc", eglQuerySurface);
    fun("C3vniWm8dNw", glGetUniformuiv);
    fun("zmymoDNsdKM", glUniform3i);
    fun("APjMbicny1Q", glFinish);
    fun("1p5uDHGLvd4", glGetProgramBinary);
    fun("326-26W1Ht4", glFramebufferTexture2D);
    fun("e5HOpV5feq8", glUniform2f);
    fun("hZarPUiZevM", glStencilFunc);
    fun("VPgPm-9j054", glFlush);
    fun("xaQHq4LK5zQ", glPushGroupMarkerColorSCE);
    fun("B62IAjtQimk", _OrbisTextureImage2DCanvas);
    fun("mTcFOuWRrhw", glStencilOpSeparate);
    fun("56f34U0xe8c", glGetInternalformativ);
    fun("4p8IUn5CM0I", glGenBuffers);
    fun("qC8BIIGtfNk", glUniform2i);
    fun("oOigDJBsN2c", glDeleteProgram);
    fun("ADsMyXKjHVo", glUseProgram);
    fun("nl8lQxwAceM", glBindBuffer);
    fun("mr4YjdpfVFg", glProgramBinary);
    fun("nMoFUeBjzrU", glLineWidth);
    fun("D947DWfj9tY", eglMakeCurrent);
    fun("UFb4eVjw-Sw", glBindBufferBase);
    fun("VBNYE1MPf4I", glIsTransformFeedback);
    fun("BpP68iXiK5I", glDisableVertexAttribArray);
    fun("bUxXBzkgul0", glCullFace);
    fun("eelf17HNGa4", glDeleteSync);
    fun("1hAznJNzbI8", glUnmapBuffer);
    fun("U6zCblLII4w", glUniform2fv);
    fun("Q6OKS0BRf0A", eglWaitClient);
    fun("hKN5OJIT5ko", glBufferData);
    fun("tY7yQOR9-8s", glGetActiveUniform);
    fun("rJWnDO35mR8", glGetQueryObjectuiv);
    fun("19N0joauSsE", glDrawArrays);
    fun("yCo3HzKQSts", eglBindAPI);
    fun("T-+xooU3VvQ", scePigletReleaseSystemMemoryEx);
    fun("OR6-CZL+oOw", glGenTextures);
    fun("0kYe0bK58zk", glProgramParameteri);
    fun("uVoPGNUyeZU", glGetProgramInfoLog);
    fun("tcVQcBMJato", eglCreatePixmapSurface);
    fun("fesoNJCZpSA", eglChooseConfig);
    fun("SCdKl+0EnuQ", glHint);
    fun("XQZkyJMxI18", glColorMask);
    fun("6e64qI81ak8", glTexParameteriv);
    fun("-y-tRnUpuVE", glTexStorage3D);
    fun("uVdONJ4yNrc", glAttachShader);
    fun("3DK2dxDedi0", glTexSubImage2D);
    fun("S7VZWmI1u04", glIsFramebuffer);
    fun("tqnmnBCbNZI", glVertexAttrib2fv);
    fun("-CyF1XyqmaQ", glGetIntegerv);
    fun("2m41tMRyPpc", glIsSync);
    fun("1Tw-GwQJvug", glStencilMaskSeparate);
    fun("1dtq0xFWSNU", glTexParameterf);
    fun("Yymje0BT12Y", glValidateProgram);
    fun("igpSrATXmAY", glCompressedTexSubImage3D);
    fun("Ig0Ts6ju8rQ", glVertexAttrib4fv);
    fun("POq8+5-inHY", glUniform1f);
    fun("R3grMrGyJho", glGetInteger64i_v);
    fun("piWdZNlOTLo", glCopyTexSubImage3D);
    fun("QBcAfwrr094", glClearStencil);
    fun("2jKVW2uBSw8", glRenderbufferStorage);
    fun("B3wg5t9bCdk", glGenSamplers);
    fun("aN1B2MP3kLU", glDepthMask);
    fun("gAT2BtxphDM", glSamplerParameterfv);
    fun("LFLRtqqi4AQ", glSampleCoverage);
    fun("XZB2-9dSghM", glCompressedTexSubImage2D);
    fun("qSbpBon7f2s", glUniformBlockBinding);
    fun("4hwL0vHb1Jw", glUniform3fv);
    fun("GNZpcrnC7uE", glPixelStorei);
    fun("vrBLBaiUr5g", eglDestroyContext);
    fun("r5IsX1dPwUk", glDeleteShader);
    fun("-4nCkJB-Wpk", glUniform2ui);
    fun("sw2+TzZR71g", glGetFramebufferAttachmentParameteriv);
    fun("6z5ySLhOFQE", glStencilFuncSeparate);
    fun("Ab0R4oOE-DY", glWaitSync);
    fun("PWEUl-4tXqg", glPigletGetShaderBinarySCE);
    fun("RUbJ1HDWEHs", glUniform3uiv);
    fun("yiOOLA2vkLA", glGetQueryiv);
    fun("sA3IshgN8u8", glEnableVertexAttribArray);
    fun("gaFx7Axnsuo", glGetVertexAttribiv);
    fun("Z7pNCsK2dAs", eglGetDisplay);
    fun("XkZ7aJ4mICw", glUniform1ui);
    fun("OHKoGVZ7xKA", glDeleteQueries);
    fun("UKSkPc4YLt8", glClientWaitSync);
    fun("ArYdZGEUW8w", glGetUniformiv);
    fun("V6fVkEahkLM", glMapBufferRange);
    fun("GmP2ZgUoTPo", glScissor);
    fun("F35Uj-GMte0", glDrawArraysInstancedEXT);
    fun("5CL3pzRNzJ4", glClearBufferuiv);
    fun("KJmeNzmA6UM", eglInitialize);
    fun("jzjrS8gGNdY", glGetRenderbufferParameteriv);
    fun("YjT8GrH5bsM", glFramebufferRenderbuffer);
    fun("39BK09X7zLY", glBlendFunc);
    fun("I82nyOgGKjY", glUniform3f);
    fun("PSqt1KLK7o0", eglCreateWindowSurface);
    fun("q3nWOTJHIM4", glGetUniformfv);
    fun("4M5OQvD2uE8", glRenderbufferStorageMultisample);
    fun("oWhbxCJiBMM", scePigletGetInteger);
    fun("9rH9n8530IU", glBufferSubData);
    fun("tZSQ+VFbsxE", glUniform4fv);
    fun("M9RtXpjSYtE", scePigletSetConfigurationVSH);
    fun("G2U3S-VCv-c", glIsVertexArray);
    fun("1Ml5nNkeImU", eglGetProcAddress);
    fun("AoKQpye80kU", glCompressedTexImage3D);
    fun("rP1Z5VoDsWw", glUniform3ui);
    fun("3gtNzvkq-XY", scePigletGetConfigurationVSH);
    fun("2-+fJU7sdx8", glGetShaderSource);
    fun("AcruVcmKz78", scePigletReleaseVideoMemoryEx);
    fun("UEiVB1s+wBw", eglReleaseTexImage);
    fun("6II7YIgxbSU", glUniformMatrix4x2fv);
    fun("GrLInwUuV2k", glDrawBuffers);
    fun("uyjjXGqy-Ek", glGetError);
    fun("RWc9kPMyEYU", glQueryCounterEXT);
    fun("-+8sSDc61uU", glGetActiveUniformBlockiv);
    fun("ConU-nXswn8", eglTerminate);
    fun("9G2LYp3FlE4", glBindSampler);
    fun("m-7cQfah6KY", glGetProgramiv);
    fun("CNx0ylkWoCg", glBeginQuery);
    fun("6OfeCf-u4bg", glGenFramebuffers);
    fun("Ai5yrljUSjI", glGenVertexArrays);
    fun("kF-yS5OChO8", glGetActiveAttrib);
    fun("0oZKK6QmW-k", eglCreateContext);
    fun("AsueLb+3C4k", glSamplerParameteri);
    fun("mPN01lKHg0g", glGetTexParameterfv);
    fun("uUjwGu7aZuU", glDeleteTransformFeedbacks);
    fun("LVLwSxJwwnw", glGetVertexAttribIuiv);
    fun("kOZTCBZCk4g", glUniformMatrix2x4fv);
    fun("R+lZMziFVTQ", glGenQueriesEXT);
    fun("hGmiwXj3cKU", eglWaitNative);
    fun("SOZzgxqVHIY", glVertexAttrib3f);
    fun("GE+2k2MFilI", glUniform2uiv);
    fun("JEt20zWYCIE", glVertexAttrib2f);
    fun("rTb6vWn9eL0", glTextureStorage2DEXT);
    fun("mNDMTLdRYR8", eglReleaseThread);
    fun("VWq4YfFcCz0", glVertexAttribI4iv);
    fun("416vjcKYsfo", glUniform2iv);
    fun("syWd-JCitpo", glVertexAttrib1fv);
    fun("mPRFA5CE48g", glDeleteBuffers);
    fun("tm+Ktm7aelA", glGetBooleanv);
#undef fun
}
}
