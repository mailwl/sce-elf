#include <cstdio>
#include <cstring>
#include <ctime>
#include <map>
#include <fcntl.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <pthread.h>
#include <time.h>

#include <signal.h>

#if defined(_WIN64)
#include <cassert>
#include <ws2tcpip.h>
#include <windows.h>
#include <io.h>

// TODO: right implementation!
#ifndef mmap
void* mmap(void* start, size_t length, int prot, int flags, int fd, off_t offset) {
    (void)start;
    (void)prot;
    (void)flags;
    auto const h_mapping{
        ::CreateFileMappingW(INVALID_HANDLE_VALUE, nullptr, PAGE_READWRITE | SEC_COMMIT, 0u, length, nullptr)};
    auto const p_view{::MapViewOfFile(h_mapping, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, length)};
    return p_view;
}
#endif

#ifndef sigset_t
#ifndef _sigset_t
typedef unsigned long long _sigset_t;
#endif
typedef _sigset_t sigset_t;
#endif

#ifndef sigfillset
#define sigfillset(set) (*(set) = ~(sigset_t)0, 0)
#endif

#ifndef SIG_BLOCK
#define SIG_BLOCK 1
#endif

#ifndef SIG_UNBLOCK
#define SIG_UNBLOCK 2
#endif

#ifndef SIG_SETMASK
#define SIG_SETMASK 2
#endif

#ifndef sigprocmask
int sigprocmask(int how, const sigset_t* set, sigset_t* old_set) {
    return 0;
}
#endif

#ifndef msync
int msync(void* start, size_t length, int flags) {
    return 0;
}
#endif

#ifndef getpagesize
int getpagesize() {
    return 16 * 1024;
}
#endif

#ifndef useconds_t 
typedef unsigned int useconds_t;
#endif

#ifndef getpid
#include <process.h>
#define getpid _getpid
#endif

#ifndef getrimeofday
#include <chrono>

int gettimeofday(struct timeval* tp, struct timezone* tzp) {
    namespace sc = std::chrono;
    sc::system_clock::duration d = sc::system_clock::now().time_since_epoch();
    sc::seconds s = sc::duration_cast<sc::seconds>(d);
    tp->tv_sec = s.count();
    tp->tv_usec = sc::duration_cast<sc::microseconds>(d - s).count();

    return 0;
}

#endif // !getrimeofday



#ifndef clockid_t 
typedef int clockid_t;
#endif

//#ifndef clock_gettime
//#define CLOCK_REALTIME 1
//#define CLOCK_MONOTONIC 2
//#define CLOCK_PROCESS_CPUTIME_ID 3
////struct timespec { uint64_t tv_sec; uint64_t tv_nsec; };    //header part
//int clock_gettime(int, struct timespec *spec)      //C-file part
//{
//    __int64 wintime; GetSystemTimeAsFileTime((FILETIME*)&wintime);
//    wintime -= 116444736000000000ll;  //1jan1601 to 1jan1970
//    spec->tv_sec = wintime / 10000000ll;           //seconds
//    spec->tv_nsec = wintime % 10000000ll * 100;      //nano-seconds
//    return 0;
//}
//#endif


#ifndef _usleep
int _usleep(__int64 usec) {
    HANDLE timer;
    LARGE_INTEGER ft;

    ft.QuadPart = -(10 * usec); // Convert to 100 nanosecond interval, negative value indicates relative time

    timer = CreateWaitableTimer(NULL, TRUE, NULL);
    SetWaitableTimer(timer, &ft, 0, NULL, NULL, 0);
    WaitForSingleObject(timer, INFINITE);
    CloseHandle(timer);
    return 0;
}
#endif

#ifndef pthread_mutexattr_setprotocol
int pthread_mutexattr_setprotocol(pthread_mutexattr_t*, int) {
    return 0;
}
#endif

#else
#include <errno.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>
#endif

#include "kernel/ipmi.h"
#include "kernel/memory.h"
#include "libkernel.h"
#include "libkernel_pthreads.h"
#include "process/process.h"

static bool MUTEX_LOG = false;
static bool FS_LOG = true;


namespace libkernel {

int error_code;
uint64_t __stack_chk_guard{1};
char __progname[255]{"eboot.bin"};

#ifndef mode_t
typedef int mode_t;
#endif

typedef mode_t SceKernelMode;

typedef sched_param SceKernelSchedParam;

typedef int32_t SceKernelModule;

template <typename PS4, typename Native>
struct proxy {
public:
    static bool add(PS4 m, Native* pm) {
        auto v = map.find(m);
        if (v == map.end()) {
            map[m] = pm;
            return true;
        }
        return false;
    }
    static Native* get(PS4 m) {
        auto v = map.find(m);
        if (v == map.end()) {
            return nullptr;
        }
        return v->second;
    }
    static bool free(PS4 m) {
        auto v = map.find(m);
        if (v == map.end()) {
            return false;
        }
        ::delete v->second;
        map.erase(m);
        ::delete m;
        return true;
    }

private:
    inline static std::map<PS4, Native*> map = {};
};

typedef uint64_t SceKernelCpumask;

typedef struct _SceKernelSema {
    uint64_t pad[2]{};
    sem_t sem{};
} * SceKernelSema;

typedef struct _SceKernelSemaOptParam {
    uint64_t pad[2];
} SceKernelSemaOptParam;

typedef uint64_t suseconds_t;
struct sce_timeval {
    time_t tv_sec;
    suseconds_t tv_usec;
};
typedef struct sce_timeval SceKernelTimeval;

enum SceKernelClockid { SCE_KERNEL_CLOCK_REALTIME = 0, SCE_KERNEL_CLOCK_MONOTONIC = 4 };

typedef struct SceKernelLoadModuleOpt {
    size_t size;
} SceKernelLoadModuleOpt;

typedef struct _SceKernelEventFlagOptParam {
    size_t size;
} SceKernelEventFlagOptParam;

typedef struct _SceKernelEventFlag {
    char name[64]{};
    uint64_t initPattern{};
    uint32_t attr{};
    // pthread_mutex_t mutex{};
} * SceKernelEventFlag;

typedef struct {
    void* start;
    void* end;
    off_t offset;
    int protection;
    int memoryType;
    unsigned isFlexibleMemory : 1;
    unsigned isDirectMemory : 1;
    unsigned isStack : 1;
    unsigned isPooledMemory : 1;
    unsigned isCommitted : 1;
    char name[32];
} SceKernelVirtualQueryInfo;

PS4API void sceKernelDebugRaiseException(uint32_t error_code, uint32_t p1) {

    LOG_DEBUG("%s(0x%x, 0x%x)", __FUNCTION__, error_code, p1);
}
PS4API void sceKernelDebugRaiseExceptionOnReleaseMode(uint32_t p0, uint32_t p1) {
    LOG_DEBUG("%s(0x%x, %x)", __FUNCTION__, p0, p1);
    //::exit(0);
}
PS4API void* __tls_get_addr(const uint64_t* slot) {
    LOG_DEBUG("(%llu)", *slot);
    void* rc = process::get_tls(*slot);
    return rc;
}
//
PS4API void __stack_chk_fail() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceKernelConvertUtcToLocaltime() {
    LOG_DEBUG("unimplemented %s()", __FUNCTION__);
    int rc = 0;
    return rc;
}
PS4API int scePthreadCondInit(ScePthreadCond* cond, const ScePthreadCondattr* attr, const char* name) {
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p, %p, '%s')", __FUNCTION__, cond, attr, name);
    int rc = ::pthread_cond_init(cond, attr);
    return rc;
}
PS4API int scePthreadCondDestroy(ScePthreadCond* cond) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, cond);
    int rc = ::pthread_cond_destroy(cond);
    return rc;
}
PS4API int scePthreadCondWait(ScePthreadCond* cond, ScePthreadMutex* mutex) {
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p, %p)", __FUNCTION__, cond, mutex);
    pthread_mutex_t* m = (*mutex)->pthread_mutex;
    int rc = ::pthread_cond_wait(cond, m);
    return rc;
}
PS4API int scePthreadCondTimedwait(ScePthreadCond* cond, ScePthreadMutex* mutex, SceKernelUseconds usec) {
    const struct timespec t { usec / 1000, 0 };
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p, %p, %lu)", __FUNCTION__, cond, mutex, usec);
    pthread_mutex_t* m = (*mutex)->pthread_mutex;
    int rc = ::pthread_cond_timedwait(cond, m, &t);
    return rc;
}
PS4API int scePthreadCondSignal(ScePthreadCond* cond) {
    int rc = ::pthread_cond_signal(cond);
    LOG_DEBUG("%p", cond);
    return rc;
}
PS4API int scePthreadCondBroadcast(ScePthreadCond* cond) {
    int rc = ::pthread_cond_broadcast(cond);
    LOG_DEBUG("%s(%p) = %d", __FUNCTION__, cond, rc);
    return rc;
}

struct thread_data_t {
    PS4API void* (*start_routine)(void*){};
    void* arg{};
};
void* thread_routine(void* params) {
    const auto thread_data = (thread_data_t*)params;
    void* arg = thread_data->arg;
    PS4API void* (*start_routine)(void*) = thread_data->start_routine;
    delete thread_data;
    return start_routine(arg);
}

PS4API int scePthreadCreate(ScePthread* thread, /*const*/ ScePthreadAttr* attr, PS4API void* (*start_routine)(void*),
                            void* arg, const char* name) {
    LOG_DEBUG("%s(%p, %p, %p, %p, '%s')", __FUNCTION__, thread, attr, start_routine, arg, name);
#if defined(_WIN64) || defined(__CYGWIN__)
    auto thread_data = new thread_data_t;
    thread_data->start_routine = start_routine;
    thread_data->arg = arg;
    auto a = attr ? proxy<ScePthreadAttr, pthread_attr_t>::get(*attr) : nullptr;
    int rc = ::pthread_create(thread, a, thread_routine, thread_data);
    ::pthread_setname_np(*thread, name);
#else

    auto a = attr ? proxy<ScePthreadAttr, pthread_attr_t>::get(*attr) : nullptr;
    int rc = ::pthread_create(thread, a, start_routine, arg);
#endif
    return rc;
}
PS4API int scePthreadJoin(ScePthread thread, void** value_ptr) {
    LOG_DEBUG("%s(%p, %p)", __FUNCTION__, thread, value_ptr);
    int rc = ::pthread_join(thread, value_ptr);
    return rc;
}
PS4API int scePthreadDetach(ScePthread thread) {
    LOG_DEBUG("STUB %s()", __FUNCTION__);
    int rc = ::pthread_detach(thread);
    return rc;
}
PS4API void scePthreadExit(void* value_ptr) {
    LOG_DEBUG("STUB %s(%p)", __FUNCTION__, value_ptr);
    ::pthread_exit(value_ptr);
//    while(true) {
//        ::_sleep(10000);
//    }
}
PS4API void nanosleep() {
    UNIMPLEMENTED_FUNC();
}
PS4API void __pthread_cxa_finalize() {
    UNIMPLEMENTED_FUNC();
}

PS4API int* sce__error() {
    // printf("STUB %s() = %d", __FUNCTION__, errno);
    return &errno;
}
PS4API void scePthreadYield() {
    LOG_DEBUG("%s()", __FUNCTION__);
#if defined(_WIN64) || defined(__CYGWIN__)
    ::_sleep(0);
#elif defined(__APPLE__)
    ::pthread_yield_np();
#else
    ::pthread_yield();
#endif
}
PS4API int scePthreadEqual(ScePthread thread1, ScePthread thread2) {
    int rc = ::pthread_equal(thread1, thread2);
    LOG_DEBUG("(%p, %p) = %d", thread1, thread2, rc);
    return rc;
}
PS4API pthread_t scePthreadSelf() {
    if (MUTEX_LOG)
        LOG_DEBUG("%s()", __FUNCTION__);
    pthread_t rc = ::pthread_self();
    return rc;
}
PS4API int scePthreadMutexattrInit(ScePthreadMutexattr* attr) {
    if (MUTEX_LOG)
        LOG_DEBUG("scePthreadMutexattrInit(%p)", attr);
    *attr = ::new _ScePthreadMutexattr; // (_ScePthreadMutexattr*)::malloc(sizeof(_ScePthreadMutexattr));
    int rc = ::pthread_mutexattr_init(&(*attr)->mutex_attr);
    return rc;
}
PS4API ssize_t _write(int fd, char* buf, int count) {
#if defined(_WIN64)
    ssize_t rc = ::_write(fd, buf, count);
#else
    ssize_t rc = ::write(fd, buf, count);
#endif
    buf[count - 1] = 0;
    LOG_DEBUG("%s(%d, '%s', %d)", __FUNCTION__, fd, buf, count);
    return rc;
}
PS4API int scePthreadMutexattrSettype(ScePthreadMutexattr* attr, MutexType type) {
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p, %d)", __FUNCTION__, attr, type);
    int t = convert_mutex_type(type);
    int rc = ::pthread_mutexattr_settype(&(*attr)->mutex_attr, t);
    return 0;
}
PS4API int32_t scePthreadMutexattrDestroy(ScePthreadMutexattr* attr) {
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p)", __FUNCTION__, attr);
    ::delete (*attr); //::free(*attr);
    return 0;
}
PS4API int scePthreadMutexInit(ScePthreadMutex* mutex, /*const*/ ScePthreadMutexattr* attr, const char* name) {
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p, %p, \"%s\")", __FUNCTION__, mutex, attr, name);
    auto m = ::new _ScePthreadMutex; // (_ScePthreadMutex*)::malloc(sizeof(_ScePthreadMutex));
    *mutex = m;
    if (name) {
        ::strncpy((*mutex)->name, name, 0x20);
    }
    (*mutex)->pthread_mutex = ::new pthread_mutex_t; //(pthread_mutex_t*)::malloc( sizeof(pthread_mutex_t));
    pthread_mutexattr_t* a = nullptr;
    if (attr) {
        a = &(*attr)->mutex_attr;
    }
    int rc = ::pthread_mutex_init((*mutex)->pthread_mutex, a);
    return rc;
}
PS4API int scePthreadMutexLock(ScePthreadMutex* mutex) {
    if (*mutex == nullptr) {
        return 0;
    }
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p, '%s')", __FUNCTION__, mutex, (*mutex)->name);
    pthread_mutex_t* m = (*mutex)->pthread_mutex;
    if (m == nullptr) {
        return 0; // TODO find out why unknown mutex
    }
    int rc = ::pthread_mutex_lock(m);
    return rc;
}
PS4API int scePthreadMutexUnlock(ScePthreadMutex* mutex) {
    if (*mutex == 0) {
        return 0;
    }
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p)", __FUNCTION__, mutex);
    pthread_mutex_t* m = (*mutex)->pthread_mutex;
    if (m == nullptr) {
        return 0;
    }
    int rc = ::pthread_mutex_unlock(m);
    return rc; // TODO: find why error
}
PS4API int scePthreadMutexDestroy(ScePthreadMutex* mutex) {
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p)", __FUNCTION__, mutex);
    pthread_mutex_t* m = (*mutex)->pthread_mutex;
    int rc = ::pthread_mutex_destroy(m);
    ::delete ((*mutex)->pthread_mutex); //::free((*mutex)->pthread_mutex);
    ::delete (*mutex);                  //::free(*mutex);
    return rc;
}
PS4API int scePthreadMutexTrylock(ScePthreadMutex* mutex) {
    pthread_mutex_t* m = (*mutex)->pthread_mutex;
    int rc = ::pthread_mutex_trylock(m);
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p) = %d", __FUNCTION__, mutex, rc);
    return rc;
}
PS4API int scePthreadOnce(pthread_once_t* o, void (*func)()) {
    int rc = ::pthread_once(o, func);
    LOG_DEBUG("%p, %p", o, func);
    return rc;
}
PS4API int scePthreadKeyCreate(pthread_key_t* key, PS4API void (*dest)(void*)) {
#if defined(_WIN64) || defined(__CYGWIN__)
    (void)dest;
    int rc = ::pthread_key_create(key, nullptr); // TODO: right impl for windows
#else
    int rc = ::pthread_key_create(key, dest);
#endif

    return rc;
}
PS4API void scePthreadKeyDelete() {
    UNIMPLEMENTED_FUNC();
}

PS4API int scePthreadSetspecific(pthread_key_t key, const void* value) {
    LOG_DEBUG("%s()", __FUNCTION__);
    int rc = ::pthread_setspecific(key, value);
    return rc;
}

PS4API void* scePthreadGetspecific(pthread_key_t key) {
    LOG_DEBUG("%s()", __FUNCTION__);
    void* rc = ::pthread_getspecific(key);
    return rc;
}

PS4API int gettimeofday(struct timeval* tv, void* tzp) {
    (void)tzp;
    LOG_DEBUG("%p, %p", tv, tzp);
    int rc = ::gettimeofday(tv, nullptr); // tzp);
    return rc;
}
PS4API void sceKernelConvertLocaltimeToUtc() {
    UNIMPLEMENTED_FUNC();
}
PS4API void signal() {
    UNIMPLEMENTED_FUNC();
}

PS4API void* sceKernelGetProcParam(uint64_t p1, uint64_t p2) {
    void* rc = process::get_proc_param();
    LOG_DEBUG("STUB %s(%lx, %lx) = %p", __FUNCTION__, p1, p2, rc);
    return rc;
}
PS4API void _sceKernelRtldSetApplicationHeapAPI(void* heap_api) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, heap_api);
}
PS4API
void sceKernelPrintBacktraceWithModuleInfo() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceKernelMunmap(void* addr, size_t len) {
    LOG_DEBUG("%p, %zu", addr, len);
    return 0; // TODO: implement this! memory leak
}
PS4API int sceKernelMapNamedFlexibleMemory(void** addrInOut, size_t len, int prot, int flags, const char* name) {
    void* direct = nullptr;
    direct = ::malloc(len);
    *addrInOut = (void*)direct; // TODO: alignment
    return 0;
}
PS4API int sceKernelMlock() {
    // LOG_DEBUG("STUB %s()", __FUNCTION__);
    return 0;
}
PS4API int _read(int _FileHandle, void* _DstBuf, unsigned int _MaxCharCount) {
    LOG_DEBUG("STUB %s(%d, %p, %u)", __FUNCTION__, _FileHandle, _DstBuf, _MaxCharCount);
#if defined(_WIN64)
    int rc = ::_read(_FileHandle, _DstBuf, _MaxCharCount);
#else
    int rc = ::read(_FileHandle, _DstBuf, _MaxCharCount);
#endif
    return rc;
}
PS4API off_t sceKernelLseek(int fildes, off_t offset, int whence) {
#if defined(_WIN64)
    off_t rc = ::_lseek(fildes, offset, whence);
#else
    off_t rc = ::lseek(fildes, offset, whence);
#endif
    if (FS_LOG)
        LOG_DEBUG("%s(%d, %x, %d) = 0x%x", __FUNCTION__, fildes, offset, whence, rc);
    return rc;
}
PS4API int getrusage() {
    LOG_DEBUG("STUB %s()", __FUNCTION__);
    return 0;
}

PS4API int clock_gettime(int clk_id, struct sce_timespec* tp) {
    clockid_t id = CLOCK_REALTIME;
    switch (clk_id) {
    case 0:
        id = CLOCK_REALTIME;
        break;
    case 1:
        id = CLOCK_REALTIME; // virtual
        break;
    case 2:
        id = CLOCK_REALTIME; // prof
        break;
    case 4:
        id = CLOCK_MONOTONIC;
        break;
    case 15:
        id = CLOCK_PROCESS_CPUTIME_ID;
    default:
        break;
    }
    struct timespec ts;
    int rc = ::clock_gettime(id, &ts);
    std::memset(tp, 0, sizeof(*tp));
    tp->tv_sec = ts.tv_sec;
    tp->tv_nsec = ts.tv_nsec;

    LOG_DEBUG("(%d, sec=0x%x, nsec=0x%x) = %d", clk_id, tp->tv_sec, tp->tv_nsec, rc);
    return rc;
}

PS4API uint64_t sceKernelGetProcessTime() {
    sce_timespec ts{};
    clock_gettime(15, &ts);

    return 1000000LL * ts.tv_sec + ts.tv_nsec / 1000;
}

PS4API int sceKernelGetModuleInfoFromAddr(void* addr, int unk, void* out) {
    LOG_DEBUG("Unimplemented %s(%p, %d, %p)", __FUNCTION__, addr, unk, out);
    return -1;
}
PS4API void __elf_phdr_match_addr() {
    UNIMPLEMENTED_FUNC();
}
PS4API ssize_t sceKernelRead(int d, void* buf, size_t nbytes) {

#if defined(_WIN64)
    auto rc = ::_read(d, buf, nbytes);
#else
    ssize_t rc = ::read(d, buf, nbytes);
#endif
    if (FS_LOG)
        LOG_DEBUG("%s(%d, %p, %zx) = 0x%zx", __FUNCTION__, d, buf, nbytes, rc);
    return rc;
}
PS4API ssize_t sceKernelWrite(int fd, const char* buf, size_t size) {
#if defined(_WIN64)
    ssize_t rc = ::_write(fd, buf, size);
#else
    ssize_t rc = ::write(fd, buf, size);
#endif
    ((char*)buf)[size - 1] = 0;
    LOG_DEBUG("%s(%d, '%s', %zu)", __FUNCTION__, fd, (const char*)buf, size);
    return rc;
}
PS4API void sceKernelGettimezone() {
    UNIMPLEMENTED_FUNC();
}
PS4API void _exit(int code) {
    LOG_DEBUG("%s(0x%x)", __FUNCTION__, code);
    process::free_images();
    ::_exit(code);
}
PS4API int _sigprocmask(int how, const sigset_t* set, sigset_t* old_set) {
    LOG_DEBUG("%s(%d, %p, %p)", __FUNCTION__, how, set, old_set);
    return ::sigprocmask(how, set, old_set);
}
PS4API pthread_t pthread_self() {
    pthread_t rc = ::pthread_self();
    LOG_DEBUG("");
    return rc;
}
PS4API int pthread_create(ScePthread* thread, /*const*/ ScePthreadAttr* attr, PS4API void* (*start_routine)(void*),
                          void* arg, const char* name) {
    LOG_DEBUG("%s(%p, %p, %p, %p, '%s')", __FUNCTION__, thread, attr, start_routine, arg, name);
#if defined(_WIN64) || defined(__CYGWIN__)
    auto thread_data = new thread_data_t;
    thread_data->start_routine = start_routine;
    thread_data->arg = arg;
    int rc = ::pthread_create(thread, attr ? proxy<ScePthreadAttr, pthread_attr_t>::get(*attr) : nullptr,
                              thread_routine, thread_data);
#else
    int rc = ::pthread_create(thread, attr ? proxy<ScePthreadAttr, pthread_attr_t>::get(*attr) : nullptr, start_routine,
                              arg);
#endif
    return rc;
}
PS4API int pthread_attr_setdetachstate(ScePthreadAttr* attr, int state) {
    pthread_attr_t* a = proxy<ScePthreadAttr, pthread_attr_t>::get(*attr);
    int st = (state == 0) ? PTHREAD_CREATE_JOINABLE : PTHREAD_CREATE_DETACHED;
    int rc = ::pthread_attr_setdetachstate(a, st);
    LOG_DEBUG("%s(%p, %d) = %d", __FUNCTION__, attr, state, rc);
    return rc; // TODO find out why error 22
}
PS4API int pthread_attr_init(ScePthreadAttr* attr) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, attr);
    auto a = ::new pthread_attr_t();
    *attr = ::new _ScePthreadAttr();
    int rc = ::pthread_attr_init(a);
    proxy<ScePthreadAttr, pthread_attr_t>::add(*attr, a);
    return rc;
}
PS4API void pthread_join_() {
    UNIMPLEMENTED_FUNC();
}
PS4API void pthread_cond_signal() {
    UNIMPLEMENTED_FUNC();
}

PS4API int sceKernelClockGettime(SceKernelClockid clockId, SceKernelTimespec* tp) {
    LOG_DEBUG("%d, %p", clockId, tp);
    timespec ts{};
    clockid_t id = CLOCK_REALTIME;
    if (clockId == SCE_KERNEL_CLOCK_MONOTONIC) {
        id = CLOCK_MONOTONIC;
    }
    int rc = ::clock_gettime(id, &ts);
    tp->tv_sec = ts.tv_sec;
    tp->tv_nsec = ts.tv_nsec;
    LOG_DEBUG("%d, %p", clockId, tp);
    return rc;
}

PS4API int sceKernelGettimeofday(SceKernelTimeval* tp) {
    struct timeval tv {};
    int rc = ::gettimeofday(&tv, nullptr);
    tp->tv_sec = tv.tv_sec;
    tp->tv_usec = tv.tv_usec;
    LOG_DEBUG("%p", tp);
    return rc;
}
PS4API int sceKernelOpen(const char* path, int flags, SceKernelMode mode) {

    char full_path[0x100]{};
    process::get_resource_path(path, full_path);
#if defined(_WIN64)
    int rc = 0;
    if (flags & 0x20000) {
        // directory
        rc = ::_open(full_path, flags & 0x7, mode);
    } else if (flags & 0x0200) {
        rc = ::_creat(full_path, 0);
    } else {
        flags |= _O_BINARY;
        rc = ::_open(full_path, flags);
    }
    LOG_DEBUG("('%s', %x, %x) = %d", full_path, flags, mode, rc);
#else
    int rc = ::open(full_path, flags, mode);
    LOG_DEBUG("%s('%s', %x, %x) = %d", __FUNCTION__, path, flags, mode, rc);
#endif

    return rc;
}

PS4API ssize_t sceKernelPread(int d, void* buf, size_t nbytes, off_t offset) {
    LOG_DEBUG("%s(%d, %p, %lu, %d)", __FUNCTION__, d, buf, nbytes, offset);
#if defined(_WIN64)
    ::_lseek(d, offset, SEEK_SET);
    ssize_t rc = ::_read(d, buf, nbytes);
#else
    ssize_t rc = ::pread(d, buf, nbytes, offset);
#endif
    return rc;
}

PS4API ssize_t sceKernelPwrite(int d, const void *buf, size_t nbytes, off_t offset) {
    UNIMPLEMENTED_FUNC();
    return 0; // TODO: implement this!
}
PS4API int sceKernelClose(int d) {
    LOG_DEBUG("%s(%d)", __FUNCTION__, d);
#if defined(_WIN64)
    int rc = 0;
    ::_close(d);
#else
    int rc = ::close(d);
#endif
    return rc;
}

PS4API int sceKernelFstat(int fd, SceKernelStat* sb) {
    LOG_DEBUG("(%d, %p)", fd, sb);
    struct stat st {};
    int rc = ::fstat(fd, &st);
    ::memset(sb, 0, sizeof(SceKernelStat));
    sb->st_dev = st.st_dev;
    sb->st_ino = st.st_ino;
    sb->st_nlink = st.st_nlink;
    sb->st_rdev = st.st_rdev;
    sb->st_uid = st.st_uid;
    sb->st_gid = st.st_gid;
    sb->st_size = st.st_size;
    sb->st_mode = st.st_mode;
    // sb->st_atim = st.st_atime;
    // sb->st_mtim = st.st_mtime;
    // sb->st_ctim = st.st_ctime;
    return rc;
}

PS4API void sceKernelFsync() {
    UNIMPLEMENTED_FUNC();
}

PS4API int sceKernelStat(const char* path, SceKernelStat* sb) {
    struct stat st {};
    char full_path[0x100]{};
    process::get_resource_path(path, full_path);
    int rc = ::stat(full_path, &st);
    ::memset(sb, 0, sizeof(SceKernelStat));
    sb->st_dev = st.st_dev;
    sb->st_ino = st.st_ino;
    sb->st_nlink = st.st_nlink;
    sb->st_rdev = st.st_rdev;
    sb->st_uid = st.st_uid;
    sb->st_gid = st.st_gid;
    sb->st_size = st.st_size;
    sb->st_mode = st.st_mode;
    // sb->st_atim = st.st_atime;
    // sb->st_mtim = st.st_mtime;
    // sb->st_ctim = st.st_ctime;
    LOG_DEBUG("'%s', %p", path, sb);
    return rc;
}

PS4API void sceKernelGetdirentries() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceKernelMkdir(const char *path, SceKernelMode mode) {
    LOG_DEBUG("'%s', %x", path, mode);
    char path_name[0x100];
    process::get_resource_path(path, path_name);
    int rc = 0; //::mkdir(path_name);

    return rc;

}
PS4API void sceKernelRmdir() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceKernelUnlink(const char* name) {
    LOG_DEBUG("%s('%s')", __FUNCTION__, name);
    char full_path[0x100]{};
    process::get_resource_path(name, full_path);
    int rc = ::unlink(full_path);
    return rc;
}
PS4API void sceKernelTruncate() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceKernelFtruncate() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceKernelRename() {
    UNIMPLEMENTED_FUNC();
}
PS4API int scePthreadMutexattrSetprotocol(ScePthreadMutexattr* attr, int protocol) {
#ifndef PTHREAD_PRIO_INHERIT
#define PTHREAD_PRIO_INHERIT 1
#endif
#ifndef PTHREAD_PRIO_PROTECT
#define PTHREAD_PRIO_PROTECT 2
#endif
    if (protocol == 1)
        protocol = PTHREAD_PRIO_INHERIT;
    if (protocol == 2)
        protocol = PTHREAD_PRIO_PROTECT;
    int rc = ::pthread_mutexattr_setprotocol(&(*attr)->mutex_attr, protocol);
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p, %d) = %d", __FUNCTION__, attr, protocol, rc);
    return 0;
}
PS4API int scePthreadRwlockInit(ScePthreadRwlock* rwlock, const ScePthreadRwlockattr* attr, const char* name) {
    int rc = ::pthread_rwlock_init(rwlock, attr);
    LOG_DEBUG("%s(%p, %p, '%s') = %d", __FUNCTION__, rwlock, attr, name, rc);
    return rc;
}
PS4API int scePthreadRwlockDestroy(ScePthreadRwlock* rwlock) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, rwlock);
    int rc = ::pthread_rwlock_destroy(rwlock);
    return rc;
}
PS4API int scePthreadRwlockRdlock(ScePthreadRwlock* rwlock) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, rwlock);
    int rc = ::pthread_rwlock_rdlock(rwlock);
    return rc;
}
PS4API void scePthreadRwlockTryrdlock() {
    UNIMPLEMENTED_FUNC();
}
PS4API int scePthreadRwlockUnlock(ScePthreadRwlock* rwlock) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, rwlock);
    int rc = ::pthread_rwlock_unlock(rwlock);
    return rc;
}
PS4API int scePthreadRwlockWrlock(ScePthreadRwlock* rwlock) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, rwlock);
    int rc = ::pthread_rwlock_wrlock(rwlock);
    return rc;
}

PS4API void scePthreadRwlockTrywrlock() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceKernelUsleep(SceKernelUseconds microseconds) {
    // LOG_DEBUG("%s(%u)", __FUNCTION__, microseconds);
#if defined(_WIN64)
    ::_usleep(microseconds);
#else
    ::usleep(microseconds);
#endif
    return 0;
}
PS4API int scePthreadAttrInit(ScePthreadAttr* attr) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, attr);
    auto a = ::new pthread_attr_t();
    *attr = ::new _ScePthreadAttr();
    int rc = ::pthread_attr_init(a);
    proxy<ScePthreadAttr, pthread_attr_t>::add(*attr, a);
    return rc;
}
PS4API int scePthreadAttrSetdetachstate(ScePthreadAttr* attr, int state) {
    LOG_DEBUG("%s(%p, %d)", __FUNCTION__, attr, state);
    pthread_attr_t* a = proxy<ScePthreadAttr, pthread_attr_t>::get(*attr);
    int st = (state == 0) ? PTHREAD_CREATE_JOINABLE : PTHREAD_CREATE_DETACHED;
    int rc = ::pthread_attr_setdetachstate(a, st);
    return rc; // TODO find out why error 22
}
PS4API int scePthreadAttrSetinheritsched(ScePthreadAttr* attr, int shed) {
    LOG_DEBUG("%s(%p, %d)", __FUNCTION__, attr, shed);
    pthread_attr_t* a = proxy<ScePthreadAttr, pthread_attr_t>::get(*attr);
    int rc = ::pthread_attr_setinheritsched(a, shed);
    return rc;
}
PS4API int scePthreadAttrSetschedparam(ScePthreadAttr* attr, const SceKernelSchedParam* shed) {
    LOG_DEBUG("%s(%p, %p)", __FUNCTION__, attr, shed);
    pthread_attr_t* a = proxy<ScePthreadAttr, pthread_attr_t>::get(*attr);
    int rc = ::pthread_attr_setschedparam(a, shed);
    return rc;
}
PS4API int scePthreadAttrSetaffinity(ScePthreadAttr* pattr, const SceKernelCpumask mask) {
    LOG_DEBUG("%p, %x", pattr, mask);
    return 0;
}
PS4API int scePthreadAttrSetstacksize(ScePthreadAttr* attr, size_t stackSize) {
    pthread_attr_t* a = proxy<ScePthreadAttr, pthread_attr_t>::get(*attr);
    int rc = ::pthread_attr_setstacksize(a, stackSize);
    LOG_DEBUG("%p, %zu", attr, stackSize);
    return rc;
}
PS4API int scePthreadAttrDestroy(ScePthreadAttr* attr) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, attr);
    pthread_attr_t* a = proxy<ScePthreadAttr, pthread_attr_t>::get(*attr);
    int rc = ::pthread_attr_destroy(a);
    proxy<ScePthreadAttr, pthread_attr_t>::free(*attr);
    return rc;
}
PS4API int pthread_attr_destroy(ScePthreadAttr* attr) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, attr);
    pthread_attr_t* a = proxy<ScePthreadAttr, pthread_attr_t>::get(*attr);
    int rc = ::pthread_attr_destroy(a);
    proxy<ScePthreadAttr, pthread_attr_t>::free(*attr);
    return rc;
}

PS4API int scePthreadSetprio(ScePthread thread, int prio) {
    LOG_DEBUG("unimplemented %s(%p, %d)", __FUNCTION__, thread, prio);
    int rc = 0;
    return rc;
}
PS4API int scePthreadGetprio(ScePthread* thread, int* prio) {
    LOG_DEBUG("unimplemented %s(%p, %p)", __FUNCTION__, thread, prio);
    *prio = 0;
    return 0;
}
PS4API int scePthreadSetaffinity(ScePthread thread, const SceKernelCpumask mask) {
    LOG_DEBUG("unimplemented %s(%p, %d)", __FUNCTION__, thread, mask);
    int rc = 0;
    return rc;
}
PS4API int scePthreadGetaffinity(ScePthread thread, SceKernelCpumask* mask) {
    LOG_DEBUG("%p, %p", thread, mask);
    *mask = 127;
    return 0;
}
PS4API void sched_yield() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sysctl() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API void* mmap(void* start, size_t length, int prot, int flags, int fd, sceoff_t offset) {

#if defined(_WIN64)
    if (fd == -1) {
        return ::malloc(length);
    }
#endif
    int fl = flags & 0xF;
#if !defined(_WIN64)
    if (flags & 0x1000) {
        fl |= MAP_ANON;
    }
#endif
    auto rc = ::mmap(start, length, prot, fl, fd, offset);
    LOG_DEBUG("%s(%p, 0x%lx, 0x%x, 0x%x, %d, %ld) = %p", __FUNCTION__, start, length, prot, flags, fd, offset, rc);
    int n = errno;

    return rc;
}
PS4API int fstat(int fd, SceKernelStat* sb) {
    LOG_DEBUG("(%d, %p)", fd, sb);
    struct stat st{};
    int rc = ::fstat(fd, &st);
    ::memset(sb, 0, sizeof(SceKernelStat));
    sb->st_dev = st.st_dev;
    sb->st_ino = st.st_ino;
    sb->st_nlink = st.st_nlink;
    sb->st_rdev = st.st_rdev;
    sb->st_uid = st.st_uid;
    sb->st_gid = st.st_gid;
    sb->st_size = st.st_size;
    sb->st_mode = st.st_mode;
    // sb->st_atim = st.st_atime;
    // sb->st_mtim = st.st_mtime;
    // sb->st_ctim = st.st_ctime;
    return rc;
}

PS4API void munmap() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sce_sigfillset(sigset_t* set) {
    int rc = sigfillset(set);
    LOG_DEBUG("%s(%p) = %d", __FUNCTION__, set, rc);
    return rc;
}
PS4API int sigprocmask(int how, const sigset_t* set, sigset_t* old_set) {
    int rc = ::sigprocmask(how, set, old_set);
    LOG_DEBUG("%s(%d, %p, %p) = %d", __FUNCTION__, how, set, old_set, rc);
    return rc;
}

PS4API int msync(void* start, size_t length, int flags) {
    int rc = ::msync(start, length, flags);
    LOG_DEBUG("%s(%p, %llu, %x) = %d", __FUNCTION__, start, length, flags, rc);
    return rc;
}
PS4API int getpid() {
    int rc = ::getpid(); // TODO: windows _getpid
    LOG_DEBUG("() = %d", rc);
    return rc;
}
PS4API int _is_signal_return(uint64_t unk) {
    LOG_DEBUG("%s(0x%llx)", __FUNCTION__, unk);
    return 1;
}
PS4API void sigreturn() {
    UNIMPLEMENTED_FUNC();
}
PS4API void __Ux86_64_setcontext() {
    UNIMPLEMENTED_FUNC();
}
PS4API int getpagesize() {
    int rc = ::getpagesize();
    LOG_DEBUG("%s() = 0x%x", __FUNCTION__, rc);
    return rc;
}
PS4API void sceKernelAddUserEvent() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceKernelTriggerUserEvent() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceKernelDeleteEqueue() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceKernelCreateEqueue(SceKernelEqueue* eq, const char* name) {
    LOG_DEBUG("%s(%p, \"%s\")", __FUNCTION__, eq, name);
    auto eq1 = new _SceKernelEqueue;
    std::strcpy(eq1->name, name);
    *eq = eq1;
    return 0;
}
PS4API int sceKernelWaitEqueue(SceKernelEqueue eq, SceKernelEvent* ev, int num, int* out, SceKernelUseconds* timo) {
    LOG_DEBUG("STUB %s('%s', %p, %d, %p, %p)", __FUNCTION__, eq->name, ev, num, out, timo);
    *out = 0;
    return 0;
}
PS4API void sceKernelGetEventUserData() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceKernelDeleteUserEvent() {
    UNIMPLEMENTED_FUNC();
}

PS4API int pthread_mutexattr_destroy(ScePthreadMutexattr* attr) {
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p)", __FUNCTION__, attr);
    int rc = ::pthread_mutexattr_destroy(&(*attr)->mutex_attr);
    ::delete *attr;
    return rc;
}

PS4API int pthread_mutexattr_init(ScePthreadMutexattr* attr) {
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p)", __FUNCTION__, attr);
    *attr = ::new _ScePthreadMutexattr; // (_ScePthreadMutexattr*)::malloc(sizeof(_ScePthreadMutexattr));
    int rc = ::pthread_mutexattr_init(&(*attr)->mutex_attr);
    return rc;
}
PS4API int pthread_mutexattr_settype(ScePthreadMutexattr* attr, MutexType type) {
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p, %d)", __FUNCTION__, attr, type);
    int t = convert_mutex_type(type);
    int rc = ::pthread_mutexattr_settype(&(*attr)->mutex_attr, t);
    return rc;
}
PS4API int pthread_mutex_init(ScePthreadMutex* mutex, /*const*/ ScePthreadMutexattr* attr) {
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p, %p)", __FUNCTION__, mutex, attr);
    *mutex = ::new _ScePthreadMutex;
    (*mutex)->pthread_mutex = ::new pthread_mutex_t;
    pthread_mutexattr_t* a = nullptr;
    if (attr) {
        a = &(*attr)->mutex_attr;
    }
    int rc = ::pthread_mutex_init((*mutex)->pthread_mutex, a);
    return rc;
}
PS4API int pthread_mutex_destroy(ScePthreadMutex* mutex) {
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p)", __FUNCTION__, mutex);
    pthread_mutex_t* m = (*mutex)->pthread_mutex;
    int rc = ::pthread_mutex_destroy(m);
    return rc;
}
PS4API int pthread_mutex_lock(ScePthreadMutex* mutex) {
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p)", __FUNCTION__, mutex);
    pthread_mutex_t* m = (*mutex)->pthread_mutex;
    int rc = ::pthread_mutex_lock(m);
    return rc;
}
PS4API int pthread_mutex_unlock(ScePthreadMutex* mutex) {
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p)", __FUNCTION__, mutex);
    pthread_mutex_t* m = (*mutex)->pthread_mutex;
    int rc = ::pthread_mutex_unlock(m);
    return rc;
}

PS4API int open(const char* path, int flags, SceKernelMode mode) {
    LOG_DEBUG("%s('%s', %x, %x)", __FUNCTION__, path, flags, mode);
    char full_path[0x100]{};
    process::get_resource_path(path, full_path);
#if defined(_WIN64)
    flags |= _O_BINARY;
    int rc = ::_open(full_path, flags); // TODO: _lcreat
#else
    int rc = ::open(full_path, flags, mode);
#endif
    return rc;
}
PS4API void read() {
    UNIMPLEMENTED_FUNC();
}
PS4API void lseek() {
    UNIMPLEMENTED_FUNC();
}
PS4API int socket(int domain, int type, int protocol) {
    LOG_DEBUG("%s(%d, %d, %d)", __func__, domain, type, protocol);
#if defined(_WIN64)
    WSADATA wsaData = {};
    WSAStartup(MAKEWORD(2, 2), &wsaData);
#endif
    uint64_t rc = ::socket(domain, type, protocol);
    return rc;
}
PS4API void recv() {
    UNIMPLEMENTED_FUNC();
}
PS4API void send() {
    UNIMPLEMENTED_FUNC();
}
PS4API void bind() {
    UNIMPLEMENTED_FUNC();
}
PS4API void listen() {
    UNIMPLEMENTED_FUNC();
}
PS4API void select() {
    UNIMPLEMENTED_FUNC();
}
PS4API void write() {
    UNIMPLEMENTED_FUNC();
}
PS4API void fsync() {
    UNIMPLEMENTED_FUNC();
}
PS4API int pthread_condattr_init(pthread_condattr_t* a) {
    LOG_DEBUG("%p", a);
    int rc = ::pthread_condattr_init(a);
    return rc;
}
PS4API int pthread_cond_init(pthread_cond_t *cv, const pthread_condattr_t *a) {
    LOG_DEBUG("%p, %p", cv, a);
    int rc = ::pthread_cond_init(cv, a);
    return rc;
}
PS4API void pthread_cond_destroy() {
    UNIMPLEMENTED_FUNC();
}
PS4API int pthread_cond_wait(pthread_cond_t *cv, pthread_mutex_t *external_mutex) {
    LOG_DEBUG("%p, %p", cv, external_mutex);
    int rc = ::pthread_cond_wait(cv, external_mutex);
    return rc;
}
PS4API int scePthreadAttrGet(ScePthread thread, ScePthreadAttr* attr) {
    LOG_DEBUG("%s(%p, %p)", __FUNCTION__, thread, attr);
    int rc = 0;
    return rc;
}
PS4API int scePthreadAttrGetaffinity(ScePthread thread, SceKernelCpumask* mask) {
    LOG_DEBUG("%s(%p, %p)", __FUNCTION__, thread, mask);
    int rc = 0;
    return rc;
}
PS4API void scePthreadAttrGetdetachstate() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceKernelInternalMemoryGetModuleSegmentInfo() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceKernelGetModuleInfoForUnwind(void* a1, int p1, int* p2) {
    LOG_DEBUG("%s(%p, %d, %p)", __FUNCTION__, a1, p1, p2);
    int rc = -1; // TODO: implement this
    return rc;
}
PS4API void sceKernelUtimes() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceKernelSync() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceCoredumpWriteUserData() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceCoredumpRegisterCoredumpHandler() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0; // TODO: implement this
}
PS4API void inet_pton() {
    UNIMPLEMENTED_FUNC();
}

PS4API int sceKernelGetModuleList(SceKernelModule* array, size_t numArray, size_t* actualNum) {
    LOG_DEBUG("%p, %zu, %p", array, numArray, actualNum);
    return 0;
}
PS4API int32_t sceKernelGetModuleInfo() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API void sceKernelGetEventFilter() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceKernelGetEventData() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceKernelGetEventId() {
    UNIMPLEMENTED_FUNC();
}
PS4API int usleep(useconds_t us) {
    LOG_DEBUG("%s(%u)", __FUNCTION__, us);
    int rc = 0;
#if defined(_WIN64)
    ::_usleep(us);
#else
    ::usleep(us);
#endif      
    return rc;
}
PS4API int sceKernelReleaseDirectMemory(sceoff_t start, size_t len) {
    LOG_DEBUG("%s(0x%lx, 0x%lx)", __FUNCTION__, start, len);
    return memory::free_direct(start, len);
}
PS4API int32_t sceKernelReleaseFlexibleMemory(void* addr, size_t len) {
    LOG_DEBUG("%s(%p, %zu)", __FUNCTION__, addr, len);
    ::free(addr);
    return 0;
}
PS4API size_t sceKernelGetDirectMemorySize() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return memory::size_direct();
}

PS4API int32_t sceKernelAvailableDirectMemorySize(sceoff_t searchStart, sceoff_t searchEnd, size_t alignment,
                                                  sceoff_t* physAddrOut, size_t* sizeOut) {
    LOG_DEBUG("%s()", __FUNCTION__);
    *sizeOut = memory::size_direct();
    return 0;
}

// TODO: implement allocate/map!
PS4API int sceKernelAllocateDirectMemory(uint64_t searchStart, uint64_t searchEnd, size_t len, size_t alignment,
                                             int memoryType, uint64_t* physAddrOut) {
    LOG_DEBUG("(start:0x%llx, end:0x%llx, len:0x%lx, algn:0x%lx, type:%d, out:%p)", searchStart,
              searchEnd, len, alignment, memoryType, physAddrOut);
    return memory::alloc_direct(searchStart, searchEnd, len, alignment, memoryType, physAddrOut);
}

PS4API int sceKernelAllocateMainDirectMemory(size_t len, size_t alignment, int memoryType, uint64_t* physAddrOut) {
    LOG_DEBUG("(len:0x%lx, algn:0x%lx, type:%d, out:%p)", len, alignment, memoryType, physAddrOut);
    return memory::alloc_direct(0, 0x10000000, len, alignment, memoryType, physAddrOut);
}

PS4API int32_t sceKernelMapDirectMemory(void** addr, size_t len, int prot, int flags, sceoff_t directMemoryStart,
                                        size_t alignment) {
    LOG_DEBUG("%s(out:%p, len:0x%lx, prot:0x%x, flags:0x%x, start:%x, algn:%lx)", __FUNCTION__, addr, len, prot, flags,
              directMemoryStart, alignment);

    return memory::map_direct(addr, len, prot, flags, directMemoryStart, alignment);
}

PS4API int32_t sceKernelMapFlexibleMemory(void** addrInOut, size_t len, int prot, int flags) {
    LOG_DEBUG("STUB %s(%p ,%zx, 0x%x, 0x%x)", __FUNCTION__, addrInOut, len, prot, flags);
    *addrInOut = ::malloc(len);
    return 0;
}
PS4API int32_t sceKernelSetVirtualRangeName(const void* addr, size_t len, const char* name) {
    LOG_DEBUG("STUB %s(%p ,%zx, '%s')", __FUNCTION__, addr, len, name);
    return 0;
}
PS4API int sceKernelVirtualQuery(const void* addr, int flags, SceKernelVirtualQueryInfo* info, size_t infoSize) {
    LOG_DEBUG("STUB %s(%p ,%x, %p, %zu)", __FUNCTION__, addr, flags, info, infoSize);
    return -1;
}
PS4API void scePthreadMutexTimedlock() {
    UNIMPLEMENTED_FUNC();
}
PS4API int scePthreadCondattrInit(pthread_condattr_t* a) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, a);
    int rc = ::pthread_condattr_init(a);
    return rc;
}
PS4API int scePthreadCondattrDestroy(pthread_condattr_t* a) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, a);
    int rc = ::pthread_condattr_destroy(a);
    return rc;
}

enum ShedPolicy {
    SCE_KERNEL_SCHED_FIFO = 1,
    SCE_KERNEL_SCHED_RR = 3,
};

PS4API int scePthreadAttrSetschedpolicy(ScePthreadAttr* attr, int pol) {

    LOG_DEBUG("%s(%p ,%llx)", __FUNCTION__, attr, pol);
    pthread_attr_t* a = proxy<ScePthreadAttr, pthread_attr_t>::get(*attr);
    int policy = SCHED_RR;
    if (pol == SCE_KERNEL_SCHED_FIFO) {
        policy = SCHED_FIFO;
    }
    int rc = ::pthread_attr_setschedpolicy(a, policy);
    return 0; // rc; //TODO: find out why error
}

PS4API int scePthreadGetschedparam(ScePthread thread, int* policy, SceKernelSchedParam* param) {
    LOG_DEBUG("%p, %p, %p", thread, policy, param);
    *policy = 1;
    return 0;
}

PS4API int sceKernelWaitSema() {
    // UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API void sceKernelPollSema() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceKernelSignalSema(SceKernelSema sem, int signalCount) {
    LOG_DEBUG("UNIMPLEMENTED %s(%p, %d)", __FUNCTION__, sem, signalCount);
    return 0;
}
PS4API int sceKernelCreateSema(SceKernelSema* sem, const char* pName, uint32_t attr, int initCount, int maxCount,
                               const SceKernelSemaOptParam* pOptParam) {
    LOG_DEBUG("%s(%p, '%s', %x, %x, %x, %p)", __FUNCTION__, sem, pName, attr, initCount, maxCount, pOptParam);

    *sem = ::new _SceKernelSema;
    int rc = ::sem_init(&(*sem)->sem, 0, initCount);
    return rc;
}
PS4API int sceKernelDeleteSema(SceKernelSema sem) {
    LOG_DEBUG("UNIMPLEMENTED %s(%p, %d)", __FUNCTION__, sem);
    return 0; // TODO implement
}
PS4API void setsockopt() {
    UNIMPLEMENTED_FUNC();
}
PS4API void getsockopt() {
    UNIMPLEMENTED_FUNC();
}
PS4API void accept() {
    UNIMPLEMENTED_FUNC();
}
PS4API void shutdown() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sendto() {
    UNIMPLEMENTED_FUNC();
}
PS4API void recvfrom() {
    UNIMPLEMENTED_FUNC();
}
PS4API void getpeername() {
    UNIMPLEMENTED_FUNC();
}
PS4API int close(int fd) {
#if defined(_WIN64)
    int rc = ::_close(fd);
#else
    int rc = ::close(fd);
#endif
    LOG_DEBUG("%s(%d) = %d", __FUNCTION__, fd, rc);
    return rc;
}
PS4API int connect(int sockfd, const struct sockaddr* addr, socklen_t addrlen) {
    int rc = ::connect(sockfd, addr, addrlen);
    LOG_DEBUG("%s(%d, %p, %d) = %d", __FUNCTION__, sockfd, addr, addr, rc);
    return rc;
}

PS4API uint64_t sceKernelGetTscFrequency() {
    return 0x10000000;
}

PS4API uint64_t sceKernelReadTsc() {
    return 0x10000000;
}
PS4API void sceKernelStopUnloadModule() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceKernelLoadStartModule(const char* moduleFileName, size_t args, const void* argp, uint32_t flags,
                                    const SceKernelLoadModuleOpt* pOpt, int* pRes) {
    LOG_DEBUG("%s('%s', %x, %p, %x, %p, %p)", __FUNCTION__, moduleFileName, args, argp, flags, pOpt, pRes);
    if (pRes)
        *pRes = 0;
    return 1;
}
PS4API void sceKernelDlsym() {
    UNIMPLEMENTED_FUNC();
}

PS4API void _nanosleep() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceKernelIsAddressSanitizerEnabled() {
    LOG_DEBUG("STUB %s()", __FUNCTION__);
    return 0;
}

PS4API void sceKernelGetPrtAperture() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceKernelQueryMemoryProtection(void* addr, void** start, void** end, int* prot) {
    LOG_DEBUG("%s(%p, %p, %p, %p)", __FUNCTION__, addr, start, end, prot);
    *prot = 0;
    return 0;
}
PS4API bool sceKernelIsNeoMode() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return false;
}
PS4API pthread_t scePthreadGetthreadid() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return ::pthread_self();
}
PS4API int scePthreadRename(ScePthread thread, const char *name) {
    LOG_DEBUG("unimplemented, t: %p, name: '%s'", thread, name);
    return 0;
}
PS4API int scePthreadSetschedparam(ScePthread thread, int policy, const SceKernelSchedParam* param) {
    LOG_DEBUG("%p, %d, %p", thread, policy, param);
    return 0;
}

PS4API int sceKernelCreateEventFlag(SceKernelEventFlag* ef, const char* pName, uint32_t attr, uint64_t initPattern,
                                    const SceKernelEventFlagOptParam* pOptParam) {
    LOG_DEBUG("%s(%p, '%s', %x, %lx, %p)", __FUNCTION__, ef, pName, attr, initPattern, pOptParam);
    int rc = 0;
    *ef = ::new _SceKernelEventFlag; //(pName, attr, initPattern);
    //::pthread_mutex_lock(&(*ef)->mutex);
    return rc;
}

PS4API int sceKernelWaitEventFlag(SceKernelEventFlag ef, uint64_t bitPattern, uint32_t waitMode, uint64_t* pResultPat,
                                  SceKernelUseconds* pTimeout) {
    LOG_DEBUG("Unimplemented %s(%p, %llx, %x, %p, %p)", __FUNCTION__, ef, bitPattern, waitMode, pResultPat, pTimeout);
    int rc = 0; // ::pthread_mutex_lock(&(ef->mutex));
    return rc;
}
PS4API int sceKernelDeleteEventFlag(SceKernelEventFlag ef) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, ef);
    return 0;
}
PS4API int sceKernelSetEventFlag(SceKernelEventFlag ef, uint64_t bitPattern) {
    LOG_DEBUG("%s(%p, %llx)", __FUNCTION__, ef, bitPattern);
    (*ef).initPattern |= bitPattern;
    return 0;
}
PS4API void scePthreadAttrGetschedparam() {
    LOG_DEBUG("%s()", __FUNCTION__);
}
PS4API int pthread_exit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int rename() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePthreadCancel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int stat(const char* path, struct sce_stat* sb) {
    struct stat st {};
    char full_path[0x100]{};
    process::get_resource_path(path, full_path);
    int rc = ::stat(full_path, &st);
    ::memset(sb, 0, sizeof(SceKernelStat));
    sb->st_dev = st.st_dev;
    sb->st_ino = st.st_ino;
    sb->st_nlink = st.st_nlink;
    sb->st_rdev = st.st_rdev;
    sb->st_uid = st.st_uid;
    sb->st_gid = st.st_gid;
    sb->st_size = st.st_size;
    sb->st_mode = st.st_mode;
    // sb->st_atim = st.st_atime;
    // sb->st_mtim = st.st_mtime;
    // sb->st_ctim = st.st_ctime;
    LOG_DEBUG("'%s', %p", path, sb);
    return rc;
}
PS4API int sceKernelAddReadEvent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceKernelDeleteReadEvent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceKernelGetdents() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceKernelClearEventFlag() {
    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
    return 0;
}
PS4API int sceKernelPollEventFlag(SceKernelEventFlag ef, uint64_t bitPattern, uint32_t waitMode, uint64_t* pResultPat) {
    LOG_DEBUG("Unimplemented %s(%p, %llx, %x, %p)", __FUNCTION__, ef, bitPattern, waitMode, pResultPat);
    return 0;
}
PS4API int pthread_setcanceltype(int type, int* oldtype) {
    LOG_DEBUG("%s(%d, %p)", __FUNCTION__, type, oldtype);
    int rc = ::pthread_setcanceltype(type, oldtype);
    return rc;
}
PS4API void pthread_getschedparam() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sched_get_priority_min() {
    LOG_DEBUG("");
    return 0;
}
PS4API void sched_get_priority_max() {
    UNIMPLEMENTED_FUNC();
}
PS4API void pthread_setschedparam() {
    UNIMPLEMENTED_FUNC();
}
PS4API void pthread_detach() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sem_init(sem_t* sem, int pshared, unsigned int value) {
    LOG_DEBUG("%s()", __FUNCTION__);
    int rc = ::sem_init(sem, pshared, value);
    return rc;
}
PS4API int sem_destroy(sem_t* sem) {
    LOG_DEBUG("%s()", __FUNCTION__);
    int rc = ::sem_destroy(sem);
    return rc;
}
PS4API void sem_trywait() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sem_wait(sem_t* sem) {
    LOG_DEBUG("%s()", __FUNCTION__);
    int rc = ::sem_wait(sem);
    return rc;
}
PS4API int sem_getvalue(sem_t* sem, int* sval) {
    LOG_DEBUG("%p, %p", sem, sval);
    return ::sem_getvalue(sem, sval);
}
PS4API int sem_post(sem_t* sem) {
    LOG_DEBUG("%s()", __FUNCTION__);
    int rc = ::sem_post(sem);
    return rc;
}

PS4API int pthread_key_create(pthread_key_t* key, PS4API void (*dest)(void*)) {
#if defined(_WIN64) || defined(__CYGWIN__)
    (void)dest;
    int rc = ::pthread_key_create(key, nullptr); // TODO: right impl for windows
#else
    int rc = ::pthread_key_create(key, dest);
#endif

    return rc;
}
PS4API void* pthread_getspecific(pthread_key_t key) {
    LOG_DEBUG("%s()", __FUNCTION__);
    void* rc = ::pthread_getspecific(key);
    return rc;
}
PS4API int pthread_setspecific(pthread_key_t key, const void* value) {
    LOG_DEBUG("%s()", __FUNCTION__);
    int rc = ::pthread_setspecific(key, value);
    return rc;
}
PS4API void sceKernelNanosleep() {
    UNIMPLEMENTED_FUNC();
}
PS4API int pthread_mutex_trylock(ScePthreadMutex* mutex) {
    pthread_mutex_t* m = (*mutex)->pthread_mutex;
    int rc = ::pthread_mutex_trylock(m);
    if (MUTEX_LOG)
        LOG_DEBUG("%s(%p) = %d", __FUNCTION__, mutex, rc);
    return rc;
}
PS4API int32_t sceKernelMapNamedDirectMemory(void** addr, size_t len, int prot, int flags, off_t directMemoryStart,
                                             size_t alignment, const char* name) {
    LOG_DEBUG("(%p, %llx, 0x%x, 0x%x, %x, %llu, '%s')", addr, len, prot, flags, directMemoryStart, alignment, name);
    return memory::map_direct(addr, len, prot, flags, directMemoryStart, alignment);
}

PS4API unsigned int sceKernelSleep(unsigned int seconds) {
    LOG_DEBUG("%s(%d)", __func__, seconds);
#if defined(_WIN64)
    ::_sleep(seconds);
#else
    ::sleep(seconds);
#endif
    return 0;
}

PS4API int sceKernelMprotect() {
    LOG_DEBUG("unimplemented %s()", __func__);
    return 0;
}
PS4API int kevent(int kq, const struct kevent* changelist, int nchanges, struct kevent* eventlist, int nevents,
                  const struct timespec* timeout) {
    LOG_DEBUG("%s(%d, %p, %d, %p, %d, %p)", __func__, kq, changelist, nchanges, eventlist, nevents, timeout);
    return 0;
}
PS4API int sceKernelError(int error) {
    LOG_DEBUG("%s(0x%x)", __func__, error);
    auto result = (unsigned int)(error - 0x7FFE0000);
    if (!error)
        result = 0LL;
    return result;
}
PS4API int sceKernelGetCompiledSdkVersion(uint32_t* version) {
    LOG_DEBUG("%s()", __FUNCTION__);

    *version = ((uint32_t*)process::get_proc_param())[4]; // 0x4508101;
    return 0;
}

PS4API void getuid() {
    UNIMPLEMENTED_FUNC();
}

PS4API int ioctl(int d, int request, ...) {
    LOG_DEBUG("%s(%d, 0x%x)", __func__, d, request);
    int rc = 0; //::ioctl()
    return rc;
}
PS4API int sceKernelMapNamedSystemFlexibleMemory(void** addrInOut, size_t len, int prot, int flags, const char* name) {
    LOG_DEBUG("%s(%p, 0x%llx, 0x%x, 0x%x, '%s')", __FUNCTION__, addrInOut, len, prot, flags, name);
    void* direct = nullptr;
    direct = ::malloc(len);
    *addrInOut = (void*)direct; // TODO: alignment
    return 0;
}
PS4API int sceKernelSetProcessProperty(const char* prop) {
    LOG_DEBUG("%s('%s')", __func__, prop);
    return 0;
}

PS4API int ipmimgr_call(uint32_t op, uint32_t handle, uint32_t* result, void* args_buffer, size_t args_size,
                        uint64_t cookie) {
    LOG_DEBUG("ipmimgr_call(%u, %u, %p, %p, %x, %llx)", op, handle, result, args_buffer, args_size, cookie);
    return kernel::ipmimgr_call(op, handle, result, args_buffer, args_size, cookie);
}
PS4API int get_authinfo() {
    LOG_DEBUG("%s()", __func__);
    return 0;
}
PS4API void get_self_auth_info() {
    UNIMPLEMENTED_FUNC();
}
PS4API void b0cryxaTM4k() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceKernelGetProcessType(int pid) {
    LOG_DEBUG("%s(%d)", __FUNCTION__, pid);
    return 1;
}
PS4API int recvmsg() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int sceKernelGetAppInfo() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API char* call_sys_randomized_path() {
    static char rand_path[0xFF] = {};
    return rand_path;
}
PS4API void pthread_equal() {
    UNIMPLEMENTED_FUNC();
}
PS4API void clock_settime() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceKernelSettimeofday() {
    UNIMPLEMENTED_FUNC();
}
PS4API void scePthreadCondSignalto() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceKernelCancelEventFlag() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int sceKernelCloseEventFlag() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int sceKernelOpenEventFlag() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API void sceKernelCloseSema() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceKernelOpenSema(SceKernelSema* sem) {
    LOG_DEBUG("%s()", __FUNCTION__);
    return -1;
}
PS4API void sceKernelGetSystemSwVersion() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceKernelSetCallRecord(uint64_t param1) {
    LOG_DEBUG("%llx", param1);
    return 0;
}
PS4API void sceKernelTitleWorkaroundIsEnabled() {
    UNIMPLEMENTED_FUNC();
}
PS4API void ftruncate() {
    UNIMPLEMENTED_FUNC();
}
PS4API int shm_open(const char* name, int oflag, mode_t mode) {
    (void)name;
    (void)oflag;
    (void)mode;
    UNIMPLEMENTED_FUNC();
    return -1;
}
PS4API int fcntl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int pipe() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int pthread_condattr_destroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int pthread_condattr_setclock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int poll() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int pthread_cond_broadcast() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int pthread_cond_timedwait() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int __sys_dynlib_load_prx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int __sys_get_proc_type_info() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int __sys_workaround8849() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int dynlib_get_obj_member() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceKernelGetExecutableModuleHandle() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int sceKernelGetMainSocId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceKernelIsInSandbox() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _2xg4JXvaJ6k() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceKernelLoadStartModuleForSysmodule() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceKernelSendNotificationRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ioctl() {
    LOG_DEBUG("unimpl %s()", __FUNCTION__);
    return 0;
}
PS4API int __sys_regmgr_call() {
    LOG_DEBUG("unimpl %s()", __FUNCTION__);
    return 0;
}
PS4API int sceKernelGetSanitizerNewReplace() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceKernelGetSanitizerMallocReplace() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePthreadMutexInitForInternalLibc(ScePthreadMutex* mutex, /*const*/ ScePthreadMutexattr* attr,
                                              const char* name) {
    if (MUTEX_LOG)
        LOG_DEBUG("scePthreadMutexInit(%p, %p, \"%s\")", mutex, attr, name);
    *mutex = ::new _ScePthreadMutex; //(_ScePthreadMutex*)::malloc(sizeof(_ScePthreadMutex));
    if (name) {
        ::strncpy((*mutex)->name, name, 0x20);
    }
    (*mutex)->pthread_mutex = ::new pthread_mutex_t; //(pthread_mutex_t*)::malloc( sizeof(pthread_mutex_t));
    pthread_mutexattr_t* a = nullptr;
    if (attr) {
        a = &(*attr)->mutex_attr;
    }
    int rc = ::pthread_mutex_init((*mutex)->pthread_mutex, a);
    return rc;
}

PS4API int scePthreadMutexattrInitForInternalLibc(ScePthreadMutexattr* attr) {
    if (MUTEX_LOG)
        LOG_DEBUG("scePthreadMutexattrInit(%p)", attr);
    *attr = ::new _ScePthreadMutexattr; // (_ScePthreadMutexattr*)::malloc(sizeof(_ScePthreadMutexattr));
    int rc = ::pthread_mutexattr_init(&(*attr)->mutex_attr);
    return rc;
}
PS4API int _close() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _open(const char* filename, int flags, int mode) {
#if defined(_WIN64)
    flags |= _O_BINARY;
     int rc = ::_open(filename, flags);
#else
    int rc = ::open(filename, flags);
#endif
    LOG_DEBUG("('%s', 0x%x, 0x%x) = %d", filename, flags, mode, rc);
    return rc;
}
PS4API int rmdir() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int unlink() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int setcontext() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sce_sigdelset() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sigaction() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sigsuspend() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sce_sigaddset() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sce_sigemptyset() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sce_sigismember() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int setitimer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _dup2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int chdir() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int fork() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int setsid() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sysctlbyname(const char* name, void* oldp, size_t* oldlenp, const void* newp, size_t newlen) {
    LOG_DEBUG("%s('%s', %p, %p, %p, %lu)", __FUNCTION__, name, oldp, oldlenp, newp, newlen);
    return 0;
}
PS4API int __getcwd() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _fstat() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _openat() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int fstatat() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int lstat() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _fcntl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _fstatfs() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _getdirentries() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _execve() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _execvpe() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int getgid() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sched_setparam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sched_setscheduler() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int setegid() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int seteuid() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int setpgid() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int vfork() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _fpathconf() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int pathconf() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int statfs() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int utimes() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int __inet_ntop() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int readlink() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sigintr() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sce_environ() {
    UNIMPLEMENTED_FUNC();
    return 0;
}

PS4API int sceKernelMunlock() {
    LOG_DEBUG("STUB %s()", __FUNCTION__);
    return 0;
}

PS4API int sceKernelHasNeoMode() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}

PS4API int shm_unlink(const char* name) {
    (void)name;
    UNIMPLEMENTED_FUNC();
    return 0;
}

PS4API int mlock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}

PS4API int sceKernelGetCpumode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceKernelProtectDirectMemory() {
    UNIMPLEMENTED_FUNC();
    return memory::protect_direct();
}
PS4API int sceKernelFdatasync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceKernelMmap() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int fstatfs() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePthreadRwlockattrDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePthreadRwlockattrInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API uint64_t sceKernelGetProcessTimeCounter() {
    LOG_DEBUG("called");
    return 0x10000;
}
PS4API uint64_t sceKernelGetProcessTimeCounterFrequency() {
    LOG_DEBUG("called");
    return 60;
}
PS4API int sceKernelTitleWorkdaroundIsEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int mkdir() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int aio_cancel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int aio_fsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int aio_read() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int aio_suspend() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int aio_write() {
    UNIMPLEMENTED_FUNC();
    return 0;
}

PS4API int __sys_netcontrol() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}

PS4API int __sys_socketex() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}

PS4API int __sys_kqueueex() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}

PS4API int __sys_netabort() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}

PS4API int __sys_netgetiflist() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}

PS4API int __sys_netgetsockinfo() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}

PS4API int __sys_socketclose() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}

PS4API int scePthreadGetname() {
    UNIMPLEMENTED_FUNC();
    return -1;
}

PS4API int scePthreadAttrGetstack() {
    UNIMPLEMENTED_FUNC();
    return -1;
}

PS4API int _accept() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int _bind() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int _connect() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int _getpeername() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int _getsockname() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int _getsockopt() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int _listen() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int _recvfrom() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int _recvmsg() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int _sendmsg() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int _sendto() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int _setsockopt() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int kqueue() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}

PS4API int openat(int dirfd, const char *pathname, int flags, mode_t mode) {
    UNIMPLEMENTED_FUNC();
    return -1;
}

PS4API int chown() {
    UNIMPLEMENTED_FUNC();
    return -1;
}

PS4API int unimpl() {
    UNIMPLEMENTED_FUNC();
    return -1;
}

PS4API const char* sceKernelGetFsSandboxRandomWord() {
    LOG_DEBUG("");
    static char path[0x100] {"app0"};
    return path;
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("f7uOxY9mM1U", &__stack_chk_guard);
    fun("djxxOmW6-aw", &__progname);
    fun("OMDRKKAZ8I4", sceKernelDebugRaiseException);
    fun("zE-wXIZjLoM", sceKernelDebugRaiseExceptionOnReleaseMode);
    fun("vNe1w4diLCs", __tls_get_addr);
    fun("Ou3iL1abvng", __stack_chk_fail);
    // fun("-o5uEDpN+oY", sceKernelConvertUtcToLocaltime);
    fun("2Tb92quprl0", scePthreadCondInit);
    fun("g+PZd2hiacg", scePthreadCondDestroy);
    fun("WKAXJ4XBPQ4", scePthreadCondWait);
    fun("BmMjYxmew1w", scePthreadCondTimedwait);
    fun("kDh-NfxgMtE", scePthreadCondSignal);
    fun("JGgj7Uvrl+A", scePthreadCondBroadcast);
    fun("6UgtwV+0zb4", scePthreadCreate);
    fun("onNY9Byn-W8", scePthreadJoin);
    fun("4qGrR6eoP9Y", scePthreadDetach);
    fun("3kg7rT0NQIs", scePthreadExit);
    fun("yS8U2TGCe1A", nanosleep);
    fun("kbw4UHHSYy0", __pthread_cxa_finalize);
    fun("9BcDykPmo1I", sce__error);
    fun("T72hz6ffq08", scePthreadYield);
    fun("3PtV6p3QNX4", scePthreadEqual);
    fun("F8bUHwAG284", scePthreadMutexattrInit);
    fun("FxVZqBAA7ks", _write);
    fun("cmo1RIYva9o", scePthreadMutexInit);
    fun("smWEktiyyG0", scePthreadMutexattrDestroy);
    fun("9UK1vLZQft4", scePthreadMutexLock);
    fun("tn3VlD0hG60", scePthreadMutexUnlock);
    fun("aI+OeCz8xrQ", scePthreadSelf);
    fun("2Of0f+3mhhE", scePthreadMutexDestroy);
    fun("upoVrzMHFeE", scePthreadMutexTrylock);
    fun("14bOACANTBo", scePthreadOnce);
    fun("geDaqgH9lTg", scePthreadKeyCreate);
    fun("PrdHuuDekhY", scePthreadKeyDelete);
    fun("+BzXYkqYeLE", scePthreadSetspecific);
    fun("eoht7mQOCmo", scePthreadGetspecific);
    fun("n88vx3C5nW8", gettimeofday);
    fun("0NTHN1NKONI", sceKernelConvertLocaltimeToUtc);
    fun("VADc3MNQ3cM", signal);
    fun("959qrazPIrg", sceKernelGetProcParam);
    fun("p5EcQeEeJAE", _sceKernelRtldSetApplicationHeapAPI);
    fun("Wl2o5hOVZdw", sceKernelPrintBacktraceWithModuleInfo);
    fun("cQke9UuBQOk", sceKernelMunmap);
    fun("mL8NDH86iQI", sceKernelMapNamedFlexibleMemory);
    fun("3k6kx-zOOSQ", sceKernelMlock);
    fun("DRuBt2pvICk", _read);
    fun("oib76F-12fk", sceKernelLseek);
    fun("hHlZQUnlxSM", getrusage);
    // fun("4J2sUJmuHZQ", sceKernelGetProcessTime);
    fun("lLMT9vJAck0", clock_gettime);
    fun("f7KBOafysXo", sceKernelGetModuleInfoFromAddr);
    fun("Fjc4-n1+y2g", __elf_phdr_match_addr);
    fun("Cg4srZ6TKbU", sceKernelRead);
    fun("4wSze92BhLI", sceKernelWrite);
    fun("kOcnerypnQA", sceKernelGettimezone);
    fun("6Z83sYWFlA8", _exit);
    fun("6xVpy0Fdq+I", _sigprocmask);
    fun("EotR8a3ASf4", pthread_self);
    fun("OxhIB8LB-PQ", pthread_create);
    fun("E+tyo3lp5Lw", pthread_attr_setdetachstate);
    fun("wtkt-teR1so", pthread_attr_init);
    fun("h9CcP3J0oVM", pthread_join_);
    fun("2MOy+rUfuhQ", pthread_cond_signal);
    fun("QBi7HCK03hw", sceKernelClockGettime);
    fun("ejekcaNQNq0", sceKernelGettimeofday);
    fun("1G3lF1Gg1k8", sceKernelOpen);
    fun("+r3rMFwItV4", sceKernelPread);
    fun("nKWi-N2HBV4", sceKernelPwrite);
    fun("UK2Tl2DWUns", sceKernelClose);
    fun("kBwCPsYX-m4", sceKernelFstat);
    fun("fTx66l5iWIA", sceKernelFsync);
    fun("eV9wAD2riIA", sceKernelStat);
    fun("taRWhTJFTgE", sceKernelGetdirentries);
    fun("1-LFLmRFxxM", sceKernelMkdir);
    fun("naInUjYt3so", sceKernelRmdir);
    fun("AUXVxWeJU-A", sceKernelUnlink);
    fun("WlyEA-sLDf0", sceKernelTruncate);
    fun("VW3TVZiM4-E", sceKernelFtruncate);
    fun("52NcYU9+lEo", sceKernelRename);
    fun("1FGvU0i9saQ", scePthreadMutexattrSetprotocol);
    fun("6ULAa0fq4jA", scePthreadRwlockInit);
    fun("BB+kb08Tl9A", scePthreadRwlockDestroy);
    fun("Ox9i0c7L5w0", scePthreadRwlockRdlock);
    fun("XD3mDeybCnk", scePthreadRwlockTryrdlock);
    fun("+L98PIbGttk", scePthreadRwlockUnlock);
    fun("mqdNorrB+gI", scePthreadRwlockWrlock);
    fun("bIHoZCTomsI", scePthreadRwlockTrywrlock);
    fun("1jfXLRVzisc", sceKernelUsleep);
    fun("nsYoNRywwNg", scePthreadAttrInit);
    fun("-Wreprtu0Qs", scePthreadAttrSetdetachstate);
    fun("eXbUSpEaTsA", scePthreadAttrSetinheritsched);
    fun("DzES9hQF4f4", scePthreadAttrSetschedparam);
    fun("3qxgM4ezETA", scePthreadAttrSetaffinity);
    fun("UTXzJbWhhTE", scePthreadAttrSetstacksize);
    fun("62KCwEMmzcM", scePthreadAttrDestroy);
    fun("W0Hpm2X0uPE", scePthreadSetprio);
    fun("1tKyG7RlMJo", scePthreadGetprio);
    fun("bt3CTBKmGyI", scePthreadSetaffinity);
    fun("rcrVFJsQWRY", scePthreadGetaffinity);
    fun("6XG4B33N09g", sched_yield);
    fun("DFmMT80xcNI", sysctl);
    fun("BPE9s9vQQXo", mmap);
    fun("mqQMh1zPPT8", fstat);
    fun("UqDGjXA5yUM", munmap);
    fun("VkTAsrZDcJ0", sce_sigfillset);
    fun("aPcyptbOiZs", sigprocmask);
    fun("tZY4+SZNFhA", msync);
    fun("HoLVWNanBBc", getpid);
    // fun("crb5j7mkk1c", _is_signal_return);
    fun("mo0bFmWppIw", sigreturn);
    fun("OjWstbIRPUo", __Ux86_64_setcontext);
    fun("k+AXqu2-eBc", getpagesize);
    fun("4R6-OvI2cEA", sceKernelAddUserEvent);
    fun("F6e0kwo4cnk", sceKernelTriggerUserEvent);
    fun("jpFjmgAC5AE", sceKernelDeleteEqueue);
    fun("D0OdFMjp46I", sceKernelCreateEqueue);
    fun("fzyMKs9kim0", sceKernelWaitEqueue);
    fun("vz+pg2zdopI", sceKernelGetEventUserData);
    fun("LJDwdSNTnDg", sceKernelDeleteUserEvent);
    fun("dQHWEsJtoE4", pthread_mutexattr_init);
    fun("mDmgMOGVUqg", pthread_mutexattr_settype);
    fun("ttHNfU+qDBU", pthread_mutex_init);
    fun("ltCfaGr2JGE", pthread_mutex_destroy);
    fun("7H0iTOciTLo", pthread_mutex_lock);
    fun("2Z+PpY6CaJg", pthread_mutex_unlock);
    fun("wuCroIGjt2g", open);
    fun("AqBioC2vF3I", read);
    fun("Oy6IpwgtYOk", lseek);
    fun("TU-d9PfIHPM", socket);
    fun("Ez8xjo9UF4E", recv);
    fun("fZOeZIOEmLw", send);
    fun("KuOmgKoqCdY", bind);
    fun("pxnCmagrtao", listen);
    fun("T8fER+tIGgk", select);
    fun("FN4gaPmuFV8", write);
    fun("juWbTNM+8hw", fsync);
    fun("mKoTx03HRWA", pthread_condattr_init);
    fun("0TyVk4MSLt0", pthread_cond_init);
    fun("RXXqi4CtF8w", pthread_cond_destroy);
    fun("Op8TBGY5KHg", pthread_cond_wait);
    fun("x1X76arYMxU", scePthreadAttrGet);
    fun("8+s5BzZjxSg", scePthreadAttrGetaffinity);
    fun("JaRMy+QcpeU", scePthreadAttrGetdetachstate);
    fun("-YTW+qXc3CQ", sceKernelInternalMemoryGetModuleSegmentInfo);
    fun("RpQJJVKTiFM", sceKernelGetModuleInfoForUnwind);
    fun("0Cq8ipKr9n0", sceKernelUtimes);
    fun("uvT2iYBBnkY", sceKernelSync);
    fun("Dbbkj6YHWdo", sceCoredumpWriteUserData);
    fun("8zLSfEfW5AU", sceCoredumpRegisterCoredumpHandler);
    fun("4n51s0zEf0c", inet_pton);
    fun("IuxnUuXk6Bg", sceKernelGetModuleList);
    fun("kUpgrXIrz7Q", sceKernelGetModuleInfo);
    fun("23CPPI1tyBY", sceKernelGetEventFilter);
    fun("kwGyyjohI50", sceKernelGetEventData);
    fun("mJ7aghmgvfc", sceKernelGetEventId);
    fun("QcteRwbsnV0", usleep);
    fun("MBuItvba6z8", sceKernelReleaseDirectMemory);
    fun("teiItL2boFw", sceKernelReleaseFlexibleMemory);
    fun("pO96TwzOm5E", sceKernelGetDirectMemorySize);
    fun("C0f7TJcbfac", sceKernelAvailableDirectMemorySize);
    fun("rTXw65xmLIA", sceKernelAllocateDirectMemory);
    fun("B+vc2AO2Zrc", sceKernelAllocateMainDirectMemory);
    fun("L-Q3LEjIbgA", sceKernelMapDirectMemory);
    fun("IWIBBdTHit4", sceKernelMapFlexibleMemory);
    fun("DGMG3JshrZU", sceKernelSetVirtualRangeName);
    fun("rVjRvHJ0X6c", sceKernelVirtualQuery);
    fun("IafI2PxcPnQ", scePthreadMutexTimedlock);
    fun("m5-2bsNfv7s", scePthreadCondattrInit);
    fun("waPcxYiR3WA", scePthreadCondattrDestroy);
    fun("4+h9EzwKF4I", scePthreadAttrSetschedpolicy);
    fun("Zxa0VhQVTsk", sceKernelWaitSema);
    fun("12wOHk8ywb0", sceKernelPollSema);
    fun("4czppHBiriw", sceKernelSignalSema);
    fun("188x57JYp0g", sceKernelCreateSema);
    fun("R1Jvn8bSCW8", sceKernelDeleteSema);
    fun("fFxGkxF2bVo", setsockopt);
    fun("6O8EwYOgH9Y", getsockopt);
    fun("3e+4Iv7IJ8U", accept);
    fun("TUuiYS2kE8s", shutdown);
    fun("oBr313PppNE", sendto);
    fun("lUk6wrGXyMw", recvfrom);
    fun("TXFFFiNldU8", getpeername);
    fun("bY-PO6JhzhQ", close);
    fun("XVL8So3QJUk", connect);
    fun("1j3S3n-tTW4", sceKernelGetTscFrequency);
    fun("-2IRUCO--PM", sceKernelReadTsc);
    fun("QKd0qM58Qes", sceKernelStopUnloadModule);
    fun("wzvqT4UqKX8", sceKernelLoadStartModule);
    fun("LwG8g3niqwA", sceKernelDlsym);
    fun("NhpspxdjEKU", _nanosleep);
    fun("jh+8XiK4LeE", sceKernelIsAddressSanitizerEnabled);
    fun("L0v2Go5jOuM", sceKernelGetPrtAperture);
    fun("WFcfL2lzido", sceKernelQueryMemoryProtection);
    fun("WslcK1FQcGI", sceKernelIsNeoMode);
    fun("EI-5-jlq2dE", scePthreadGetthreadid);
    fun("GBUY7ywdULE", scePthreadRename);
    fun("oIRFTjoILbg", scePthreadSetschedparam);
    fun("BpFoboUJoZU", sceKernelCreateEventFlag);
    fun("JTvBflhYazQ", sceKernelWaitEventFlag);
    fun("8mql9OcQnd4", sceKernelDeleteEventFlag);
    fun("IOnSvHzqu6A", sceKernelSetEventFlag);
    fun("FXPWHNk8Of0", scePthreadAttrGetschedparam);
    fun("FJrT5LuUBAU", pthread_exit);
    fun("NN01qLRhiqU", rename);
    fun("qBDmpCyGssE", scePthreadCancel);
    fun("E6ao34wPw+U", stat);
    fun("VHCS3rCd0PM", sceKernelAddReadEvent);
    fun("JxJ4tfgKlXA", sceKernelDeleteReadEvent);
    fun("j2AIqSqJP0w", sceKernelGetdents);
    fun("7uhBFWRAS60", sceKernelClearEventFlag);
    fun("9lvj5DjHZiA", sceKernelPollEventFlag);
    fun("2dEhvvjlq30", pthread_setcanceltype);
    fun("FIs3-UQT9sg", pthread_getschedparam);
    fun("m0iS6jNsXds", sched_get_priority_min);
    fun("CBNtXOoef-E", sched_get_priority_max);
    fun("Xs9hdiD7sAA", pthread_setschedparam);
    fun("+U1R4WtXvoc", pthread_detach);
    fun("pDuPEf3m4fI", sem_init);
    fun("cDW233RAwWo", sem_destroy);
    fun("WBWzsRifCEA", sem_trywait);
    fun("YCV5dGGBcCo", sem_wait);
    fun("Bq+LRV-N6Hk", sem_getvalue);
    fun("IKP8typ0QUk", sem_post);
    fun("mqULNdimTn0", pthread_key_create);
    fun("0-KXaS70xy4", pthread_getspecific);
    fun("WrOLvHU0yQM", pthread_setspecific);
    fun("QvsZxomvUHs", sceKernelNanosleep);
    fun("K-jXhbt2gn4", pthread_mutex_trylock);
    fun("NcaWUxfMNIQ", sceKernelMapNamedDirectMemory);
    fun("-ZR+hG7aDHw", sceKernelSleep);
    fun("vSMAm3cxYTY", sceKernelMprotect);
    fun("RW-GEfpnsqg", kevent);
    fun("D4yla3vx4tY", sceKernelError);
    fun("WB66evu8bsU", sceKernelGetCompiledSdkVersion);
    fun("kg4x8Prhfxw", getuid);
    fun("PfccT7qURYE", ioctl);
    fun("kc+LEEIYakc", sceKernelMapNamedSystemFlexibleMemory);
    fun("-W4xI5aVI8w", sceKernelSetProcessProperty);
    fun("Hk7iHmGxB18", ipmimgr_call);
    fun("igMefp4SAv0", get_authinfo);
    fun("p2xgfB-30g8", get_self_auth_info);
    fun("b0cryxaTM4k", b0cryxaTM4k); // unknown
    fun("+g+UP8Pyfmo", sceKernelGetProcessType);
    fun("hI7oVeOluPM", recvmsg);
    fun("G-MYv5erXaU", sceKernelGetAppInfo);
    fun("JGfTMBOdUJo", call_sys_randomized_path);
    fun("7Xl257M4VNI", pthread_equal);
    fun("d7nUj1LOdDU", clock_settime);
    fun("ChCOChPU-YM", sceKernelSettimeofday);
    fun("o69RpYO-Mu0", scePthreadCondSignalto);
    fun("PZku4ZrXJqg", sceKernelCancelEventFlag);
    fun("s9-RaxukuzQ", sceKernelCloseEventFlag);
    fun("1vDaenmJtyA", sceKernelOpenEventFlag);
    fun("HBkUky2PZPA", sceKernelCloseSema);
    fun("MwhHNKdBXq8", sceKernelOpenSema);
    fun("Mv1zUObHvXI", sceKernelGetSystemSwVersion);
    fun("P6dUEXUHXjo", sceKernelSetCallRecord);
    fun("1yca4VvfcNA", sceKernelTitleWorkaroundIsEnabled);
    fun("ih4CD9-gghM", ftruncate);
    fun("QuJYZ2KVGGQ", shm_open);
    fun("nZHk+lpqwVQ", __sys_dynlib_load_prx);
    fun("nG-FYqFutUo", __sys_get_proc_type_info);
    fun("jTPE1AS7uak", __sys_workaround8849);
    fun("6eh9QGpaRWw", dynlib_get_obj_member);
    fun("wJABa1X4+ec", sceKernelGetExecutableModuleHandle);
    fun("0vTn5IDMU9A", sceKernelGetMainSocId);
    fun("xeu-pV8wkKs", sceKernelIsInSandbox);
    fun("2xg4JXvaJ6k", _2xg4JXvaJ6k); // unknown
    fun("Gg3+yWL6DWU", sceKernelLoadStartModuleForSysmodule);
    fun("zl7hupSO0C0", sceKernelSendNotificationRequest);
    fun("wW+k21cmbwQ", _ioctl);
    fun("8nY19bKoiZk", fcntl);
    fun("-Jp7F+pXxNg", pipe);
    fun("dJcuQVn6-Iw", pthread_condattr_destroy);
    fun("EjllaAqAPZo", pthread_condattr_setclock);
    fun("ku7D4q1Y9PI", poll);
    fun("mkx2fVhNMsg", pthread_cond_broadcast);
    fun("27bAgiJmOh0", pthread_cond_timedwait);
    fun("F4Kib3Mb0wI", sceKernelGetSanitizerNewReplace);
    fun("bt0POEUZddE", sceKernelGetSanitizerMallocReplace);
    fun("qH1gXoq71RY", scePthreadMutexInitForInternalLibc);
    fun("n2MMpvU8igI", scePthreadMutexattrInitForInternalLibc);
    fun("NNtFaKJbPt0", _close);
    fun("6c3rCVE-fTU", _open);
    fun("c7ZnT7V1B98", rmdir);
    fun("VAzswvTOCzI", unlink);
    fun("2VRU7xiqLO8", setcontext);
    fun("Nd-u09VFSCA", sce_sigdelset);
    fun("UDCI-WazohQ", _sigaction);
    fun("9zpLsLESzTs", _sigsuspend);
    fun("JUimFtKe0Kc", sce_sigaddset);
    fun("+F7C-hdk7+E", sce_sigemptyset);
    fun("JnNl8Xr-z4Y", sce_sigismember);
    fun("hPWDGx8ioXQ", setitimer);
    fun("W8f1adVl+48", _dup2);
    fun("6mMQ1MSPW-Q", chdir);
    fun("cjt-4ySmYTs", fork);
    fun("edJoqV2FIWs", setsid);
    fun("MhC53TKmjVA", sysctlbyname);
    fun("Qsxbb+aty0U", __getcwd);
    fun("A0O5kF5x4LQ", _fstat);
    fun("bGVEgWXy6dg", _openat);
    fun("t6haf4s-eE0", fstatat);
    fun("DRGXpDDh8Ng", lstat);
    fun("t0fXUzq61Z4", _fcntl);
    fun("17Mfe1B3X6U", _fstatfs);
    fun("sfKygSjIbI8", _getdirentries);
    fun("-3nj+K1elI0", _execve);
    fun("ScfDTOqLOMw", _execvpe);
    fun("AfuS23bX6kg", getgid);
    fun("yawdym+zDvw", sched_setparam);
    fun("puT82CSQzDE", sched_setscheduler);
    fun("4oKwKmeOKjM", setegid);
    fun("HTxb6gmexa0", seteuid);
    fun("8fryvN6BDtY", setpgid);
    fun("9jJTRrWCWK0", vfork);
    fun("cZq1zIzFN7s", _fpathconf);
    fun("oc98+zOImus", pathconf);
    fun("ngZnQWyUvGI", statfs);
    fun("GDuV00CHrUg", utimes);
    fun("4pYihoPggn8", __inet_ntop);
    fun("pd02UI9TbOA", readlink);
    fun("7NwggrWJ5cA", __sys_regmgr_call);
    fun("nQVWJEGHObc", _sigintr);
    fun("+2thxYZ4syk", sce_environ);
    fun("zHchY8ft5pk", pthread_attr_destroy);
    fun("xQIIfJ860sk", sceKernelMunlock);
    fun("rNRtm1uioyY", sceKernelHasNeoMode);
    fun("tPWsbOUGO8k", shm_unlink);
    fun("HF7lK46xzjY", pthread_mutexattr_destroy);
    fun("mTBZfEal2Bw", mlock);
    fun("VOx8NGmHXTs", sceKernelGetCpumode);
    fun("k1jIkFHa9OU", sceKernelProtectDirectMemory);
    fun("30Rh4ixbKy4", sceKernelFdatasync);
    fun("PGhQHd-dzv8", sceKernelMmap);
    fun("+rWFNYiNaEM", fstatfs);
    fun("i2ifZ3fS2fo", scePthreadRwlockattrDestroy);
    fun("yOfGg-I1ZII", scePthreadRwlockattrInit);
    //fun("fgxnMeTNUtY", sceKernelGetProcessTimeCounter);
    fun("BNowx2l588E", sceKernelGetProcessTimeCounterFrequency);
    fun("GST42pfUfMc", sceKernelTitleWorkdaroundIsEnabled);
    fun("JGMio+21L4c", mkdir);
    fun("phnjLKz4njM", aio_cancel);
    fun("j64iXjsjnCY", aio_fsync);
    fun("kaLFzWxqkFY", aio_read);
    fun("52r68vPvrBA", aio_suspend);
    fun("IiofFKEgSew", aio_write);
    fun("2M+dFM8Wmq4", __sys_kqueueex);
    fun("zsTvhCDrOjQ", __sys_netcontrol);
    fun("X86Q0qQJ1m0", __sys_netgetiflist);
    fun("UTR6wAkajxk", __sys_netabort);
    fun("X86Q0qQJ1m0", __sys_netgetiflist);
    fun("Wx2+2t1mX1E", __sys_netgetsockinfo);
    fun("t+rTx5NW+qU", __sys_socketclose);
    fun("pG70GT5yRo4", __sys_socketex);
    fun("B+zfF7bMswI", _accept);
    fun("4boImm4wxu0", _bind);
    fun("KFDGyLj80MY", _connect);
    fun("rTNM7adVYog", _getpeername);
    fun("6Gwl39KKUEI", _getsockname);
    fun("cL2QUlo9Vnk", _getsockopt);
    fun("6EYF3tXjXbU", _listen);
    fun("gPcQ3OrFBUA", _recvfrom);
    fun("PcKApW9kVoQ", _recvmsg);
    fun("+L22kkFiXok", _sendmsg);
    fun("lvDhwhUC05E", _sendto);
    fun("a4KKvF-ME4M", _setsockopt);
    fun("nh2IFMgKTv8", kqueue);
    fun("How7B8Oet6k", scePthreadGetname);
    fun("-quPa4SEJUw", scePthreadAttrGetstack);
    fun("iMp8QpE+XO4", scePthreadMutexattrSettype);
    fun("P41kTWUS3EI", scePthreadGetschedparam);
    fun("A8Yk5AhNDck", openat);
    fun("+2HAuc7yVbc", chown);
    fun("JGfTMBOdUJo", sceKernelGetFsSandboxRandomWord);
#undef fun
}

} // namespace libkernel
