#pragma once

#include <pthread.h>

#include "common.h"

namespace libkernel {
typedef pthread_t ScePthread;
typedef pthread_rwlock_t ScePthreadRwlock;
typedef pthread_rwlockattr_t ScePthreadRwlockattr;
typedef pthread_cond_t ScePthreadCond;
typedef pthread_condattr_t ScePthreadCondattr;

typedef struct _ScePthreadMutexattr {
    uint64_t pad[0x2]{};
    pthread_mutexattr_t mutex_attr{};
} * ScePthreadMutexattr;

typedef struct _ScePthreadMutex {
    uint64_t pad[8]{};
    char name[0x20]{};
    pthread_mutex_t* pthread_mutex{};
} * ScePthreadMutex;

typedef struct _ScePthreadAttr {
    uint64_t pad[0x20]{};
} * ScePthreadAttr;

enum MutexType {
    SCE_PTHREAD_MUTEX_ERRORCHECK = 1, // Perform error check (default)
    SCE_PTHREAD_MUTEX_RECURSIVE = 2,  // Lock recursively
    SCE_PTHREAD_MUTEX_NORMAL = 3,     // Do not perform error check
};
inline int convert_mutex_type(MutexType mt) {
    switch (mt) {
    case SCE_PTHREAD_MUTEX_ERRORCHECK:
        return PTHREAD_MUTEX_ERRORCHECK;
    case SCE_PTHREAD_MUTEX_RECURSIVE:
        return PTHREAD_MUTEX_RECURSIVE;
    case SCE_PTHREAD_MUTEX_NORMAL:
        return PTHREAD_MUTEX_NORMAL;
    default:
        return PTHREAD_MUTEX_ERRORCHECK;
    }
}
} // namespace libkernel