#pragma once

#include <queue>
#include "common.h"

namespace libkernel {

struct sce_timespec {
    time_t tv_sec;
    uint64_t tv_nsec;
};
typedef struct sce_timespec SceKernelTimespec;

struct kevent {
    uintptr_t ident; /* identifier for this event */
    short filter;    /* filter for event */
    uint16_t flags;
    uint32_t fflags;
    intptr_t data;
    void* udata; /* opaque user data identifier */
};
typedef struct kevent SceKernelEvent;

typedef struct _SceKernelEqueue {
    std::queue<kevent> q{};
    char name[32]{};
} * SceKernelEqueue;

typedef unsigned int SceKernelUseconds;

typedef uint32_t sce__dev_t;
typedef uint32_t sce_ino_t;
typedef uint16_t sce_mode_t;
typedef uint16_t sce_nlink_t;
typedef uint32_t sce_uid_t;
typedef uint32_t sce_gid_t;
typedef int64_t sce_blkcnt_t;
typedef uint32_t sce_blksize_t;
typedef uint32_t sce_fflags_t;
struct sce_stat {
    sce__dev_t st_dev;           /* inode's device */
    sce_ino_t st_ino;            /* inode's number */
    sce_mode_t st_mode;          /* inode protection mode */
    sce_nlink_t st_nlink;        /* number of hard links */
    sce_uid_t st_uid;            /* user ID of the file's owner */
    sce_gid_t st_gid;            /* group ID of the file's group */
    sce__dev_t st_rdev;          /* device type */
    struct sce_timespec st_atim; /* time of last access */
    struct sce_timespec st_mtim; /* time of last data modification */
    struct sce_timespec st_ctim; /* time of last file status change */
    sceoff_t st_size;               /* file size, in bytes */
    sce_blkcnt_t st_blocks;      /* blocks allocated for file */
    sce_blksize_t st_blksize;    /* optimal blocksize for I/O */
    sce_fflags_t st_flags;       /* user defined flags for file */
    uint32_t st_gen;             /* file generation number */
    int32_t st_lspare;
    struct sce_timespec st_birthtim; /* time of file creation */
    unsigned int : (8 / 2) * (16 - (int)sizeof(struct sce_timespec));
    unsigned int : (8 / 2) * (16 - (int)sizeof(struct sce_timespec));
};

typedef sce_stat SceKernelStat;

void load(export_funcs& exp);

} // namespace libkernel
