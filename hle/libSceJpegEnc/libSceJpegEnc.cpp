#include "libSceJpegEnc.h"

namespace libSceJpegEnc {
    PS4API void sceJpegEncQueryMemorySize() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceJpegEncCreate() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceJpegEncEncode() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceJpegEncDelete() {
        UNIMPLEMENTED_FUNC();
    }

    void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
        fun("o6ZgXfFdWXQ", sceJpegEncQueryMemorySize); // libSceJpegEnc
        fun("K+rocojkr-I", sceJpegEncCreate); // libSceJpegEnc
        fun("QbrU0cUghEM", sceJpegEncEncode); // libSceJpegEnc
        fun("j1LyMdaM+C0", sceJpegEncDelete); // libSceJpegEnc
#undef fun
    }
}
