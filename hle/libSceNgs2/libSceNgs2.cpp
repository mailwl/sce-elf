#include "libSceNgs2.h"

namespace libSceNgs2 {

#define SCE_NGS2_SYSTEM_NAME_LENGTH (16)

typedef struct SceNgs2SystemOption {
    size_t size;
    char name[SCE_NGS2_SYSTEM_NAME_LENGTH];
    uint32_t flags;
    uint32_t maxGrainSamples;
    uint32_t numGrainSamples;
    uint32_t sampleRate;
    uint32_t aReserved[6];
} SceNgs2SystemOption;

typedef struct SceNgs2ContextBufferInfo {
    void* hostBuffer;
    size_t hostBufferSize;
    uintptr_t reserved[5];
    uintptr_t userData;
} SceNgs2ContextBufferInfo;

typedef struct SceNgs2RenderBufferInfo {
    void* buffer;
    size_t bufferSize;
    uint32_t waveformType;
    uint32_t numChannels;
} SceNgs2RenderBufferInfo;

typedef PS4API int32_t (*SceNgs2BufferAllocHandler)(SceNgs2ContextBufferInfo* ioBufferInfo);

typedef PS4API int32_t (*SceNgs2BufferFreeHandler)(SceNgs2ContextBufferInfo* ioBufferInfo);

typedef struct SceNgs2BufferAllocator {
    SceNgs2BufferAllocHandler allocHandler;
    SceNgs2BufferFreeHandler freeHandler;
    uintptr_t userData;
} SceNgs2BufferAllocator;

typedef uintptr_t SceNgs2Handle;
#define SCE_NGS2_HANDLE_INVALID (NULL)

typedef struct SceNgs2VoiceState {
    uint32_t stateFlags;
} SceNgs2VoiceState;

#define SCE_NGS2_RACK_NAME_LENGTH (16)

typedef struct SceNgs2RackOption {
    size_t size;
    char name[SCE_NGS2_RACK_NAME_LENGTH];
    uint32_t flags;
    uint32_t maxGrainSamples;
    uint32_t maxVoices;
    uint32_t maxInputDelayBlocks;
    uint32_t maxMatrices;
    uint32_t maxPorts;
    uint32_t aReserved[20];
} SceNgs2RackOption;

typedef struct SceNgs2VoiceParamHeader {
    uint16_t size;
    int16_t next;
    uint32_t id;
} SceNgs2VoiceParamHeader;

#define SCE_NGS2_WAVEFORM_INFO_MAX_BLOCKS (4)
typedef struct SceNgs2WaveformFormat {
    uint32_t waveformType;
    uint32_t numChannels;
    uint32_t sampleRate;
    uint32_t configData;
    uint32_t frameOffset;
    uint32_t frameMargin;
} SceNgs2WaveformFormat;

typedef struct SceNgs2WaveformBlock {
    uint32_t dataOffset;
    uint32_t dataSize;
    uint32_t numRepeats;
    uint32_t numSkipSamples;
    uint32_t numSamples;
    uint32_t reserved;
    uintptr_t userData;
} SceNgs2WaveformBlock;

typedef struct SceNgs2WaveformInfo {
    SceNgs2WaveformFormat format;
    uint32_t dataOffset;
    uint32_t dataSize;
    uint32_t loopBeginPosition;
    uint32_t loopEndPosition;
    uint32_t numSamples;
    uint32_t audioUnitSize;
    uint32_t numAudioUnitSamples;
    uint32_t numAudioUnitPerFrame;
    uint32_t audioFrameSize;
    uint32_t numAudioFrameSamples;
    uint32_t numDelaySamples;
    uint32_t numBlocks;
    SceNgs2WaveformBlock aBlock[SCE_NGS2_WAVEFORM_INFO_MAX_BLOCKS];
} SceNgs2WaveformInfo;


typedef struct SceNgs2RackInfo {
    char name[SCE_NGS2_RACK_NAME_LENGTH];
    // Name
    SceNgs2Handle rackHandle; // Rack handle
    SceNgs2ContextBufferInfo bufferInfo;
    // Setup buffer information
    SceNgs2Handle ownerSystemHandle;
    // Owner system handle
    uint32_t type;            // Rack type (SCE_NGS2_RACK_TYPE_...)
    uint32_t rackId;          // Rack ID (SCE_NGS2_RACK_ID_...)
    uint32_t uid;             // Unique ID
    uint32_t minGrainSamples; // Number of minimum grain samples (64~1024)
    uint32_t maxGrainSamples; // Number of maximum grain samples (64~1024)
    uint32_t maxVoices;       // Maximum of voices (0~1024)
    uint32_t maxChannelWorks; // Maximum of channel works (0~8192)
    uint32_t maxInputs;       // Maximum of inputs (0~1)
    uint32_t maxMatrices;     // Maximum of matrices (0~16)
    uint32_t maxPorts;        // Maximum of ports (0~16)

    uint32_t stateFlags;       // State flags (SCE_NGS2_RACK_STATE_FLAG_... | ...)
    float lastProcessRatio;    // Last process ratio (0.0~1.0~ = 0~100~ [%])
    uint64_t lastProcessTick;  // Last process tick
    uint64_t renderCount;      // Rendering count
    uint32_t activeVoiceCount; // Active voice count
    uint32_t activeChannelWorkCount;
    // Active channel work count
} SceNgs2RackInfo;

PS4API int sceNgs2CalcWaveformBlock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2CustomRackGetModuleInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2GeomApply() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2GeomCalcListener() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2GeomResetListenerParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2GeomResetSourceParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2GetWaveformFrameInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2PanGetVolumeMatrix() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2PanInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2ParseWaveformData(const void* data, size_t dataSize, SceNgs2WaveformInfo* outInfo) {
    return 0;
}
PS4API int sceNgs2ParseWaveformFile() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2ParseWaveformUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2RackCreate() {
    // UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int32_t sceNgs2RackCreateWithAllocator(SceNgs2Handle systemHandle, uint32_t rackId,
                                              const SceNgs2RackOption* option, const SceNgs2BufferAllocator* allocator,
                                              SceNgs2Handle* outHandle) {
    *outHandle = 2;
    return 0;
}
PS4API int sceNgs2RackDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2RackGetInfo(SceNgs2Handle rackHandle, SceNgs2RackInfo* outInfo, size_t infoSize) {
    LOG_DEBUG("%s(%d, %p, %lu)\n", __FUNCTION__, rackHandle, outInfo, infoSize);
    outInfo->maxVoices = 6;
    return 0;
}
PS4API int sceNgs2RackGetUserData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int32_t sceNgs2RackGetVoiceHandle(SceNgs2Handle rackHandle, uint32_t voiceId, SceNgs2Handle* outHandle) {
    *outHandle = 3 + voiceId;
    return 0;
}
PS4API int sceNgs2RackLock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2RackQueryBufferSize(uint32_t rackId, const SceNgs2RackOption* option,
                                      SceNgs2ContextBufferInfo* outBufferInfo) {
    outBufferInfo->hostBufferSize = 0x1000;
    return 0;
}
PS4API int sceNgs2RackSetUserData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2RackUnlock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2ReportRegisterHandler() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2ReportUnregisterHandler() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2SystemCreate(const SceNgs2SystemOption* option, const SceNgs2ContextBufferInfo* bufferInfo,
                               SceNgs2Handle* outHandle) {
    *outHandle = 4;
    return 0;
}
PS4API int32_t sceNgs2SystemCreateWithAllocator(const SceNgs2SystemOption* option,
                                                const SceNgs2BufferAllocator* allocator, SceNgs2Handle* outHandle) {
    *outHandle = 1;
    return 0;
}
PS4API int sceNgs2SystemDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2SystemEnumHandles() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2SystemEnumRackHandles() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2SystemGetInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2SystemGetUserData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2SystemLock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2SystemQueryBufferSize(const SceNgs2SystemOption* option, SceNgs2ContextBufferInfo* outBufferInfo) {
    // UNIMPLEMENTED_FUNC();
    outBufferInfo->hostBufferSize = 0x1000;
    return 0;
}
PS4API int32_t sceNgs2SystemRender(SceNgs2Handle systemHandle, const SceNgs2RenderBufferInfo* aBufferInfo,
                                   uint32_t numBufferInfo) {

    return 0;
}
PS4API int sceNgs2SystemSetGrainSamples() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2SystemSetSampleRate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2SystemSetUserData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2SystemUnlock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int32_t sceNgs2VoiceControl(SceNgs2Handle voiceHandle, const SceNgs2VoiceParamHeader* paramList) {
    return 0;
}
PS4API int sceNgs2VoiceGetMatrixInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2VoiceGetOwner() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2VoiceGetPortInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNgs2VoiceGetState(SceNgs2Handle voiceHandle, SceNgs2VoiceState* outState, size_t stateSize) {
    (void)voiceHandle;
    (void)stateSize;
    outState->stateFlags = 1;
    return 0;
}
PS4API int sceNgs2VoiceGetStateFlags() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("xa8oL9dmXkM", sceNgs2PanInit);
    fun("MzTa7VLjogY", sceNgs2RackLock);
    fun("u-WrYDaJA3k", sceNgs2SystemDestroy);
    fun("gbMKV+8Enuo", sceNgs2PanGetVolumeMatrix);
    fun("vU7TQ62pItw", sceNgs2SystemGetInfo);
    fun("jjBVvPN9964", sceNgs2VoiceGetMatrixInfo);
    fun("cLV4aiT9JpA", sceNgs2RackCreate);
    fun("t9T0QM17Kvo", sceNgs2ParseWaveformUser);
    fun("U546k6orxQo", sceNgs2RackCreateWithAllocator);
    fun("iprCTXPVWMI", sceNgs2ParseWaveformFile);
    fun("rEh728kXk3w", sceNgs2VoiceGetStateFlags);
    fun("0eFLVCfWVds", sceNgs2RackQueryBufferSize);
    fun("1WsleK-MTkE", sceNgs2GeomCalcListener);
    fun("W-Z8wWMBnhk", sceNgs2VoiceGetOwner);
    fun("uu94irFOGpA", sceNgs2VoiceControl);
    fun("eF8yRCC6W64", sceNgs2GeomApply);
    fun("++YZ7P9e87U", sceNgs2RackUnlock);
    fun("-tbc2SxQD60", sceNgs2SystemSetSampleRate);
    fun("3pCNbVM11UA", sceNgs2CalcWaveformBlock);
    fun("pgFAiLR5qT4", sceNgs2SystemQueryBufferSize);
    fun("l4Q2dWEH6UM", sceNgs2SystemSetGrainSamples);
    fun("6qN1zaEZuN0", sceNgs2CustomRackGetModuleInfo);
    fun("GZB2v0XnG0k", sceNgs2SystemSetUserData);
    fun("gThZqM5PYlQ", sceNgs2SystemLock);
    fun("vubFP0T6MP0", sceNgs2SystemEnumHandles);
    fun("MwmHz8pAdAo", sceNgs2RackGetVoiceHandle);
    fun("7Lcfo8SmpsU", sceNgs2GeomResetListenerParam);
    fun("hyVLT2VlOYk", sceNgs2ParseWaveformData);
    fun("ekGJmmoc8j4", sceNgs2GetWaveformFrameInfo);
    fun("lCqD7oycmIM", sceNgs2RackDestroy);
    fun("-TOuuAQ-buE", sceNgs2VoiceGetState);
    fun("U-+7HsswcIs", sceNgs2SystemEnumRackHandles);
    fun("JXRC5n0RQls", sceNgs2SystemUnlock);
    fun("JNTMIaBIbV4", sceNgs2RackSetUserData);
    fun("Mn4XNDg03XY", sceNgs2RackGetUserData);
    fun("mPYgU4oYpuY", sceNgs2SystemCreateWithAllocator);
    fun("nPzb7Ly-VjE", sceNgs2ReportUnregisterHandler);
    fun("uBIN24Tv2MI", sceNgs2ReportRegisterHandler);
    fun("4lFaRxd-aLs", sceNgs2SystemGetUserData);
    fun("M4LYATRhRUE", sceNgs2RackGetInfo);
    fun("koBbCMvOKWw", sceNgs2SystemCreate);
    fun("i0VnXM-C9fc", sceNgs2SystemRender);
    fun("WCayTgob7-o", sceNgs2VoiceGetPortInfo);
    fun("0lbbayqDNoE", sceNgs2GeomResetSourceParam);
#undef fun
}
} // namespace libSceNgs2
