#include "libSceRemoteplay.h"

namespace libSceRemoteplay {
PS4API int sceRemoteplayInitialize(void *pHeap, size_t heapSize) {
    LOG_DEBUG("dummy");
    return 0;
}
PS4API void sceRemoteplayTerminate() {
    UNIMPLEMENTED_FUNC();
}
PS4API int  sceRemoteplayGetConnectionStatus(SceUserServiceUserId userId, int *pStatus) {
    LOG_DEBUG("dummy");
    *pStatus = 0;
    return 0;
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("k1SwgkMSOM8", sceRemoteplayInitialize);
    fun("BOwybKVa3Do", sceRemoteplayTerminate);
    fun("g3PNjYKWqnQ", sceRemoteplayGetConnectionStatus);
#undef fun
}
}
