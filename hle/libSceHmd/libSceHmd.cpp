#include "libSceHmd.h"

namespace libSceHmd {

PS4API int sceHmdClose() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdGet2DEyeOffset() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdGetDeviceInformation() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdGetDeviceInformationByHandle() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdGetFieldOfView() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInitialize() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInitialize315() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternal3dAudioOpen() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalAnotherScreenClose() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalAnotherScreenGetAudioStatus() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalAnotherScreenGetFadeState() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalAnotherScreenGetVideoStatus() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalAnotherScreenOpen() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalAnotherScreenSendAudio() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalAnotherScreenSendVideo() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalAnotherScreenSetFadeAndSwitch() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalBindDeviceWithUserId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalDfuCheckAfterPvt() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalDfuCheckPartialUpdateAvailable() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalDfuGetStatus() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalDfuOpen() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalDfuSetMode() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalGetHmuSerialNumber() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalGetPUSerialNumber() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalGetPUVersion() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalGetStatusReport() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalMirroringModeSetAspect() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalMmapGetModeId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalSeparateModeClose() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalSeparateModeGetAudioStatus() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalSeparateModeGetVideoStatus() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalSeparateModeOpen() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalSeparateModeSendAudio() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalSeparateModeSendVideo() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalSetBrightness() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalSetDeviceConnection() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalSetHmuPowerControl() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalSetIPD() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalSetPortConnection() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalSetPortStatus() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalSocialScreenGetFadeState() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalSocialScreenSetFadeAndSwitch() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdInternalSocialScreenSetOutput() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdOpen() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionAddDisplayBuffer() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionClearUserEventEnd() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionClearUserEventStart() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionDebugGetLastInfo() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionFinalize() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionInitialize() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionQueryGarlicBuffAlign() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionQueryGarlicBuffSize() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionQueryOnionBuffAlign() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionQueryOnionBuffSize() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionSetCallback() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionSetDisplayBuffers() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionSetOutputMinColor() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionSetUserEventEnd() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionSetUserEventStart() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionStart() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionStart2dVr() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionStartMultilayer() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionStartMultilayer2() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionStartWideNear() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionStartWideNearWithOverlay() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionStartWithOverlay() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionStop() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionUnsetCallback() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdReprojectionUnsetDisplayBuffers() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceHmdTerminate() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("-6FjKlMA+Yc", sceHmdInternalSocialScreenSetOutput);
    fun("d2g5Ij7EUzo", sceHmdOpen);
    fun("q3e8+nEguyE", sceHmdReprojectionStart2dVr);
    fun("NPQwYFqi0bs", sceHmdGetFieldOfView);
    fun("v243mvYg0Y0", sceHmdInternalSetPortStatus);
    fun("6RclvsKxr3I", sceHmdInternalDfuCheckAfterPvt);
    fun("uGyN1CkvwYU", sceHmdInternalGetStatusReport);
    fun("4hTD8I3CyAk", sceHmdInternalMirroringModeSetAspect);
    fun("MdV0akauNow", sceHmdReprojectionDebugGetLastInfo);
    fun("UhFPniZvm8U", sceHmdInternalGetHmuSerialNumber);
    fun("roHN4ml+tB8", sceHmdInternalSetBrightness);
    fun("d2TeoKeqM5U", sceHmdInternalSeparateModeClose);
    fun("0cQDAbkOt2A", sceHmdInternalAnotherScreenOpen);
    fun("mKa8scOc4-k", sceHmdReprojectionStartWideNearWithOverlay);
    fun("1pxQfif1rkE", sceHmdGetDeviceInformationByHandle);
    fun("NTIbBpSH9ik", sceHmdReprojectionAddDisplayBuffer);
    fun("7as0CjXW1B8", sceHmdReprojectionSetUserEventStart);
    fun("Asczi8gw1NM", sceHmdInternalAnotherScreenSendAudio);
    fun("ZrV5YIqD09I", sceHmdReprojectionFinalize);
    fun("+PDyXnclP5w", sceHmdInternalGetPUVersion);
    fun("gCjTEtEsOOw", sceHmdInternalMmapGetModeId);
    fun("bl4MkWNLxKs", sceHmdInternalDfuSetMode);
    fun("s5EqYh5kbwM", sceHmdInternalSocialScreenGetFadeState);
    fun("s-J66ar9g50", sceHmdInitialize315);
    fun("0wnZViigP9o", sceHmdReprojectionUnsetCallback);
    fun("vzMEkwBQciM", sceHmdReprojectionStop);
    fun("UTqrWB+1+SU", sceHmdInternalBindDeviceWithUserId);
    fun("Wr5KVtyVDG0", sceHmdInternalAnotherScreenGetAudioStatus);
    fun("kLUAkN6a1e8", sceHmdReprojectionQueryOnionBuffSize);
    fun("3JyuejcNhC0", sceHmdReprojectionStartWideNear);
    fun("cE99PJR6b8w", sceHmdInternalDfuCheckPartialUpdateAvailable);
    fun("6CRWGc-evO4", sceHmdReprojectionSetCallback);
    fun("z0KtN1vqF2E", sceHmdReprojectionQueryGarlicBuffSize);
    fun("LjdLRysHU6Y", sceHmdReprojectionSetOutputMinColor);
    fun("6+v7m1vwE+0", sceHmdInternalAnotherScreenSendVideo);
    fun("knyIhlkpLgE", sceHmdReprojectionSetUserEventEnd);
    fun("a1LMFZtK9b0", sceHmdInternalSocialScreenSetFadeAndSwitch);
    fun("eOOeG9SpEuc", sceHmdInternalSeparateModeGetVideoStatus);
    fun("3jcyx7XOm7A", sceHmdInternalGetPUSerialNumber);
    fun("TkcANcGM0s8", sceHmdReprojectionQueryGarlicBuffAlign);
    fun("QasPTUPWVZE", sceHmdInternalAnotherScreenClose);
    fun("K4KnH0QkT2c", sceHmdInitialize);
    fun("OuygGEWkins", sceHmdReprojectionInitialize);
    fun("LkBkse9Pit0", sceHmdInternalSetPortConnection);
    fun("dv2RqD7ZBd4", sceHmdInternalDfuOpen);
    fun("U9kPT4g1mFE", sceHmdInternalSetHmuPowerControl);
    fun("E0BLvy57IiQ", sceHmdInternalAnotherScreenSetFadeAndSwitch);
    fun("WxsnAsjPF7Q", sceHmdInternalSeparateModeGetAudioStatus);
    fun("kcldQ7zLYQQ", sceHmdReprojectionStartWithOverlay);
    fun("8gH1aLgty5I", sceHmdReprojectionStartMultilayer);
    fun("z-RMILqP6tE", sceHmdTerminate);
    fun("gA4Xnn+NSGk", sceHmdInternalSeparateModeOpen);
    fun("6biw1XHTSqQ", sceHmdClose);
    fun("IWybWbR-xvA", sceHmdReprojectionQueryOnionBuffAlign);
    fun("E+dPfjeQLHI", sceHmdReprojectionSetDisplayBuffers);
    fun("mdyFbaJj66M", sceHmdReprojectionClearUserEventStart);
    fun("5f-6lp7L5cY", sceHmdInternalDfuGetStatus);
    fun("w8BEUsIYn8w", sceHmdInternalAnotherScreenGetVideoStatus);
    fun("gqAG7JYeE7A", sceHmdReprojectionStartMultilayer2);
    fun("BWY-qKM5hxE", sceHmdGet2DEyeOffset);
    fun("iGNNpDDjcwo", sceHmdReprojectionUnsetDisplayBuffers);
    fun("4KIjvAf8PCA", sceHmdInternalSetIPD);
    fun("dntZTJ7meIU", sceHmdReprojectionStart);
    fun("stQ7AsondmE", sceHmdInternalSeparateModeSendAudio);
    fun("wHnZU1qtiqw", sceHmdInternal3dAudioOpen);
    fun("hyATMTuQSoQ", sceHmdInternalSetDeviceConnection);
    fun("thDt9upZlp8", sceHmdGetDeviceInformation);
    fun("94+Ggm38KCg", sceHmdReprojectionClearUserEventEnd);
    fun("whRxl6Hhrzg", sceHmdInternalAnotherScreenGetFadeState);
    fun("jfnS-OoDayM", sceHmdInternalSeparateModeSendVideo);
#undef fun
}
}
