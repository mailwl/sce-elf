#include "libScePngDec.h"
#include "lodepng.h"

#include <cstring>

namespace libScePngDec {
typedef struct ScePngDecParseParam {
    const void* pngMemAddr;
    uint32_t pngMemSize;
    uint32_t reserved0;
} ScePngDecParseParam;

typedef struct ScePngDecImageInfo {
    uint32_t imageWidth;
    uint32_t imageHeight;
    uint16_t colorSpace;
    uint16_t bitDepth;
    uint32_t imageFlag;
} ScePngDecImageInfo;

typedef struct ScePngDecCreateParam {
    uint32_t thisSize;
    uint32_t attribute;
    uint32_t maxImageWidth;
} ScePngDecCreateParam;

typedef struct ScePngDecDecodeParam {
    const void* pngMemAddr;
    void* imageMemAddr;
    uint32_t pngMemSize;
    uint32_t imageMemSize;
    uint16_t pixelFormat;
    uint16_t alphaValue;
    uint32_t imagePitch;
} ScePngDecDecodeParam;

typedef void* ScePngDecHandle;
//PS4API int32_t scePngDecQueryMemorySize(const ScePngDecCreateParam* param) {
//    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
//    return 0; // TODO: SIZE
//}
//PS4API int32_t scePngDecCreate(const ScePngDecCreateParam* param, void* memoryAddress, uint32_t memorySize,
//                               ScePngDecHandle* handle) {
//    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
//    return 0;
//}
//PS4API int32_t scePngDecParseHeader(const ScePngDecParseParam* param, ScePngDecImageInfo* imageInfo) {
//    LOG_DEBUG("Unimplemented %s(%p, %p)", __FUNCTION__, param, imageInfo);
//    int32_t rc = 0;
//    return rc;
//}
PS4API int scePngDecDecode(ScePngDecHandle handle, const ScePngDecDecodeParam* param,
                               ScePngDecImageInfo* imageInfo) {
    LOG_DEBUG("%s(%p, %p, %p)", __FUNCTION__, handle, param, imageInfo);

    unsigned char* image = 0;
    unsigned width{}, height{};
    //unsigned error = lodepng::decode(image, width, height, (const unsigned char*)param->imageMemAddr, param->imageMemSize);
    int error = lodepng_decode_memory(&image, &width, &height, (const unsigned char*)param->pngMemAddr, param->pngMemSize, LCT_RGBA, 32u);
    if (error != 0)
        return 0;
    std::memcpy(param->imageMemAddr, image, param->imageMemSize);
    free(image);
    int rc = ((uint16_t)width << 16u) | (uint16_t)height;
    return rc;
}
//PS4API int32_t scePngDecDelete(ScePngDecHandle handle) {
//    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
//    return 0;
//}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    //fun("-6srIGbLTIU", scePngDecQueryMemorySize);
    //fun("m0uW+8pFyaw", scePngDecCreate);
    //fun("U6h4e5JRPaQ", scePngDecParseHeader);
    fun("WC216DD3El4", scePngDecDecode);
    //fun("QbD+eENEwo8", scePngDecDelete);
#undef fun
}
} // namespace libScePngDec
