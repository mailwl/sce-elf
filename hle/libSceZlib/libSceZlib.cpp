#include "libSceZlib.h"

namespace libSceZlib {
    PS4API void sceZlibFinalize() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceZlibGetResult() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceZlibInflate() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceZlibInitialize() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceZlibWaitForDone() {
        UNIMPLEMENTED_FUNC();
    }

    void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
        fun("6na+Sa-B83w", sceZlibFinalize); // libSceZlib
        fun("2eDcGHC0YaM", sceZlibGetResult); // libSceZlib
        fun("TLar1HULv1Q", sceZlibInflate); // libSceZlib
        fun("m1YErdIXCp4", sceZlibInitialize); // libSceZlib
        fun("uB8VlDD4e0s", sceZlibWaitForDone); // libSceZlib
#undef fun
    }
}
