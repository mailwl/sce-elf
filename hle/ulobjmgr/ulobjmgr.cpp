
#include "ulobjmgr.h"

namespace ulobjmgr {

PS4API int SweJO7t3pkk(void* param) {
    LOG_DEBUG("Unimplemented: %p", param);
    return 0;
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    //fun("SweJO7t3pkk", SweJO7t3pkk);
#undef fun
}
}
