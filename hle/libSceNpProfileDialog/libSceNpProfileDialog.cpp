#include "libSceNpProfileDialog.h"

namespace libSceNpProfileDialog {

PS4API int sceNpProfileDialogClose() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpProfileDialogGetResult() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpProfileDialogGetStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpProfileDialogInitialize() {
    return 0;
}
PS4API int sceNpProfileDialogOpen() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpProfileDialogOpenA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpProfileDialogTerminate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpProfileDialogUpdateStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("haVZE9FgKqE", sceNpProfileDialogUpdateStatus);
    fun("nrQRlLKzdwE", sceNpProfileDialogOpenA);
    fun("uj9Cz7Tk0cc", sceNpProfileDialogOpen);
    fun("wkwjz0Xdo2A", sceNpProfileDialogClose);
    fun("Lg+NCE6pTwQ", sceNpProfileDialogInitialize);
    fun("8rhLl1-0W-o", sceNpProfileDialogGetResult);
    fun("3BqoiFOjSsk", sceNpProfileDialogGetStatus);
    fun("0Sp9vJcB1-w", sceNpProfileDialogTerminate);
#undef fun
}
}
