#include "libSceVideodec.h"

namespace libSceVideodec {
    PS4API void sceVideodecDecode() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceVideodecMapMemory() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceVideodecDeleteDecoder() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceVideodecQueryResourceInfo() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceVideodecCreateDecoder() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceVideodecReset() {
        UNIMPLEMENTED_FUNC();
    }

    void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
        fun("q0W5GJMovMs", sceVideodecDecode); // libSceVideodec
        fun("jeigLlKdp5I", sceVideodecMapMemory); // libSceVideodec
        fun("U0kpGF1cl90", sceVideodecDeleteDecoder); // libSceVideodec
        fun("leCAscipfFY", sceVideodecQueryResourceInfo); // libSceVideodec
        fun("qkgRiwHyheU", sceVideodecCreateDecoder); // libSceVideodec
        fun("f8AgDv-1X8A", sceVideodecReset); // libSceVideodec
#undef fun
    }
}
