#include "libSceVoice.h"

namespace libSceVoice {
    PS4API void sceVoiceInit() {
        LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    }

    PS4API void sceVoiceCreatePort() {
        LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    }

    PS4API void sceVoiceStart() {
        LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    }

    PS4API void sceVoiceConnectIPortToOPort() {
        LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    }

    PS4API void sceVoiceSetVolume() {
        LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    }

    PS4API void sceVoiceGetVolume() {
        LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    }

    PS4API void sceVoiceDeletePort() {
        LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    }

    PS4API void sceVoiceStop() {
        LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    }

    PS4API void sceVoiceEnd() {
        LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    }

    PS4API void sceVoiceGetPortInfo() {
        LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    }

    PS4API void sceVoiceReadFromOPort() {
        LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    }

    PS4API void sceVoiceWriteToIPort() {
        LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    }

    PS4API void sceVoiceSetMuteFlagAll() {
        LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    }

    PS4API void sceVoiceSetMuteFlag() {
        LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    }

    PS4API void sceVoiceGetMuteFlag() {
        LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    }

    void load(export_funcs &exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
        fun("9TrhuGzberQ", sceVoiceInit); // libSceVoice
        fun("nXpje5yNpaE", sceVoiceCreatePort); // libSceVoice
        fun("54phPH2LZls", sceVoiceStart); // libSceVoice
        fun("oV9GAdJ23Gw", sceVoiceConnectIPortToOPort); // libSceVoice
        fun("QBFoAIjJoXQ", sceVoiceSetVolume); // libSceVoice
        fun("jjkCjneOYSs", sceVoiceGetVolume); // libSceVoice
        fun("b7kJI+nx2hg", sceVoiceDeletePort); // libSceVoice
        fun("Ao2YNSA7-Qo", sceVoiceStop); // libSceVoice
        fun("Oo0S5PH7FIQ", sceVoiceEnd); // libSceVoice
        fun("CrLqDwWLoXM", sceVoiceGetPortInfo); // libSceVoice
        fun("cQ6DGsQEjV4", sceVoiceReadFromOPort); // libSceVoice
        fun("YeJl6yDlhW0", sceVoiceWriteToIPort); // libSceVoice
        fun("oUha0S-Ij9Q", sceVoiceSetMuteFlagAll); // libSceVoice
        fun("gwUynkEgNFY", sceVoiceSetMuteFlag); // libSceVoice
        fun("Pc4z1QjForU", sceVoiceGetMuteFlag); // libSceVoice
#undef fun
    }
}
