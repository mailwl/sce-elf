#include "libSceRegMgr.h"

namespace libSceRegMgr {

PS4API int sceRegMgrBackupNeedMem() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrBackupPullData() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrBackupPushData() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrCheckError() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrCntlGetFilesCount() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrDrvDataCheckGet() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrDrvDataClose() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrDrvDataOpen() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrDrvGetEntCnt() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrDrvGetUpdateCnt() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrEvtGetCnt() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrEvtGetRegId() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrGetBin() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrGetBinInitVal() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrGetInt() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrGetIntInitVal() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrGetStr() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrGetStrInitVal() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrGetVersion() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrIsChange() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrIsInitOK() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrLogPull() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrLogStart() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrNonSysCheckError() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrNonSysGetBin() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrNonSysGetInt() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrNonSysGetStr() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrNonSysSetBin() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrNonSysSetInt() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrNonSysSetStr() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrResetVal() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrSetBin() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrSetInitLevel() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrSetInt() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrSetStr() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrSrvCnvRegionInt() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrSrvCnvRegionStr() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrSrvGetMachineType() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrSrvGetQAFforReg() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrSrvGetRealMachineType() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrSrvGetRegion() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrSrvGetRegionStr() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrToolDataCheckGet() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrToolGetEntryCnt() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
PS4API int sceRegMgrToolGetUpdateCnt() {
    LOG_DEBUG("Unimplemented %s",__FUNCTION__);
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("jqyhb1oMgHw", sceRegMgrGetStrInitVal);
    fun("djHSzoTfixE", sceRegMgrEvtGetRegId);
    fun("eL7sMZaIdac", sceRegMgrResetVal);
    fun("gqPjTzsu9vU", sceRegMgrBackupPushData);
    fun("LZBANaFbPqM", sceRegMgrCheckError);
    fun("eXxrXkrLXa4", sceRegMgrSetBin);
    fun("9BhoVC0q85k", sceRegMgrBackupNeedMem);
    fun("voOYmhF1vsQ", sceRegMgrToolGetEntryCnt);
    fun("q+lPgqZniW4", sceRegMgrGetVersion);
    fun("Ds-BHoPDTHY", sceRegMgrDrvDataCheckGet);
    fun("wQlp94zzrWg", sceRegMgrSrvGetQAFforReg);
    fun("YRPHqS8TcnI", sceRegMgrSetInitLevel);
    fun("NqxMleeTiLs", sceRegMgrGetBin);
    fun("DKWSr89zMsI", sceRegMgrNonSysGetStr);
    fun("PPRCIaNpJJY", sceRegMgrNonSysSetInt);
    fun("6yU1KEvOl14", sceRegMgrNonSysSetBin);
    fun("Ena7T6bVG5s", sceRegMgrCntlGetFilesCount);
    fun("khaYelw1Ytc", sceRegMgrSetInt);
    fun("HGxgXwGSAzQ", sceRegMgrLogStart);
    fun("CUSk0qEDj7s", sceRegMgrSrvCnvRegionInt);
    fun("sywg-RnhZMA", sceRegMgrSrvGetRegion);
    fun("04zv+0zNJkg", sceRegMgrSrvGetMachineType);
    fun("PwmjM-dGut4", sceRegMgrSrvCnvRegionStr);
    fun("lwQpbDXKpbg", sceRegMgrDrvGetEntCnt);
    fun("rebo0q4yREE", sceRegMgrIsInitOK);
    fun("CTplLrrndUg", sceRegMgrGetStr);
    fun("yfNwXqOshk0", sceRegMgrLogPull);
    fun("i64ig3BF6f8", sceRegMgrGetIntInitVal);
    fun("HCUhvdmryuQ", sceRegMgrDrvDataClose);
    fun("x0oTVOySvTU", sceRegMgrSrvGetRealMachineType);
    fun("dhuH8HjNhUY", sceRegMgrGetBinInitVal);
    fun("dKeshzt29G4", sceRegMgrNonSysGetInt);
    fun("jQ0656do1V4", sceRegMgrToolGetUpdateCnt);
    fun("NwwkHSVFYBA", sceRegMgrDrvGetUpdateCnt);
    fun("ffInidSqRss", sceRegMgrSetStr);
    fun("f-qy0soAkfA", sceRegMgrBackupPullData);
    fun("VmQkpRjBp3s", sceRegMgrToolDataCheckGet);
    fun("xhrI4zhlBuA", sceRegMgrIsChange);
    fun("xCxMQ7Efh4k", sceRegMgrSrvGetRegionStr);
    fun("k9LC1z8kh-E", sceRegMgrNonSysGetBin);
    fun("mPYKD12UDQI", sceRegMgrGetInt);
    fun("AxUlf9UUxCg", sceRegMgrNonSysSetStr);
    fun("okzABktskwU", sceRegMgrDrvDataOpen);
    fun("Eq+fDFaK3i0", sceRegMgrNonSysCheckError);
    fun("grCFSFvMxgU", sceRegMgrEvtGetCnt);
#undef fun
}
}
