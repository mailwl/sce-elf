#include <cstring>
#include "libSceSystemService.h"

namespace libSceSystemService {

#define SCE_SYSTEM_SERVICE_PARAM_ID_LANG (1)    /*E Settings of language (Integer) */
#define SCE_SYSTEM_SERVICE_PARAM_ID_DATE_FORMAT (2)    /*E Date display format (Integer) */
#define SCE_SYSTEM_SERVICE_PARAM_ID_TIME_FORMAT (3)    /*E Time display format (Integer) */
#define SCE_SYSTEM_SERVICE_PARAM_ID_TIME_ZONE (4)    /*E Time zone (Integer, by offset) */
#define SCE_SYSTEM_SERVICE_PARAM_ID_SUMMERTIME (5)    /*E Daylight savings time (Integer, 0:Disabled, 1:Enabled) */
#define SCE_SYSTEM_SERVICE_PARAM_ID_SYSTEM_NAME (6)    /*E system name (String) */
#define SCE_SYSTEM_SERVICE_PARAM_ID_GAME_PARENTAL_LEVEL (7)    /*E Parental control level (Integer) */
#define SCE_SYSTEM_SERVICE_PARAM_ID_ENTER_BUTTON_ASSIGN	 (1000)   /*E Assignment of ENTER button (Integer) */

typedef int32_t SceSystemServiceParamId;

typedef struct _SceSystemServiceStatus {
    int32_t eventNum;
    bool isSystemUiOverlaid;
    bool isInBackgroundExecution;
    bool isCpuMode7CpuNormal;
    bool isGameLiveStreamingOnAir;
    bool isOutOfVrPlayArea;
    uint8_t reserved[];
} SceSystemServiceStatus;


PS4API int sceSystemServiceAddLocalProcess() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceAddLocalProcessForPsmKit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceChangeCpuClock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceChangeGpuClock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceChangeMemoryClockToDefault() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceChangeMemoryClockToMultiMediaMode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceChangeNumberOfGpuCu() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceDisableMusicPlayer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceDisablePersonalEyeToEyeDistanceSetting() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceDisableSuspendConfirmationDialog() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceEnablePersonalEyeToEyeDistanceSetting() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceEnableSuspendConfirmationDialog() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceGetAppFocusedAppStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceGetAppIdOfBigApp() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceGetAppIdOfMiniApp() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceGetAppStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceGetAppType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceGetDisplaySafeAreaInfo(int* a1) {
    *a1 = 0x3F800000;
    return 0;
}
PS4API int sceSystemServiceGetEventForDaemon() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceGetGpuLoadEmulationMode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceGetLocalProcessStatusList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceGetPSButtonEvent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceGetStatus(SceSystemServiceStatus* status) {
    LOG_DEBUG("%p", status);
    ::memset(status, 0, sizeof(SceSystemServiceStatus));
    status->eventNum = 2;
    status->isCpuMode7CpuNormal = true;
    status->isGameLiveStreamingOnAir = false;
    status->isInBackgroundExecution = false;
    status->isOutOfVrPlayArea = false;
    status->isSystemUiOverlaid = false;
    return 0;
}
PS4API int sceSystemServiceGetTitleWorkaroundInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceHideSplashScreen() {
    return 0;
}
PS4API int sceSystemServiceIsAppSuspended() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceIsScreenSaverOn() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceIsShellUiFgAndGameBgCpuMode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceKillApp() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceKillLocalProcess() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceKillLocalProcessForPsmKit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceLaunchApp() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceLaunchEventDetails() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceLaunchWebBrowser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceLoadExec() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceNavigateToAnotherApp() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceNavigateToGoHome() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int32_t sceSystemServiceParamGetInt(
        SceSystemServiceParamId paramId,
        int32_t *value
) {
    LOG_DEBUG("%s(%d)\n",__FUNCTION__, paramId);
    switch(paramId) {
        case SCE_SYSTEM_SERVICE_PARAM_ID_LANG:
            *value = 8; //russian. 1 - english
            break;
        default:
            *value = 0;
            break;
    }
    return 0;
}
PS4API int sceSystemServiceParamGetString() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServicePowerTick() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceRaiseExceptionLocalProcess() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceReceiveEvent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceReenableMusicPlayer() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceRegisterDaemon() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceReportAbnormalTermination() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceResumeLocalProcess() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceSetControllerFocusPermission() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceSetGpuLoadEmulationMode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceShowDisplaySafeAreaSettings() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceSuspendBackgroundApp() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceSuspendLocalProcess() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSystemServiceTurnOffScreenSaver() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceShellCoreUtilGetFreeSizeOfAvContentsTmp() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceLncUtilUnblockAppSuspend() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceLncUtilGetAppStatus() {
    LOG_DEBUG("%s()\n",__FUNCTION__);
    return 0;
}
PS4API int sceLncUtilBlockAppSuspend() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceLncUtilTryBlockAppSuspend() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
//    fun("9ScDVErRRgw", sceSystemServiceNavigateToAnotherApp);
//    fun("1n37q1Bvc5Y", sceSystemServiceGetDisplaySafeAreaInfo);
//    fun("wX9wVFaegaM", sceSystemServiceLaunchEventDetails);
//    fun("KQFyDkgAjVs", sceSystemServiceIsShellUiFgAndGameBgCpuMode);
//    fun("x1UB9bwDSOw", sceSystemServiceDisableMusicPlayer);
//    fun("Pi3K47Xw0ss", sceSystemServiceRegisterDaemon);
//    fun("vY1-RZtvvbk", sceSystemServiceSuspendBackgroundApp);
//    fun("Mr1IgQaRff0", sceSystemServiceDisablePersonalEyeToEyeDistanceSetting);
//    fun("vOhqz-IMiW4", sceSystemServiceTurnOffScreenSaver);
//    fun("Rn32O5PDlmo", sceSystemServiceEnableSuspendConfirmationDialog);
//    fun("eLWnPuja+Y8", sceSystemServiceSetGpuLoadEmulationMode);
//    fun("tPfQU2pD4-M", sceSystemServiceShowDisplaySafeAreaSettings);
    fun("Vo5V8KAwCmk", sceSystemServiceHideSplashScreen);
//    fun("JFg3az5ITN4", sceSystemServiceGetEventForDaemon);
//    fun("DNE77sfNw5Y", sceSystemServiceSetControllerFocusPermission);
//    fun("d-15YTCUMVU", sceSystemServiceIsAppSuspended);
//    fun("xjE7xLfrLUk", sceSystemServiceGetAppFocusedAppStatus);
//    fun("fOsE5pTieqY", sceSystemServiceChangeMemoryClockToMultiMediaMode);
//    fun("cltshBrDLC0", sceSystemServiceAddLocalProcessForPsmKit);
//    fun("656LMQSrg6U", sceSystemServiceReceiveEvent);
//    fun("O3irWUQ2s-g", sceSystemServiceEnablePersonalEyeToEyeDistanceSetting);
    fun("rPo6tV8D9bM", sceSystemServiceGetStatus);
//    fun("SsC-m-S9JTA", sceSystemServiceParamGetString);
//    fun("f4oDTxAJCHE", sceSystemServiceGetAppIdOfBigApp);
//    fun("bMDbofWFNfQ", sceSystemServiceIsScreenSaverOn);
//    fun("N4RkyJh7FtA", sceSystemServiceKillApp);
//    fun("l4FB3wNa-Ac", sceSystemServiceLaunchApp);
//    fun("gbUBqHCEgAI", sceSystemServiceGetPSButtonEvent);
//    fun("qv+X8gozqF4", sceSystemServiceChangeMemoryClockToDefault);
//    fun("XbbJC3E+L5M", sceSystemServicePowerTick);
//    fun("7cTc7seJLfQ", sceSystemServiceKillLocalProcessForPsmKit);
//    fun("2xenlv7M-UU", sceSystemServiceRaiseExceptionLocalProcess);
//    fun("tMuzuZcUIcA", sceSystemServiceResumeLocalProcess);
//    fun("4imyVMxX5-8", sceSystemServiceGetGpuLoadEmulationMode);
//    fun("ZNIuJjqdtgI", sceSystemServiceGetLocalProcessStatusList);
//    fun("JoBqSQt1yyA", sceSystemServiceLoadExec);
//    fun("9kPCz7Or+1Y", sceSystemServiceReenableMusicPlayer);
//    fun("VrvpoJEoSSU", sceSystemServiceGetTitleWorkaroundInfo);
//    fun("Z5RgV4Chwxg", sceSystemServiceChangeGpuClock);
//    fun("ec72vt3WEQo", sceSystemServiceChangeCpuClock);
//    fun("6jpZY0WUwLM", sceSystemServiceKillLocalProcess);
//    fun("0cl8SuwosPQ", sceSystemServiceAddLocalProcess);
//    fun("fZo48un7LK4", sceSystemServiceParamGetInt);
//    fun("x2-o9eBw3ZU", sceSystemServiceNavigateToGoHome);
//    fun("BBSmGrxok5o", sceSystemServiceGetAppIdOfMiniApp);
//    fun("PQ+SjXAg3EM", sceSystemServiceDisableSuspendConfirmationDialog);
//    fun("kTiAx7e2zU4", sceSystemServiceSuspendLocalProcess);
//    fun("YLbhAXS20C0", sceSystemServiceGetAppType);
//    fun("t5ShV0jWEFE", sceSystemServiceGetAppStatus);
//    fun("5MLppFJZyX4", sceSystemServiceChangeNumberOfGpuCu);
//    fun("-+3hY+y8bNo", sceSystemServiceLaunchWebBrowser);
//    fun("3s8cHiCBKBE", sceSystemServiceReportAbnormalTermination);
//    fun("guf+xcMoCas", sceShellCoreUtilGetFreeSizeOfAvContentsTmp);
//    fun("iRZduYIV1hs", sceLncUtilUnblockAppSuspend); // libSceSystemService
//    fun("DxRki7T2E44", sceLncUtilGetAppStatus); // libSceSystemService
//    fun("u1JVDP28ycg", sceLncUtilBlockAppSuspend); // libSceSystemService
//    fun("msW-hp1U0zo", sceLncUtilTryBlockAppSuspend); // libSceSystemService
#undef fun
}
}
