#include "libSceHttp.h"

namespace libSceHttp {

typedef struct SceSslData {
    char *ptr;
    size_t size;
} SceSslData;

PS4API int sceHttpAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpAbortRequestForce() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpAbortWaitRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpAddCookie() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpAddRequestHeader() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpAddRequestHeaderRaw() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpAuthCacheExport() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpAuthCacheFlush() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpAuthCacheImport() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpCookieExport() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpCookieFlush() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpCookieImport() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpCreateConnection() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpCreateConnectionWithURL() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpCreateEpoll() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpCreateRequest2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpCreateRequestWithURL() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpCreateRequestWithURL2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpCreateTemplate(int libhttpCtxId,
                                 const char*userAgent,
                                 int httpVer,
                                 int autoProxyConf) {
    LOG_DEBUG("unimlemented");
    return 0;
}
PS4API int sceHttpDbgEnableProfile() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpDbgGetConnectionStat() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpDbgGetRequestStat() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpDbgSetPrintf() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpDbgShowConnectionStat() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpDbgShowMemoryPoolStat() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpDbgShowRequestStat() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpDbgShowStat() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpDeleteConnection() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpDeleteRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpDeleteTemplate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpDestroyEpoll() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpGetAcceptEncodingGZIPEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpGetAllResponseHeaders() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpGetAuthEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpGetAutoRedirect() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpGetConnectionStat() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpGetCookie() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpGetCookieEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpGetCookieStats() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpGetEpoll() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpGetEpollId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpGetLastErrno() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpGetMemoryPoolStats() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpGetNonblock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpGetResponseContentLength() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpGetStatusCode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpInit(int libnetMemId, int libsslCtxId, size_t poolSize) {
    LOG_DEBUG("Unimplemented (%d, %d, %zu)", libnetMemId, libsslCtxId, poolSize);
    return 1;
}
PS4API int sceHttpParseResponseHeader() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpParseStatusLine() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpReadData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpRedirectCacheFlush() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpRemoveRequestHeader() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpRequestGetAllHeaders() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpsDisableOption() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpsDisableOptionPrivate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpsEnableOption() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpsEnableOptionPrivate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSendRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetAcceptEncodingGZIPEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetAuthEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetAuthInfoCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetAutoRedirect() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetChunkedTransferEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetConnectTimeOut(int id,
                                    uint32_t usec) {
    LOG_DEBUG("dummy");
    return 0;
}
PS4API int sceHttpSetCookieEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetCookieMaxNum() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetCookieMaxNumPerDomain() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetCookieMaxSize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetCookieRecvCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetCookieSendCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetCookieTotalMaxSize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetDefaultAcceptEncodingGZIPEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetEpoll() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetEpollId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetHttp09Enabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetInflateGZIPEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetNonblock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetPolicyOption() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetPriorityOption() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetProxy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetRecvBlockSize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetRecvTimeOut(int id,
                                 uint32_t usec) {
    LOG_DEBUG("dummy");
    return 0;
}
PS4API int sceHttpSetRedirectCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetRequestContentLength() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetRequestStatusCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetResolveRetry() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetResolveTimeOut() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetResponseHeaderMaxSize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpSetSendTimeOut(int id,
                                 uint32_t usec) {
    LOG_DEBUG("dummy");
    return 0;
}
PS4API int sceHttpsFreeCaList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpsGetCaList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpsGetSslError() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpsLoadCert(int libhttpCtxId, int caCertNum, const SceSslData **caList, const SceSslData *cert, const SceSslData *privKey ) {
    LOG_DEBUG("Unimplemented (%d, %d, %p, ....)", libhttpCtxId, caCertNum, caList);
    return 0;
}
PS4API int sceHttpsSetSslCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpsSetSslVersion() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpsUnloadCert() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpTerm() {
    //UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpTryGetNonblock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpTrySetNonblock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpUnsetEpoll() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpUriBuild() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpUriCopy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpUriEscape() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpUriMerge() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpUriParse() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpUriSweepPath() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpUriUnescape() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceHttpWaitRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("+G+UsJpeXPc", sceHttpGetCookie);
    fun("gcUjwU3fa0M", sceHttpsGetCaList);
    fun("fzzBpJjm9Kw", sceHttpDbgGetRequestStat);
    fun("aCYPMSUIaP8", sceHttpGetAllResponseHeaders);
    fun("pk0AuomQM1o", sceHttpSetHttp09Enabled);
    fun("s2-NPIvz+iA", sceHttpSetNonblock);
    fun("Qq8SfuJJJqE", sceHttpParseStatusLine);
    fun("Kp6juCJUJGQ", sceHttpSetCookieMaxNumPerDomain);
    fun("T-mGo9f3Pu4", sceHttpSetAutoRedirect);
    fun("mSQCxzWTwVI", sceHttpsDisableOption);
    fun("mNan6QSnpeY", sceHttpAddCookie);
    fun("JBN6N-EY+3M", sceHttpsGetSslError);
    fun("7Y4364GBras", sceHttpSetCookieMaxSize);
    fun("P6A3ytpsiYc", sceHttpDeleteConnection);
    fun("gZ9TpeFQ7Gk", sceHttpSetPolicyOption);
    fun("16sMmVuOvgU", sceHttpGetMemoryPoolStats);
    fun("1rpZqxdMRwQ", sceHttpGetAcceptEncodingGZIPEnabled);
    fun("7j9VcwnrZo4", sceHttpGetEpoll);
    fun("KJtUHtp6y0U", sceHttpDbgShowConnectionStat);
    fun("5LZA+KPISVA", sceHttpUriBuild);
    fun("9m8EcOGzcIQ", sceHttpGetAuthEnabled);
    fun("Aeu5wVKkF9w", sceHttpCreateRequestWithURL);
    fun("zNGh-zoQTD0", sceHttpRemoveRequestHeader);
    fun("Ik-KpLTlf7Q", sceHttpTerm);
    fun("1e2BNwI-XzE", sceHttpSendRequest);
    fun("VmqSnjZ5mE4", sceHttpDbgSetPrintf);
    fun("YuOW3dDAKYc", sceHttpUriEscape);
    fun("L-DwVoHXLtU", sceHttpGetConnectionStat);
    fun("zzB0StvRab4", sceHttpAuthCacheFlush);
    fun("wYhXVfS2Et4", sceHttpDestroyEpoll);
    fun("U5ExQGyyx9s", sceHttpsSetSslVersion);
    fun("3lgQ5Qk42ok", sceHttpUriMerge);
    fun("qISjDHrxONc", sceHttpWaitRequest);
    fun("rGNm+FjIXKk", sceHttpCreateRequest2);
    fun("fmOs6MzCRqk", sceHttpTrySetNonblock);
    fun("iSZjWw1TGiA", sceHttpGetCookieEnabled);
    fun("0gYjPTR-6cY", sceHttpCreateTemplate);
    fun("nOkViL17ZOo", sceHttpCookieExport);
    fun("59tL1AQBb8U", sceHttpUnsetEpoll);
    fun("f42K37mm5RM", sceHttpsEnableOption);
    fun("PTiFIUxCpJc", sceHttpSetRequestContentLength);
    fun("6381dWF+xsQ", sceHttpCreateEpoll);
    fun("h9wmFZX4i-4", sceHttpSetRedirectCallback);
    fun("qe7oZ+v4PWA", sceHttpDeleteRequest);
    fun("xegFfZKBVlw", sceHttpSetSendTimeOut);
    fun("CR-l-yI-o7o", sceHttpUriCopy);
    fun("GnVDzYfy-KI", sceHttpSetCookieSendCallback);
    fun("i+quCZCL+D8", sceHttpSetProxy);
    fun("Kh6bS2HQKbo", sceHttpSetCookieRecvCallback);
    fun("yuO2H2Uvnos", sceHttpGetResponseContentLength);
    fun("mMcB2XIDoV4", sceHttpSetRecvBlockSize);
    fun("0S9tTH0uqTU", sceHttpSetConnectTimeOut);
    fun("LG1YW1Uhkgo", sceHttpSetEpollId);
    fun("P5pdoykPYTk", sceHttpReadData);
    fun("Cnp77podkCU", sceHttpCreateRequestWithURL2);
    fun("hPTXo3bICzI", sceHttpParseResponseHeader);
    fun("A9cVMUtEp4Y", sceHttpInit);
    fun("i9mhafzkEi8", sceHttpSetInflateGZIPEnabled);
    fun("oEuPssSYskA", sceHttpDbgShowMemoryPoolStat);
    fun("zXqcE0fizz0", sceHttpsUnloadCert);
    fun("pM--+kIeW-8", sceHttpSetCookieMaxNum);
    fun("DK+GoXCNT04", sceHttpsLoadCert);
    fun("pxBsD-X9eH0", sceHttpDbgShowStat);
    fun("I4+4hKttt1w", sceHttpsEnableOptionPrivate);
    fun("0onIrKx9NIE", sceHttpGetLastErrno);
    fun("HRX1iyDoKR8", sceHttpSetAcceptEncodingGZIPEnabled);
    fun("6gyx-I0Oob4", sceHttpDbgGetConnectionStat);
    fun("EY28T2bkN7k", sceHttpAddRequestHeader);
    fun("IQOP6McWJcY", sceHttpGetEpollId);
    fun("sWQiqKvYTVA", sceHttpAbortWaitRequest);
    fun("Y1DCjN-s2BA", sceHttpAuthCacheExport);
    fun("yigr4V0-HTM", sceHttpSetRecvTimeOut);
    fun("hvG6GfBMXg8", sceHttpAbortRequest);
    fun("tsGVru3hCe8", sceHttpCreateRequest);
    fun("jf4TB2nUO40", sceHttpSetAuthInfoCallback);
    fun("XNUoD2B9a6A", sceHttpSetCookieEnabled);
    fun("PDxS48xGQLs", sceHttpSetChunkedTransferEnabled);
    fun("7WcNoAI9Zcw", sceHttpsFreeCaList);
    fun("0a2TBNfE3BU", sceHttpGetStatusCode);
    fun("pFnXDxo3aog", sceHttpCookieImport);
    fun("vO4B-42ef-k", sceHttpSetRequestStatusCallback);
    fun("8kzIXsRy1bY", sceHttpSetDefaultAcceptEncodingGZIPEnabled);
    fun("qgxDBjorUxs", sceHttpCreateConnectionWithURL);
    fun("K1d1LqZRQHQ", sceHttpSetResolveRetry);
    fun("seCvUt91WHY", sceHttpCookieFlush);
    fun("V-noPEjSB8c", sceHttpTryGetNonblock);
    fun("-xm7kZQNpHI", sceHttpSetEpoll);
    fun("IWalAn-guFs", sceHttpUriParse);
    fun("4I8vEpuEhZ8", sceHttpDeleteTemplate);
    fun("zJYi5br6ZiQ", sceHttpsDisableOptionPrivate);
    fun("L2gM3qptqHs", sceHttpDbgShowRequestStat);
    fun("u05NnI+P+KY", sceHttpRedirectCacheFlush);
    fun("wF0KcxK20BE", sceHttpAuthCacheImport);
    fun("a4VsZ4oqn68", sceHttpSetResponseHeaderMaxSize);
    fun("lGAjftanhFs", sceHttpAddRequestHeaderRaw);
    fun("4fgkfVeVsGU", sceHttpRequestGetAllHeaders);
    fun("Lffcxao-QMM", sceHttpDbgEnableProfile);
    fun("Wq4RNB3snSQ", sceHttpGetNonblock);
    fun("qFg2SuyTJJY", sceHttpSetAuthEnabled);
    fun("htyBOoWeS58", sceHttpsSetSslCallback);
    fun("thTS+57zoLM", sceHttpUriUnescape);
    fun("JKl06ZIAl6A", sceHttpAbortRequestForce);
    fun("mmLexUbtnfY", sceHttpGetAutoRedirect);
    fun("pHc3bxUzivU", sceHttpSetCookieTotalMaxSize);
    fun("Kiwv9r4IZCc", sceHttpCreateConnection);
    fun("mUU363n4yc0", sceHttpUriSweepPath);
    fun("2NeZnMEP3-0", sceHttpSetPriorityOption);
    fun("xkymWiGdMiI", sceHttpGetCookieStats);
    fun("Tc-hAYDKtQc", sceHttpSetResolveTimeOut);
#undef fun
}
}
