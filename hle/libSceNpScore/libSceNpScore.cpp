#include "libSceNpScore.h"

namespace libSceNpScore {

PS4API int sceNpScoreAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreCensorComment() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreCensorCommentAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreChangeModeForOtherSaveDataOwners() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreCreateNpTitleCtx(int p1, int* p2) {
    LOG_DEBUG("unimplemented %s", __FUNCTION__);
    *p2 = 1;
    return 0;
}
PS4API int sceNpScoreCreateNpTitleCtxA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreCreateTitleCtx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreDeleteNpTitleCtx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreDeleteRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetBoardInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetBoardInfoAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetFriendsRanking() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetFriendsRankingA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetFriendsRankingAAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetFriendsRankingAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetGameData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetGameDataAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetRankingByAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetRankingByAccountIdAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetRankingByNpId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetRankingByNpIdAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetRankingByNpIdPcId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetRankingByNpIdPcIdAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetRankingByRange() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetRankingByRangeA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetRankingByRangeAAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreGetRankingByRangeAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScorePollAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreRecordGameData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreRecordGameDataAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreRecordScore() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreRecordScoreAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreSanitizeComment() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreSanitizeCommentAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreSetPlayerCharacterId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreSetThreadParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreSetTimeout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpScoreWaitAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("r4oAo9in0TA", sceNpScoreSanitizeComment);
    fun("LoVMVrijVOk", sceNpScoreGetBoardInfo);
    fun("K9tlODTQx3c", sceNpScoreGetRankingByAccountId);
    fun("zT0XBtgtOSI", sceNpScoreRecordScore);
    fun("gW8qyjYrUbk", sceNpScoreCreateRequest);
    fun("G0pE+RNCwfk", sceNpScoreDeleteNpTitleCtx);
    fun("bcoVwcBjQ9E", sceNpScoreRecordGameData);
    fun("1i7kmKbX6hk", sceNpScoreAbortRequest);
    fun("Q0Avi9kebsY", sceNpScoreGetBoardInfoAsync);
    fun("1gL5PwYzrrw", sceNpScoreRecordGameDataAsync);
    fun("ANJssPz3mY0", sceNpScoreRecordScoreAsync);
    fun("2b3TI0mDYiI", sceNpScoreCensorComment);
    fun("gMbOn+-6eXA", sceNpScoreGetFriendsRankingA);
    fun("Rd27dqUFZV8", sceNpScoreGetRankingByNpIdAsync);
    fun("fqk8SC63p1U", sceNpScoreWaitAsync);
    fun("MA9vSt7JImY", sceNpScoreGetRankingByRangeA);
    fun("dK8-SgYf6r4", sceNpScoreDeleteRequest);
    fun("8kuIzUw6utQ", sceNpScoreGetFriendsRanking);
    fun("KBHxDjyk-jA", sceNpScoreGetRankingByRange);
    fun("y5ja7WI05rs", sceNpScoreGetRankingByRangeAAsync);
    fun("9mZEgoiEq6Y", sceNpScoreGetRankingByNpId);
    fun("FsouSN0ykN8", sceNpScoreGetRankingByNpIdPcIdAsync);
    fun("qW9M0bQ-Zx0", sceNpScoreCreateTitleCtx);
    fun("zKoVok6FFEI", sceNpScoreGetGameData);
    fun("bygbKdHmjn4", sceNpScoreSetPlayerCharacterId);
    fun("KnNA1TEgtBI", sceNpScoreCreateNpTitleCtx);
    fun("3UVqGJeDf30", sceNpScoreSanitizeCommentAsync);
    fun("6-G9OxL5DKg", sceNpScoreGetFriendsRankingAAsync);
    fun("m1DfNRstkSQ", sceNpScorePollAsync);
    fun("S3xZj35v8Z8", sceNpScoreSetTimeout);
    fun("dTXC+YcePtM", sceNpScoreChangeModeForOtherSaveDataOwners);
    fun("dRszNNyGWkw", sceNpScoreGetRankingByAccountIdAsync);
    fun("4eOvDyN-aZc", sceNpScoreCensorCommentAsync);
    fun("ETS-uM-vH9Q", sceNpScoreGetRankingByNpIdPcId);
    fun("yxK68584JAU", sceNpScoreSetThreadParam);
    fun("GWnWQNXZH5M", sceNpScoreCreateNpTitleCtxA);
    fun("rShmqXHwoQE", sceNpScoreGetRankingByRangeAsync);
    fun("JjOFRVPdQWc", sceNpScoreGetGameDataAsync);
    fun("7SuMUlN7Q6I", sceNpScoreGetFriendsRankingAsync);
#undef fun
}
}
