#include "libSceNpCommon.h"

#include "process/process.h"
#include "hle/libkernel/libkernel_pthreads.h"

#define export(nid) process::get_export("libSceNpCommon", nid, true)

namespace libSceNpCommon {

PS4API int _sceNpAllocatorStrdup() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceNpAllocatorStrndup() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10Cancelable4InitEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10CancelableC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10CancelableD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10CancelableD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10EventQueue4ctorEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10EventQueue4dtorEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10EventQueue5AbortEt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10EventQueueD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10EventQueueD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10JsonParserD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np10JsonParserD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np12HttpTemplateC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np12HttpTemplateD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np12HttpTemplateD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np12WorkerThread10ThreadMainEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np12WorkerThreadC1EPNS0_9WorkQueueE() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np12WorkerThreadC2EPNS0_9WorkQueueE() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np12WorkerThreadD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np12WorkerThreadD1Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np12WorkerThreadD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np13JsonDocParserD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np13JsonDocParserD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np13RingBufMemoryD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np13RingBufMemoryD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np14CalloutContextC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np14CalloutContextD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np14CalloutContextD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np14JsonDocBuilderD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np14JsonDocBuilderD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np15CancelableScopeC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np15CancelableScopeD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np15CancelableScopeD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4Cond4ctorEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4Cond4dtorEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4Cond4InitEPKcPNS0_5MutexE() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4Cond4WaitEj() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4Cond6SignalEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4Cond7DestroyEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4Cond9SignalAllEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4CondC1Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4CondC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4CondD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4CondD1Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4CondD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np4TimeplERKS1_() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np5Mutex4ctorEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np5Mutex4dtorEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np5Mutex4InitEPKcj() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np5Mutex4LockEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np5Mutex6UnlockEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np5Mutex7DestroyEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np5MutexC1Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np5MutexC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np5MutexD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np5MutexD1Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np5MutexD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np5NpEnv8GetNpEnvEPS1_() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6Handle10CancelImplEi() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6Handle4InitEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6Handle7DestroyEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6HandleC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6HandleD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6HandleD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6ObjectdaEPv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6ObjectdaEPvR14SceNpAllocator() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6ObjectdlEPv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6ObjectdlEPvR14SceNpAllocator() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6Thread12DoThreadMainEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6Thread4ctorEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6Thread4dtorEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6Thread4InitEPKcimm() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6Thread4InitEPKNS1_5ParamE() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6Thread4JoinEPi() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6Thread5StartEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6Thread7DestroyEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6Thread9EntryFuncEPv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6Thread9GetResultEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6Thread9IsRunningEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6ThreadC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6ThreadD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6ThreadD1Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np6ThreadD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np7Callout4StopEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np7Callout9IsStartedEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np7CalloutD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np7CalloutD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np7HttpUriD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np7HttpUriD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np7RingBuf4ctorEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np7RingBuf4dtorEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np7RingBuf5ClearEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np7RingBuf7DestroyEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np7RingBufC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np7RingBufD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np7RingBufD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8NpCommId5ClearEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8NpCommIdC2ERKS1_() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8NpCommIdC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8NpCommIdD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8NpCommIdD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8WorkItem10SetPendingEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8WorkItem10SetRunningEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8WorkItem11SetFinishedEi() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8WorkItem14FinishCallbackEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8WorkItem15RemoveFromQueueEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8WorkItem6CancelEi() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8WorkItem9BindQueueEPNS0_9WorkQueueEi() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8WorkItemC2EPKc() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8WorkItemD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8WorkItemD1Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np8WorkItemD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9EventFlag3SetEm() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9EventFlag4OpenEPKc() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9EventFlag4PollEmjPm() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9EventFlag5ClearEm() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9EventFlag6CancelEm() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9EventFlag7DestroyEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9EventFlagC1Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9EventFlagC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9EventFlagD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9EventFlagD1Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9EventFlagD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9HttpTrans7DestroyEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9HttpTransD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9HttpTransD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9JsonValueD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9JsonValueD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9RefObject6AddRefEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9RefObject7ReleaseEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9RefObjectC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9RefObjectD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9RefObjectD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9Semaphore4WaitEj() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9Semaphore7DestroyEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9SemaphoreC1Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9SemaphoreC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9SemaphoreD1Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9SemaphoreD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue11GetItemByIdEi() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue15GetFinishedItemENS0_14WorkItemStatusE() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue16WorkItemFinishedEPNS0_8WorkItemEi() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue17ProcFinishedItemsENS0_14WorkItemStatusE() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue18RemoveFinishedItemEPNS0_8WorkItemE() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue18WaitForPendingItemEPPNS0_8WorkItemEPb() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue4ctorEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue4dtorEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue4InitEPKNS0_6Thread5ParamE() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue4StopEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue5StartEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue6CancelEii() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue6IsInitEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue7DestroyEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue7EnqueueEiPNS0_8WorkItemE() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue9CancelAllEi() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueue9IsRunningEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueueC1Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueueC2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueueD0Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueueD1Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2np9WorkQueueD2Ev() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2npeqERKNS0_4TimeES3_() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2npgeERKNS0_4TimeES3_() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2npgtERKNS0_4TimeES3_() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2npleERKNS0_4TimeES3_() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2npltERKNS0_4TimeES3_() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZN3sce2npneERKNS0_4TimeES3_() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZNK3sce2np4Cond6IsInitEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZNK3sce2np5Mutex6IsInitEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZNK3sce2np6Handle6IsInitEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZNK3sce2np6Thread6IsInitEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZNK3sce2np7RingBuf6IsFullEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZNK3sce2np7RingBuf7IsEmptyEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZNK3sce2np8NpCommId7IsEmptyEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _ZNK3sce2np9HttpTrans6IsInitEv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAllocateKernelMemoryNoAlignment() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAllocateKernelMemoryWithAlignment() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpArchInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpArchTerm() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAtomicCas32() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAtomicDec32() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpAtomicInc32() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpBase64Decoder() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpBase64Encoder() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpBase64GetDecodeSize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpBase64UrlDecoder() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpBase64UrlEncoder() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpBase64UrlGetDecodeSize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCalloutInitCtx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCalloutStartOnCtx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCalloutStopOnCtx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCalloutTermCtx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpClearEventFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCmpNpId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCmpNpIdInOrder() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCmpOnlineId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCondDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCondInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCondSignal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCondSignalAll() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCondSignalTo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCondTimedwait() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCondWait() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCreateEventFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCreateSema() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpCreateThread() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpDeleteEventFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpDeleteSema() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpFreeKernelMemory() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetNavSdkVersion() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetPlatformType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetRandom() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetSdkVersion() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGetSystemClockUsec() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGlobalHeapGetAllocator() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpGlobalHeapGetAllocatorEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpHeapDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpHeapGetAllocator() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpHeapGetStat() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpHeapInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpHexToInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpInt32ToStr() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpInt64ToStr() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpIntIsOnlineIdString() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpIntIsValidOnlineId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpIntToHex() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpIpc2ClientInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpIpc2ClientTerm() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpJoinThread() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpJsonParse() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpJsonParse2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpJsonParse2Init() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpJsonParseEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpJsonParseInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLwMutexDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLwMutexInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLwMutexLock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLwMutexTryLock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpLwMutexUnlock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMemoryHeapDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMemoryHeapGetAllocator() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMemoryHeapGetAllocatorEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMemoryHeapInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMutexDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMutexInit(libkernel::ScePthreadMutex* mutex, const char* name, bool b) {
    LOG_DEBUG("calling native version");
    typedef PS4API int(*sceNpMutexInit_t)(libkernel::ScePthreadMutex*, const char*, bool);
    auto func = (sceNpMutexInit_t)export("uEwag-0YZPc");
    int rc = 0;
    if (func) {
        rc = func(mutex, name, b);
    }
    return rc;
}
PS4API int sceNpMutexLock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMutexTryLock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMutexUnlock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpPanic() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpPollEventFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpPollSema() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpRtcConvertToPosixTime() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpRtcFormatRFC3339() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpRtcParseRFC3339() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpServerErrorJsonParse() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpServerErrorJsonParseInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSetEventFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSetPlatformType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpSignalSema() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpStrBuildHex() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpStrnParseHex() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpStrParseHex() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpStrToInt32() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpStrToInt64() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpStrToUInt32() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpStrToUInt64() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpThreadGetId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUInt32ToStr() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUInt64ToStr() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilBuildTitleId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilCanonicalizeNpIdForPs4() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilCanonicalizeNpIdForPsp2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilCmpAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilGetDateSetAuto() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilGetDbgCommerce() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilGetEnv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilGetNpDebug() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilGetNpTestPatch() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilGetNthChar() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilGetSystemLanguage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilHttpUrlEncode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilJidToNpId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilJsonEscape() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilJsonGetOneChar() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilJsonUnescape() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilNpIdToJid() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilNumChars() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilParseJid() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilParseTitleId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilSerializeJid() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilXmlEscape() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilXmlGetOneChar() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpUtilXmlUnescape() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWaitEventFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWaitSema() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpXmlParse() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpXmlParseInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
//    fun("9pLoHoPMxeg", sceNpClearEventFlag);
//    fun("UbStlMKTBeU", sceNpServerErrorJsonParseInit);
//    fun("KQSxXJBepQ4", _ZN3sce2np9WorkQueue7EnqueueEiPNS0_8WorkItemE);
//    fun("6jFWpAfqAcc", sceNpCreateEventFlag);
//    fun("fElyBSn-l24", sceNpStrToInt32);
//    fun("uMJFOA62mVU", sceNpCondSignal);
//    fun("fhJ5uKzcn0w", sceNpCreateThread);
//    fun("3rGzxcMK-Mg", _ZN3sce2np8WorkItem15RemoveFromQueueEv);
//    fun("4YJ5gYtRAAE", sceNpUtilJsonUnescape);
//    fun("ovc4ZvD0YjY", _ZN3sce2np9WorkQueue18RemoveFinishedItemEPNS0_8WorkItemE);
//    fun("a5IfPlpchXI", sceNpBase64GetDecodeSize);
//    fun("wY9g+hVxLTM", _ZN3sce2np14CalloutContextD2Ev);
//    fun("ga4OW9MGahU", _ZN3sce2np8WorkItemD2Ev);
//    fun("dt0A2cWjwLs", _ZN3sce2np6HandleD2Ev);
//    fun("u+A16O-TAHk", _ZN3sce2np10JsonParserD0Ev);
//    fun("j4IAvbKKTzw", _ZN3sce2np14CalloutContextC2Ev);
//    fun("1x0jThSUr4w", _ZN3sce2np6ObjectdaEPv);
//    fun("SXUNKr9Zkv0", sceNpUtilGetSystemLanguage);
//    fun("EftEB4kmkSg", sceNpUtilJsonEscape);
//    fun("9YmBJ8KF9eI", sceNpPollEventFlag);
//    fun("JRtw5pROOiM", _ZN3sce2np9RefObjectC2Ev);
//    fun("zmOmSLnqlBQ", _ZN3sce2np9WorkQueue9CancelAllEi);
//    fun("xpLjHhJBhpo", _ZN3sce2np15CancelableScopeD2Ev);
//    fun("eYgHIWx0Hco", _ZN3sce2np5Mutex6UnlockEv);
//    fun("UskWpVWxSvg", sceNpArchTerm);
//    fun("pJlGhXEt5CU", sceNpGetRandom);
//    fun("oZyb9ktuCpA", sceNpMutexUnlock);
//    fun("bvJ207-8qBQ", _ZNK3sce2np7RingBuf6IsFullEv);
//    fun("PvGTq9AGFfk", _ZN3sce2np12WorkerThreadD1Ev);
//    fun("3I2FbiV6fhw", _ZNK3sce2np8NpCommId7IsEmptyEv);
//    fun("Pglk7zFj0DI", sceNpGetSdkVersion);
//    fun("Es-CwSVnalY", _ZN3sce2np9Semaphore7DestroyEv);
//    fun("xPrF2nGPBXQ", sceNpDeleteSema);
//    fun("dj+O5aD2a0Q", sceNpCmpOnlineId);
//    fun("Ls4eWDrbNmg", sceNpUtilXmlGetOneChar);
//    fun("3g7tdl9Ppds", _ZNK3sce2np7RingBuf7IsEmptyEv);
//    fun("05KEwpDf4Ls", _ZN3sce2np6ObjectdlEPvR14SceNpAllocator);
//    fun("-W28+9p1CKI", sceNpSignalSema);
//    fun("4mEAk-UKVNw", sceNpUtilCanonicalizeNpIdForPs4);
//    fun("MWPOkqzYss0", sceNpAllocateKernelMemoryNoAlignment);
//    fun("l67qBmMmKP4", sceNpBase64Decoder);
//    fun("I5uzTXxbziU", _ZN3sce2np4Cond7DestroyEv);
//    fun("ZbdPHUm7jOY", sceNpWaitEventFlag);
//    fun("cMOgkE2M2e8", _ZN3sce2np9EventFlagC1Ev);
//    fun("fZShld2PQ7w", sceNpCondWait);
//    fun("yvaNTRiKXmo", sceNpThreadGetId);
//    fun("vj04qzp7uKY", sceNpUtilJsonGetOneChar);
//    fun("-91vFSqiuKw", _ZN3sce2np8WorkItem11SetFinishedEi);
//    fun("-3gV5N2u-sc", _ZN3sce2np6Thread4dtorEv);
//    fun("KyB1IAY2BiU", sceNpUtilNpIdToJid);
//    fun("BtXPJQEg41Y", _ZN3sce2np4Cond4dtorEv);
//    fun("MEYMyfJxWXg", _ZN3sce2np6ThreadD0Ev);
//    fun("OoK0Ah0l1ko", _ZN3sce2np6Thread4InitEPKNS1_5ParamE);
//    fun("dfXSH2Tsjkw", sceNpMemoryHeapDestroy);
//    fun("SLPuaDLbeD4", _ZN3sce2np4Cond4WaitEj);
//    fun("TWPY1x1Atys", sceNpIntToHex);
//    fun("TcwEFnakiSc", sceNpCmpNpIdInOrder);
//    fun("LtkeQwMIEWY", sceNpRtcParseRFC3339);
//    fun("FfSNfBmn+K8", _ZN3sce2np9JsonValueD2Ev);
//    fun("1P-MUvbtyTM", _ZN3sce2np7RingBuf4ctorEv);
//    fun("8k1rNqvczTc", _ZN3sce2np9SemaphoreC2Ev);
//    fun("+9+kKMY9YIw", sceNpAtomicCas32);
//    fun("417JucZaE3g", _ZN3sce2np9RefObjectD2Ev);
//    fun("9zi9FTPol74", _ZN3sce2np5MutexD1Ev);
//    fun("sPti0OkVM8c", _ZN3sce2np6Thread7DestroyEv);
//    fun("EjMsfO3GCIA", sceNpJoinThread);
//    fun("GRvK1ZE+FEQ", sceNpUtilGetNpDebug);
//    fun("XeCZTzqIk2k", _ZN3sce2np8NpCommIdD0Ev);
//    fun("qy4V8O+snLU", _ZN3sce2np6Thread9IsRunningEv);
//    fun("slmKkuIoC28", _ZN3sce2np10EventQueue5AbortEt);
//    fun("LnjjzlJ+L5c", _ZN3sce2np8NpCommId5ClearEv);
//    fun("3ErXia+y89M", _ZN3sce2np7Callout9IsStartedEv);
//    fun("p5ZkSMRR7AU", sceNpJsonParseEx);
//    fun("lQ11BpMM4LU", sceNpMutexDestroy);
//    fun("WSQxnAVLKgw", sceNpServerErrorJsonParse);
//    fun("VMjIo2Z-aW0", sceNpRtcConvertToPosixTime);
//    fun("8kUkQPQP7bA", _ZN3sce2np10EventQueueD2Ev);
//    fun("N3FB4r8JoRE", sceNpUtilCanonicalizeNpIdForPsp2);
//    fun("lvek8w7yqyE", sceNpHeapGetAllocator);
//    fun("c+ssxRf1Si0", sceNpUtilNumChars);
//    fun("KCk4OGu8+sc", sceNpUtilGetNthChar);
//    fun("p5hRe1k4Wlg", _ZN3sce2np13JsonDocParserD0Ev);
//    fun("3CJl5ewd7-0", _ZN3sce2np6Thread4ctorEv);
//    fun("dGuEML7cY1k", _ZNK3sce2np9HttpTrans6IsInitEv);
//    fun("LotC9rVP3Lo", _ZN3sce2np9JsonValueD0Ev);
//    fun("D-dT+vERWmU", _ZN3sce2np7HttpUriD2Ev);
//    fun("70qFzq4z3UI", _ZN3sce2np13RingBufMemoryD0Ev);
//    fun("fEcrs9UPPyo", sceNpXmlParse);
//    fun("zurkNUps5o8", _ZN3sce2np9RefObject6AddRefEv);
//    fun("n9pzAHeCCVU", _ZN3sce2np4Cond4ctorEv);
//    fun("OA8f3KF9JsM", sceNpUtilGetNpTestPatch);
//    fun("9+NmoosRoBA", _ZN3sce2np7RingBufD2Ev);
//    fun("omf1GoUEJCA", _ZN3sce2np5MutexD0Ev);
//    fun("E4uoqSdo8ek", _ZN3sce2np10EventQueueD0Ev);
//    fun("owm52JoZ8uc", sceNpUtilGetDateSetAuto);
//    fun("2jdHoPpS+W0", sceNpHeapGetStat);
//    fun("XTi+x-uTX9o", sceNpJsonParse2Init);
//    fun("pp88xnRgJrM", _ZN3sce2np15CancelableScopeC2Ev);
//    fun("uj86YxCYid0", sceNpStrToUInt32);
//    fun("AKiHGWhC2KU", _ZN3sce2np4CondD0Ev);
//    fun("kdOC-2AE06w", _ZN3sce2np10CancelableD0Ev);
//    fun("wESN-qrVhOU", _ZN3sce2np9WorkQueue4dtorEv);
//    fun("Fx2UwoQVVmo", _ZN3sce2np7CalloutD0Ev);
//    fun("VM+CXTW4F-s", _ZN3sce2np5Mutex4LockEv);
//    fun("4DE+nnCVRPA", _ZN3sce2np9WorkQueue4StopEv);
//    fun("8i-vOVRVt5w", _ZN3sce2np9EventFlag3SetEm);
//    fun("fY4XQoA20i8", sceNpIntIsOnlineIdString);
//    fun("-9cU3y6rXVM", _ZN3sce2np9WorkQueue17ProcFinishedItemsENS0_14WorkItemStatusE);
//    fun("aTNOl9EB4V4", _ZN3sce2np5Mutex4InitEPKcj);
//    fun("B+yGIX1+BTI", sceNpHeapInit);
//    fun("CnDHI7sU+l0", _ZN3sce2np6ObjectdlEPv);
//    fun("hw-UPUK9T+w", _ZN3sce2np12HttpTemplateC2Ev);
//    fun("gimH2zdBANg", _ZN3sce2np12WorkerThread10ThreadMainEv);
//    fun("dnwItoXLoy4", _ZN3sce2np8WorkItemD1Ev);
//    fun("X6NVkdpRnog", _ZN3sce2np9WorkQueueC1Ev);
//    fun("pfJgSA4jO3M", sceNpAtomicInc32);
//    fun("CI7ciM21NXs", _ZN3sce2np5MutexD2Ev);
//    fun("I1kBZV6keO4", _ZN3sce2np5Mutex4ctorEv);
//    fun("1a+iY5YUJcI", sceNpCondDestroy);
//    fun("4zxevggtYrQ", sceNpLwMutexDestroy);
//    fun("29ftOGIrUCo", sceNpSetEventFlag);
//    fun("ne77q1GOlF8", _ZN3sce2np6Thread4JoinEPi);
//    fun("q2tsVO3lM4A", sceNpCondInit);
//    fun("BBtBjx9-bMI", _ZN3sce2np8NpCommIdC2Ev);
//    fun("zepqHjfGe0M", _ZN3sce2np8WorkItem14FinishCallbackEv);
//    fun("6RQRpTn+-cc", _ZN3sce2np4CondD2Ev);
//    fun("h6XPsGpHAtc", _ZN3sce2np12HttpTemplateD2Ev);
//    fun("f5L6ax7EWHk", _ZN3sce2np12WorkerThreadD0Ev);
//    fun("FaMNvjMA6to", sceNpMemoryHeapGetAllocator);
//    fun("wWTqVcTnep8", _ZN3sce2np4Cond4InitEPKcPNS0_5MutexE);
//    fun("kBON3bAtfGs", sceNpUtilGetEnv);
//    fun("XMIv42L5bEA", _ZN3sce2np9WorkQueue4ctorEv);
//    fun("hp0kVgu5Fxw", sceNpLwMutexTryLock);
//    fun("458yjI+OECI", sceNpUtilJidToNpId);
//    fun("ZOHgNNSZq4Q", _ZN3sce2np6HandleD0Ev);
//    fun("03UlDLFsTfw", _ZN3sce2np9EventFlag5ClearEm);
//    fun("hfJ1gGLgvq8", _ZN3sce2np7RingBufD0Ev);
//    fun("pu39pU8UgCo", sceNpBase64Encoder);
//    fun("w+C8QXqZKSw", _ZN3sce2np7HttpUriD0Ev);
//    fun("bsjWg59A7aE", sceNpCondSignalAll);
//    fun("fK5e+K6kSBg", _ZNK3sce2np6Thread6IsInitEv);
//    fun("Ehkz-BkTPwI", _ZN3sce2np6Thread12DoThreadMainEv);
//    fun("XUCjhejJvPc", _ZN3sce2np8WorkItem10SetRunningEv);
//    fun("rRN89jBArEM", sceNpUInt32ToStr);
//    fun("YKz2oBW3ZkM", _ZN3sce2np12WorkerThreadC1EPNS0_9WorkQueueE);
//    fun("ttA9TcO06uA", _ZN3sce2npleERKNS0_4TimeES3_);
//    fun("Y-I66cSNp+A", _ZN3sce2np8WorkItemD0Ev);
//    fun("UVLmT9lzRYA", _ZN3sce2npeqERKNS0_4TimeES3_);
//    fun("5tYi1l9CXD0", _ZN3sce2np9RefObject7ReleaseEv);
//    fun("1atFu71dFAU", _ZN3sce2np6Handle7DestroyEv);
//    fun("N1gnYosdK7Q", _ZN3sce2np10EventQueue4dtorEv);
//    fun("ifqJb-V1QZw", _ZN3sce2np6Handle4InitEv);
//    fun("xHAiSVEEjSI", sceNpMemoryHeapGetAllocatorEx);
//    fun("sjnIeFCuTD0", sceNpDeleteEventFlag);
//    fun("ss2xO9IJxKQ", sceNpCondTimedwait);
//    fun("cXYOwTVAuMs", _ZN3sce2np12HttpTemplateD0Ev);
//    fun("ajoqGz0D9Dw", sceNpUtilHttpUrlEncode);
//    fun("cnoM7EjlLe4", _ZN3sce2npneERKNS0_4TimeES3_);
//    fun("5Wy+JxpCBxg", _ZN3sce2np9EventFlag4OpenEPKc);
//    fun("in19gH7G040", sceNpCalloutStopOnCtx);
//    fun("OukNoRur97E", _ZN3sce2np9SemaphoreD2Ev);
//    fun("DuslmoqQ+nk", sceNpMutexTryLock);
//    fun("1Gfhi+tZ9IE", sceNpUtilGetDbgCommerce);
//    fun("rB0oqLSjH6g", _ZN3sce2np8NpCommIdC2ERKS1_);
//    fun("37Rd2JS+FCM", _ZN3sce2np9EventFlag4PollEmjPm);
//    fun("CwqYdG4TrjA", sceNpStrToInt64);
//    fun("teVnFAL6GNY", sceNpJsonParseInit);
//    fun("EFES6UR65oU", _ZN3sce2npgtERKNS0_4TimeES3_);
//    fun("T61vWaA+d9Q", sceNpJsonParse2);
//    fun("WWW4bvT-rSw", _ZN3sce2np9EventFlagD1Ev);
//    fun("okX7IjW0QsI", sceNpUtilSerializeJid);
//    fun("nW9XeX3eokI", _ZN3sce2np9SemaphoreD1Ev);
//    fun("F9khEfgTmsE", _ZN3sce2np4TimeplERKS1_);
//    fun("svAQxJ3yow4", _ZN3sce2npgeERKNS0_4TimeES3_);
//    fun("Yohe0MMDfj0", sceNpAtomicDec32);
//    fun("eTy3L1azX4E", _ZN3sce2np9WorkQueue9IsRunningEv);
//    fun("2e9GLlHTKA4", _ZN3sce2np9HttpTrans7DestroyEv);
//    fun("VnQolo6vTr4", _ZN3sce2np9WorkQueue5StartEv);
//    fun("oz2SlXNAnuI", sceNpUtilParseJid);
//    fun("EqX45DhWUpo", _ZN3sce2np6Thread4InitEPKcimm);
//    fun("IeNj+OcWgU8", sceNpBase64UrlEncoder);
//    fun("0Q5aKjYErBA", _ZN3sce2np6ThreadD1Ev);
//    fun("CI2p6Viee9w", sceNpIpc2ClientTerm);
//    fun("RgGW4f0ox1g", _ZN3sce2np5Mutex7DestroyEv);
//    fun("hP18CDS6eBU", _ZN3sce2np8NpCommIdD2Ev);
//    fun("NeopmYshD0U", _ZN3sce2np9WorkQueue7DestroyEv);
//    fun("vZXDqs2x7t0", _ZN3sce2np10CancelableD2Ev);
//    fun("wVdn78HKc30", sceNpHeapDestroy);
//    fun("Hvpr+otU4bo", sceNpHexToInt);
//    fun("VUHUasztbUY", sceNpGlobalHeapGetAllocatorEx);
//    fun("m9JzZSoDVFY", sceNpSetPlatformType);
//    fun("HoPC33siDD4", sceNpInt64ToStr);
//    fun("8DrClRz7Z2U", _ZN3sce2np9RefObjectD0Ev);
//    fun("UYAD7sUQcYU", _ZN3sce2np9WorkQueue16WorkItemFinishedEPNS0_8WorkItemEi);
//    fun("14PDhhMEBKY", _ZN3sce2np7Callout4StopEv);
//    fun("1QFKnDJxk3A", _ZN3sce2np9WorkQueueD1Ev);
//    fun("sXVQUIGmk2U", sceNpGetPlatformType);
//    fun("U0YoWwgg8aI", _ZN3sce2np9WorkQueue4InitEPKNS0_6Thread5ParamE);
//    fun("AIDhc3KCK7w", _ZN3sce2np9WorkQueueD2Ev);
//    fun("CQG2oyx1-nM", sceNpLwMutexUnlock);
//    fun("uHOOEbuzjEQ", _ZN3sce2np9EventFlagD0Ev);
//    fun("ebomQLbpptw", _ZN3sce2np7CalloutD2Ev);
//    fun("fClnlkZmA6k", sceNpCalloutStartOnCtx);
//    fun("6750DaF5Pas", _ZN3sce2np6ThreadD2Ev);
//    fun("E8yuDNYbzl0", _ZN3sce2np15CancelableScopeD0Ev);
//    fun("h1SWCcBdImo", sceNpStrnParseHex);
//    fun("-gN6uE+zWng", sceNpGlobalHeapGetAllocator);
//    fun("RpWWfCEs9xA", _ZN3sce2np9EventFlagD2Ev);
//    fun("QmDEFikd3VA", sceNpGetNavSdkVersion);
//    fun("MCLGkfBmw4c", sceNpXmlParseInit);
//    fun("5y0wMPQkaeU", sceNpInt32ToStr);
//    fun("bAHIOyNnx5Y", sceNpCondSignalTo);
//    fun("kZizwrFvWZY", sceNpMemoryHeapInit);
//    fun("6nW8WXQYRgM", _ZN3sce2np4CondC2Ev);
//    fun("hkeX9iuCwlI", sceNpIntIsValidOnlineId);
//    fun("moGcgMNTHvQ", sceNpBase64UrlDecoder);
//    fun("AqJ4xkWsV+I", sceNpCalloutTermCtx);
//    fun("kgDwlmy78k0", sceNpIpc2ClientInit);
//    fun("wJ-k9+UShJg", _ZN3sce2np9EventFlag6CancelEm);
//    fun("-hchsElmzXY", _ZN3sce2np4Cond9SignalAllEv);
//    fun("2YbS+GhInZQ", _ZN3sce2np8WorkItem10SetPendingEv);
//    fun("r9Bet+s6fKc", sceNpMutexLock);
//    fun("5bBPLZV49kY", sceNpUtilXmlEscape);
//    fun("jGF+MaB4b-M", sceNpArchInit);
//    fun("F2umEBpQFHc", _ZN3sce2np9WorkQueue11GetItemByIdEi);
//    fun("18j+qk6dRwk", sceNpLwMutexLock);
//    fun("gnwCmkY-V70", _ZN3sce2np6Thread9GetResultEv);
//    fun("-c9QK+CpQLg", _ZN3sce2np6Handle10CancelImplEi);
//    fun("CznMfhTIvVY", _ZN3sce2np9WorkQueue6IsInitEv);
//    fun("wM4q1JMisvA", _ZN3sce2np9WorkQueue15GetFinishedItemENS0_14WorkItemStatusE);
//    fun("vqekW3s-eFg", _ZN3sce2np10CancelableC2Ev);
//    fun("vJGDnNh4I0g", sceNpJsonParse);
//    fun("Yc+qj4TIEY0", _ZNK3sce2np5Mutex6IsInitEv);
//    fun("hQLw6eE4O44", _ZN3sce2np9Semaphore4WaitEj);
//    fun("O1AvlQU33pI", _ZN3sce2np5MutexC1Ev);
//    fun("7BjZKcN+oZ4", sceNpBase64UrlGetDecodeSize);
//    fun("0f3ylOQJwqE", _ZN3sce2np6ThreadC2Ev);
//    fun("ZoXUrTiwKNw", sceNpPanic);
//    fun("Ted2YU9lv94", sceNpStrToUInt64);
//    fun("p+bd65J177I", _ZN3sce2np9WorkQueueC2Ev);
//    fun("uphWwLZAuXA", _ZN3sce2np6Thread9EntryFuncEPv);
//    fun("W0YWLVDndx0", sceNpRtcFormatRFC3339);
//    fun("i5TP5NLmkoQ", sceNpStrBuildHex);
//    fun("1YezZ6DJXgY", _ZNK3sce2np4Cond6IsInitEv);
//    fun("WR4mjQeqz6s", _ZN3sce2np14CalloutContextD0Ev);
//    fun("Z68UCklzmHs", _ZNK3sce2np6Handle6IsInitEv);
//    fun("IZOGdJ+LFFU", _ZN3sce2np7RingBuf5ClearEv);
//    fun("6adrFGe2cpU", sceNpWaitSema);
//    fun("Oq5aepLkEWg", _ZN3sce2np8WorkItem6CancelEi);
//    fun("AvvE5A5A6ZA", _ZN3sce2np10JsonParserD2Ev);
//    fun("e2a1ZA+lJC4", _ZN3sce2np7RingBufC2Ev);
//    fun("2beu2bHw6qo", _ZN3sce2np5MutexC2Ev);
//    fun("3z5EPY-ph14", _ZN3sce2np4CondC1Ev);
//    fun("1CiXI-MyEKs", sceNpLwMutexInit);
//    fun("uyNO0GnFhPw", _ZN3sce2np9WorkQueueD0Ev);
//    fun("xmF0yIF4iXc", sceNpPollSema);
//    fun("Uv1IQpTWecw", _ZN3sce2np9EventFlagC2Ev);
//    fun("gMlY6eewr-c", sceNpAllocateKernelMemoryWithAlignment);
//    fun("QlaBcxSFPZI", _ZN3sce2np9EventFlag7DestroyEv);
//    fun("vqA9bl6WsF0", _sceNpAllocatorStrndup);
//    fun("gnh2cpEgSS8", _ZN3sce2np8WorkItem9BindQueueEPNS0_9WorkQueueEi);
//    fun("MH0LyghLJEE", _ZN3sce2np14JsonDocBuilderD2Ev);
//    fun("GSVe-aaTiEg", _ZN3sce2np9HttpTransD2Ev);
//    fun("9+m5nRdJ-wQ", sceNpCalloutInitCtx);
//    fun("N3tAHlBnowE", sceNpUtilBuildTitleId);
//    fun("4il4PZAZOnQ", _ZN3sce2np6ObjectdaEPvR14SceNpAllocator);
//    fun("laqZEULcfgw", _ZN3sce2np9WorkQueue6CancelEii);
//    fun("xu9qWN0YYC4", _ZN3sce2np10EventQueue4ctorEv);
//    fun("L9Ty-fG1IM4", _ZN3sce2np12WorkerThreadC2EPNS0_9WorkQueueE);
//    fun("yX9ISVXv+0M", _ZN3sce2np4CondD1Ev);
//    fun("c8-4aC9opYE", _sceNpAllocatorStrdup);
//    fun("OhpofCxYOJc", _ZN3sce2np6HandleC2Ev);
//    fun("d0zSLZMER34", _ZN3sce2npltERKNS0_4TimeES3_);
//    fun("VNKdE2Dgp0Y", _ZN3sce2np6Thread5StartEv);
//    fun("vjwlDmsGtME", sceNpFreeKernelMemory);
//    fun("i8UmXTSq7N4", sceNpCmpNpId);
//    fun("vPju3W13byw", _ZN3sce2np9WorkQueue18WaitForPendingItemEPPNS0_8WorkItemEPb);
//    fun("EfnfZtjjyR0", sceNpUtilParseTitleId);
//    fun("+qB+WcQlMio", _ZN3sce2np12WorkerThreadD2Ev);
//    fun("xPRHNaD3kTc", sceNpUtilCmpAccountId);
//    fun("Pe9fHKX7krE", _ZN3sce2np9HttpTransD0Ev);
//    fun("DUHzVPNlugg", sceNpStrParseHex);
//    fun("mo+gaebiE+M", _ZN3sce2np5Mutex4dtorEv);
//    fun("xS-Hjw1psYs", _ZN3sce2np13JsonDocParserD2Ev);
//    fun("Tuth2BRl4x0", _ZN3sce2np9SemaphoreC1Ev);
//    fun("HldN461O2Dw", _ZN3sce2np8WorkItemC2EPKc);
//    fun("uuyEiBHghY4", _ZN3sce2np5NpEnv8GetNpEnvEPS1_);
//    fun("Y7f+qBjKxdo", _ZN3sce2np10Cancelable4InitEv);
//    fun("+0rj9KhmYb0", sceNpUtilXmlUnescape);
//    fun("rvz8xYxhMW0", _ZN3sce2np7RingBuf4dtorEv);
//    fun("LHZtCT2W1Pw", sceNpCreateSema);
//    fun("EaxLv8TfsrM", _ZN3sce2np13RingBufMemoryD2Ev);
//    fun("u-TlLaJUJEA", _ZN3sce2np7RingBuf7DestroyEv);
//    fun("PVVsRmMkO1g", sceNpGetSystemClockUsec);
//    fun("wDLaq7IgfIc", _ZN3sce2np14JsonDocBuilderD0Ev);
    fun("uEwag-0YZPc", sceNpMutexInit);
//    fun("QjNUYQbGoHA", sceNpUInt64ToStr);
//    fun("OQiPXR6gfj0", _ZN3sce2np4Cond6SignalEv);
#undef fun
}
}
