#include "libSceNetCtl.h"

namespace libSceNetCtl {

    typedef void (PS4API *SceNetCtlCallback)(
            int eventType,
            void *arg
    );

PS4API int sceNetCtlCheckCallback() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int sceNetCtlCheckCallbackForLibIpcInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlClearEventForLibIpcInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlClearEventIpcInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlConnectIpcInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlDisconnectIpcInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlGetBandwidthInfoIpcInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlGetIfStat() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlGetInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlGetInfoIpcInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlGetNatInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlGetNatInfoIpcInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlGetResult() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlGetResultIpcInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlGetState(int *state) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, state);
    *state = 0; // disconnected
    return 0;
}
PS4API int sceNetCtlGetStateIpcInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlInit() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int sceNetCtlRegisterCallback(	SceNetCtlCallback func,
                                         void *arg,
                                         int *cid
) {
    LOG_DEBUG("%s(%p, %p, %p)", __FUNCTION__, func, arg, cid);
    return 0;
}
PS4API int sceNetCtlRegisterCallbackForLibIpcInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlRegisterCallbackIpcInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API void sceNetCtlTerm() {
    LOG_DEBUG("%s()", __FUNCTION__);
    //return 0;
}
PS4API int sceNetCtlUnregisterCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlUnregisterCallbackForLibIpcInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetCtlUnregisterCallbackIpcInt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
//    fun("qOefcpoSs0k", sceNetCtlDisconnectIpcInt);
//    fun("WRvDk2syatE", sceNetCtlRegisterCallbackForLibIpcInt);
//    fun("eCUIlA2t5CE", sceNetCtlGetBandwidthInfoIpcInt);
//    fun("0cBgduPRR+M", sceNetCtlGetResult);
//    fun("gky0+oaNM4k", sceNetCtlInit);
//    fun("x+cnsAxKSHo", sceNetCtlGetNatInfoIpcInt);
//    fun("vv6g8zoanL4", sceNetCtlClearEventForLibIpcInt);
//    fun("gvnJPMkSoAY", sceNetCtlGetStateIpcInt);
//    fun("ID+Gq3Ddzbg", sceNetCtlConnectIpcInt);
//    fun("rqkh2kXvLSw", sceNetCtlRegisterCallbackIpcInt);
//    fun("Rqm2OnZMCz0", sceNetCtlUnregisterCallback);
//    fun("teuK4QnJTGg", sceNetCtlGetIfStat);
//    fun("UF6H6+kjyQs", sceNetCtlCheckCallbackForLibIpcInt);
//    fun("xstcTqAhTys", sceNetCtlGetInfoIpcInt);
//    fun("by9cbB7JGJE", sceNetCtlUnregisterCallbackIpcInt);
//    fun("uBPlr0lbuiI", sceNetCtlGetState);
//    fun("obuxdTiwkF8", sceNetCtlGetInfo);
    fun("UJ+Z7Q+4ck0", sceNetCtlRegisterCallback);
//    fun("Z4wwCFiBELQ", sceNetCtlTerm);
//    fun("JO4yuTuMoKI", sceNetCtlGetNatInfo);
//    fun("8OJ86vFucfo", sceNetCtlClearEventIpcInt);
//    fun("NEtnusbZyAs", sceNetCtlGetResultIpcInt);
//    fun("urWaUWkEGZg", sceNetCtlUnregisterCallbackForLibIpcInt);
//    fun("iQw3iQPhvUQ", sceNetCtlCheckCallback);
#undef fun
}
}
