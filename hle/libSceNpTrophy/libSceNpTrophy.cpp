#include "libSceNpTrophy.h"

namespace libSceNpTrophy {

#define SCE_NP_TROPHY_INVALID_CONTEXT (-1)
typedef int32_t SceNpTrophyContext;
typedef uint32_t SceNpServiceLabel;
#define SCE_NP_INVALID_SERVICE_LABEL (0xFFFFFFFF)
#define SCE_NP_TROPHY_INVALID_HANDLE (-1)
typedef int32_t SceNpTrophyHandle;

#define SCE_NP_TROPHY_FLAG_SETSIZE         (128)
#define SCE_NP_TROPHY_FLAG_BITS_SHIFT      (5)
typedef uint32_t SceNpTrophyFlagMask;
typedef struct SceNpTrophyFlagArray {
    SceNpTrophyFlagMask flag_bits[		SCE_NP_TROPHY_FLAG_SETSIZE >> SCE_NP_TROPHY_FLAG_BITS_SHIFT	];
} SceNpTrophyFlagArray;

PS4API int sceNpTrophyAbortHandle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyCaptureScreenshot() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyCreateContext(SceNpTrophyContext* context, SceUserServiceUserId userId,
                                    SceNpServiceLabel serviceLabel, uint64_t options) {
    (void)userId;
    (void)serviceLabel;
    (void)options;
    *context = 1;
    return 0;
}
PS4API int sceNpTrophyCreateHandle(SceNpTrophyHandle* handle) {
    *handle = 1;
    return 0;
}
PS4API int sceNpTrophyDestroyContext() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
//PS4API int sceNpTrophyDestroyHandle(SceNpTrophyHandle handle) {
//    return 0;
//}
PS4API int sceNpTrophyGetGameIcon() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyGetGameInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyGetGroupIcon() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyGetGroupInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyGetTrophyIcon() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyGetTrophyInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyGetTrophyUnlockState(SceNpTrophyContext context, SceNpTrophyHandle handle, SceNpTrophyFlagArray *flags, uint32_t *count) {
    LOG_DEBUG("dummy");
    *count = 0;
    return 0;
}
PS4API int sceNpTrophyIntAbortHandle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyIntCheckNetSyncTitles() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyIntCreateHandle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyIntDestroyHandle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyIntGetLocalTrophySummary() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyIntGetProgress() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyIntGetRunningTitle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyIntGetRunningTitles() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyIntGetTrpIconByUri() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyIntNetSyncTitle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyIntNetSyncTitles() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyRegisterContext(SceNpTrophyContext context, SceNpTrophyHandle handle, uint64_t options) {
    (void)context;
    (void)handle;
    (void)options;
    return 0;
}
PS4API int sceNpTrophyShowTrophyList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophySystemIsServerAvailable() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTrophyUnlockTrophy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("28xmRUFao68", sceNpTrophyUnlockTrophy);
    fun("LHuSmO3SLd8", sceNpTrophyGetTrophyUnlockState);
    fun("TJCAxto9SEU", sceNpTrophyRegisterContext);
    fun("aTnHs7W-9Uk", sceNpTrophyAbortHandle);
    fun("YYP3f2W09og", sceNpTrophyGetGameInfo);
    fun("PEo09Dkqv0o", sceNpTrophyIntGetTrpIconByUri);
    fun("UXiyfabxFNQ", sceNpTrophyIntNetSyncTitles);
    fun("sng98qULzPA", sceNpTrophyIntGetLocalTrophySummary);
    fun("cqGkYAN-gRw", sceNpTrophyCaptureScreenshot);
    fun("pE5yhroy9m0", sceNpTrophyIntCheckNetSyncTitles);
    fun("XbkjbobZlCY", sceNpTrophyCreateContext);
    fun("DSh3EXpqAQ4", sceNpTrophyIntDestroyHandle);
    fun("HLwz1fRIycA", sceNpTrophyGetGameIcon);
    fun("qqUVGDgQBm0", sceNpTrophyGetTrophyInfo);
    fun("t3CQzag7-zs", sceNpTrophyIntGetProgress);
    fun("edPIOFpEAvU", sceNpTrophyIntCreateHandle);
    fun("eBL+l6HG9xk", sceNpTrophyGetTrophyIcon);
    fun("jF-mCgGuvbQ", sceNpTrophyIntGetRunningTitle);
    fun("u9plkqa2e0k", sceNpTrophyIntAbortHandle);
    fun("wTUwGfspKic", sceNpTrophyGetGroupInfo);
    fun("q7U6tEAQf7c", sceNpTrophyCreateHandle);
    fun("E1Wrwd07Lr8", sceNpTrophyDestroyContext);
    fun("PeAyBjC5kp8", sceNpTrophyIntGetRunningTitles);
    fun("w4uMPmErD4I", sceNpTrophyGetGroupIcon);
    //fun("GNcF4oidY0Y", sceNpTrophyDestroyHandle);
    fun("kF9zjnlAzIA", sceNpTrophyIntNetSyncTitle);
    fun("d9jpdPz5f-8", sceNpTrophyShowTrophyList);
    fun("-qjm2fFE64M", sceNpTrophySystemIsServerAvailable);
#undef fun
}
} // namespace libSceNpTrophy
