#include "hle.h"
#include "hle/libkernel/libkernel.h"
#include "hle/libSceFios2/libSceFios2.h"
#include "hle/libSceGnmDriver/libSceGnmDriver.h"
#include "hle/libSceFiber/libSceFiber.h"
#include "hle/libSceNet/libSceNet.h"
#include "hle/libSceNetCtl/libSceNetCtl.h"
#include "hle/libSceSysmodule/libSceSysmodule.h"
#include "hle/libSceVideoOut/libSceVideoOut.h"
#include "hle/libSceMouse/libSceMouse.h"
#include "hle/libScePad/libScePad.h"
#include "hle/libSceUserService/libSceUserService.h"
#include "hle/libSceIme/libSceIme.h"
#include "hle/libSceAudioIn/libSceAudioIn.h"
#include "hle/libSceAudioOut/libSceAudioOut.h"
#include "hle/libSceAjm/libSceAjm.h"
#include "hle/libSceNpCommon/libSceNpCommon.h"
#include "hle/libSceNpTrophy/libSceNpTrophy.h"
#include "hle/libSceNpTus/libSceNpTus.h"
#include "hle/libSceNpUtility/libSceNpUtility.h"
#include "hle/libSceNpAuth/libSceNpAuth.h"
#include "hle/libSceNpCommerce/libSceNpCommerce.h"
#include "hle/libSceNpManager/libSceNpManager.h"
#include "hle/libSceNpMatching2/libSceNpMatching2.h"
#include "hle/libSceNpWebApi/libSceNpWebApi.h"
#include "hle/libSceNpSignaling/libSceNpSignaling.h"
#include "hle/libSceNpParty/libSceNpParty.h"
#include "hle/libSceNpScore/libSceNpScore.h"
#include "hle/libSceNpSnsFacebookDialog/libSceNpSnsFacebookDialog.h"
#include "hle/libSceHttp/libSceHttp.h"
#include "hle/libSceSsl/libSceSsl.h"
#include "hle/libSceInvitationDialog/libSceInvitationDialog.h"
#include "hle/libGameCustomDataDialog/libGameCustomDataDialog.h"
#include "hle/libSceRtc/libSceRtc.h"
#include "hle/libSceAppContentUtil/libSceAppContentUtil.h"
#include "hle/libSceVideoRecording/libSceVideoRecording.h"
#include "hle/libSceJpegEnc/libSceJpegEnc.h"
#include "hle/libScePngEnc/libScePngEnc.h"
#include "hle/libSceSystemService/libSceSystemService.h"
#include "hle/libSceVoice/libSceVoice.h"
#include "hle/libSceCommonDialog/libSceCommonDialog.h"
#include "hle/libScePngDec/libScePngDec.h"
#include "hle/libSceJpegDec/libSceJpegDec.h"
#include "hle/libSceVideodec/libSceVideodec.h"
#include "hle/libSceSaveData/libSceSaveData.h"
#include "hle/libSceNpProfileDialog/libSceNpProfileDialog.h"
#include "hle/libSceSaveDataDialog/libSceSaveDataDialog.h"
#include "hle/libSceMsgDialog/libSceMsgDialog.h"
#include "hle/libSceRemoteplay/libSceRemoteplay.h"
#include "hle/libScePlayGo/libScePlayGo.h"
#include "hle/libSceDiscMap/libSceDiscMap.h"
#include "hle/libc/libc.h"
#include "hle/libSceLibcInternal/libSceLibcInternal.h"
#include "hle/libSceJson/libSceJson.h"
#include "hle/libSceNpFriendListDialog/libSceNpFriendListDialog.h"
#include "hle/libSceImeDialog/libSceImeDialog.h"
#include "hle/libSceErrorDialog/libSceErrorDialog.h"
#include "hle/libSceAudiodec/libSceAudiodec.h"
#include "hle/libSceNgs2/libSceNgs2.h"
#include "hle/libSceVoiceQoS/libSceVoiceQoS.h"
#include "hle/libSceGameLiveStreaming/libSceGameLiveStreaming.h"
#include "hle/libSceZlib/libSceZlib.h"
#include "hle/libSceFont/libSceFont.h"
#include "hle/libSceFontFt/libSceFontFt.h"
#include "hle/libSceAvPlayer/libSceAvPlayer.h"
#include "hle/libSceMove/libSceMove.h"
#include "hle/libSceCompanionHttpd/libSceCompanionHttpd.h"
#include "hle/libSceWebBrowserDialog/libSceWebBrowserDialog.h"
#include "hle/libSceScreenShot/libSceScreenShot.h"
#include "hle/libSceUlt/libSceUlt.h"
#include "hle/libSceXml/libSceXml.h"
#include "hle/libSceCompanionUtil/libSceCompanionUtil.h"
#include "hle/libSceRudp/libSceRudp.h"
#include "hle/libSceUsbd/libSceUsbd.h"
#include "hle/libScePigletv2VSH/libScePigletv2VSH.h"
#include "hle/libSceMbus/libSceMbus.h"
#include "hle/libSceIpmi/libSceIpmi.h"
#include "hle/libSceAvSetting/libSceAvSetting.h"
#include "hle/libSceDolbyVision/libSceDolbyVision.h"
#include "hle/libSceDipsw/libSceDipsw.h"
#include "hle/libSceRegMgr/libSceRegMgr.h"
#include "hle/libSceSysUtil/libSceSysUtil.h"
#include "hle/libSceSigninDialog/libSceSigninDialog.h"
#include "hle/libSceSysCore/libSceSysCore.h"
#include "hle/libSceHmd/libSceHmd.h"
#include "hle/libSceFreeType/libSceFreeType.h"
#include "hle/libSceRazorCpu/libSceRazorCpu.h"
#include "hle/libSceNpToolkit/libSceNpToolkit.h"
#include "hle/ulobjmgr/ulobjmgr.h"
#include "hle/libSceAvcap/libSceAvcap.h"
#include "hle/libSceVideoOutSecondary/libSceVideoOutSecondary.h"
#include "hle/libSceVdecCore/libSceVdecCore.h"
#include "hle/libSceVdecSavc/libSceVdecSavc.h"
#include "hle/libSceVdecSavc2/libSceVdecSavc2.h"
#include "hle/libSceVdecShevc/libSceVdecShevc.h"
#include "hle/libSceImeBackend/libSceImeBackend.h"
#include "hle/libSceSharePlay/libSceSharePlay.h"
#include "hle/libScePlayGoDialog/libScePlayGoDialog.h"
#include "hle/libSceAudio3d/libSceAudio3d.h"
#include "hle/libSceAvPlayerStreaming/libSceAvPlayerStreaming.h"
#include "hle/libSceVideodec2/libSceVideodec2.h"
#include "hle/libSceVideoDecoderArbitration/libSceVideoDecoderArbitration.h"
#include "hle/libSceVrTracker/libSceVrTracker.h"
#include "hle/libSceCamera/libSceCamera.h"
#include "hle/libSceRazorGpuThreadTrace/libSceRazorGpuThreadTrace.h"

namespace hle {
bool load_hle_library(const char* lib_name, export_funcs& exports) {
    static const hle_libs hle {
#define lib(a) {#a, a::load}
        lib(libkernel),
        lib(libSceFios2),
        lib(libSceGnmDriver),
        lib(libSceFiber),
        lib(libSceNet),
        lib(libSceNetCtl),
        lib(libSceSysmodule),
        lib(libSceVideoOut),
        lib(libSceMouse),
        lib(libScePad),
        lib(libSceUserService),
        lib(libSceIme),
        lib(libSceAudio3d),
        lib(libSceAudioIn),
        lib(libSceAudioOut),
        lib(libSceAjm),
        lib(libSceNpCommon),
        lib(libSceNpTrophy),
        lib(libSceNpTus),
        lib(libSceNpUtility),
        lib(libSceNpAuth),
        lib(libSceNpCommerce),
        lib(libSceNpManager),
        lib(libSceNpMatching2),
        lib(libSceNpWebApi),
        lib(libSceNpSignaling),
        lib(libSceNpParty),
        lib(libSceNpScore),
        lib(libSceNpSnsFacebookDialog),
        lib(libSceHttp),
        lib(libSceSsl),
        lib(libSceInvitationDialog),
        lib(libGameCustomDataDialog),
        lib(libSceRtc),
        lib(libSceAppContentUtil),
        lib(libSceVideoRecording),
        lib(libSceJpegEnc),
        lib(libScePngEnc),
        lib(libSceSystemService),
        lib(libSceVoice),
        lib(libSceCommonDialog),
        lib(libScePngDec),
        lib(libSceJpegDec),
        lib(libSceVideodec),
        lib(libSceSaveData),
        lib(libSceNpProfileDialog),
        lib(libSceSaveDataDialog),
        lib(libSceMsgDialog),
        lib(libSceRemoteplay),
        lib(libScePlayGo),
        lib(libSceDiscMap),
        lib(libc),
        lib(libSceLibcInternal),
        lib(libSceJson),
        lib(libSceNpFriendListDialog),
        lib(libSceImeDialog),
        lib(libSceErrorDialog),
        lib(libSceAudiodec),
        lib(libSceNgs2),
        lib(libSceVoiceQoS),
        lib(libSceGameLiveStreaming),
        lib(libSceZlib),
        lib(libSceFont),
        lib(libSceFontFt),
        lib(libSceAvPlayer),
        lib(libSceMove),
        lib(libSceCompanionHttpd),
        lib(libSceWebBrowserDialog),
        lib(libSceScreenShot),
        lib(libSceUlt),
        lib(libSceXml),
        lib(libSceCompanionUtil),
        lib(libSceRudp),
        lib(libSceUsbd),
        lib(libScePigletv2VSH),
        lib(libSceMbus),
        lib(libSceIpmi),
        lib(libSceAvSetting),
        lib(libSceDolbyVision),
        lib(libSceDipsw),
        lib(libSceRegMgr),
        lib(libSceSysUtil),
        lib(libSceSigninDialog),
        lib(libSceSysCore),
        lib(libSceHmd),
        lib(libSceFreeType),
        lib(libSceRazorCpu),
        lib(libSceNpToolkit),
        lib(libSceAvcap),
        lib(libSceVideoOutSecondary),
        lib(libSceVdecCore),
        lib(libSceVdecSavc),
        lib(libSceVdecSavc2),
        lib(libSceVdecShevc),
        lib(libSceImeBackend),
        lib(libSceSharePlay),
        lib(libScePlayGoDialog),
        lib(libSceAvPlayerStreaming),
        lib(libSceVideodec2),
        lib(libSceVideoDecoderArbitration),
        lib(libSceVrTracker),
        lib(libSceCamera),
        lib(libSceRazorGpuThreadTrace),
        lib(ulobjmgr),
#undef lib
    };

    auto lib = hle.find(lib_name);
    if (lib != hle.end()) {
        lib->second(exports);
        //LOG_DEBUG("loaded hle library '%s'\n", lib_name);
    } else {
        LOG_DEBUG("cannot load hle library %s\n", lib_name);
        return false;
    }
    return true;
}
}