
#include "libSceAvcap.h"

namespace libSceAvcap {
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
#undef fun
}
}
