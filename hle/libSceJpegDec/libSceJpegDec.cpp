#include "libSceJpegDec.h"

namespace libSceJpegDec {
    PS4API void sceJpegDecQueryMemorySize() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceJpegDecCreate() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceJpegDecParseHeader() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceJpegDecDecode() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceJpegDecDelete() {
        UNIMPLEMENTED_FUNC();
    }

    void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
        fun("uNAUmANZMEw", sceJpegDecQueryMemorySize); // libSceJpegDec
        fun("JPh3Zgg0Zwc", sceJpegDecCreate); // libSceJpegDec
        fun("LSinoSQH790", sceJpegDecParseHeader); // libSceJpegDec
        fun("1kzQRoWEgSA", sceJpegDecDecode); // libSceJpegDec
        fun("Hwh11+m5KoI", sceJpegDecDelete); // libSceJpegDec
#undef fun
    }
}
