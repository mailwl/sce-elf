#include "libSceNpTus.h"

namespace libSceNpTus {

PS4API int sceNpTssCreateNpTitleCtx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTssCreateNpTitleCtxA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTssGetData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTssGetDataAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTssGetSmallStorage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTssGetSmallStorageAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTssGetStorage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTssGetStorageAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusAddAndGetVariable() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusAddAndGetVariableA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusAddAndGetVariableAAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusAddAndGetVariableAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusAddAndGetVariableAVUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusAddAndGetVariableAVUserAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusAddAndGetVariableVUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusAddAndGetVariableVUserAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusChangeModeForOtherSaveDataOwners() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusCreateNpTitleCtx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusCreateNpTitleCtxA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusCreateTitleCtx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusDeleteMultiSlotData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusDeleteMultiSlotDataA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusDeleteMultiSlotDataAAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusDeleteMultiSlotDataAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusDeleteMultiSlotDataVUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusDeleteMultiSlotDataVUserAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusDeleteMultiSlotVariable() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusDeleteMultiSlotVariableA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusDeleteMultiSlotVariableAAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusDeleteMultiSlotVariableAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusDeleteMultiSlotVariableVUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusDeleteMultiSlotVariableVUserAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusDeleteNpTitleCtx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusDeleteRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetDataA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetDataAAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetDataAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetDataAVUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetDataAVUserAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetDataForCrossSave() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetDataVUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetDataVUserAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetFriendsDataStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetFriendsDataStatusA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetFriendsDataStatusAAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetFriendsDataStatusAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetFriendsVariable() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetFriendsVariableA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetFriendsVariableAAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetFriendsVariableAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiSlotDataStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiSlotDataStatusA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiSlotDataStatusAAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiSlotDataStatusAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiSlotDataStatusVUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiSlotDataStatusVUserAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiSlotVariable() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiSlotVariableA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiSlotVariableAAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiSlotVariableAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiSlotVariableVUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiSlotVariableVUserAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiUserDataStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiUserDataStatusA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiUserDataStatusAAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiUserDataStatusAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiUserDataStatusVUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiUserDataStatusVUserAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiUserVariable() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiUserVariableA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiUserVariableAAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiUserVariableAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiUserVariableVUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusGetMultiUserVariableVUserAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusPollAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetDataA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetDataAAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetDataAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetDataAVUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetDataAVUserAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetDataVUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetDataVUserAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetMultiSlotVariable() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetMultiSlotVariableA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetMultiSlotVariableAAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetMultiSlotVariableAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetMultiSlotVariableVUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetMultiSlotVariableVUserAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetThreadParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusSetTimeout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusTryAndSetVariable() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusTryAndSetVariableA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusTryAndSetVariableAAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusTryAndSetVariableAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusTryAndSetVariableAVUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusTryAndSetVariableAVUserAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusTryAndSetVariableVUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusTryAndSetVariableVUserAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpTusWaitAsync() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("Nt1runsPVJc", sceNpTusAddAndGetVariableAVUser);
    fun("iaH+Sxlw32k", sceNpTusGetDataAVUser);
    fun("3bh2aBvvmvM", sceNpTusCreateRequest);
    fun("1n-dGukBgnY", sceNpTusCreateNpTitleCtxA);
    fun("C8TY-UnQoXg", sceNpTusGetFriendsVariableA);
    fun("4NrufkNCkiE", sceNpTusSetData);
    fun("G68xdfQuiyU", sceNpTusSetDataAsync);
    fun("wjNhItL2wzg", sceNpTusChangeModeForOtherSaveDataOwners);
    fun("lBtrk+7lk14", sceNpTssCreateNpTitleCtxA);
    fun("F+eQlfcka98", sceNpTusGetMultiSlotVariable);
    fun("LUwvy0MOSqw", sceNpTusGetFriendsDataStatusAsync);
    fun("WCzd3cxhubo", sceNpTusDeleteMultiSlotVariableVUserAsync);
    fun("YRje5yEXS0U", sceNpTusGetMultiUserVariableAsync);
    fun("NvHjFkx2rnU", sceNpTusGetMultiUserDataStatus);
    fun("o02Mtf8G6V0", sceNpTusDeleteMultiSlotVariableVUser);
    fun("xzG8mG9YlKY", sceNpTusGetDataAAsync);
    fun("ZbitD262GhY", sceNpTusTryAndSetVariableVUser);
    fun("uHtKS5V1T5k", sceNpTusGetDataAsync);
    fun("kt+k6jegYZ8", sceNpTusGetMultiUserDataStatusAAsync);
    fun("KMlHj+tgfdQ", sceNpTusSetTimeout);
    fun("eGunerNP9n0", sceNpTusGetMultiUserVariableAAsync);
    fun("bHWFSg6jvXc", sceNpTusGetMultiSlotDataStatusVUserAsync);
    fun("GjlEgLCh4DY", sceNpTusAddAndGetVariableAVUserAsync);
    fun("pgcNwFHoOL4", sceNpTusGetMultiSlotDataStatus);
    fun("qp-rTrq1klk", sceNpTusGetMultiSlotVariableVUserAsync);
    fun("lxNDPDnWfMc", sceNpTusGetMultiUserDataStatusA);
    fun("+RhzSuuXwxo", sceNpTusSetDataVUser);
    fun("7uLPqiNvNLc", sceNpTusGetMultiSlotDataStatusAAsync);
    fun("6-+Yqc-NppQ", sceNpTusDeleteMultiSlotDataAAsync);
    fun("6GKDdRCFx8c", sceNpTusSetThreadParam);
    fun("YFYWOwYI6DY", sceNpTusGetFriendsVariableAsync);
    fun("t7b6dmpQNiI", sceNpTusPollAsync);
    fun("0up4MP1wNtc", sceNpTusTryAndSetVariableA);
    fun("wPFah4-5Xec", sceNpTusAddAndGetVariableA);
    fun("H3uq7x0sZOI", sceNpTusDeleteNpTitleCtx);
    fun("ukC55HsotJ4", sceNpTusTryAndSetVariable);
    fun("CJAxTxQdwHM", sceNpTusSetMultiSlotVariableVUserAsync);
    fun("zDeH4tr+0cQ", sceNpTusDeleteMultiSlotDataVUserAsync);
    fun("GQHCksS7aLs", sceNpTusGetDataVUser);
    fun("0DT5bP6YzBo", sceNpTusDeleteMultiSlotData);
    fun("hhy8+oecGac", sceNpTusCreateTitleCtx);
    fun("iXzUOM9sXU0", sceNpTusDeleteMultiSlotDataA);
    fun("XOzszO4ONWU", sceNpTusGetData);
    fun("f2Pe4LGS2II", sceNpTssGetSmallStorageAsync);
    fun("2BnPSY1Oxd8", sceNpTusGetMultiSlotVariableAAsync);
    fun("ukr6FBSrkJw", sceNpTusAddAndGetVariableVUser);
    fun("BIkMmUfNKWM", sceNpTusCreateNpTitleCtx);
    fun("hYPJFWzFPjA", sceNpTusWaitAsync);
    fun("DXigwIBTjWE", sceNpTusGetFriendsDataStatus);
    fun("bcPB2rnhQqo", sceNpTusGetMultiSlotVariableAsync);
    fun("lL+Z3zCKNTs", sceNpTssGetSmallStorage);
    fun("kbWqOt3QjKU", sceNpTusSetDataAVUser);
    fun("NGCeFUl5ckM", sceNpTusGetMultiSlotDataStatusVUser);
    fun("Qyek420uZmM", sceNpTusGetMultiSlotDataStatusAsync);
    fun("6G9+4eIb+cY", sceNpTusGetMultiUserVariable);
    fun("uoFvgzwawAY", sceNpTusGetDataAVUserAsync);
    fun("lliK9T6ylJg", sceNpTusAddAndGetVariableVUserAsync);
    fun("OheijxY5RYE", sceNpTusGetFriendsDataStatusAAsync);
    fun("0zkr0T+NYvI", sceNpTusGetMultiUserDataStatusAsync);
    fun("ypMObSwfcns", sceNpTusSetMultiSlotVariableAAsync);
    fun("GDXlRTxgd+M", sceNpTusGetMultiSlotVariableA);
    fun("xwJIlK0bHgA", sceNpTusGetMultiUserDataStatusVUser);
    fun("sRVb2Cf0GHg", sceNpTssCreateNpTitleCtx);
    fun("c6aYoa47YgI", sceNpTusSetMultiSlotVariable);
    fun("-SUR+UoLS6c", sceNpTssGetData);
    fun("1TE3OvH61qo", sceNpTusGetDataForCrossSave);
    fun("bGTjTkHPHTE", sceNpTusTryAndSetVariableAAsync);
    fun("mYhbiRtkE1Y", sceNpTusDeleteMultiSlotVariable);
    fun("cf-WMA0jYCc", sceNpTusSetMultiSlotVariableA);
    fun("5R6kI-8f+Hk", sceNpTusGetDataVUserAsync);
    fun("zB0vaHTzA6g", sceNpTusGetMultiUserVariableVUser);
    fun("2eq1bMwgZYo", sceNpTusAbortRequest);
    fun("0nDVqcYECoM", sceNpTusDeleteMultiSlotVariableAsync);
    fun("uf77muc5Bog", sceNpTusTryAndSetVariableAVUserAsync);
    fun("IVSbAEOxJ6I", sceNpTssGetStorage);
    fun("yixh7HDKWfk", sceNpTusGetFriendsDataStatusA);
    fun("5J9GGMludxY", sceNpTusSetMultiSlotVariableAsync);
    fun("833Y2TnyonE", sceNpTusGetMultiSlotDataStatusA);
    fun("yWEHUFkY1qI", sceNpTusGetDataA);
    fun("1Cz0hTJFyh4", sceNpTusSetMultiSlotVariableVUser);
    fun("Q2UmHdK04c8", sceNpTusAddAndGetVariableAsync);
    fun("I5dlIKkHNkQ", sceNpTusGetMultiUserDataStatusVUserAsync);
    fun("CcIH40dYS88", sceNpTusDeleteRequest);
    fun("pwnE9Oa1uF8", sceNpTusDeleteMultiSlotVariableA);
    fun("xutwCvsydkk", sceNpTusDeleteMultiSlotDataVUser);
    fun("uFxVYJEkcmc", sceNpTusGetMultiSlotVariableVUser);
    fun("k5NZIzggbuk", sceNpTssGetStorageAsync);
    fun("xZXQuNSTC6o", sceNpTusGetMultiUserVariableVUserAsync);
    fun("4u58d6g6uwU", sceNpTusSetDataAAsync);
    fun("VzxN3tOouj8", sceNpTusSetDataA);
    fun("oGIcxlUabSA", sceNpTusTryAndSetVariableAVUser);
    fun("trZ6QGW6jHs", sceNpTusTryAndSetVariableVUserAsync);
    fun("cy+pAALkHp8", sceNpTusGetFriendsVariable);
    fun("Gjixv5hqRVY", sceNpTusGetMultiUserVariableA);
    fun("E4BCVfx-YfM", sceNpTusSetDataVUserAsync);
    fun("Fmx4tapJGzo", sceNpTusSetDataAVUserAsync);
    fun("DS2yu3Sjj1o", sceNpTssGetDataAsync);
    fun("2dB427dT3Iw", sceNpTusAddAndGetVariableAAsync);
    fun("xQfR51i4kck", sceNpTusTryAndSetVariableAsync);
    fun("wrImtTqUSGM", sceNpTusGetFriendsVariableAAsync);
    fun("cRVmNrJDbG8", sceNpTusAddAndGetVariable);
    fun("NQIw7tzo0Ow", sceNpTusDeleteMultiSlotVariableAAsync);
    fun("OCozl1ZtxRY", sceNpTusDeleteMultiSlotDataAsync);
#undef fun
}
}
