#include "libSceVoiceQoS.h"

namespace libSceVoiceQoS {

PS4API int sceVoiceQoSConnect() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSCreateLocalEndpoint() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSCreateRemoteEndpoint() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSDebugGetStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSDeleteLocalEndpoint() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSDeleteRemoteEndpoint() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSDisconnect() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSEnd() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSGetConnectionAttribute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSGetLocalEndpoint() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSGetLocalEndpointAttribute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSGetRemoteEndpoint() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSGetRemoteEndpointAttribute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSGetStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSReadPacket() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSSetConnectionAttribute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSSetLocalEndpointAttribute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSSetRemoteEndpointAttribute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVoiceQoSWritePacket() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("PWokFqab5q4", sceVoiceQoSReadPacket);
    fun("lvNClhNHzxI", sceVoiceQoSCreateLocalEndpoint);
    fun("F7wS7FbfumQ", sceVoiceQoSSetLocalEndpointAttribute);
    fun("XmvdN3atbXY", sceVoiceQoSGetConnectionAttribute);
    fun("iqQQW2cBmWU", sceVoiceQoSCreateRemoteEndpoint);
    fun("SpxLratrO1Q", sceVoiceQoSWritePacket);
    fun("kE0kdvcHTiY", sceVoiceQoSDeleteLocalEndpoint);
    fun("H4zqFaDhHW4", sceVoiceQoSDeleteRemoteEndpoint);
    fun("j9Xt85krooc", sceVoiceQoSDisconnect);
    fun("KCGOxg8iX7s", sceVoiceQoSSetConnectionAttribute);
    fun("cpC-zyHoMik", sceVoiceQoSSetRemoteEndpointAttribute);
    fun("kLU6hhXsa2A", sceVoiceQoSConnect);
    fun("XPC8EyEuvyk", sceVoiceQoSGetLocalEndpoint);
    fun("3vmrvQ4rvgs", sceVoiceQoSGetRemoteEndpointAttribute);
    fun("eZu2RP0Ma3w", sceVoiceQoSGetLocalEndpointAttribute);
    fun("Trpt2QBZHCI", sceVoiceQoSGetStatus);
    fun("ATRGkmbolVM", sceVoiceQoSEnd);
    fun("U8IfNl6-Css", sceVoiceQoSInit);
    fun("9tfy4+aDxrM", sceVoiceQoSGetRemoteEndpoint);
    fun("D7P8dL0ZKsI", sceVoiceQoSDebugGetStatus);
#undef fun
}
}
