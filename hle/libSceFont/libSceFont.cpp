#include "libSceFont.h"
#include "hle/libSceFontFt/libSceFontFt.h"
namespace libSceFont {

typedef void*(SceFontMallocFunction)(void* object, uint32_t size);
typedef void(SceFontFreeFunction)(void* object, void* p);
typedef void*(SceFontReallocFunction)(void* object, void* p, uint32_t newSize);
typedef void*(SceFontCallocFunction)(void* object, uint32_t nBlock, uint32_t size);
typedef void*(SceFontMspaceCreateFunction)(void* parent, const char* name, void* address, uint32_t size, uint32_t attr);
typedef void(SceFontMspaceDestroyFunction)(void* parent, void* mspace);
typedef void(SceFontMemoryDestroyFunction)(struct SceFontMemory* fontMemory, void* object, void* destroyArg);
typedef SceFontMallocFunction* SceFontMallocCallback;
typedef SceFontFreeFunction* SceFontFreeCallback;
typedef SceFontReallocFunction* SceFontReallocCallback;
typedef SceFontCallocFunction* SceFontCallocCallback;
typedef SceFontMspaceCreateFunction* SceFontMspaceCreateCallback;
typedef SceFontMspaceDestroyFunction* SceFontMspaceDestroyCallback;
typedef SceFontMemoryDestroyFunction* SceFontMemoryDestroyCallback;

typedef struct SceFontMemoryInterface {
    SceFontMallocCallback Malloc;
    SceFontFreeCallback Free;
    SceFontReallocCallback Realloc;
    SceFontCallocCallback Calloc;
    SceFontMspaceCreateCallback MspaceCreate;
    SceFontMspaceDestroyCallback MspaceDestroy;

} SceFontMemoryInterface;

typedef struct SceFontMemory {
    uint16_t type;
    uint16_t attr;
    uint32_t size;
    void* address;
    void* mspaceObject;
    const SceFontMemoryInterface* memInterface;
    SceFontMemoryDestroyCallback DestroyCallback;
    void* destroyObject;
    void* userObject;
    void* parentObject;
} SceFontMemory;

typedef struct _SceFontLibrarySelection {
    char pad[0x10];
} * SceFontLibrarySelection;
typedef struct _SceFontLibrary {
    char pad[0x10];
} * SceFontLibrary;

PS4API void sceFontAttachDeviceCacheBuffer() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceFontBindRenderer() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceFontCloseFont() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceFontCreateLibraryWithEdition(const SceFontMemory* memory, const SceFontLibrarySelection selection,
                                            uint64_t edition, SceFontLibrary* pLibrary) {
    LOG_TRACE("");
    return 0;
}

PS4API int sceFontCreateRendererWithEdition(const SceFontMemory* memory, SceFontRendererSelection selection,
                                            uint64_t edition, SceFontRenderer* pRenderer) {
    LOG_TRACE("");
    return 0;
}
PS4API void sceFontDestroyLibrary() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceFontDestroyRenderer() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceFontGetCharGlyphMetrics() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceFontGetHorizontalLayout() {
    UNIMPLEMENTED_FUNC();
}

PS4API int sceFontMemoryInit(SceFontMemory* fontMemory, void* address, uint32_t sizeByte,
                             const SceFontMemoryInterface* memoryInterface, void* mspaceObject,
                             SceFontMemoryDestroyCallback destroyCallback, void* destroyObject) {
    LOG_DEBUG("");
    return 0;
}

PS4API void sceFontMemoryTerm() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceFontOpenFontSet() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceFontRenderCharGlyphImageHorizontal() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceFontRenderSurfaceInit() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceFontRenderSurfaceSetScissor() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceFontSetScalePixel() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceFontSetupRenderScalePixel() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceFontSupportExternalFonts(SceFontLibrary library, uint32_t fontMax, uint32_t formats) {
    LOG_DEBUG("");
    return 0;
}

PS4API int sceFontSupportSystemFonts(SceFontLibrary library) {
    LOG_DEBUG("");
    return 0;
}

PS4API void sceFontUnbindRenderer() {
    UNIMPLEMENTED_FUNC();
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
//    fun("CUKn5pX-NVY", sceFontAttachDeviceCacheBuffer);
//    fun("3OdRkSjOcog", sceFontBindRenderer);
//    fun("vzHs3C8lWJk", sceFontCloseFont);
//    fun("n590hj5Oe-k", sceFontCreateLibraryWithEdition);
//    fun("WaSFJoRWXaI", sceFontCreateRendererWithEdition);
//    fun("FXP359ygujs", sceFontDestroyLibrary);
//    fun("exAxkyVLt0s", sceFontDestroyRenderer);
//    fun("L97d+3OgMlE", sceFontGetCharGlyphMetrics);
//    fun("imxVx8lm+KM", sceFontGetHorizontalLayout);
//    fun("whrS4oksXc4", sceFontMemoryInit);
//    fun("h6hIgxXEiEc", sceFontMemoryTerm);
//    fun("cKYtVmeSTcw", sceFontOpenFontSet);
//    fun("kAenWy1Zw5o", sceFontRenderCharGlyphImageHorizontal);
//    fun("gdUCnU0gHdI", sceFontRenderSurfaceInit);
//    fun("vRxf4d0ulPs", sceFontRenderSurfaceSetScissor);
//    fun("N1EBMeGhf7E", sceFontSetScalePixel);
//    fun("6vGCkkQJOcI", sceFontSetupRenderScalePixel);
//    fun("mz2iTY0MK4A", sceFontSupportExternalFonts);
//    fun("SsRbbCiWoGw", sceFontSupportSystemFonts);
//    fun("1QjhKxrsOB8", sceFontUnbindRenderer);
#undef fun
}
} // namespace libSceFont
