#include <array>
#include <memory.h>
#include "libSceVideoOut.h"
#include "libkernel/libkernel.h"

namespace libSceVideoOut {
static int32_t flip_rate{};

#define MAIN 0x100
#define AUX 0x200

enum SceVideoOutBusType {
    SCE_VIDEO_OUT_BUS_TYPE_MAIN = 0,                    // Main output
    SCE_VIDEO_OUT_BUS_TYPE_AUX_SOCIAL_SCREEN = 5,       // Aux output for social screens
    SCE_VIDEO_OUT_BUS_TYPE_AUX_GAME_LIVE_STREAMING = 6, // Aux output for live streaming
};

std::array<void*, 16> main_buffers{};
std::array<void*, 8> aux_buffers{};

typedef struct SceVideoOutResolutionStatus {
    uint32_t fullWidth;
    uint32_t fullHeight;
    uint32_t paneWidth;
    uint32_t paneHeight;
    uint64_t refreshRate;
    float screenSizeInInch;
    uint16_t flags;
    uint16_t _reserved0;
    uint32_t _reserved1[3];
} SceVideoOutResolutionStatus;

typedef struct SceVideoOutBufferAttribute {
    int32_t pixelFormat; // SceVideoOutPixelFormat
    int32_t tilingMode;  // SceVideoOutTilingMode
    int32_t aspectRatio; // SceVideoOutAspectRatio
    uint32_t width;
    uint32_t height;
    uint32_t pitchInPixel;
    uint32_t option; // SceVideoOutBufferAttributeOption
    uint32_t _reserved0;
    uint64_t _reserved1;
} SceVideoOutBufferAttribute;

typedef struct SceVideoOutFlipStatus {
    uint64_t count;
    uint64_t processTime;
    uint64_t tsc;
    int64_t flipArg;
    uint64_t submitTsc;
    uint64_t _reserved0;
    int32_t gcQueueNum;
    int32_t flipPendingNum;
    int32_t currentBuffer;
    uint32_t _reserved1;
} SceVideoOutFlipStatus;

static SceVideoOutFlipStatus flip_status{};

typedef struct SceVideoOutColorSettings {
} SceVideoOutColorSettings;

PS4API int32_t sceVideoOutColorSettingsSetGamma_(SceVideoOutColorSettings* pSettings, float gamma) {
    LOG_DEBUG("STUB %s(%p, %f)", __FUNCTION__, pSettings, gamma);
    return 0;
}
PS4API int32_t sceVideoOutAdjustColor_(int32_t handle, SceVideoOutColorSettings* pSettings) {
    LOG_DEBUG("STUB %s(%x, %p)", __FUNCTION__, handle, pSettings);
    return 0;
}
PS4API int32_t sceVideoOutGetResolutionStatus(int32_t handle, SceVideoOutResolutionStatus* status) {
    LOG_DEBUG("%s(%x, %p)", __FUNCTION__, handle, status);
    status->fullWidth = 1920;
    status->fullHeight = 1080;
    return 0;
}
PS4API int32_t sceVideoOutAddFlipEvent(libkernel::SceKernelEqueue eq, int32_t handle, void* udata) {
    LOG_DEBUG("%s(%p, 0x%x, %p)", __FUNCTION__, eq, handle, udata);
    return 0;
}

PS4API int32_t sceVideoOutOpen(SceUserServiceUserId userId, SceVideoOutBusType busType, int32_t index,
                               const void* param) {
    LOG_DEBUG("%s(%d, %d, %d, %p)", __FUNCTION__, userId, busType, index, param);
    if (busType == SCE_VIDEO_OUT_BUS_TYPE_MAIN) {
        return MAIN;
    }
    return AUX;
}
PS4API int32_t sceVideoOutSetFlipRate(int32_t handle, int32_t rate) {
    LOG_DEBUG("%s(0x%x, %d)", __FUNCTION__, handle, rate);
    flip_rate = rate; // (0, 1, 2 skip vsync)
    return 0;
}
PS4API void sceVideoOutSetBufferAttribute(SceVideoOutBufferAttribute* attribute, uint32_t pixelFormat,
                                          uint32_t tilingMode, uint32_t aspectRatio, uint32_t width, uint32_t height,
                                          uint32_t pitchInPixel) {
    LOG_DEBUG("STUB %s(%p, %x, %x, %x, %x, %x, %x)", __FUNCTION__, attribute, pixelFormat, tilingMode, aspectRatio,
              width, height, pitchInPixel);
    attribute->pixelFormat = pixelFormat;
    attribute->tilingMode = tilingMode;
    attribute->aspectRatio = aspectRatio;
    attribute->width = width;
    attribute->height = height;
    attribute->pitchInPixel = pitchInPixel;
    attribute->option = 0;
}
PS4API int32_t sceVideoOutRegisterBuffers(int32_t handle, int32_t startIndex, void* const* addresses, int32_t bufferNum,
                                          const SceVideoOutBufferAttribute* attribute) {
    LOG_DEBUG("%s(%d, %d, %p, %d, %p)", __FUNCTION__, handle, startIndex, addresses, bufferNum, attribute);
    if (handle == MAIN) {
        for (int32_t i = startIndex; i < bufferNum; ++i) {
            main_buffers[i] = addresses[i - startIndex];
        }
    } else {
        for (int32_t i = startIndex; i < bufferNum; ++i) {
            aux_buffers[i] = addresses[i - startIndex];
        }
    }
    return 0;
}
PS4API int32_t sceVideoOutClose(int32_t handle) {
    LOG_DEBUG("STUB %s(%x)", __FUNCTION__, handle);
    return 0;
}
PS4API int32_t sceVideoOutGetFlipStatus(int32_t handle, SceVideoOutFlipStatus* status) {
    LOG_DEBUG("STUB %s(%x, %p)", __FUNCTION__, handle, status);
    ::memcpy(status, &flip_status, sizeof(SceVideoOutFlipStatus));
    return 0;
}
PS4API int32_t sceVideoOutIsFlipPending(int32_t handle) {
    LOG_DEBUG("STUB %s(%x)", __FUNCTION__, handle);
    return 0; // flip count pending
}
PS4API int sceVideoOutSubmitFlip(int32_t handle, int32_t bufferIndex, int32_t flipMode, int64_t flipArg) {
    LOG_DEBUG("STUB %s(0x%x, %d, %d, 0x%llx)", __FUNCTION__, handle, bufferIndex, flipMode, flipArg);
    flip_status.count++;
    flip_status.flipArg = flipArg;
    flip_status.currentBuffer = bufferIndex;
    flip_status.flipPendingNum = 0;

    return 0;
}
PS4API int sceVideoOutWaitVblank(int32_t handle) {
    LOG_DEBUG("STUB %s(%x)", __FUNCTION__, handle);
    return 0;
}
PS4API int sceVideoOutConfigureOutputMode_() {
    LOG_DEBUG("");
    return 0;
}
PS4API void sceVideoOutGetDeviceCapabilityInfo_() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceVideoOutModeSetAny_() {
    LOG_DEBUG("");
    return 0;
}
PS4API int sceVideoOutRegisterStereoBuffers(int32_t handle, int32_t startIndex, void* const* addresses,
                                            int32_t bufferNum, const SceVideoOutBufferAttribute* attribute) {
    LOG_DEBUG("");
    return 0;
}
PS4API void sceVideoOutUnregisterBuffers() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceVideoOutDeleteFlipEvent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVideoOutGetBufferLabelAddress(uint32_t handle, void** addr) {
    LOG_DEBUG("(%x, %p)", handle, addr);
    *addr = ::malloc(0x10000); // TODO!
    return 0;
}
PS4API int sceVideoOutSubmitEopFlip() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVideoOutAddVblankEvent(libkernel::SceKernelEqueue eq, int32_t handle, void* udata) {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int sceVideoOutGetEventCount() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVideoOutGetVblankStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVideoOutSetWindowModeMargins(int32_t handle, int32_t top, int32_t bottom) {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVideoOutAddDriver() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int sceVideoOutDeleteDriver() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVideoOutDriverDecrementBufferLabel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVideoOutDriverDeleteEvent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVideoOutDriverGetEopHandle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVideoOutDriverGetFinishedEopHandle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVideoOutDriverIncrementBufferLabel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVideoOutDriverResetBufferLabel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVideoOutDriverTriggerEvent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceVideoOutDriverAddEvent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("DYhhWbJSeRg", sceVideoOutColorSettingsSetGamma_);
    fun("pv9CI5VC+R0", sceVideoOutAdjustColor_);
    fun("6kPnj51T62Y", sceVideoOutGetResolutionStatus);
    fun("HXzjK9yI30k", sceVideoOutAddFlipEvent);
    fun("Up36PTk687E", sceVideoOutOpen);
    fun("CBiu4mCE1DA", sceVideoOutSetFlipRate);
    fun("i6-sR91Wt-4", sceVideoOutSetBufferAttribute);
    fun("w3BY+tAEiQY", sceVideoOutRegisterBuffers);
    fun("uquVH4-Du78", sceVideoOutClose);
    fun("SbU3dwp80lQ", sceVideoOutGetFlipStatus);
    fun("zgXifHT9ErY", sceVideoOutIsFlipPending);
    fun("U46NwOiJpys", sceVideoOutSubmitFlip);
    fun("j6RaAUlaLv0", sceVideoOutWaitVblank);
    fun("N1bEoJ4SRw4", sceVideoOutConfigureOutputMode_);
    fun("kGVLc3htQE8", sceVideoOutGetDeviceCapabilityInfo_);
    fun("pjkDsgxli6c", sceVideoOutModeSetAny_);
    fun("lCTCOogRbk0", sceVideoOutRegisterStereoBuffers);
    fun("N5KDtkIjjJ4", sceVideoOutUnregisterBuffers);
    fun("-Ozn0F1AFRg", sceVideoOutDeleteFlipEvent);
    fun("j8xl+92A0q4", sceVideoOutSubmitEopFlip);
    fun("OcQybQejHEY", sceVideoOutGetBufferLabelAddress);
    fun("Xru92wHJRmg", sceVideoOutAddVblankEvent);
    fun("Mt4QHHkxkOc", sceVideoOutGetEventCount);
    fun("1FZBKy8HeNU", sceVideoOutGetVblankStatus);
    fun("MTxxrOCeSig", sceVideoOutSetWindowModeMargins);
    fun("HtwSd4H2Tws", sceVideoOutAddDriver);                  // libSceVideoOut
    fun("KA515kD5rAY", sceVideoOutDeleteDriver);               // libSceVideoOut
    fun("oeNtdmvV4II", sceVideoOutDriverAddEvent);             // libSceVideoOut
    fun("0jGJbNTpSV4", sceVideoOutDriverDecrementBufferLabel); // libSceVideoOut
    fun("lOR+Nos+Je8", sceVideoOutDriverDeleteEvent);          // libSceVideoOut
    fun("P1bIHoga4jE", sceVideoOutDriverGetEopHandle);         // libSceVideoOut
    fun("G6peSSDP4iA", sceVideoOutDriverGetFinishedEopHandle); // libSceVideoOut
    fun("Ztx4b2LhRDk", sceVideoOutDriverIncrementBufferLabel); // libSceVideoOut
    fun("soAIjspgdt8", sceVideoOutDriverResetBufferLabel);     // libSceVideoOut
    fun("OHnnkrUcShM", sceVideoOutDriverTriggerEvent);         // libSceVideoOut
#undef fun
}
} // namespace libSceVideoOut