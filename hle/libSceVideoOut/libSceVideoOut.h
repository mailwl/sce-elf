#pragma  once

#include "common.h"

namespace libSceVideoOut {
PS4API int sceVideoOutGetBufferLabelAddress(uint32_t a1, void** addr);
void load(export_funcs& exp);
}
