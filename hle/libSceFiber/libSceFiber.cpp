#include "libSceFiber.h"

namespace libSceFiber {

typedef void PS4API (*SceFiberEntry)(uint64_t argOnInitialize, uint64_t argOnRun) __attribute__((noreturn));
typedef struct {
    uint8_t opaque[0x200];
    SceFiberEntry entry;
} SceFiber;

SceFiber* current{nullptr};


typedef struct SceFiberOptParam {
    /* omitted */
} SceFiberOptParam;

PS4API int sceFiberRun(SceFiber* fiber, uint64_t argOnRunTo, uint64_t* argOnReturn) {
    LOG_DEBUG("%p", fiber);
    current = fiber;
    return 0;
}
PS4API void sceFiberReturnToThread() {
    UNIMPLEMENTED_FUNC();
}
PS4API int sceFiberGetSelf(SceFiber** fiber) {
    LOG_DEBUG("%p", fiber);
    *fiber = current;
    return 0;
}
PS4API int sceFiberSwitch(SceFiber* fiber,
                           uint64_t argOnRunTo,
                           uint64_t* argOnRun) {
    LOG_DEBUG("%p", fiber);
    return 0;
}
PS4API void sceFiberOptParamInitialize() {
    UNIMPLEMENTED_FUNC();
}
PS4API int _sceFiberInitializeImpl(SceFiber* fiber, const char* name, SceFiberEntry entry, uint64_t argOnInitialize,
                                   void* addrContext, uint64_t sizeContext) {
    LOG_DEBUG("%p", fiber);
    return 0;
}
PS4API void sceFiberFinalize() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceFiberGetInfo() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceFiberRename() {
    UNIMPLEMENTED_FUNC();
}

PS4API int _sceFiberInitializeWithInternalOptionImpl(SceFiber* fiber, const char* name, SceFiberEntry entry, uint64_t argOnInitialize,
                                                     void* addrContext, uint64_t sizeContext, const SceFiberOptParam* option) {
    LOG_DEBUG("");
    return 0;
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
//    fun("a0LLrZWac0M", sceFiberRun);
//    fun("B0ZX2hx9DMw", sceFiberReturnToThread);
//    fun("p+zLIOg27zU", sceFiberGetSelf);
//    fun("PFT2S-tJ7Uk", sceFiberSwitch);
//    fun("asjUJJ+aa8s", sceFiberOptParamInitialize);
//    fun("hVYD7Ou2pCQ", _sceFiberInitializeImpl);
//    fun("7+OJIpko9RY", _sceFiberInitializeWithInternalOptionImpl);
//    fun("JeNX5F-NzQU", sceFiberFinalize);
//    fun("uq2Y5BFz0PE", sceFiberGetInfo);
//    fun("JzyT91ucGDc", sceFiberRename);
#undef fun
}
} // namespace libSceFiber
