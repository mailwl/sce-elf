#include "libSceMbus.h"

namespace libSceMbus {

PS4API int sceMbusAcquireControl() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusAcquireControlList() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusAcquireControlWithState() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusAcquireControlWithState2() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusAcquireControlWithStateFlag() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusAddHandle() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusAddHandleByDeviceId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusAddHandleByUserId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusBindDeviceWithUserId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusConvertToLocalDeviceId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusConvertToLocalDeviceId2() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusDisableBgmForShellUi() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusDisconnectDevice() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusEventBusStatusChangeSubscribe() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusEventBusStatusChangeUnsubscribe() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusEventCreate_() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusEventDestroy() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusEventReceive() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusGetControlStatus() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusGetDeviceDescription() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusGetDeviceInfo_() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusGetSimulatedHandleStatusInfo() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusGetSparkState() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusInit() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusIsUsingDevice() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusReenableBgmForShellUi() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusReleaseControl() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusRemoveHandle() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusRemoveProxy() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusResolveByDeviceId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusResolveByPlayerId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusResolveByUserId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusSetAutoLoginMode() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusSetProxy() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusSetScratchData() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusSetTestFlag() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusTerm() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusUserLogin() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceMbusUserLogout() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("qDP21JPNjTs", sceMbusIsUsingDevice);
    fun("3ic-p1mwH1I", sceMbusAcquireControlWithStateFlag);
    fun("84H-GMsBU6M", sceMbusUserLogin);
    fun("eTpWp430DA4", sceMbusAcquireControlWithState2);
    fun("wpm6Yq7c4YE", sceMbusSetAutoLoginMode);
    fun("BIQaOpcytRc", sceMbusAcquireControlList);
    fun("wxmyc+71ijI", sceMbusConvertToLocalDeviceId2);
    fun("GssDGmm3rGM", sceMbusGetControlStatus);
    fun("WW1zKfNo6HI", sceMbusAddHandleByDeviceId);
    fun("LbHgD9w6uAE", sceMbusRemoveProxy);
    fun("wRPXMGtkOq0", sceMbusInit);
    fun("AcARz3iay1I", sceMbusEventDestroy);
    fun("+la07V7EES0", sceMbusRemoveHandle);
    fun("WNxCIjZdcTo", sceMbusDisconnectDevice);
    fun("hUAjs1MUwGY", sceMbusResolveByUserId);
    fun("ZiBFdjUIV3A", sceMbusAddHandle);
    fun("RsKVKhSWt28", sceMbusGetDeviceInfo_);
    fun("mP31WHwSVjQ", sceMbusSetProxy);
    fun("b28tsShnGuQ", sceMbusGetSimulatedHandleStatusInfo);
    fun("c08SEHicDNU", sceMbusEventCreate_);
    fun("Cpo6sQmnZgU", sceMbusBindDeviceWithUserId);
    fun("UBOvPwgs85s", sceMbusReenableBgmForShellUi);
    fun("Lrelj2uF1Lc", sceMbusSetScratchData);
    fun("jQ2n+Kj8H3c", sceMbusSetTestFlag);
    fun("GcdIHVyl-aw", sceMbusResolveByDeviceId);
    fun("hIAyLXI4V8g", sceMbusGetSparkState);
    fun("Czjo1Se8pEw", sceMbusGetDeviceDescription);
    fun("puHrnP8V-dY", sceMbusEventReceive);
    fun("pM6IH8xwjQ4", sceMbusEventBusStatusChangeSubscribe);
    fun("mGMQ9igs1LM", sceMbusResolveByPlayerId);
    fun("uryBfUoKlVg", sceMbusAcquireControlWithState);
    fun("fG0IjY5r484", sceMbusDisableBgmForShellUi);
    fun("CwDVsGO6U3Q", sceMbusAddHandleByUserId);
    fun("hykD8qmO6XA", sceMbusTerm);
    fun("48qKr9oyIbQ", sceMbusConvertToLocalDeviceId);
    fun("n8+7l03wVdE", sceMbusReleaseControl);
    fun("sB-c6VjBmOs", sceMbusEventBusStatusChangeUnsubscribe);
    fun("VKZKhUg0vyU", sceMbusUserLogout);
    fun("eINK6ismSX0", sceMbusAcquireControl);
#undef fun
}
}
