
#include "libSceRazorGpuThreadTrace.h"

namespace libSceRazorGpuThreadTrace {

PS4API int eQnlGJJAFsA() {
    LOG_DEBUG("");
    return 0;
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("eQnlGJJAFsA", eQnlGJJAFsA);
#undef fun
}
}
