#include "libSceDiscMap.h"
#include "process/process.h"
namespace libSceDiscMap {

    PS4API bool sceDiscMapIsRequestOnHDD(const char* f) {
        UNIMPLEMENTED_FUNC();
        char name[0x100]{};
        process::get_resource_path(f, name);

        return false;
    }
    PS4API void ioKMruft1ek() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void fJgP_wqifno() {
        UNIMPLEMENTED_FUNC();
    }

    void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
//        fun("lbQKqsERhtE", sceDiscMapIsRequestOnHDD);
//        fun("ioKMruft1ek", ioKMruft1ek);
//        fun("fJgP+wqifno", fJgP_wqifno);
#undef fun
    }
};