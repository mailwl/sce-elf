#pragma once

#include "common.h"

namespace libSceSaveData {

typedef struct _SceSaveDataInitParams3 {

} SceSaveDataInitParams3;

typedef uint32_t SceSaveDataParamType;

typedef struct SceSaveDataDirName {
    char data[32];
} SceSaveDataDirName;
typedef uint64_t SceSaveDataBlocks;
typedef uint32_t SceSaveDataMountMode;
#define SCE_SAVE_DATA_MOUNT_MODE_RDONLY (0x00000001)
#define SCE_SAVE_DATA_MOUNT_MODE_RDWR (0x00000002)
#define SCE_SAVE_DATA_MOUNT_MODE_CREATE (0x00000004)
#define SCE_SAVE_DATA_MOUNT_MODE_DESTRUCT_OFF (0x00000008)
#define SCE_SAVE_DATA_MOUNT_MODE_COPY_ICON (0x00000010)
#define SCE_SAVE_DATA_MOUNT_MODE_CREATE2 (0x00000020)
typedef struct SceSaveDataMount2 {
    SceUserServiceUserId userId;
    int : 32;
    const SceSaveDataDirName* dirName;
    SceSaveDataBlocks blocks;
    SceSaveDataMountMode mountMode;
    uint8_t reserved[32];
    int : 32;
} SceSaveDataMount2;

typedef struct SceSaveDataMountPoint {
    char data[16];
} SceSaveDataMountPoint;

typedef uint32_t SceSaveDataMountStatus;

typedef struct SceSaveDataMountResult {
    SceSaveDataMountPoint mountPoint;
    SceSaveDataBlocks requiredBlocks;
    uint32_t unused;
    SceSaveDataMountStatus mountStatus;
    uint8_t reserved[28];
    int : 32;
} SceSaveDataMountResult;

typedef uint32_t SceSaveDataSaveDataMemoryOption;
#define SCE_SAVE_DATA_TITLE_MAXSIZE (128)    /*E max size of game title name */
#define SCE_SAVE_DATA_SUBTITLE_MAXSIZE (128) /*E max size of subtitle name */
#define SCE_SAVE_DATA_DETAIL_MAXSIZE (1024)
typedef struct SceSaveDataParam {
    char title[SCE_SAVE_DATA_TITLE_MAXSIZE];
    char subTitle[SCE_SAVE_DATA_SUBTITLE_MAXSIZE];
    char detail[SCE_SAVE_DATA_DETAIL_MAXSIZE];
    uint32_t userParam;
    int : 32;
    time_t mtime;
    uint8_t reserved[32];
} SceSaveDataParam;

typedef struct SceSaveDataIcon {
    void* buf;
    size_t bufSize;
    size_t dataSize;
    uint8_t reserved[32];
} SceSaveDataIcon;

typedef struct SceSaveDataMemorySetup2 {
    SceSaveDataSaveDataMemoryOption option;
    SceUserServiceUserId userId;
    size_t memorySize;
    size_t iconMemorySize;
    const SceSaveDataParam* initParam;
    const SceSaveDataIcon* initIcon;
    uint8_t reserved[24];
} SceSaveDataMemorySetup2;

typedef struct SceSaveDataMemorySetupResult {
    size_t existedMemorySize;
    uint8_t reserved[16];
} SceSaveDataMemorySetupResult;

typedef struct SceSaveDataTitleId {
    char data[10];
    char padding[6];
} SceSaveDataTitleId;

typedef struct SceSaveDataDelete {
    SceUserServiceUserId userId;
    int : 32;
    const SceSaveDataTitleId* titleId;
    const SceSaveDataDirName* dirName;
    uint32_t unused;
    uint8_t reserved[32];
    int : 32;
} SceSaveDataDelete;

typedef struct SceSaveDataMountInfo {
    SceSaveDataBlocks			blocks;
    SceSaveDataBlocks			freeBlocks;
    uint8_t						reserved[32];
} SceSaveDataMountInfo;

void load(export_funcs& exp);
}; // namespace libSceSaveData
