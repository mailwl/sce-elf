#include "libSceSaveData.h"
#include "process/process.h"
#include <cstring>
#ifdef _WIN64
#include <direct.h>
#else
#include <sys/stat.h>
#endif



namespace libSceSaveData {

PS4API int sceSaveDataBackup() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataCheckBackupData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataCheckBackupDataInternal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataClearProgress() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataCopy5() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataDelete(SceSaveDataDelete* del) {
    LOG_DEBUG("%s('%s')", __FUNCTION__, del->dirName);
    return 0;
}
PS4API int sceSaveDataDelete5() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataDeleteCloudData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataDirNameSearch() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataDirNameSearchInternal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataDownload() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataGetEventResult() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataGetMountInfo(const SceSaveDataMountPoint *mountPoint,
                                   SceSaveDataMountInfo *info) {
    LOG_DEBUG("%s(%p, %p)", __FUNCTION__, mountPoint, info);
    return 0;
}
PS4API int sceSaveDataGetParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataGetProgress() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataGetSaveDataMemory(SceUserServiceUserId userId, void* mem, size_t size, int p2) {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int sceSaveDataGetSaveDataMemory2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataInitialize() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int sceSaveDataInitialize2() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int sceSaveDataInitialize3(const SceSaveDataInitParams3 *initParam) {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int sceSaveDataLoadIcon() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataMount(const void* mount, SceSaveDataMountResult* mountResult) {
    LOG_DEBUG("%s(%p, %p)", __FUNCTION__, mount, mountResult);
    return 0;
}
PS4API int sceSaveDataMount2(const SceSaveDataMount2* mount, SceSaveDataMountResult* mountResult) {
    LOG_DEBUG("%s(%p, %p)", __FUNCTION__, mount, mountResult);

    mountResult->requiredBlocks = mount->blocks;
    mountResult->mountStatus = 0;
    ::strcpy(mountResult->mountPoint.data, "save/");
    ::strncat(mountResult->mountPoint.data, mount->dirName->data, 16 - 5);
    char full_path[0x100]{};
    process::get_resource_path(mountResult->mountPoint.data, full_path);
#ifdef _WIN64
    ::mkdir("save");
    ::mkdir(full_path);
#else
    ::mkdir("save", 0777);
    ::mkdir(full_path, 0777);
#endif
    return 0;
}
PS4API int sceSaveDataMount5() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataMountInternal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataRestoreBackupData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataSaveIcon(const SceSaveDataMountPoint *mountPoint,
                               const SceSaveDataIcon *param) {
    LOG_DEBUG("%s(%p, %p)", __FUNCTION__, mountPoint, param);
    return 0;
}

PS4API int sceSaveDataSetParam(const SceSaveDataMountPoint *mountPoint,
                               const SceSaveDataParamType paramType,
                               const void *paramBuf, const size_t paramBufSize) {
    LOG_DEBUG("%s(%p, %p)", __FUNCTION__, mountPoint, paramBuf);
    SceSaveDataParam* param = (SceSaveDataParam*)paramBuf;
    return 0;
}
PS4API int sceSaveDataSetSaveDataMemory(SceUserServiceUserId userId, void* p1, size_t size, uint64_t p2) {
    (void)userId;
    (void)p1;
    (void)size;
    (void)p2;
    return 0;
}
PS4API int sceSaveDataSetSaveDataMemory2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int32_t sceSaveDataSetupSaveDataMemory(SceUserServiceUserId userId, uint64_t p1, uint64_t p2) {
    (void)userId;
    (void)p1;
    (void)p2;
    return 0;
}
PS4API int32_t sceSaveDataSetupSaveDataMemory2(const SceSaveDataMemorySetup2* setupParam,
                                               SceSaveDataMemorySetupResult* result) {
    (void)setupParam;
    (void)result;
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataSyncSaveDataMemory() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataTerminate() {
    LOG_DEBUG("dummy");
    return 0;
}
PS4API int sceSaveDataTransferringMount() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataUmount(const SceSaveDataMountPoint* mountPoint) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, mountPoint);
    return 0;
}
PS4API int sceSaveDataUmountWithBackup(const SceSaveDataMountPoint *mountPoint) {
    LOG_DEBUG("%s(%p)", __FUNCTION__, mountPoint);
    return 0;
}
PS4API int sceSaveDataUpload() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("QwOO7vegnV8", sceSaveDataGetSaveDataMemory2);
    fun("85zul--eGXs", sceSaveDataSetParam);
    fun("v7AAAMo0Lz4", sceSaveDataSetupSaveDataMemory);
    fun("XgvSuIdnMlw", sceSaveDataGetParam);
    fun("RQOqDbk3bSU", sceSaveDataCheckBackupData);
    fun("cduy9v4YmT4", sceSaveDataSetSaveDataMemory2);
    fun("v1TrX+3ZB10", sceSaveDataCheckBackupDataInternal);
    fun("yKDy8S5yLA0", sceSaveDataTerminate);
    fun("lU9YRFsgwSU", sceSaveDataRestoreBackupData);
    fun("h1nP9EYv3uc", sceSaveDataDownload);
    fun("BMR4F-Uek3E", sceSaveDataUmount);
    fun("Wz-4JZfeO9g", sceSaveDataClearProgress);
    fun("TywrFKCoLGY", sceSaveDataInitialize3);
    fun("oQySEUfgXRA", sceSaveDataSetupSaveDataMemory2);
    fun("ANmSWUiyyGQ", sceSaveDataGetProgress);
    fun("wiT9jeC7xPw", sceSaveDataSyncSaveDataMemory);
    fun("msCER7Iibm8", sceSaveDataMountInternal);
    fun("YbCO38BOOl4", sceSaveDataCopy5);
    fun("j8xKtiFj0SY", sceSaveDataGetEventResult);
    fun("ZkZhskCPXFw", sceSaveDataInitialize);
    fun("fU43mJUgKcM", sceSaveDataDeleteCloudData);
    fun("WAzWTZm1H+I", sceSaveDataTransferringMount);
    fun("COwz3WBj+5s", sceSaveDataUpload);
    fun("dyIhnXq-0SM", sceSaveDataDirNameSearch);
    fun("S1GkePI17zQ", sceSaveDataDelete);
    fun("xz0YMi6BfNk", sceSaveDataMount5);
    fun("c88Yy54Mx0w", sceSaveDataSaveIcon);
    fun("SQWusLoK8Pw", sceSaveDataDelete5);
    fun("0z45PIH+SNI", sceSaveDataMount2);
    fun("32HQAQdwM2o", sceSaveDataMount);
    fun("z1JA8-iJt3k", sceSaveDataBackup);
    fun("xJ5NFWC3m+k", sceSaveDataDirNameSearchInternal);
    fun("h3YURzXGSVQ", sceSaveDataSetSaveDataMemory);
    fun("65VH0Qaaz6s", sceSaveDataGetMountInfo);
    fun("7Bt5pBC-Aco", sceSaveDataGetSaveDataMemory);
    fun("l1NmDeDpNGU", sceSaveDataInitialize2);
    fun("cGjO3wM3V28", sceSaveDataLoadIcon);
    fun("VwadwBBBJ80", sceSaveDataUmountWithBackup);
#undef fun
}
} // namespace libSceSaveData
