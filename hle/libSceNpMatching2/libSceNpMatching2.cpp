#include "libSceNpMatching2.h"

namespace libSceNpMatching2 {

PS4API int sceNpMatching2AbortContextStart() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2ContextStart() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2ContextStop() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2CreateContext() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2CreateContextA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2CreateContextInternal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2CreateJoinRoom() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2CreateJoinRoomA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2DestroyContext() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetLobbyInfoList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetLobbyMemberDataInternal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetLobbyMemberDataInternalList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetMemoryInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetRoomDataExternalList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetRoomDataInternal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetRoomMemberDataExternalList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetRoomMemberDataInternal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetRoomMemberIdListLocal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetRoomPasswordLocal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetServerId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetSignalingOptParamLocal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetSslMemoryInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetUserInfoList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetUserInfoListA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GetWorldInfoList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2GrantRoomOwner() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2Initialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2JoinLobby() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2JoinRoom() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2JoinRoomA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2KickoutRoomMember() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2LeaveLobby() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2LeaveRoom() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2RegisterContextCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2RegisterLobbyEventCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2RegisterLobbyMessageCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2RegisterRoomEventCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2RegisterRoomMessageCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2RegisterSignalingCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SearchRoom() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SendLobbyChatMessage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SendRoomChatMessage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SendRoomMessage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SetDefaultRequestOptParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SetExtraInitParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SetLobbyMemberDataInternal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SetRoomDataExternal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SetRoomDataInternal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SetRoomMemberDataInternal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SetSignalingOptParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SetUserInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SignalingCancelPeerNetInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SignalingGetConnectionInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SignalingGetConnectionInfoA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SignalingGetConnectionStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SignalingGetLocalNetInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SignalingGetPeerNetInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SignalingGetPeerNetInfoResult() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2SignalingGetPingInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpMatching2Terminate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("rJNPJqDCpiI", sceNpMatching2GetWorldInfoList);
    fun("fQQfP87I7hs", sceNpMatching2RegisterContextCallback);
    fun("5lhvOqheFBA", sceNpMatching2GetRoomMemberDataInternal);
    fun("V6KSpKv9XJE", sceNpMatching2CreateJoinRoomA);
    fun("GNSN5849fjU", sceNpMatching2SignalingCancelPeerNetInfo);
    fun("HoqTrkS9c5Q", sceNpMatching2SetRoomMemberDataInternal);
    fun("wUmwXZHaX1w", sceNpMatching2SignalingGetPingInfo);
    fun("DnPUsBAe8oI", sceNpMatching2RegisterLobbyMessageCallback);
    fun("p+2EnxmaAMM", sceNpMatching2RegisterRoomEventCallback);
    fun("qeF-q5KDtAc", sceNpMatching2GetUserInfoList);
    fun("nNeC3F8-g+4", sceNpMatching2SignalingGetConnectionInfoA);
    fun("Mqp3lJ+sjy4", sceNpMatching2Terminate);
    fun("VqZX7POg2Mk", sceNpMatching2SearchRoom);
    fun("4Nj7u5B5yCA", sceNpMatching2RegisterLobbyEventCallback);
    fun("vbtWT3lZBOM", sceNpMatching2GetRoomPasswordLocal);
    fun("BD6kfx442Do", sceNpMatching2LeaveRoom);
    fun("ir2CzSs9K-g", sceNpMatching2SetLobbyMemberDataInternal);
    fun("-f6M4caNe8k", sceNpMatching2ContextStop);
    fun("1JtbJ0kxm3E", sceNpMatching2GetLobbyMemberDataInternal);
    fun("wyvlEgZ-55w", sceNpMatching2GetLobbyInfoList);
    fun("ajvzc8e2upo", sceNpMatching2CreateContextA);
    fun("n5JmImxTiZU", sceNpMatching2JoinLobby);
    fun("+8e7wXLmjds", sceNpMatching2SetDefaultRequestOptParam);
    fun("8btynvj0KNA", sceNpMatching2GetSslMemoryInfo);
    fun("uBESzz4CQws", sceNpMatching2RegisterRoomMessageCallback);
    fun("LhCPctIICxQ", sceNpMatching2GetServerId);
    fun("CSIMDsVjs-g", sceNpMatching2JoinRoom);
    fun("q7GK98-nYSE", sceNpMatching2SetRoomDataExternal);
    fun("ES3UMUWWj9U", sceNpMatching2SetSignalingOptParam);
    fun("cgQhq3E0eGo", sceNpMatching2GetSignalingOptParamLocal);
    fun("26vWrPAWJfM", sceNpMatching2GetRoomDataExternalList);
    fun("pFzhpCMlJXQ", sceNpMatching2AbortContextStart);
    fun("6xlf9+pa0GY", sceNpMatching2CreateContextInternal);
    fun("NCP3bLGPt+o", sceNpMatching2GrantRoomOwner);
    fun("gQ6cUriNpgs", sceNpMatching2JoinRoomA);
    fun("tHD5FPFXtu4", sceNpMatching2SignalingGetConnectionStatus);
    fun("gpSAvdheZ0Q", sceNpMatching2GetMemoryInfo);
    fun("0UMeWRGnZKA", sceNpMatching2RegisterSignalingCallback);
    fun("1Z4Xxumgm+Y", sceNpMatching2GetLobbyMemberDataInternalList);
    fun("10t3e5+JPnU", sceNpMatching2Initialize);
    fun("YfmpW719rMo", sceNpMatching2CreateContext);
    fun("Nz-ZE7ur32I", sceNpMatching2DestroyContext);
    fun("Iw2h0Jrrb5U", sceNpMatching2SendRoomMessage);
    fun("opDpl74pi2E", sceNpMatching2SendRoomChatMessage);
    fun("nHZpTF30wto", sceNpMatching2SetExtraInitParam);
    fun("7vjNQ6Z1op0", sceNpMatching2ContextStart);
    fun("BBbJ92uUdCg", sceNpMatching2LeaveLobby);
    fun("8CqniKDzjvg", sceNpMatching2SignalingGetPeerNetInfo);
    fun("zCWZmXXN600", sceNpMatching2CreateJoinRoom);
    fun("KC+GnHzrK2o", sceNpMatching2GetRoomMemberIdListLocal);
    fun("380EWm2DrVg", sceNpMatching2SignalingGetLocalNetInfo);
    fun("AUVfU6byg3c", sceNpMatching2KickoutRoomMember);
    fun("K+KtxhPsMZ4", sceNpMatching2SendLobbyChatMessage);
    fun("S9D8JSYIrjE", sceNpMatching2SetRoomDataInternal);
    fun("CTy4PBhpWDw", sceNpMatching2SignalingGetPeerNetInfoResult);
    fun("meEjIdbjAA0", sceNpMatching2SetUserInfo);
    fun("Jraxifmoet4", sceNpMatching2GetRoomDataInternal);
    fun("dMQ+xGvTdqM", sceNpMatching2GetRoomMemberDataExternalList);
    fun("twVupeaYYrk", sceNpMatching2SignalingGetConnectionInfo);
    fun("GyI2f9yDUXM", sceNpMatching2GetUserInfoListA);
#undef fun
}
}
