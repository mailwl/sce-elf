
#include "libSceIpmi.h"

namespace libSceIpmi {
PS4API size_t _ZN4IPMI6Client6Config24estimateClientMemorySizeEv() {
    return 0x1000000;
}
PS4API int _ZN4IPMI6Client6ConfigC1Ev() {
    return 0;
}

PS4API int _ZN4IPMI6Client6createEPPS0_PKNS0_6ConfigEPvS6_() {
    LOG_DEBUG("UNIMPL: %s()", __FUNCTION__);
    return 0;
}

PS4API int _ZN4IPMI6Client6Config14MsgQueueConfig20estimateMsgQueueSizeEmj() {
    LOG_DEBUG("UNIMPL: %s()", __FUNCTION__);
    return 0;
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
//    fun("Rih7HnIM3JQ", _ZN4IPMI6Client6Config24estimateClientMemorySizeEv);
//    fun("O1lQ2+do5r4", _ZN4IPMI6Client6ConfigC1Ev);
//    fun("0zsTiDhM0nU", _ZN4IPMI6Client6createEPPS0_PKNS0_6ConfigEPvS6_);
//    fun("yoVdeXZzPck", _ZN4IPMI6Client6Config14MsgQueueConfig20estimateMsgQueueSizeEmj);
#undef fun
}
} // namespace libSceIpmi
