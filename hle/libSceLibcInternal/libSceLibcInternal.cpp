#include <math.h>
#include <memory.h>
#include <string.h>
#include <setjmp.h>
#include <errno.h>
#include <cstdarg>
#include <pthread.h>
#include "libSceLibcInternal.h"

namespace libSceLibcInternal {

//typedef struct _SceLibcMspace{
//    char* base;
//    size_t size;
//    char name[32];
//    size_t offset;
//} _SceLibcMspace;
//typedef _SceLibcMspace* SceLibcMspace;
//
//PS4API int __absvdi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __absvsi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __absvti2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __adddf3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __addsf3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __addvdi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __addvsi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __addvti3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __ashldi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __ashlti3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __ashrdi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __ashrti3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_compare_exchange() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_compare_exchange_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_compare_exchange_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_compare_exchange_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_compare_exchange_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_compare_exchange_n() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_exchange() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_exchange_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_exchange_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_exchange_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_exchange_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_exchange_n() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_add_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_add_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_add_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_add_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_and_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_and_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_and_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_and_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_or_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_or_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_or_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_or_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_sub_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_sub_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_sub_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_sub_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_xor_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_xor_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_xor_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_fetch_xor_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_is_lock_free() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_load() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_load_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_load_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_load_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_load_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_load_n() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_store() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_store_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_store_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_store_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_store_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __atomic_store_n() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cleanup() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __clzdi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __clzsi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __clzti2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cmpdi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cmpti2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __ctzdi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __ctzsi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __ctzti2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_allocate_dependent_exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_allocate_exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API void __cxa_atexit(void (*func) (void *), void * arg, void * dso_handle) {
//    //UNIMPLEMENTED_FUNC();
//}
//PS4API int __cxa_bad_cast() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_bad_typeid() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_begin_catch() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_call_unexpected() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_current_exception_type() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_current_primary_exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_decrement_exception_refcount() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_demangle() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_demangle_gnu3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_end_catch() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_finalize() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_free_dependent_exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_free_exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_get_exception_ptr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_get_globals() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_get_globals_fast() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_guard_abort() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//
//    static pthread_mutex_t __guard_mutex;
//    static pthread_once_t __once_control = PTHREAD_ONCE_INIT;
//static bool initializerHasRun(uint64_t* guard_object) {
//    return ( *((uint8_t*)guard_object) != 0 );
//}
//    static void makeRecusiveMutex() {
//        pthread_mutexattr_t recursiveMutexAttr;
//        pthread_mutexattr_init(&recursiveMutexAttr);
//        pthread_mutexattr_settype(&recursiveMutexAttr, PTHREAD_MUTEX_RECURSIVE);
//        pthread_mutex_init(&__guard_mutex, &recursiveMutexAttr);
//    }
//    __attribute__((noinline))
//    static pthread_mutex_t* guard_mutex() {
//        ::pthread_once(&__once_control, &makeRecusiveMutex);
//        return &__guard_mutex;
//    }
//    static bool inUse(uint64_t* guard_object) {
//        return ( ((uint8_t*)guard_object)[1] != 0 );
//    }
//
//    static void setInUse(uint64_t* guard_object) {
//        ((uint8_t*)guard_object)[1] = 1;
//    }
//
//    static void setNotInUse(uint64_t* guard_object) {
//        ((uint8_t*)guard_object)[1] = 0;
//    }
//PS4API int __cxa_guard_acquire(uint64_t* guard_object) {
//    //https://opensource.apple.com/source/libcppabi/libcppabi-14/src/cxa_guard.cxx
//    LOG_DEBUG("__cxa_guard_acquire(%p)\n", guard_object);
//
//    // Double check that the initializer has not already been run
//    if ( initializerHasRun(guard_object) )
//        return 0;
//
//    // We now need to acquire a lock that allows only one thread
//    // to run the initializer.  If a different thread calls
//    // __cxa_guard_acquire() with the same guard object, we want
//    // that thread to block until this thread is done running the
//    // initializer and calls __cxa_guard_release().  But if the same
//    // thread calls __cxa_guard_acquire() with the same guard object,
//    // we want to abort.
//    // To implement this we have one global pthread recursive mutex
//    // shared by all guard objects, but only one at a time.
//
//    int result = ::pthread_mutex_lock(guard_mutex());
//    if ( result != 0 ) {
//        ::abort();
//    }
//    // At this point all other threads will block in __cxa_guard_acquire()
//
//    // Check if another thread has completed initializer run
//    if ( initializerHasRun(guard_object) ) {
//        int r = ::pthread_mutex_unlock(guard_mutex());
//        if ( r != 0 ) {
//            ::abort();
//        }
//        return 0;
//    }
//
//    // The pthread mutex is recursive to allow other lazy initialized
//    // function locals to be evaluated during evaluation of this one.
//    // But if the same thread can call __cxa_guard_acquire() on the
//    // *same* guard object again, we call abort();
//    if ( inUse(guard_object) ) {
//        ::abort();
//    }
//
//    // mark this guard object as being in use
//    setInUse(guard_object);
//
//    // return non-zero to tell caller to run initializer
//    return 1;
//}
//    static void setInitializerHasRun(uint64_t* guard_object)
//    {
//        *((uint8_t*)guard_object)  = 1;
//    }
//PS4API void __cxa_guard_release(uint64_t* guard_object) {
//    LOG_DEBUG("%s(%p)\n", __func__, guard_object);
//
//    // first mark initalizer as having been run, so
//    // other threads won't try to re-run it.
//    setInitializerHasRun(guard_object);
//
//    // release global mutex
//    int result = ::pthread_mutex_unlock(guard_mutex());
//    if ( result != 0 ) {
//        abort();
//    }
//
//}
//PS4API int __cxa_increment_exception_refcount() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_pure_virtual() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_rethrow() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_rethrow_primary_exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __cxa_throw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __divdc3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __divdf3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __divdi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __divmoddi4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __divmodsi4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __divsc3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __divsf3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __divsi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __divti3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __divxc3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __dynamic_cast() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __eqdf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __eqsf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __extendsfdf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fe_dfl_env() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fedisableexcept() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __feenableexcept() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fflush() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __ffsdi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __ffsti2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixdfdi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixdfsi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixdfti() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixsfdi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixsfsi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixsfti() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixunsdfdi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixunsdfsi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixunsdfti() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixunssfdi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixunssfsi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixunssfti() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixunsxfdi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixunsxfsi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixunsxfti() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixxfdi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fixxfti() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floatdidf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floatdisf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floatdixf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floatsidf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floatsisf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floattidf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floattisf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floattixf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floatundidf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floatundisf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floatundixf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floatunsidf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floatunsisf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floatuntidf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floatuntisf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __floatuntixf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fpclassifyd() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fpclassifyf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __fpclassifyl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __gedf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __gesf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __gtdf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __gtsf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __gxx_personality_v0() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __inet_addr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __inet_aton() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __inet_ntoa() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __inet_ntoa_r() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __isfinite() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __isfinitef() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __isfinitel() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __isinf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __isinff() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __isinfl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __isnan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __isnanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __isnanl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __isnormal() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __isnormalf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __isnormall() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __isthreaded() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __kernel_cos() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __kernel_cosdf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __kernel_rem_pio2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __kernel_sin() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __kernel_sindf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __ledf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __lesf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_Backtrace() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_DeleteException() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_FindEnclosingFunction() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_ForcedUnwind() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_GetBSP() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_GetCFA() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_GetDataRelBase() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_GetGR() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_GetIP() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_GetIPInfo() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_GetLanguageSpecificData() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_GetRegionStart() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_GetTextRelBase() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_RaiseException() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_Resume() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_Resume_or_Rethrow() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_SetGR() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __libunwind_Unwind_SetIP() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __longjmp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __lshrdi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __lshrti3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __ltdf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __ltsf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sce__mb_cur_max() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __mb_sb_limit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __moddi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __modsi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __modti3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __muldc3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __muldf3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __muldi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __mulodi4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __mulosi4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __muloti4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __mulsc3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __mulsf3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __multi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __mulvdi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __mulvsi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __mulvti3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __mulxc3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __nedf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __negdf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __negdi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __negsf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __negti2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __negvdi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __negvsi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __negvti2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __nesf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __opendir2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __paritydi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __paritysi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __parityti2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __popcountdi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __popcountsi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __popcountti2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __powidf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __powisf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __powixf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __signbit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __signbitf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __signbitl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __srefill() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __srget() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __stderrp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __stdinp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __stdoutp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __subdf3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __subsf3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __subvdi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __subvsi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __subvti3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __swbuf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __sync_fetch_and_add_16() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __sync_fetch_and_and_16() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __sync_fetch_and_or_16() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __sync_fetch_and_sub_16() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __sync_fetch_and_xor_16() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __sync_lock_test_and_set_16() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __truncdfsf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __ucmpdi2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __ucmpti2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __udivdi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __udivmoddi4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __udivmodsi4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __udivmodti4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __udivsi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __udivti3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __umoddi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __umodsi3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __umodti3() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __unorddf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __unordsf2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int __vfprintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Assert() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_compare_exchange_strong() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_compare_exchange_strong_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_compare_exchange_strong_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_compare_exchange_strong_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_compare_exchange_strong_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_compare_exchange_weak() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_compare_exchange_weak_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_compare_exchange_weak_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_compare_exchange_weak_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_compare_exchange_weak_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_copy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_exchange() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_exchange_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_exchange_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_exchange_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_exchange_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_add_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_add_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_add_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_add_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_and_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_and_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_and_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_and_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_or_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_or_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_or_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_or_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_sub_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_sub_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_sub_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_sub_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_xor_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_xor_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_xor_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_fetch_xor_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_flag_clear() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_flag_test_and_set() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_is_lock_free_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_is_lock_free_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_is_lock_free_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_is_lock_free_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_load_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_load_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_load_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_load_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_signal_fence() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_store_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_store_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_store_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_store_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atomic_thread_fence() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atqexit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Atthreadexit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Btowc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Call_once() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Call_onceEx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Clocale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Closreg() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Cnd_broadcast() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Cnd_destroy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Cnd_do_broadcast_at_thread_exit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Cnd_init() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Cnd_init_with_name() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Cnd_register_at_thread_exit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Cnd_signal() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Cnd_timedwait() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Cnd_unregister_at_thread_exit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Cnd_wait() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Cosh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Costate() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _CStrftime() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _CStrxfrm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _CTinfo() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Ctype() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _CurrentRuneLocale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _CWcsxfrm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Daysto() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Dbl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Dclass() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _DefaultRuneLocale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Deletegloballocale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Denorm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Dint() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Divide() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Dnorm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Do_call() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Dscale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Dsign() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Dtento() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Dtest() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Dunscale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Eps() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Erf_one() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Erf_small() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Erfc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _err() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Errno() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Exit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Exp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fac_tidy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fail_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FAtan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FCosh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FDclass() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FDenorm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FDint() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FDivide() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FDnorm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FDscale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FDsign() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FDtento() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FDtest() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FDunscale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FEps() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Feraise() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FErf_one() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FErf_small() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FErfc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fetch_add_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fetch_and_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fetch_and_seq_cst_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fetch_and_seq_cst_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fetch_and_seq_cst_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fetch_or_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fetch_or_seq_cst_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fetch_or_seq_cst_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fetch_or_seq_cst_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fetch_xor_8() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fetch_xor_seq_cst_1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fetch_xor_seq_cst_2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fetch_xor_seq_cst_4() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FExp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FFpcomp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FGamma_big() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fgpos() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FHypot() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Files() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FInf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FLog() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FLogpoly() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Flt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fltrounds() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FNan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fofind() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fofree() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fopen() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Foprep() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fpcomp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FPlsw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FPmsw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FPoly() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FPow() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FQuad() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FQuadph() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FRecip() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FRint() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Frprep() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FRteps() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FSin() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FSincos() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FSinh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FSnan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fspos() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FTan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FTgamma() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Fwprep() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FXbig() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FXp_addh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FXp_addx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FXp_getw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FXp_invx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FXp_ldexpx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FXp_movx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FXp_mulh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FXp_mulx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FXp_setn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FXp_setw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FXp_sqrtx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FXp_subx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _FZero() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Gamma_big() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Genld() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Gentime() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getcloc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getctyptab() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getdst() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Geterrno() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getfld() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getfloat() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getgloballocale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getint() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getmbcurmax() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getpcostate() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getpctype() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getpmbstate() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _getprogname() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getptimes() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getptolower() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getptoupper() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getpwcostate() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getpwcstate() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getpwctrtab() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getpwctytab() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getstr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Gettime() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Getzone() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Hugeval() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Hypot() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Inf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _init_env() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _init_tls() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Isdst() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Iswctype() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LAtan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LCosh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Ldbl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LDclass() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LDenorm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LDint() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LDivide() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LDnorm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LDscale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LDsign() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LDtento() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LDtest() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Ldtob() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LDunscale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LEps() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LErf_one() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LErf_small() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LErfc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LExp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LFpcomp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LGamma_big() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LHypot() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LInf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Litob() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LLog() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LLogpoly() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LNan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Locale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Lock_shared_ptr_spin_lock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Lock_spin_lock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Lockfilelock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Locksyslock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Locsum() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Loctab() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Locterm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Locvar() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Log() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Logpoly() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LPlsw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LPmsw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LPoly() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LPow() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LQuad() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LQuadph() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LRecip() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LRint() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LRteps() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LSin() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LSincos() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LSinh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LSnan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LTan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LTgamma() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LXbig() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LXp_addh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LXp_addx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LXp_getw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LXp_invx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LXp_ldexpx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LXp_movx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LXp_mulh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LXp_mulx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LXp_setn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LXp_setw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LXp_sqrtx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LXp_subx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _LZero() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Makeloc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Makestab() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Makewct() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _malloc_finalize_lv2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _malloc_fini() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _malloc_init() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _malloc_init_lv2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _malloc_postfork() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _malloc_prefork() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _malloc_thread_cleanup() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mbcurmax() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mbstate() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mbtowc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mbtowcx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mtx_current_owns() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mtx_destroy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mtx_init() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mtx_init_with_name() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mtx_lock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mtx_timedlock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mtx_trylock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mtx_unlock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mtxdst() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mtxinit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mtxlock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Mtxunlock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Nan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _new_setup() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Nnl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _PathLocale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _PJP_C_Copyright() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _PJP_CPP_Copyright() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Plsw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Pmsw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Poly() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Pow() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Printf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Putfld() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Putstr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Puttxt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Quad() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Quadph() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Randseed() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _readdir_unlocked() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Readloc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Recip() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _reclaim_telldir() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Restore_state() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Rint() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Rteps() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _rtld_addr_phdr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _rtld_atfork_post() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _rtld_atfork_pre() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _rtld_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _rtld_get_stack_prot() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _rtld_thread_init() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Save_state() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Scanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _sceLibcGetMallocParam() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _seekdir() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Setgloballocale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Shared_ptr_flag() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Sin() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Sincos() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Sinh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Skip() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Snan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stderr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stdin() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stdout() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stod() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stodx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stof() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stoflt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stofx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stold() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stoldx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stoll() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stollx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stolx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stopfx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stoul() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stoull() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stoullx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stoulx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Stoxflt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Strcollx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Strerror() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Strxfrmx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tgamma() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Thrd_abort() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Thrd_create() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Thrd_current() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Thrd_detach() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Thrd_equal() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Thrd_exit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Thrd_join() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Thrd_lt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Thrd_sleep() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Thrd_start() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Thrd_start_with_attr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Thrd_start_with_name() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Thrd_start_with_name_attr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Thrd_yield() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _thread_autoinit_dummy_decl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _thread_autoinit_dummy_decl_stub() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _thread_init() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _thread_init_stub() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Times() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tls_setup__Costate() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tls_setup__Ctype() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tls_setup__Errno() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tls_setup__Locale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tls_setup__Mbcurmax() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tls_setup__Mbstate() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tls_setup__Times() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tls_setup__Tolotab() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tls_setup__Touptab() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tls_setup__WCostate() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tls_setup__Wcstate() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tls_setup__Wctrans() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tls_setup__Wctype() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tolotab() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Touptab() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Towctrans() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tss_create() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tss_delete() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tss_get() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tss_set() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Ttotm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Tzoff() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _U_dyn_cancel() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _U_dyn_info_list_addr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _U_dyn_register() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_create_addr_space() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_destroy_addr_space() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_dwarf_search_unwind_table() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_get_fpreg() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_get_proc_info() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_get_proc_info_by_ip() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_get_proc_name() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_get_reg() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_get_save_loc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_handle_signal_frame() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_init_local() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_init_remote() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_is_signal_frame() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_local_addr_space() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_resume() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_set_caching_policy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_set_fpreg() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_set_reg() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ULx86_64_step() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unlock_shared_ptr_spin_lock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unlock_spin_lock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unlockfilelock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unlocksyslock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_Backtrace() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_DeleteException() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_FindEnclosingFunction() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_ForcedUnwind() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_GetBSP() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_GetCFA() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_GetDataRelBase() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_GetGR() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_GetIP() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_GetIPInfo() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_GetLanguageSpecificData() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_GetRegionStart() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_GetTextRelBase() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_RaiseException() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_Resume() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_Resume_or_Rethrow() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_SetGR() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Unwind_SetIP() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Ux86_64_flush_cache() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Ux86_64_get_accessors() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Ux86_64_get_elf_image() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Ux86_64_getcontext() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Ux86_64_is_fpreg() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Ux86_64_regname() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Ux86_64_setcontext() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Ux86_64_strerror() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Vacopy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _warn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WCostate() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Wcscollx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Wcsftime() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Wcstate() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Wcsxfrmx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Wctob() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Wctomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Wctombx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Wctrans() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Wctype() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WFrprep() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WFwprep() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WGenld() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WGetfld() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WGetfloat() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WGetint() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WGetstr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WLdtob() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WLitob() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WPrintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WPutfld() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WPutstr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WPuttxt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WScanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WStod() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WStodx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WStof() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WStoflt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WStofx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WStold() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WStoldx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WStoll() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WStopfx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WStoul() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WStoull() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _WStoxflt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xbig() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xp_addh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xp_addx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xp_getw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xp_invx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xp_ldexpx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xp_movx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xp_mulh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xp_mulx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xp_setn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xp_setw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xp_sqrtx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xp_subx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xtime_diff_to_ts() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xtime_get_ticks() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Xtime_to_ts() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZdaPv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZdaPvm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZdaPvmRKSt9nothrow_t() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZdaPvRKSt9nothrow_t() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZdaPvS_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
PS4API void _ZdlPv(void * ptr) {
    LOG_DEBUG("(%p)", ptr);
    ::operator delete(ptr);
}
//PS4API int _ZdlPvm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZdlPvmRKSt9nothrow_t() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZdlPvRKSt9nothrow_t() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZdlPvS_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Zero() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt10moneypunctIcLb0EE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt10moneypunctIcLb1EE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt10moneypunctIwLb0EE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt10moneypunctIwLb1EE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt14_Error_objectsIiE14_System_objectE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt14_Error_objectsIiE15_Generic_objectE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt14_Error_objectsIiE16_Iostream_objectE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt20_Future_error_objectIiE14_Future_objectE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt7codecvtIcc9_MbstatetE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt7collateIcE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt7collateIwE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt8messagesIcE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt8messagesIwE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt8numpunctIcE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt8numpunctIwE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVZNSt13basic_filebufIcSt11char_traitsIcEE5_InitEP7__sFILENS2_7_InitflEE7_Stinit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZGVZNSt13basic_filebufIwSt11char_traitsIwEE5_InitEP7__sFILENS2_7_InitflEE7_Stinit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv116__enum_type_infoD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv116__enum_type_infoD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv116__enum_type_infoD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv117__array_type_infoD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv117__array_type_infoD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv117__array_type_infoD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv117__class_type_infoD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv117__class_type_infoD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv117__class_type_infoD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv117__pbase_type_infoD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv117__pbase_type_infoD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv117__pbase_type_infoD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv119__pointer_type_infoD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv119__pointer_type_infoD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv119__pointer_type_infoD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv120__function_type_infoD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv120__function_type_infoD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv120__function_type_infoD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv120__si_class_type_infoD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv120__si_class_type_infoD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv120__si_class_type_infoD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv121__vmi_class_type_infoD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv121__vmi_class_type_infoD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv121__vmi_class_type_infoD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv123__fundamental_type_infoD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv123__fundamental_type_infoD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv123__fundamental_type_infoD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv129__pointer_to_member_type_infoD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv129__pointer_to_member_type_infoD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN10__cxxabiv129__pointer_to_member_type_infoD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN6Dinkum7codecvt10_Cvt_checkEmm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN6Dinkum7threads10lock_errorD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN6Dinkum7threads10lock_errorD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN6Dinkum7threads17_Throw_lock_errorEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN6Dinkum7threads21_Throw_resource_errorEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN6Dinkum7threads21thread_resource_errorD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN6Dinkum7threads21thread_resource_errorD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN9pathscale13set_terminateEPFvvE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN9pathscale14set_unexpectedEPFvvE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZN9pathscale29set_use_thread_local_handlersEb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _Znam() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZnamRKSt9nothrow_t() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNK10__cxxabiv117__class_type_info11can_cast_toEPKS0_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNK10__cxxabiv117__class_type_info7cast_toEPvPKS0_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNK10__cxxabiv120__si_class_type_info11can_cast_toEPKNS_17__class_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNK10__cxxabiv120__si_class_type_info7cast_toEPvPKNS_17__class_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNK10__cxxabiv121__vmi_class_type_info11can_cast_toEPKNS_17__class_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNK10__cxxabiv121__vmi_class_type_info7cast_toEPvPKNS_17__class_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSbIwSt11char_traitsIwESaIwEE5_XlenEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSbIwSt11char_traitsIwESaIwEE5_XranEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSs5_XlenEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSs5_XranEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt10bad_typeid4whatEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt10bad_typeid8_DoraiseEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt11logic_error4whatEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt11logic_error8_DoraiseEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt12bad_weak_ptr4whatEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt12codecvt_base11do_encodingEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt12codecvt_base13do_max_lengthEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt12future_error4whatEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt12future_error8_DoraiseEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt12system_error8_DoraiseEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt13bad_exception8_DoraiseEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt13runtime_error4whatEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt14error_category10equivalentEiRKSt15error_condition() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt14error_category10equivalentERKSt10error_codei() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt14error_category23default_error_conditionEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt17bad_function_call4whatEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt19istreambuf_iteratorIcSt11char_traitsIcEE5equalERKS2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt19istreambuf_iteratorIwSt11char_traitsIwEE5equalERKS2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt22_Future_error_category4nameEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt22_Future_error_category7messageEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt22_System_error_category23default_error_conditionEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt22_System_error_category4nameEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt22_System_error_category7messageEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt23_Generic_error_category4nameEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt23_Generic_error_category7messageEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt24_Iostream_error_category4nameEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt24_Iostream_error_category7messageEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIcE10do_tolowerEc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIcE10do_tolowerEPcPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIcE10do_toupperEc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIcE10do_toupperEPcPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIcE8do_widenEc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIcE8do_widenEPKcS2_Pc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIcE9do_narrowEcc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIcE9do_narrowEPKcS2_cPc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIwE10do_scan_isEsPKwS2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIwE10do_tolowerEPwPKw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIwE10do_tolowerEw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIwE10do_toupperEPwPKw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIwE10do_toupperEw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIwE11do_scan_notEsPKwS2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIwE5do_isEPKwS2_Ps() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIwE5do_isEsw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIwE8do_widenEc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIwE8do_widenEPKcS2_Pw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIwE9do_narrowEPKwS2_cPc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt5ctypeIwE9do_narrowEwc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIcE11do_groupingEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIcE13do_neg_formatEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIcE13do_pos_formatEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIcE14do_curr_symbolEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIcE14do_frac_digitsEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIcE16do_decimal_pointEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIcE16do_negative_signEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIcE16do_positive_signEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIcE16do_thousands_sepEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIwE11do_groupingEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIwE13do_neg_formatEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIwE13do_pos_formatEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIwE14do_curr_symbolEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIwE14do_frac_digitsEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIwE16do_decimal_pointEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIwE16do_negative_signEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIwE16do_positive_signEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7_MpunctIwE16do_thousands_sepEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIcc9_MbstatetE10do_unshiftERS0_PcS3_RS3_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIcc9_MbstatetE16do_always_noconvEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIcc9_MbstatetE2inERS0_PKcS4_RS4_PcS6_RS6_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIcc9_MbstatetE3outERS0_PKcS4_RS4_PcS6_RS6_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIcc9_MbstatetE5do_inERS0_PKcS4_RS4_PcS6_RS6_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIcc9_MbstatetE6do_outERS0_PKcS4_RS4_PcS6_RS6_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIcc9_MbstatetE6lengthERS0_PKcS4_m() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIcc9_MbstatetE7unshiftERS0_PcS3_RS3_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIcc9_MbstatetE9do_lengthERS0_PKcS4_m() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIDic9_MbstatetE10do_unshiftERS0_PcS3_RS3_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIDic9_MbstatetE11do_encodingEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIDic9_MbstatetE13do_max_lengthEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIDic9_MbstatetE16do_always_noconvEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIDic9_MbstatetE5do_inERS0_PKcS4_RS4_PDiS6_RS6_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIDic9_MbstatetE6do_outERS0_PKDiS4_RS4_PcS6_RS6_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIDic9_MbstatetE9do_lengthERS0_PKcS4_m() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIDsc9_MbstatetE10do_unshiftERS0_PcS3_RS3_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIDsc9_MbstatetE11do_encodingEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIDsc9_MbstatetE13do_max_lengthEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIDsc9_MbstatetE16do_always_noconvEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIDsc9_MbstatetE5do_inERS0_PKcS4_RS4_PDsS6_RS6_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIDsc9_MbstatetE6do_outERS0_PKDsS4_RS4_PcS6_RS6_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIDsc9_MbstatetE9do_lengthERS0_PKcS4_m() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIwc9_MbstatetE10do_unshiftERS0_PcS3_RS3_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIwc9_MbstatetE11do_encodingEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIwc9_MbstatetE13do_max_lengthEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIwc9_MbstatetE16do_always_noconvEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIwc9_MbstatetE5do_inERS0_PKcS4_RS4_PwS6_RS6_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIwc9_MbstatetE6do_outERS0_PKwS4_RS4_PcS6_RS6_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7codecvtIwc9_MbstatetE9do_lengthERS0_PKcS4_m() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7collateIcE10do_compareEPKcS2_S2_S2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7collateIcE12do_transformEPKcS2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7collateIcE4hashEPKcS2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7collateIcE7compareEPKcS2_S2_S2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7collateIcE7do_hashEPKcS2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7collateIcE9transformEPKcS2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7collateIwE10do_compareEPKwS2_S2_S2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7collateIwE12do_transformEPKwS2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7collateIwE4hashEPKwS2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7collateIwE7compareEPKwS2_S2_S2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7collateIwE7do_hashEPKwS2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7collateIwE9transformEPKwS2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERd() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERj() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERPv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERd() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERj() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERPv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8_GetffldEPcRS3_S6_RSt8ios_basePi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8_GetifldEPcRS3_S6_NSt5_IosbIiE9_FmtflagsERKSt6locale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE9_GetffldxEPcRS3_S6_RSt8ios_basePi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERd() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERj() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERPv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERd() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERj() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERPv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8_GetffldEPcRS3_S6_RSt8ios_basePi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8_GetifldEPcRS3_S6_NSt5_IosbIiE9_FmtflagsERKSt6locale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE9_GetffldxEPcRS3_S6_RSt8ios_basePi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecd() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basece() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecPKv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE5_FputES3_RSt8ios_basecPKcmmmm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE5_IputES3_RSt8ios_basecPcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecd() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basece() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecPKv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewd() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewPKv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE5_FputES3_RSt8ios_basewPKcmmmm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE5_IputES3_RSt8ios_basewPcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewd() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewPKv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8bad_cast4whatEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8bad_cast8_DoraiseEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8ios_base7failure8_DoraiseEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8messagesIcE3getEiiiRKSs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8messagesIcE4openERKSsRKSt6locale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8messagesIcE5closeEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8messagesIcE6do_getEiiiRKSs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8messagesIcE7do_openERKSsRKSt6locale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8messagesIcE8do_closeEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8messagesIwE3getEiiiRKSbIwSt11char_traitsIwESaIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8messagesIwE4openERKSsRKSt6locale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8messagesIwE5closeEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8messagesIwE6do_getEiiiRKSbIwSt11char_traitsIwESaIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8messagesIwE7do_openERKSsRKSt6locale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8messagesIwE8do_closeEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIcE11do_groupingEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIcE11do_truenameEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIcE12do_falsenameEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIcE13decimal_pointEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIcE13thousands_sepEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIcE16do_decimal_pointEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIcE16do_thousands_sepEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIcE8groupingEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIcE8truenameEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIcE9falsenameEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIwE11do_groupingEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIwE11do_truenameEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIwE12do_falsenameEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIwE13decimal_pointEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIwE13thousands_sepEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIwE16do_decimal_pointEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIwE16do_thousands_sepEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIwE8groupingEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIwE8truenameEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8numpunctIwE9falsenameEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE10date_orderEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE11do_get_dateES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE11do_get_timeES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE11do_get_yearES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE11get_weekdayES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE13do_date_orderEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE13get_monthnameES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE14do_get_weekdayES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE16do_get_monthnameES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmcc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmPKcSE_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmcc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE7_GetfmtES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE7_GetintERS3_S5_iiRiRKSt5ctypeIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8get_dateES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8get_timeES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8get_yearES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE10date_orderEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE11do_get_dateES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE11do_get_timeES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE11do_get_yearES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE11get_weekdayES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE13do_date_orderEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE13get_monthnameES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE14do_get_weekdayES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE16do_get_monthnameES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmcc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmPKwSE_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmcc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE7_GetfmtES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE7_GetintERS3_S5_iiRiRKSt5ctypeIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8get_dateES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8get_timeES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8get_yearES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecPK2tmcc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecPK2tmPKcSB_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecPK2tmcc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewPK2tmcc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewPK2tmPKwSB_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewPK2tmcc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9bad_alloc4whatEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9bad_alloc8_DoraiseEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9exception4whatEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9exception6_RaiseEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9exception8_DoraiseEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERSs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERSs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8_GetmfldERS3_S5_bRSt8ios_basePc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERSbIwS2_SaIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZNKSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERSbIwS2_SaIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8_GetmfldERS3_S5_bRSt8ios_basePw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_bRSt8ios_basece() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_bRSt8ios_basecRKSs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_bRSt8ios_basece() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_bRSt8ios_basecRKSs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE8_PutmfldES3_bRSt8ios_basecbSsc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_bRSt8ios_basewe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_bRSt8ios_basewRKSbIwS2_SaIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_bRSt8ios_basewe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_bRSt8ios_basewRKSbIwS2_SaIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNKSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE8_PutmfldES3_bRSt8ios_basewbSbIwS2_SaIwEEw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSbIwSt11char_traitsIwESaIwEE5_CopyEmm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSbIwSt11char_traitsIwESaIwEE5eraseEmm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSbIwSt11char_traitsIwESaIwEE6appendEmw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSbIwSt11char_traitsIwESaIwEE6appendERKS2_mm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSbIwSt11char_traitsIwESaIwEE6assignEmw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSbIwSt11char_traitsIwESaIwEE6assignEPKwm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSbIwSt11char_traitsIwESaIwEE6assignERKS2_mm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSbIwSt11char_traitsIwESaIwEE6insertEmmw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSiD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSiD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSo6sentryC2ERSo() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSo6sentryD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSoD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSoD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSs5_CopyEmm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSs5eraseEmm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSs6appendEmc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSs6appendERKSsmm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSs6assignEmc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSs6assignEPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSs6assignERKSsmm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSs6insertEmmc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10bad_typeidD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10bad_typeidD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10bad_typeidD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb0EE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb0EE4intlE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb0EE7_GetcatEPPKNSt6locale5facetEPKS1_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb0EEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb0EEC1EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb0EEC1ERKSt8_Locinfomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb0EEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb0EEC2EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb0EEC2ERKSt8_Locinfomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb0EED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb0EED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb0EED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb1EE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb1EE4intlE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb1EE7_GetcatEPPKNSt6locale5facetEPKS1_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb1EEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb1EEC1EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb1EEC1ERKSt8_Locinfomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb1EEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb1EEC2EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb1EEC2ERKSt8_Locinfomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb1EED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb1EED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIcLb1EED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb0EE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb0EE4intlE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb0EE7_GetcatEPPKNSt6locale5facetEPKS1_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb0EEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb0EEC1EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb0EEC1ERKSt8_Locinfomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb0EEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb0EEC2EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb0EEC2ERKSt8_Locinfomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb0EED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb0EED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb0EED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb1EE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb1EE4intlE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb1EE7_GetcatEPPKNSt6locale5facetEPKS1_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb1EEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb1EEC1EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb1EEC1ERKSt8_Locinfomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb1EEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb1EEC2EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb1EEC2ERKSt8_Locinfomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb1EED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb1EED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt10moneypunctIwLb1EED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt11logic_errorD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt11logic_errorD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt11logic_errorD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt11range_errorD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt11range_errorD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt11regex_errorD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt11regex_errorD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt11regex_errorD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12bad_weak_ptrD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12bad_weak_ptrD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12bad_weak_ptrD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12domain_errorD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12domain_errorD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12future_errorD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12future_errorD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12future_errorD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12length_errorD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12length_errorD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12length_errorD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12out_of_rangeD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12out_of_rangeD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12out_of_rangeD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders2_1E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders2_2E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders2_3E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders2_4E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders2_5E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders2_6E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders2_7E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders2_8E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders2_9E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders3_10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders3_11E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders3_12E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders3_13E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders3_14E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders3_15E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders3_16E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders3_17E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders3_18E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders3_19E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12placeholders3_20E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12system_errorC2ESt10error_codePKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12system_errorD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12system_errorD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt12system_errorD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13_Num_int_base10is_boundedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13_Num_int_base10is_integerE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13_Num_int_base14is_specializedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13_Num_int_base5radixE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13_Num_int_base8is_exactE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13_Num_int_base9is_moduloE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13_Regex_traitsIcE6_NamesE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13_Regex_traitsIwE6_NamesE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13bad_exceptionD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13bad_exceptionD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13bad_exceptionD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIcSt11char_traitsIcEE4syncEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIcSt11char_traitsIcEE5_LockEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIcSt11char_traitsIcEE5imbueERKSt6locale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIcSt11char_traitsIcEE5uflowEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIcSt11char_traitsIcEE6setbufEPci() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIcSt11char_traitsIcEE7_UnlockEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIcSt11char_traitsIcEE7seekoffElNSt5_IosbIiE8_SeekdirENS4_9_OpenmodeE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIcSt11char_traitsIcEE7seekposESt4fposI9_MbstatetENSt5_IosbIiE9_OpenmodeE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIcSt11char_traitsIcEE8overflowEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIcSt11char_traitsIcEE9_EndwriteEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIcSt11char_traitsIcEE9pbackfailEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIcSt11char_traitsIcEE9underflowEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIcSt11char_traitsIcEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIcSt11char_traitsIcEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIcSt11char_traitsIcEED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIwSt11char_traitsIwEE4syncEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIwSt11char_traitsIwEE5_LockEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIwSt11char_traitsIwEE5imbueERKSt6locale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIwSt11char_traitsIwEE5uflowEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIwSt11char_traitsIwEE6setbufEPwi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIwSt11char_traitsIwEE7_UnlockEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIwSt11char_traitsIwEE7seekoffElNSt5_IosbIiE8_SeekdirENS4_9_OpenmodeE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIwSt11char_traitsIwEE7seekposESt4fposI9_MbstatetENSt5_IosbIiE9_OpenmodeE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIwSt11char_traitsIwEE8overflowEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIwSt11char_traitsIwEE9_EndwriteEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIwSt11char_traitsIwEE9pbackfailEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIwSt11char_traitsIwEE9underflowEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIwSt11char_traitsIwEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIwSt11char_traitsIwEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_filebufIwSt11char_traitsIwEED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_istreamIwSt11char_traitsIwEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_istreamIwSt11char_traitsIwEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_ostreamIwSt11char_traitsIwEE6sentryC2ERS2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_ostreamIwSt11char_traitsIwEE6sentryD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_ostreamIwSt11char_traitsIwEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13basic_ostreamIwSt11char_traitsIwEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13runtime_errorD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13runtime_errorD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt13runtime_errorD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Error_objectsIiE14_System_objectE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Error_objectsIiE15_Generic_objectE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Error_objectsIiE16_Iostream_objectE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base10has_denormE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base10is_boundedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base10is_integerE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base11round_styleE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base12has_infinityE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base13has_quiet_NaNE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base14is_specializedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base15has_denorm_lossE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base15tinyness_beforeE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base17has_signaling_NaNE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base5radixE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base5trapsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base8is_exactE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base9is_iec559E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base9is_moduloE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14_Num_ldbl_base9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14error_categoryD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIaE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIaE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIaE9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIbE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIbE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIbE9is_moduloE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIbE9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIcE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIcE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIcE9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIdE12max_digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIdE12max_exponentE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIdE12min_exponentE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIdE14max_exponent10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIdE14min_exponent10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIdE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIdE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIDiE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIDiE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIDiE9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIDsE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIDsE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIDsE9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIeE12max_digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIeE12max_exponentE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIeE12min_exponentE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIeE14max_exponent10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIeE14min_exponent10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIeE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIeE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIfE12max_digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIfE12max_exponentE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIfE12min_exponentE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIfE14max_exponent10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIfE14min_exponent10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIfE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIfE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIhE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIhE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIhE9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIiE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIiE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIiE9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIjE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIjE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIjE9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIlE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIlE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIlE9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsImE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsImE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsImE9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIsE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIsE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIsE9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsItE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsItE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsItE9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIwE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIwE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIwE9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIxE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIxE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIxE9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIyE6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIyE8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14numeric_limitsIyE9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14overflow_errorD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14overflow_errorD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt14overflow_errorD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base10has_denormE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base10is_boundedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base10is_integerE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base11round_styleE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base12has_infinityE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base13has_quiet_NaNE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base14is_specializedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base15has_denorm_lossE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base15tinyness_beforeE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base17has_signaling_NaNE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base5radixE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base5trapsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base8is_exactE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base9is_iec559E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base9is_moduloE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15_Num_float_base9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15basic_streambufIcSt11char_traitsIcEE6xsgetnEPci() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15basic_streambufIcSt11char_traitsIcEE6xsputnEPKci() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15basic_streambufIcSt11char_traitsIcEE9showmanycEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15basic_streambufIwSt11char_traitsIwEE6xsgetnEPwi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15basic_streambufIwSt11char_traitsIwEE6xsputnEPKwi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15basic_streambufIwSt11char_traitsIwEE9showmanycEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15underflow_errorD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt15underflow_errorD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt16invalid_argumentD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt16invalid_argumentD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt16invalid_argumentD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt16nested_exceptionD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt16nested_exceptionD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt16nested_exceptionD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt17bad_function_callD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt17bad_function_callD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt17bad_function_callD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt20_Future_error_objectIiE14_Future_objectE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt20bad_array_new_lengthD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt20bad_array_new_lengthD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt20bad_array_new_lengthD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt22_Future_error_categoryD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt22_Future_error_categoryD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt22_System_error_categoryD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt22_System_error_categoryD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt23_Generic_error_categoryD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt23_Generic_error_categoryD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt24_Iostream_error_categoryD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt24_Iostream_error_categoryD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt4_Pad7_LaunchEPKcPP7pthread() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt4_Pad7_LaunchEPP7pthread() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt4_Pad8_ReleaseEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt4_PadC2EPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt4_PadC2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt4_PadD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt4_PadD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt5ctypeIcE10table_sizeE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt5ctypeIcE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt5ctypeIcED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt5ctypeIcED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt5ctypeIwE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt5ctypeIwED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt5ctypeIwED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6_Mutex5_LockEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6_Mutex7_UnlockEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6_MutexC1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6_MutexC2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6_MutexD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6_MutexD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6_Winit9_Init_cntE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6_WinitC1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6_WinitC2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6_WinitD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6_WinitD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6chrono12steady_clock12is_monotonicE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6chrono12steady_clock9is_steadyE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6chrono12system_clock12is_monotonicE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6chrono12system_clock9is_steadyE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale16_GetgloballocaleEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale16_SetgloballocaleEPv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale2id7_Id_cntE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale5_InitEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale5emptyEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale5facet7_DecrefEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale5facet7_IncrefEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale5facet9_RegisterEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale6globalERKS_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale7_Locimp7_AddfacEPNS_5facetEm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale7_Locimp8_ClocptrE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale7_Locimp8_MakelocERKSt8_LocinfoiPS0_PKS_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale7_Locimp9_MakewlocERKSt8_LocinfoiPS0_PKS_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale7_Locimp9_MakexlocERKSt8_LocinfoiPS0_PKS_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale7_LocimpC1Eb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale7_LocimpC1ERKS0_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale7_LocimpC2Eb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale7_LocimpC2ERKS0_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale7_LocimpD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale7_LocimpD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale7_LocimpD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6locale7classicEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6localeD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt6thread20hardware_concurrencyEv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7_MpunctIcE5_InitERKSt8_Locinfob() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7_MpunctIcEC2Emb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7_MpunctIcEC2EPKcmbb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7_MpunctIcED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7_MpunctIcED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7_MpunctIwE5_InitERKSt8_Locinfob() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7_MpunctIwEC2Emb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7_MpunctIwEC2EPKcmbb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7_MpunctIwED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7_MpunctIwED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIcc9_MbstatetE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIcc9_MbstatetE7_GetcatEPPKNSt6locale5facetEPKS2_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIcc9_MbstatetEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIcc9_MbstatetEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIcc9_MbstatetEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIcc9_MbstatetEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIcc9_MbstatetED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIcc9_MbstatetED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIcc9_MbstatetED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIDic9_MbstatetE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIDic9_MbstatetED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIDic9_MbstatetED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIDsc9_MbstatetE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIDsc9_MbstatetED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIDsc9_MbstatetED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIwc9_MbstatetE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIwc9_MbstatetED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7codecvtIwc9_MbstatetED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIcE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIcE7_GetcatEPPKNSt6locale5facetEPKS1_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIcEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIcEC1EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIcEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIcEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIcEC2EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIcEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIcED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIcED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIcED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIwE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIwE7_GetcatEPPKNSt6locale5facetEPKS1_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIwEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIwEC1EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIwEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIwEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIwEC2EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIwEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIwED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIwED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7collateIwED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE7_GetcatEPPKNSt6locale5facetEPKS5_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE7_GetcatEPPKNSt6locale5facetEPKS5_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE7_GetcatEPPKNSt6locale5facetEPKS5_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE7_GetcatEPPKNSt6locale5facetEPKS5_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8_Locinfo8_AddcatsEiPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8_LocinfoC1EiPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8_LocinfoC1EPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8_LocinfoC1ERKSs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8_LocinfoC2EiPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8_LocinfoC2EPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8_LocinfoC2ERKSs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8_LocinfoD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8_LocinfoD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8bad_castD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8bad_castD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8bad_castD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8ios_base4Init9_Init_cntE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8ios_base4InitC1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8ios_base4InitC2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8ios_base4InitD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8ios_base4InitD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8ios_base5_SyncE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8ios_base5clearENSt5_IosbIiE8_IostateEb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8ios_base6_IndexE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8ios_base7_AddstdEPS_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8ios_base7failureD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8ios_base7failureD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8ios_base7failureD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8ios_baseD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8ios_baseD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8ios_baseD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIcE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIcE7_GetcatEPPKNSt6locale5facetEPKS1_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIcEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIcEC1EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIcEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIcEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIcEC2EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIcEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIcED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIcED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIcED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIwE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIwE7_GetcatEPPKNSt6locale5facetEPKS1_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIwEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIwEC1EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIwEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIwEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIwEC2EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIwEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIwED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIwED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8messagesIwED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIcE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIcE7_GetcatEPPKNSt6locale5facetEPKS1_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIcEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIcEC1EPKcmb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIcEC1ERKSt8_Locinfomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIcEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIcEC2EPKcmb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIcEC2ERKSt8_Locinfomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIcED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIcED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIcED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIwE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIwE5_InitERKSt8_Locinfob() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIwE7_GetcatEPPKNSt6locale5facetEPKS1_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIwEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIwEC1EPKcmb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIwEC1ERKSt8_Locinfomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIwEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIwEC2EPKcmb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIwEC2ERKSt8_Locinfomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIwED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIwED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8numpunctIwED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE7_GetcatEPPKNSt6locale5facetEPKS5_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC1EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC2EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE7_GetcatEPPKNSt6locale5facetEPKS5_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC1EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC2EPKcm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE7_GetcatEPPKNSt6locale5facetEPKS5_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE7_GetcatEPPKNSt6locale5facetEPKS5_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base10has_denormE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base10is_boundedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base10is_integerE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base11round_styleE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base12has_infinityE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base12max_digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base12max_exponentE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base12min_exponentE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base13has_quiet_NaNE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base14is_specializedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base14max_exponent10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base14min_exponent10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base15has_denorm_lossE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base15tinyness_beforeE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base17has_signaling_NaNE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base5radixE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base5trapsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base6digitsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base8digits10E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base8is_exactE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base9is_iec559E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base9is_moduloE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9_Num_base9is_signedE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9bad_allocD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9bad_allocD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9bad_allocD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_Eb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9basic_iosIwSt11char_traitsIwEE4initEPSt15basic_streambufIwS1_Eb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9exception18_Set_raise_handlerEPFvRKS_E() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9exceptionD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9exceptionD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9exceptionD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE7_GetcatEPPKNSt6locale5facetEPKS5_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE7_GetcatEPPKNSt6locale5facetEPKS5_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE7_GetcatEPPKNSt6locale5facetEPKS5_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE2idE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE7_GetcatEPPKNSt6locale5facetEPKS5_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC1Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC1ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC2Em() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC2ERKSt8_Locinfom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9type_infoD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9type_infoD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZNSt9type_infoD2Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
PS4API void* _Znwm(size_t size) {
    LOG_DEBUG("(%zu)", size);
    auto rc = ::operator new(size);
    return rc;
}
//PS4API int _ZnwmRKSt9nothrow_t() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt10_GetloctxtIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEiRT0_S5_mPKT_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt10_GetloctxtIcSt19istreambuf_iteratorIwSt11char_traitsIwEEEiRT0_S5_mPKT_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt10_GetloctxtIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEiRT0_S5_mPKT_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt10_Rng_abortPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt10adopt_lock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt10defer_lock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt10unexpectedv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt11_Xbad_allocv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt11setiosflagsNSt5_IosbIiE9_FmtflagsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt11try_to_lock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt12setprecisioni() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt13_Cl_charnames() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt13_Syserror_mapi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt13_Xregex_errorNSt15regex_constants10error_typeE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt13get_terminatev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt13resetiosflagsNSt5_IosbIiE9_FmtflagsE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt13set_terminatePFvvE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt14_Atomic_assertPKcS0_() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt14_Cl_wcharnames() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt14_Debug_messagePKcS0_j() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt14_Raise_handler() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt14_Random_devicev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt14_Throw_C_errori() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt14_Xlength_errorPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt14_Xout_of_rangePKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt14get_unexpectedv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt14set_unexpectedPFvvE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt15_sceLibcLocinfoPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt15_Xruntime_errorPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt15future_categoryv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt15get_new_handlerv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt15set_new_handlerPFvvE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt15system_categoryv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt16_Throw_Cpp_errori() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt16_Xoverflow_errorPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt16generic_categoryv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt17_Future_error_mapi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt17iostream_categoryv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt18_String_cpp_unused() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt18_Xinvalid_argumentPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt18uncaught_exceptionv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt19_Throw_future_errorRKSt10error_code() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt19_Xbad_function_callv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt21_sceLibcClassicLocale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt22_Get_future_error_whati() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt22_Random_device_entropyv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt25_Rethrow_future_exceptionSt13exception_ptr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt3cin() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt4_Fpz() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt4cerr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt4clog() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt4cout() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt4setwi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt4wcin() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt5wcerr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt5wclog() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt5wcout() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt6_ThrowRKSt9exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt6ignore() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt7_BADOFF() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt7_FiopenPKcNSt5_IosbIiE9_OpenmodeEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt7_FiopenPKwNSt5_IosbIiE9_OpenmodeEi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt7_MP_AddPyy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt7_MP_GetPy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt7_MP_MulPyyy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt7_MP_RemPyy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt7nothrow() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt7setbasei() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt8_XLgammad() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt8_XLgammae() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt8_XLgammaf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZSt9terminatev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIa() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTId() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIDh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIDi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIDn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIDs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIj() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIN10__cxxabiv116__enum_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIN10__cxxabiv117__array_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIN10__cxxabiv117__class_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIN10__cxxabiv117__pbase_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIN10__cxxabiv119__pointer_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIN10__cxxabiv120__function_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIN10__cxxabiv120__si_class_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIN10__cxxabiv121__vmi_class_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIN10__cxxabiv123__fundamental_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIN10__cxxabiv129__pointer_to_member_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIN6Dinkum7threads10lock_errorE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIN6Dinkum7threads21thread_resource_errorE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTINSt6locale5facetE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTINSt6locale7_LocimpE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTINSt8ios_base7failureE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPa() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPd() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPDh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPDi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPDn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPDs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPj() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKa() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKd() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKDh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKDi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKDn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKDs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKj() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPKy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIPy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISo() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt10bad_typeid() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt10ctype_base() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt10money_base() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt10moneypunctIcLb0EE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt10moneypunctIcLb1EE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt10moneypunctIwLb0EE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt10moneypunctIwLb1EE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt11_Facet_base() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt11logic_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt11range_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt11regex_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt12bad_weak_ptr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt12codecvt_base() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt12domain_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt12future_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt12length_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt12out_of_range() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt12system_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt13bad_exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt13basic_filebufIcSt11char_traitsIcEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt13basic_filebufIwSt11char_traitsIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt13basic_istreamIwSt11char_traitsIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt13basic_ostreamIwSt11char_traitsIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt13messages_base() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt13runtime_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt14error_category() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt14overflow_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt15basic_streambufIcSt11char_traitsIcEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt15basic_streambufIwSt11char_traitsIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt15underflow_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt16invalid_argument() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt16nested_exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt17bad_function_call() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt20bad_array_new_length() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt22_Future_error_category() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt22_System_error_category() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt23_Generic_error_category() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt24_Iostream_error_category() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt4_Pad() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt5_IosbIiE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt5ctypeIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt5ctypeIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt7_MpunctIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt7_MpunctIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt7codecvtIcc9_MbstatetE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt7codecvtIDic9_MbstatetE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt7codecvtIDsc9_MbstatetE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt7codecvtIwc9_MbstatetE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt7collateIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt7collateIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt8bad_cast() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt8ios_base() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt8messagesIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt8messagesIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt8numpunctIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt8numpunctIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt9bad_alloc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt9basic_iosIcSt11char_traitsIcEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt9basic_iosIwSt11char_traitsIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt9exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt9time_base() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTISt9type_info() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTIy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSa() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSd() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSDi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSDn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSDs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSj() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSN10__cxxabiv116__enum_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSN10__cxxabiv117__array_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSN10__cxxabiv117__class_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSN10__cxxabiv117__pbase_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSN10__cxxabiv119__pointer_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSN10__cxxabiv120__function_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSN10__cxxabiv120__si_class_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSN10__cxxabiv121__vmi_class_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSN10__cxxabiv123__fundamental_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSN10__cxxabiv129__pointer_to_member_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSN6Dinkum7threads10lock_errorE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSN6Dinkum7threads21thread_resource_errorE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSNSt6locale5facetE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSNSt6locale7_LocimpE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSNSt8ios_base7failureE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPa() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPd() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPDi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPDn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPDs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPj() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKa() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKd() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKDi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKDn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKDs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKe() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKj() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPKy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSPy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSo() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt10bad_typeid() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt10ctype_base() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt10money_base() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt10moneypunctIcLb0EE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt10moneypunctIcLb1EE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt10moneypunctIwLb0EE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt10moneypunctIwLb1EE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt11_Facet_base() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt11logic_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt11range_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt11regex_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt12bad_weak_ptr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt12codecvt_base() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt12domain_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt12future_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt12length_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt12out_of_range() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt12system_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt13bad_exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt13basic_filebufIcSt11char_traitsIcEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt13basic_filebufIwSt11char_traitsIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt13basic_istreamIwSt11char_traitsIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt13basic_ostreamIwSt11char_traitsIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt13messages_base() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt13runtime_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt14error_category() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt14overflow_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt15basic_streambufIcSt11char_traitsIcEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt15basic_streambufIwSt11char_traitsIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt15underflow_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt16invalid_argument() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt16nested_exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt17bad_function_call() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt20bad_array_new_length() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt22_Future_error_category() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt22_System_error_category() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt23_Generic_error_category() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt24_Iostream_error_category() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt4_Pad() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt5_IosbIiE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt5ctypeIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt5ctypeIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt7_MpunctIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt7_MpunctIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt7codecvtIcc9_MbstatetE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt7codecvtIDic9_MbstatetE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt7codecvtIDsc9_MbstatetE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt7codecvtIwc9_MbstatetE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt7collateIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt7collateIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt8bad_cast() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt8ios_base() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt8messagesIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt8messagesIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt8numpunctIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt8numpunctIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt9bad_alloc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt9basic_iosIcSt11char_traitsIcEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt9basic_iosIwSt11char_traitsIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt9exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt9time_base() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSSt9type_info() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTSy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTv0_n24_NSiD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTv0_n24_NSiD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTv0_n24_NSoD0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTv0_n24_NSoD1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTv0_n24_NSt13basic_istreamIwSt11char_traitsIwEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTv0_n24_NSt13basic_istreamIwSt11char_traitsIwEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTv0_n24_NSt13basic_ostreamIwSt11char_traitsIwEED0Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTv0_n24_NSt13basic_ostreamIwSt11char_traitsIwEED1Ev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVN10__cxxabiv116__enum_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVN10__cxxabiv117__array_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVN10__cxxabiv117__class_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVN10__cxxabiv117__pbase_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVN10__cxxabiv119__pointer_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVN10__cxxabiv120__function_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVN10__cxxabiv120__si_class_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVN10__cxxabiv121__vmi_class_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVN10__cxxabiv123__fundamental_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVN10__cxxabiv129__pointer_to_member_type_infoE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVN6Dinkum7threads10lock_errorE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVN6Dinkum7threads21thread_resource_errorE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVNSt6locale7_LocimpE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVNSt8ios_base7failureE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSo() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt10bad_typeid() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt10moneypunctIcLb0EE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt10moneypunctIcLb1EE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt10moneypunctIwLb0EE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt10moneypunctIwLb1EE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt11logic_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt11range_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt11regex_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt12bad_weak_ptr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt12domain_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt12future_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt12length_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt12out_of_range() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt12system_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt13bad_exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt13basic_filebufIcSt11char_traitsIcEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt13basic_filebufIwSt11char_traitsIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt13basic_istreamIwSt11char_traitsIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt13basic_ostreamIwSt11char_traitsIwEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt13runtime_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt14error_category() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt14overflow_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt15underflow_error() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt16invalid_argument() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt16nested_exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt17bad_function_call() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt20bad_array_new_length() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt22_Future_error_category() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt22_System_error_category() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt23_Generic_error_category() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt24_Iostream_error_category() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt4_Pad() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt5ctypeIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt5ctypeIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt7_MpunctIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt7_MpunctIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt7codecvtIcc9_MbstatetE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt7codecvtIDic9_MbstatetE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt7codecvtIDsc9_MbstatetE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt7codecvtIwc9_MbstatetE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt7collateIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt7collateIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt8bad_cast() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt8ios_base() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt8messagesIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt8messagesIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt8numpunctIcE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt8numpunctIwE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt9bad_alloc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt9exception() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZTVSt9type_info() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8_GetffldEPcRS3_S6_RSt8ios_basePiE4_Src() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8_GetifldEPcRS3_S6_NSt5_IosbIiE9_FmtflagsERKSt6localeE4_Src() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE9_GetffldxEPcRS3_S6_RSt8ios_basePiE4_Src() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8_GetffldEPcRS3_S6_RSt8ios_basePiE4_Src() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8_GetifldEPcRS3_S6_NSt5_IosbIiE9_FmtflagsERKSt6localeE4_Src() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE9_GetffldxEPcRS3_S6_RSt8ios_basePiE4_Src() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZZNKSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8_GetmfldERS3_S5_bRSt8ios_basePcE4_Src() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZZNKSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8_GetmfldERS3_S5_bRSt8ios_basePwE4_Src() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZZNKSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_bRSt8ios_basecRKSsE4_Src() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int
//_ZZNKSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_bRSt8ios_basewRKSbIwS2_SaIwEEE4_Src() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZZNSt13basic_filebufIcSt11char_traitsIcEE5_InitEP7__sFILENS2_7_InitflEE7_Stinit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _ZZNSt13basic_filebufIwSt11char_traitsIwEE5_InitEP7__sFILENS2_7_InitflEE7_Stinit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int abort() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int abort_handler_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int abs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int acos() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int acosf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int acosh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int acoshf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int acoshl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int acosl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int alarm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int asctime() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int asctime_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int asin() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int asinf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int asinh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int asinhf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int asinhl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int asinl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int asprintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int at_quick_exit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int atan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int atan2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int atan2f() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int atan2l() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int atanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int atanh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int atanhf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int atanhl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int atanl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int atexit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int atof() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int atoi() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int atol() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int atoll() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int backtrace() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int basename() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int basename_r() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int bcmp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int bcopy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int bsearch() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int bsearch_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int btowc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int bzero() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int c16rtomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int c32rtomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API void* calloc(size_t _NumOfElements,size_t _SizeOfElements) {
//    LOG_DEBUG("%s(0x%zx, 0x%xx)\n", __FUNCTION__, _NumOfElements, _SizeOfElements);
//    auto rc = ::calloc(_NumOfElements, _SizeOfElements);
//    return rc;
//}
//PS4API int cbrt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int cbrtf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int cbrtl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ceil() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ceilf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ceill() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int clearerr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sce_clearerr_unlocked() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int clock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int clock_1700() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int closedir() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int copysign() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int copysignf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int copysignl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int cos() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int cosf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int cosh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int coshf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int coshl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int cosl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ctime() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ctime_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int daemon() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int daylight() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int devname() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int devname_r() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int difftime() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int dirname() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int div() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int drand48() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int drem() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int dremf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int erand48() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int erf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int erfc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int erfcf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int erfcl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int erff() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int erfl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int err() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int err_set_exit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int err_set_file() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int errc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int errx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int exit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int exp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int exp2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int exp2f() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int exp2l() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API float expf(float x) {
//    auto rc = ::expf(x);
//    return rc;
//}
//PS4API int expl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int expm1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int expm1f() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int expm1l() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fabs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fabsf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fabsl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fclose() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fcloseall() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fdim() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fdimf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fdiml() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fdopen() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fdopendir() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int feclearexcept() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fedisableexcept() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int feenableexcept() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fegetenv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fegetexcept() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fegetexceptflag() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fegetround() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fegettrapenable() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int feholdexcept() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int feof() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sce_feof_unlocked() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int feraiseexcept() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ferror() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sce_ferror_unlocked() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fesetenv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fesetexceptflag() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fesetround() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fesettrapenable() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fetestexcept() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int feupdateenv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fflush() {
//    //UNIMPLEMENTED_FUNC();
//    return 0; // TODO: implement this!
//}
//PS4API int fgetc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fgetln() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fgetpos() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fgets() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fgetwc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fgetws() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fileno() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sce_fileno_unlocked() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int finite() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int finitef() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int flockfile() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int floor() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int floorf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int floorl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int flsl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fma() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fmaf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fmal() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fmax() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fmaxf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fmaxl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fmin() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fminf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fminl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fmod() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fmodf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fmodl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fopen() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fopen_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fprintf(FILE *stream, const char *format, ...) {
//    int rc = ::fprintf(stream, "%s", format);
//    return rc; // TODO: implement this!
//}
//PS4API int fprintf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fpurge() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fputc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fputs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fputwc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fputws() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fread() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API void free(void* p) {
//    LOG_DEBUG("%s(%p)\n",__FUNCTION__, p);
//    ::free(p);
//}
//PS4API int freeifaddrs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int freopen() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int freopen_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int frexp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int frexpf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int frexpl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fscanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fscanf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fseek() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fseeko() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fsetpos() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fstatvfs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ftell() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ftello() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ftrylockfile() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int funlockfile() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fwide() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fwprintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fwprintf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fwrite() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fwscanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int fwscanf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int gamma() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int gamma_r() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int gammaf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int gammaf_r() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int getc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sce_getc_unlocked() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int getchar() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sce_getchar_unlocked() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int getcwd() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int getenv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int gethostname() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int getifaddrs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int getopt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int getopt_long() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int getopt_long_only() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int getprogname() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int gets() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int gets_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int getw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int getwc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int getwchar() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int gmtime() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int gmtime_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int hypot() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int hypotf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int hypotl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ignore_handler_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ilogb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ilogbf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ilogbl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int imaxabs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int imaxdiv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int index() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int inet_addr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int inet_aton() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int inet_ntoa() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int inet_ntoa_r() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int initstate() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int isalnum() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int isalpha() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int isblank() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int iscntrl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int isdigit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int isgraph() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int isinf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int islower() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int isnan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int isnanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int isprint() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ispunct() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int isspace() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int isupper() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int iswalnum() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int iswalpha() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int iswblank() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int iswcntrl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int iswctype() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int iswdigit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int iswgraph() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int iswlower() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int iswprint() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int iswpunct() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int iswspace() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int iswupper() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int iswxdigit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int isxdigit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int j0() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int j0f() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int j1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int j1f() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int jn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int jnf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int jrand48() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int labs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int lcong48() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ldexp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ldexpf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ldexpl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ldiv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int lgamma() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int lgamma_r() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int lgammaf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int lgammaf_r() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int lgammal() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int llabs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int lldiv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int llrint() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int llrintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int llrintl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int llround() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int llroundf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int llroundl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int localeconv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int localtime() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int localtime_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int log() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int log10() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int log10f() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int log10l() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int log1p() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int log1pf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int log1pl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int log2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int log2f() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int log2l() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int logb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int logbf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int logbl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int logf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int logl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int longjmp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int lrand48() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int lrint() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int lrintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int lrintl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int lround() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int lroundf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int lroundl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int makecontext() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
PS4API void* malloc(size_t _Size) {
    LOG_DEBUG("size = 0x%zx", _Size);
    void* rc = ::malloc(_Size);
    return rc;
}
//PS4API int malloc_finalize() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int malloc_initialize() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int malloc_stats() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int malloc_stats_fast() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int malloc_usable_size() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int mblen() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int mbrlen() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int mbrtoc16() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int mbrtoc32() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int mbrtowc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int mbsinit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int mbsrtowcs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int mbsrtowcs_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int mbstowcs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int mbstowcs_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int mbtowc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int memalign() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int memchr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int memcmp(const void* _Buf1, const void* _Buf2, size_t _Size) {
//    int rc = ::memcmp(_Buf1, _Buf2, _Size);
//    return rc;
//}
//PS4API void* memcpy(void* __restrict__ _Dst, const void* __restrict__ _Src, size_t _Size) {
//    void* rc = ::memcpy(_Dst, _Src, _Size);
//    return rc;
//}
//#ifndef errno_t
//#define errno_t int
//#endif
//PS4API errno_t memcpy_s(void* _dest, size_t _numberOfElements, const void* _src, size_t _count) {
//#if defined(_WIN64)
//    auto rc = ::memcpy_s(_dest, _numberOfElements, _src, _count);
//#else
//    int rc = 0;
//    ::memcpy(_dest, _src, _count);
//#endif
//
//    return rc;
//}
//PS4API void* memmove(void *_Dst,const void *_Src,size_t _Size) {
//    auto rc =::memmove(_Dst, _Src, _Size);
//    return rc;
//}
//PS4API int memmove_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int memrchr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API void* memset(void* dst, int val, size_t size) {
//    LOG_DEBUG("%s(%p, %d, %zx)\n",__FUNCTION__, dst, val, size);
//    void* rc = ::memset(dst, val, size);
//    return rc;
//}
//PS4API int mergesort() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int mktime() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int modf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int modff() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int modfl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int mrand48() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int nan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int nanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int nanl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int nearbyint() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int nearbyintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int nearbyintl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int Need_sceLibcInternal() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int nextafter() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int nextafterf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int nextafterl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int nexttoward() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int nexttowardf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int nexttowardl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int nrand48() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int opendir() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int optarg() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int opterr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int optind() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int optopt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int optreset() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int perror() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_memalign() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawn_file_actions_addclose() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawn_file_actions_adddup2() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawn_file_actions_addopen() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawn_file_actions_destroy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawn_file_actions_init() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawnattr_destroy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawnattr_getflags() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawnattr_getpgroup() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawnattr_getschedparam() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawnattr_getschedpolicy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawnattr_getsigdefault() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawnattr_getsigmask() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawnattr_init() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawnattr_setflags() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawnattr_setpgroup() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawnattr_setschedparam() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawnattr_setschedpolicy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawnattr_setsigdefault() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawnattr_setsigmask() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int posix_spawnp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int pow() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int powf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int powl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//
//#if defined(_WIN64) || defined(__CYGWIN__)
//    PS4API int printf(/*const char *__format, ...*/) {
//        uint64_t* stack;
//        const char* p0;
//        uint64_t p1;
//        uint64_t p2;
//        uint64_t p3;
//        uint64_t p4;
//        uint64_t p5;
//        __asm__ ("movq %%rbp, %0 ;\n" : "=r"(stack) ::);
//        __asm__ ("movq %%rdi, %0 ;\n" : "=r"(p0) ::);
//        __asm__ ("movq %%rsi, %0 ;\n" : "=r"(p1) ::);
//        __asm__ ("movq %%rdx, %0 ;\n" : "=r"(p2) ::);
//        __asm__ ("movq %%rcx, %0 ;\n" : "=r"(p3) ::);
//        __asm__ ("movq %%r8,  %0 ;\n" : "=r"(p4) ::);
//        __asm__ ("movq %%r9,  %0 ;\n" : "=r"(p5) ::);
//
//        uint64_t p6 = *(stack + 2);
//        uint64_t p7 = *(stack + 3);
//
//        int rc = LOG_DEBUG(p0, p1, p2, p3, p4, p5, p6, p7);
//        return rc;
//    }
//#else
//    PS4API int printf(const char *format, ...) {
//    char buffer[256];
//    va_list args;
//    va_start (args, format);
//    int rc = vprintf(format, args);
//    va_end (args);
//    return rc;
//}
//#endif
//
////PS4API int printf(const char* format, ...) {
////    int rc = ::printf("%s", format); // TODO implement this!
////    return rc;
////}
//PS4API int printf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int psignal() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int putc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sce_putc_unlocked() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int putchar() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sce_putchar_unlocked() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int putenv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int puts(const char* text) {
//    LOG_DEBUG("%s(%s)\n", __FUNCTION__, text);
//    int rc = ::puts(text);
//    return rc;
//}
//PS4API int putw() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int putwc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int putwchar() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int qsort() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int qsort_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int quick_exit() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int rand() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sce_rand_r() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int rand_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int random() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int readdir() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int readdir_r() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int realloc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int reallocalign() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int reallocf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int realpath() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int remainder() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int remainderf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int remainderl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int remove() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int remquo() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int remquof() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int remquol() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int rewind() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int rewinddir() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int rindex() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int rint() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int rintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int rintl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int round() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int roundf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int roundl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int scalb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int scalbf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int scalbln() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int scalblnf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int scalblnl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int scalbn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int scalbnf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int scalbnl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int scanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int scanf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcMspaceCalloc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API SceLibcMspace sceLibcMspaceCreate(const char* name, void* base, size_t capacity, unsigned int flag) {
//    ::printf("sceLibcMspaceCreate(\"%s\", %p, 0x%llx, %u)\n", name, base, capacity, flag);
//    SceLibcMspace msp = ::new _SceLibcMspace;
//    msp->base = (char*)base;
//    msp->size = capacity;
//    msp->offset = 0;
//    ::strncpy(msp->name, name, 32);
//    return msp;
//}
//PS4API int sceLibcMspaceDestroy(SceLibcMspace msp) {
//    ::delete msp;
//    return 0;
//}
//PS4API int sceLibcMspaceFree(SceLibcMspace msp, void *ptr) {
//    (void)msp;
//    if (ptr != nullptr) {
//        ::free(ptr);
//    }
//    return 0;
//}
//PS4API int sceLibcMspaceIsHeapEmpty(SceLibcMspace msp) {
//    (void)msp;
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API void* sceLibcMspaceMalloc(SceLibcMspace msp, size_t size) {
//    (void)msp;
//    void* rc = ::malloc(size);
//    return rc; // TODO: implement this!
//}
//PS4API int sceLibcMspaceMallocStats() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcMspaceMallocStatsFast() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcMspaceMallocUsableSize() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcMspaceMemalign() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcMspacePosixMemalign() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcMspaceRealloc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcMspaceReallocalign() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcOnce() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceCalloc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceCheckMemoryBounds() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceCreate() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceDestroy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceFree() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceGetFooterValue() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceIsHeapEmpty() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceMalloc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceMallocStats() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceMallocStatsFast() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceMallocUsableSize() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceMemalign() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspacePosixMemalign() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceRealloc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceReallocalign() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceReportMemoryBlocks() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sceLibcPafMspaceTrim() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int seed48() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int seekdir() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int set_constraint_handler_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int setbuf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int setenv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sce_setjmp(jmp_buf env) {
//    //UNIMPLEMENTED_FUNC();
//    int rc = ::setjmp(env);
//    return rc;
//}
//PS4API int setlocale() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int setstate() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int setvbuf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sigblock() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int siginterrupt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int signalcontext() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//
//PS4API int significand() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int significandf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sigsetmask() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sigvec() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sin() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sinf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sinh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sinhf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sinhl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sinl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//
//#if defined(_WIN64) || defined(__CYGWIN__)
//PS4API int snprintf(/*char *__stream, size_t __n, const char *__format, ...*/) {
//    uint64_t* stack;
//    char* p0;
//    uint64_t p1;
//    const char*  p2;
//    uint64_t p3;
//    uint64_t p4;
//    uint64_t p5;
//    __asm__ ("movq %%rbp, %0 ;\n" : "=r"(stack) ::);
//    __asm__ ("movq %%rdi, %0 ;\n" : "=r"(p0) ::);
//    __asm__ ("movq %%rsi, %0 ;\n" : "=r"(p1) ::);
//    __asm__ ("movq %%rdx, %0 ;\n" : "=r"(p2) ::);
//    __asm__ ("movq %%rcx, %0 ;\n" : "=r"(p3) ::);
//    __asm__ ("movq %%r8,  %0 ;\n" : "=r"(p4) ::);
//    __asm__ ("movq %%r9,  %0 ;\n" : "=r"(p5) ::);
//
//    uint64_t p6 = *(stack + 2);
//    uint64_t p7 = *(stack + 3);
//
//    int rc = std::snprintf(p0, p1, p2, p3, p4, p5, p6, p7);
//    return rc;
//    }
//#else
//    PS4API int snprintf(char *stream, size_t n, const char *format, ...) {
//    char buffer[256];
//    va_list args;
//    va_start (args, format);
//    int rc = vsnprintf(stream, n, format, args);
//    va_end (args);
//    return rc;
//}
//#endif
//
//PS4API int snprintf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int snwprintf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sprintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sprintf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sqrt() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sqrtf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sqrtl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int srand() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int srand48() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int srandom() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int srandomdev() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sscanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sscanf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int statvfs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sce_stderr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sce_stdin() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sce_stdout() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int stpcpy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strcasecmp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strcat() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strcat_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strchr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strcmp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strcoll() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strcpy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API errno_t strcpy_s(char* _Dst, size_t _SizeInBytes, const char* _Src) {
//#if defined(_WIN64)
//    errno_t rc = ::strcpy_s(_Dst, _SizeInBytes, _Src);
//#else
//    int rc = 0;
//    ::strncpy(_Dst, _Src, _SizeInBytes);
//#endif
//    return rc;
//}
//PS4API int strcspn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strdup() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strerror() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strerror_r() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strerror_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strerrorlen_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strftime() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strlcat() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//
//#ifndef strlcpy
//    PS4API size_t strlcpy(char *dst, const char *src, size_t dsize)
//    {
//        const char *osrc = src;
//        size_t nleft = dsize;
//
//        /* Copy as many bytes as will fit. */
//        if (nleft != 0) {
//            while (--nleft != 0) {
//                if ((*dst++ = *src++) == '\0')
//                    break;
//            }
//        }
//
//        /* Not enough room in dst, add NUL and traverse rest of src. */
//        if (nleft == 0) {
//            if (dsize != 0)
//                *dst = '\0';		/* NUL-terminate dst */
//            while (*src++)
//                ;
//        }
//
//        return(src - osrc - 1);	/* count does not include NUL */
//    }
//#else
//    PS4API size_t strlcpy(char * destination, const char * source, size_t num) {
//    ::printf("libc(SfQIZcqvvms), strlcpy(%p, \"%s\", %zu)\n", destination, source, num);
//    return ::strlcpy(destination, source, num);
//}
//#endif
//
//PS4API size_t strlen(const char *_Str) {
//    auto rc = ::strlen(_Str);
//    return rc;
//}
//PS4API int strncasecmp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strncat() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strncat_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strncmp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API char* strncpy(char * __restrict__ _Dest,const char * __restrict__ _Source,size_t _Count) {
//    char* rc = ::strncpy(_Dest, _Source, _Count);
//    return rc;
//}
//PS4API int strncpy_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strndup() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strnlen() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strnlen_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strnstr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strpbrk() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strrchr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strsep() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strspn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strstr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strtod() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strtof() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strtoimax() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strtok() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strtok_r() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strtok_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strtol() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strtold() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strtoll() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strtoul() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strtoull() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strtoumax() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strtouq() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int strxfrm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int swprintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int swprintf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int swscanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int swscanf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sys_nsig() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sys_siglist() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sys_signame() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int syslog() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int tan() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int tanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int tanh() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int tanhf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int tanhl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int tanl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int telldir() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int tgamma() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int tgammaf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int tgammal() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int time() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int timezone() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int tolower() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int toupper() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int towctrans() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int towlower() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int towupper() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int trunc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int truncf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int truncl() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int tzname() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int tzset() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ungetc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int ungetwc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int unsetenv() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int unw_backtrace() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int utime() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vasprintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int verr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int verrc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int verrx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vfprintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vfprintf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vfscanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vfscanf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vfwprintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vfwprintf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vfwscanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vfwscanf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vprintf() {
//    //UNIMPLEMENTED_FUNC();
//    return 0; // TODO: implement this!
//}
//PS4API int vprintf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vscanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vscanf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vsnprintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vsnprintf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vsnwprintf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vsprintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vsprintf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vsscanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vsscanf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vswprintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vswprintf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vswscanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vswscanf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vsyslog() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vwarn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vwarnc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vwarnx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vwprintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vwprintf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vwscanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int vwscanf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int warn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int warnc() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int warnx() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcrtomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcrtomb_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcscat() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcscat_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcschr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcscmp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcscoll() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcscpy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcscpy_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcscspn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcsftime() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcslen() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcsncat() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcsncat_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcsncmp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcsncpy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcsncpy_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcsnlen_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcspbrk() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcsrchr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcsrtombs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcsrtombs_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcsspn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcsstr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcstod() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcstof() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcstoimax() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcstok() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcstok_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcstol() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcstold() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcstoll() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcstombs() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcstombs_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcstoul() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcstoull() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcstoumax() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wcsxfrm() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wctob() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wctomb() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wctomb_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wctrans() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wctype() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wmemchr() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wmemcmp() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wmemcpy() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wmemcpy_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wmemmove() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wmemmove_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wmemset() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wprintf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wprintf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wscanf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int wscanf_s() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int xtime_get() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int y0() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int y0f() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int y1() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int y1f() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int yn() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//
//PS4API int sceLibcInternalMemoryMutexEnable() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int _SceLibcDebugOut() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//
//PS4API int ynf() {
//    UNIMPLEMENTED_FUNC();
//    return 0;
//}
//PS4API int sce_signgam() {
//    return signgam;
//}
//
//PS4API int sceLibcHeapGetTraceInfo(uint64_t* p[4]) {
//    static uint64_t p1[2]{};
//    static uint64_t p2[0x10]{};
//
//    p[0] = 0;
//    p[1] = 0;
//    p[2] = p1;
//    p[3] = p2;
//    return 0;
//}
//
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
////    fun("0sHarDG9BY4",
////        _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE7_GetcatEPPKNSt6locale5facetEPKS5_);
////    fun("XqbpfYmAZB4", _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC2Em);
////    fun("bItEABINEm0", _Getfld);
////    fun("ZtjspkJQ+vw", _FSin);
////    fun("S8kp05fMCqU", _ZTSSt16nested_exception);
////    fun("RfpPDUaxVJM", _ZTVSt10moneypunctIwLb0EE);
////    fun("8Jr4cvRM6EM", _Getgloballocale);
////    fun("dXy+lFOiaQA", _ZNKSt22_System_error_category4nameEv);
////    fun("mdLGxBXl6nk", __gesf2);
////    fun("jykT-VWQVBY", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE5_IputES3_RSt8ios_basecPcm);
////    fun("vAc9y8UQ31o", funlockfile);
////    fun("C3sAx2aJy3E", _ZTISt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("cjmJLdYnT5A", _ZN6Dinkum7threads21_Throw_resource_errorEv);
////    fun("ec0YLGHS8cw", _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE7_GetcatEPPKNSt6locale5facetEPKS5_);
////    fun("E5NdzqEmWuY", _ZNKSt7codecvtIDic9_MbstatetE13do_max_lengthEv);
////    fun("3EgxfDRefdw", expm1f);
////    fun("J3XuGS-cC0Q", lround);
////    fun("Lc-l1GQi7tg", _ZTISt13basic_ostreamIwSt11char_traitsIwEE);
////    fun("IvF98yl5u4s", erfcf);
////    fun("w6Aq68dFoP4", fwide);
////    fun("akpGErA1zdg", iscntrl);
////    fun("PsrRUg671K0", __cxa_increment_exception_refcount);
////    fun("fSZ+gbf8Ekc", __ashrdi3);
////    fun("w9NzCYAjEpQ", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecd);
////    fun("Noj9PsJrsa8", wmemmove);
////    fun("TEMThaOLu+c", _ZNSt14numeric_limitsIDiE9is_signedE);
////    fun("dmHEVUqDYGw", _rtld_error);
////    fun("DBO-xlHHEn8", _ZTVSt13basic_istreamIwSt11char_traitsIwEE);
////    fun("jT3xiGpA3B4", asctime);
////    fun("UhKI6z9WWuo", _err);
////    fun("tkZ7jVV6wJ8", _ZNSt10bad_typeidD2Ev);
////    fun("M5KJmq-gKM8", jnf);
////    fun("gOzIhGUAkME", _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC1EPKcm);
////    fun("diSRws0Ppxg", _ZNSt13_Regex_traitsIcE6_NamesE);
////    fun("CDm+TUClE7E", _ZNSt8numpunctIcEC1ERKSt8_Locinfomb);
////    fun("15lB7flw-9w", _ZNSt20bad_array_new_lengthD2Ev);
////    fun("nqD9bBu-W6U", _Unwind_GetDataRelBase);
////    fun("yL91YD-WTBc", _Exp);
////    fun("HcpmBnY1RGE", _FDint);
////    fun("TuhGCIxgLvA", _ZNSt7codecvtIDic9_MbstatetED1Ev);
////    fun("sOOMlZoy1pg", wcsrtombs);
////    fun("Al8MZJh-4hM", wmemset);
////    fun("m2lChTx-9tM", _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC2ERKSt8_Locinfom);
////    fun("N03wZLr2RrE", _ZNSt15_Num_float_base14is_specializedE);
////    fun("cqvea9uWpvQ", _ZTVSt12length_error);
////    fun("EH-x713A99c", atan2f);
////    fun("v9XNTmsmz+M", __absvti2);
////    fun("uiRALKOdAZk", _ZNSt8numpunctIwE5_InitERKSt8_Locinfob);
////    fun("M0n7l76UVyE", _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC2EPKcm);
////    fun("pSQz254t3ug", _ZNKSt5ctypeIwE10do_scan_isEsPKwS2_);
////    fun("0NwCmZv7XcU", _FDnorm);
////    fun("BbXxNgTW1x4", _ZNKSt10bad_typeid4whatEv);
////    fun("fttiF7rDdak", _Call_once);
////    fun("iWtXRduTjHA", _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC2ERKSt8_Locinfom);
////    fun("VoUwme28y4w", _ZSt18_String_cpp_unused);
////    fun("YDnLaav6W6Q", _Stoulx);
////    fun("ybn35k-I+B0", _ZSt13_Cl_charnames);
////    fun("7BGUDQDJu-A", posix_spawnattr_getflags);
////    fun("lhnrCOBiTGo", isblank);
////    fun("Q+ZnPMGI4M0", _ZN6Dinkum7threads21thread_resource_errorD0Ev);
////    fun("7SXNu+0KBYQ", wcstof);
////    fun("pNWIpeE5Wv4", _FXp_sqrtx);
////    fun("-7nRJFXMxnM", fputws);
////    fun("aUNISsPboqE", _ZNKSt7_MpunctIcE11do_groupingEv);
////    fun("tcBJa2sYx0w", __ltdf2);
////    fun("y9Ur6T0A0p8", _FQuad);
////    fun("uWZBRxLAvEg", _ZNKSt12system_error8_DoraiseEv);
////    fun("jVS263HH1b0", expm1l);
////    fun("bxLH5WHgMBY", _ZNSt8ios_base4InitD2Ev);
////    fun("WbBz4Oam3wM", _ZTISt13messages_base);
////    fun("cx-1THpef1A",
////        _ZNKSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERe);
////    fun("QMFyLoqNxIg", setvbuf);
////    fun("JoeZJ15k5vU", _Tls_setup__Ctype);
////    fun("MAalgQhejPc", _ZNSt23_Generic_error_categoryD1Ev);
////    fun("2w+4Mo2DPro", _ZNKSt5ctypeIcE10do_tolowerEPcPKc);
////    fun("oe9tS0VztYk", _ZNSt13runtime_errorD2Ev);
////    fun("Pn2dnvUmbRA", _Atomic_fetch_and_4);
////    fun("FU03r76UxaU", tgammal);
////    fun("aK1Ymf-NhAs", _ZTVSt7codecvtIcc9_MbstatetE);
////    fun("sHagUsvHBnk",
////        _ZZNKSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_bRSt8ios_basecRKSsE4_Src);
////    fun("BTsuaJ6FxKM", wmemcpy_s);
////    fun("C-3N+mEQli4", _ZTSNSt8ios_base7failureE);
////    fun("2bASh0rEeXI", _ZSt4wcin);
////    fun("lKEN2IebgJ0", longjmp);
////    fun("dU8Q2yzFNQg", _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC2Em);
////    fun("mI0SR5s7kxE", _ZTSv);
////    fun("HPlmfRhMWwo", _Unwind_GetTextRelBase);
////    fun("+KIicT+q2Cc", __libunwind_Unwind_GetIP);
////    fun("i3E1Ywn4t+8", inet_ntoa_r);
////    fun("HDvFM0iZYXo", __atomic_exchange_2);
////    fun("7ZFy2m9rc5A", _reclaim_telldir);
////    fun("7gc-QliZnMc", _ZNSt14_Num_ldbl_base5trapsE);
////    fun("NesIgTmfF0Q", bsearch);
////    fun("Qoo175Ig+-k", _ZSt21_sceLibcClassicLocale);
////    fun("m5wN+SwZOR4", putchar);
////    fun("CUkG1cK2T+U", _ZTVSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("3kjXzznHyCg",
////        _ZZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8_GetifldEPcRS3_S6_NSt5_IosbIiE9_FmtflagsERKSt6localeE4_Src);
////    fun("zrmR88ClfOs", _ZNSt6locale7_LocimpC2Eb);
////    fun("eGkOpTojJl4", isprint);
////    fun("zPWCqkg7V+o", _ZSt14get_unexpectedv);
////    fun("xEszJVGpybs", sprintf_s);
////    fun("N8KLCZc3Y1w", _ZTSx);
////    fun("b2na0Dzd5j8", _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC2ERKSt8_Locinfom);
////    fun("Lyx2DzUL7Lc", fmaxf);
////    fun("SwJ-E2FImAo", _Atomic_compare_exchange_strong_1);
////    fun("+hlZqs-XpUM", _ZTVSt7_MpunctIwE);
////    fun("a-z7wxuYO2E", _ZNSt4_Pad8_ReleaseEv);
////    fun("KvOHPTz595Y", atanl);
////    fun("DKkwPpi+uWc",
////        _ZZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE9_GetffldxEPcRS3_S6_RSt8ios_basePiE4_Src);
////    fun("Gt5RT417EGA", dremf);
////    fun("zyhiiLKndO8", _ZNSt7codecvtIcc9_MbstatetE7_GetcatEPPKNSt6locale5facetEPKS2_);
////    fun("4+y8-2NsDw0", _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8_GetffldEPcRS3_S6_RSt8ios_basePi);
////    fun("wuOrunkpIrU", _ZTVSt17bad_function_call);
////    fun("cCXjU72Z0Ow", _Sin);
////    fun("MB1VCygerRU", _ZNSt13basic_ostreamIwSt11char_traitsIwEE6sentryD2Ev);
////    fun("L3XZiuKqZUM", getchar);
////    fun("IRVqdGwSNXE",
////        _ZNKSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERe);
////    fun("HPGLb8Qo6as", _LDivide);
////    fun("AsShnG3DulM", _ZNSt12domain_errorD2Ev);
////    fun("BSVJqITGCyI", _ZNSt7collateIcE7_GetcatEPPKNSt6locale5facetEPKS1_);
////    fun("-EgSegeAKl4", _ZNSt6locale5facet7_IncrefEv);
////    fun("ABFE75lbcDc", _ZNKSt7codecvtIwc9_MbstatetE6do_outERS0_PKwS4_RS4_PcS6_RS6_);
////    fun("qeA5qUg9xBk", _ZNSt9_Num_base12max_digits10E);
////    fun("-kU6bB4M-+k", strspn);
////    fun("RZA5RZawY04", telldir);
////    fun("L2YaHYQdmHw", significandf);
////    fun("HLDcfGUMNWY", __subdf3);
////    fun("zdJ3GXAcI9M", __divsi3);
////    fun("NDcSfcYZRC8", malloc_usable_size);
////    fun("pv2etu4pocs", remainder);
////    fun("RbzWN8X21hY", _ZTVSt11range_error);
////    fun("RgJjmluR+QA", _ZTSSo);
////    fun("ksNM8H72JHg", _ZGVNSt10moneypunctIwLb1EE2idE);
////    fun("LbLiLA2biaI", _LXp_mulx);
////    fun("v7XHt2HwUVI", _ZNSt14numeric_limitsIyE6digitsE);
////    fun("q-WOrJNOlhI", _ZNSt15_Num_float_base10has_denormE);
////    fun("Qd6zUdRhrhs", _ZTIt);
////    fun("mfHdJTIvhuo", sceLibcMspaceMallocStats);
////    fun("7AaHj1O8-gI", _ZNSt14numeric_limitsImE6digitsE);
////    fun("d-YRIvO0jXI", _ZSt5wcout);
////    fun("BMuRmwMy6eE", _ZTVSt13basic_ostreamIwSt11char_traitsIwEE);
////    fun("CS91br93fag", __ashldi3);
////    fun("3-zCDXatSU4", __isfinitel);
////    fun("UbnVmck+o10", mbsinit);
////    fun("KGKBeVcqJjc", scalbn);
////    fun("ykNF6P3ZsdA", _CStrftime);
////    fun("+8jItptyeQQ", _ZTVSi);
////    fun("GfxAp9Xyiqs", __isnan);
////    fun("rkWOabkkpVo", _ZTIDs);
////    fun("5afBJmEfUQI", _ZNSt10moneypunctIcLb0EEC1EPKcm);
////    fun("-cgB1bQG6jo", _ZNSt12placeholders2_6E);
////    fun("RlewTNtWEcg", _WStodx);
////    fun("i4J5FvRPG-w", _ZSt10_GetloctxtIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEiRT0_S5_mPKT_);
////    fun("1NNR9occNS4", _Ux86_64_get_accessors);
////    fun("Lf6h5krl2fA", _ZNSt8numpunctIcED0Ev);
////    fun("m-Bu5Tzr82A", _LSin);
////    fun("FAreWopdBvE", _LXp_invx);
////    fun("Zn44KZgJtWY", _ZSt14_Debug_messagePKcS0_j);
////    fun("fn1i72X18Gs", _ZNSt11logic_errorD0Ev);
////    fun("xzYpD5d24aA",
////        _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE14do_get_weekdayES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("d3dMyWORw8A", wcstol);
////    fun("WoCt9o2SYHw", _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED1Ev);
////    fun("Lt4407UMs2o", _ZNSt7collateIcED0Ev);
////    fun("YjbpxXpi6Zk", atanh);
////    fun("2fxdcyt5tGs",
////        _ZNKSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERSs);
////    fun("dREVnZkAKRE", _Foprep);
////    fun("E1iwBYkG3CM", __fflush);
////    fun("0JlZYApT0UM", _ZNSt14_Num_ldbl_base15tinyness_beforeE);
////    fun("IJmeA5ayVJA", _ZNSbIwSt11char_traitsIwESaIwEE6assignEPKwm);
////    fun("nc6OsiDx630", _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE10date_orderEv);
////    fun("KheIhkaSrlA", _ZNKSt7codecvtIwc9_MbstatetE16do_always_noconvEv);
////    fun("HpAlvhNKb2Y", _ZNKSt22_System_error_category7messageEi);
////    fun("CSEjkTYt5dw", _ZTVN10__cxxabiv120__function_type_infoE);
////    fun("DV2AdZFFEh8", _Cnd_register_at_thread_exit);
////    fun("XxsPrrWJ52I", _ZNKSt8messagesIcE3getEiiiRKSs);
////    fun("SHlt7EhOtqA", fgetpos);
////    fun("NIfFNcyeCTo", feraiseexcept);
////    fun("CT4aR0tBgkQ", log10l);
////    fun("tZj4yquwuhI", _ZNKSt7_MpunctIcE16do_decimal_pointEv);
////    fun("emnRy3TNxFk", _ZTSPKf);
////    fun("CosTELN5ETk", getwc);
////    fun("s1b2SRBzSAY", _ZTSPv);
////    fun("pBxafllkvt0", __cxa_bad_cast);
////    fun("6A+1YZ79qFk", iswcntrl);
////    fun("XgmUR6WSeXg", _ZNSt11range_errorD2Ev);
////    fun("QZP6I9ZZxpE", clock);
////    fun("-m7FIvSBbMM", __kernel_cos);
////    fun("wCavZQ+m1PA", posix_spawnattr_setsigmask);
////    fun("banNSumaAZ0",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERy);
////    fun("2Aw366Jn07s", _LAtan);
////    fun("8F1ctQaP0uk", roundl);
////    fun("lOVQnEJEzvY", _Tss_get);
////    fun("suaBxzlL0p0", _ZNSt9_Num_base5trapsE);
////    fun("wiR+rIcbnlc", _ZSt4_Fpz);
////    fun("7pNKcscKrf8", _Stoll);
////    fun("9S960jA8tB0", _ZNSt10moneypunctIcLb1EEC1EPKcm);
////    fun("n7AepwR0s34", mktime);
////    fun("RCWKbkEaDAU", _ZNSt9_Num_base14is_specializedE);
////    fun("6WwFtNvnDag", __fixunsdfsi);
////    fun("Vj+KUu5khTE", _ZNSt12placeholders2_7E);
////    fun("zSehLdHI0FA", _ZNKSt8messagesIwE7do_openERKSsRKSt6locale);
////    fun("JrUnjJ-PCTg", _ZTIv);
////    fun("jfqTdKTGbBI", _ZTIPKw);
////    fun("2Olt9gqOauQ", _ZNKSt8numpunctIwE13thousands_sepEv);
////    fun("YhaOeniKcoA", _Fetch_xor_8);
////    fun("OnHQSrOHTks", _PJP_C_Copyright);
////    fun("Rw4J-22tu1U", __signbit);
////    fun("6iDi6e2e4x8", _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE7_GetcatEPPKNSt6locale5facetEPKS5_);
////    fun("H2KGT3vA7yQ", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecx);
////    fun("MnhsfZgm-GE", _ULx86_64_get_proc_info);
////    fun("Bm3k7JQMN5w", sigblock);
////    fun("YglrcQaNfds", _ZTIN10__cxxabiv119__pointer_type_infoE);
////    fun("KdAc8glk2+8", _Tls_setup__Wctype);
////    fun("ZD+Dp+-LsGg", sys_nsig);
////    fun("QSfaClY45dM", _Xbig);
////    fun("O+Bv-zodKLw", __muldf3);
////    fun("Ye20uNnlglA", abs);
////    fun("6vYXzFD-mrk", _ZNKSt7collateIcE4hashEPKcS2_);
////    fun("OI989Lb3WK0", _ZTSSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("pc4-Lqosxgk", _ZTSPx);
////    fun("-JjkEief9No", __atomic_store_2);
////    fun("QlcJbyd6jxM", _Stodx);
////    fun("pGxNI4JKfmY", _ZNSs6assignERKSsmm);
////    fun("mCESRUqZ+mw", __fixunssfti);
////    fun("hsn2TaF3poY", __atomic_compare_exchange_n);
////    fun("m4S+lkRvTVY", __popcountdi2);
////    fun("CRJcH8CnPSI", unsetenv);
////    fun("JyAoulEqA1c", _ZNSt12bad_weak_ptrD0Ev);
////    fun("6gAhNHCNHxY", _ZTVSt7_MpunctIcE);
////    fun("X2wfcFYusTk", _ZNSt11regex_errorD2Ev);
////    fun("ATYj6zra5W0", _ZNKSt5ctypeIwE10do_tolowerEw);
////    fun("Y6Sl4Xw7gfA", __cxa_get_exception_ptr);
////    fun("7tACjdACOBM", _ZNSt14_Num_ldbl_base5radixE);
////    fun("55nMATnNlS4", _Ux86_64_getcontext);
////    fun("B9rn6eKNPJg", _ZTSSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("jZmLD-ASDto", _ZNKSt19istreambuf_iteratorIwSt11char_traitsIwEE5equalERKS2_);
////    fun("8R00hgzXQDY", _ZNSt16nested_exceptionD2Ev);
////    fun("IIUY-5hk-4k", initstate);
////    fun("Q6oqEnefZQ8", __atomic_load_n);
////    fun("zwcNQT0Avck", _ZNKSt5ctypeIcE8do_widenEc);
////    fun("g5cG7QtKLTA", _Tss_delete);
////    fun("aQjlTguvFMw", _ZNSt14numeric_limitsIiE8digits10E);
////    fun("73GV+sRHbeY",
////        _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE11do_get_timeES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("g8phA3duRm8", _ZTSPi);
////    fun("o-kMHRBvkbQ", lgamma);
////    fun("Nt6eyVKm+Z4", _ZNKSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewPK2tmcc);
////    fun("VrXGNMTgNdE", _ZNSt13basic_filebufIwSt11char_traitsIwEE5uflowEv);
////    fun("HmgKoOWpUc8", sceLibcOnce);
////    fun("Nr+GiZ0tGAk", _ZTSSt8ios_base);
////    fun("So6gSmJMYDs", _ZNSt6locale7_LocimpD2Ev);
////    fun("-m2YPwVCwJQ",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERl);
////    fun("Q17eavfOw2Y", _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE2idE);
////    fun("lbLEAN+Y9iI", _ZTISt20bad_array_new_length);
////    fun("rrgxakQtvc0", isgraph);
////    fun("zv1EMhI7R1c", _ZNKSt8numpunctIcE16do_thousands_sepEv);
////    fun("iO5AOflrTaA", _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC1ERKSt8_Locinfom);
////    fun("4ITASgL50uc", lroundl);
////    fun("ki5SQPsB4m4", _ZNSt10moneypunctIwLb0EED1Ev);
////    fun("4QwxZ3U0OK0", _Do_call);
////    fun("xsRN6gUx-DE", _ZNSt13_Regex_traitsIwE6_NamesE);
////    fun("wT+HL7oqjYc", _ZNSt10moneypunctIcLb1EED1Ev);
////    fun("t8sFCgJAClE", errx);
////    fun("ywv2X-q-9is", _ZNSt15underflow_errorD1Ev);
////    fun("PtsB1Q9wsFA", setlocale);
////    fun("Xb9FhMysEHo", _ZNSt9_Num_base5radixE);
////    fun("ZWy6IcBqs1c", _LSinh);
////    fun("F+cp2B3cWNU", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecd);
////    fun("B6JXVOMDdlw", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE5_FputES3_RSt8ios_basewPKcmmmm);
////    fun("bBGvmspg3Xs", _ZNSt10moneypunctIwLb0EEC2Em);
////    fun("AEPvEZkaLsU", iswpunct);
////    fun("M+F+0jd4+Y0", _ZNSt14_Num_ldbl_base11round_styleE);
////    fun("MpxhMh8QFro", fwrite);
////    fun("ShanbBWU3AA", _Thrd_start_with_name_attr);
////    fun("Fk7-KFKZi-8", acosh);
////    fun("pE4Ot3CffW0", _Mtxlock);
////    fun("J8JRHcUKWP4", __modti3);
////    fun("QUeUgxy7PTA", _ZNSt20_Future_error_objectIiE14_Future_objectE);
////    fun("dDIjj8NBxNA", _ZTISt12codecvt_base);
////    fun("Zs4p6RemDxM", fdim);
////    fun("aFMVMBzO5jk", _ZTSf);
////    fun("Sc0lXhQG5Ko", _ZNKSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewPK2tmPKwSB_);
////    fun("vX+NfOHOI5w", _ZN6Dinkum7threads10lock_errorD1Ev);
////    fun("gklG+J87Pq4", y1f);
////    fun("mmq9OwwYx74", _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED0Ev);
////    fun("BD-xV2fLe2M", gamma);
////    fun("Jp6dZm7545A", _Skip);
////    fun("041c37QfoUc", _ZNSt15basic_streambufIwSt11char_traitsIwEE6xsputnEPKwi);
////    fun("VUzjXknPPBs", mbstowcs);
////    fun("3op2--wf660", _ZTSPKv);
////    fun("my9ujasm6-0",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERPv);
////    fun("lTTrDj5OIwQ", _ZNSoD0Ev);
////    fun("Hg1im-rUeHc", _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC1ERKSt8_Locinfom);
////    fun("ikjnoeemspQ", _ZNSs6assignEmc);
////    fun("aHDdLa7jA1U", _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED0Ev);
////    fun("Re5FGTBVpq8", __libunwind_Unwind_SetGR);
////    fun("fWuQSVGOivA", _ZNSt10moneypunctIwLb1EED0Ev);
////    fun("A1R5T0xOyn8", fmodl);
////    fun("m7qAgircaZY", _ZNSt7_MpunctIcE5_InitERKSt8_Locinfob);
////    fun("5DzttCF356U", _ZNSt9_Num_base9is_signedE);
////    fun("ELSr5qm4K1M", __umodti3);
////    fun("YfJUGNPkbK4", vprintf_s);
////    fun("0Ys3rv0tw7Y", _ZTSSt13basic_istreamIwSt11char_traitsIwEE);
////    fun("lnOqjnXNTwQ", _ZNSt14numeric_limitsIbE8digits10E);
////    fun("CpWcnrEZbLA", _Stof);
////    fun("idsapmYZ49w", _ZTSSt10ctype_base);
////    fun("9daYeu+0Y-A", __divdi3);
////    fun("gqKfOiJaCOo", expm1);
////    fun("uMMG8cuJNr8", _ZNSt15_Num_float_base17has_signaling_NaNE);
////    fun("xmJYAfy6m1w", _ULx86_64_init_remote);
////    fun("bWHwovVFfqc", _ZN10__cxxabiv120__si_class_type_infoD2Ev);
////    fun("5bQqdR3hTZw", _ZNSt10moneypunctIwLb0EEC2EPKcm);
////    fun("BcbHFSrcg3Y", _ZNSt6locale7_Locimp9_MakewlocERKSt8_LocinfoiPS0_PKS_);
////    fun("o1B4dkvesMc", _FXp_mulx);
////    fun("hAJZ7-FBpEQ", fesetround);
////    fun("kW5K7R4rXy8", _ZNSt14numeric_limitsIsE8digits10E);
////    fun("2PXZUKjolAA", _ZTIPKf);
////    fun("ljFisaQPwYQ", wcstoimax);
////    fun("mZW-My-zemM",
////        _ZZNKSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8_GetmfldERS3_S5_bRSt8ios_basePcE4_Src);
////    fun("xsXQD5ybREw", _ZNSt22_Future_error_categoryD1Ev);
////    fun("IntAnFb+tw0",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERf);
////    fun("rrqNi95bhMs",
////        _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8get_timeES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("Zbaaq-d70ms", _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED1Ev);
////    fun("8PJ9qmklj2c", _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED2Ev);
////    fun("7TW4UgJjwJ8", _ZTIDi);
////    fun("IjlonFkCFDs", __addvti3);
////    fun("OWO5cpNw3NA",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERb);
////    fun("ALEXgLx9fqU",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERl);
////    fun("tLB5+4TEOK0", putc);
////    fun("7tIwDZyyKHo", _ZNKSt7codecvtIcc9_MbstatetE16do_always_noconvEv);
////    fun("WSaroeRDE5Q", _LXbig);
////    fun("VrWUXy1gqn8", _ZSt10_Rng_abortPKc);
////    fun("e5jQyuEE+9U", _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE7_GetcatEPPKNSt6locale5facetEPKS5_);
////    fun("YUKO57czb+0", _CTinfo);
////    fun("QdE7Arjzxos", jn);
////    fun("J5QA0ZeLmhs", _ZNSt15_Num_float_base8is_exactE);
////    fun("uODuM76vS4U", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecm);
////    fun("bY9Y0J3GGbA", _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC1Em);
////    fun("vxgqrJxDPHo", sinl);
////    fun("RKvygdQzGaY", _ZTIPKh);
////    fun("1EyHxzcz6AM", _FXp_addh);
////    fun("S7kkgAPGxLQ", _ZNSt15_Num_float_base9is_signedE);
////    fun("RIa6GnWp+iU", strerror);
////    fun("c+4r-T-tEIc", nearbyintf);
////    fun("Zofiv1PMmR4", __negti2);
////    fun("xzZiQgReRGE", labs);
////    fun("odyn6PGg5LY", _ZNSt14numeric_limitsIdE14min_exponent10E);
////    fun("pS-t9AJblSM", _ZNKSt9bad_alloc8_DoraiseEv);
////    fun("YsZuwZrJZlU", _ZN10__cxxabiv123__fundamental_type_infoD0Ev);
////    fun("Z6CCOW8TZVo", _WGetfld);
////    fun("oAidKrxuUv0", _ZTVSt12domain_error);
////    fun("HD9vSXqj6zI", _FXp_subx);
////    fun("tnlAgPCKyTk", __atomic_compare_exchange_8);
////    fun("rNYLEsL7M0k", _ZNSt12future_errorD0Ev);
////    fun("rLiFc4+HyHw", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basece);
////    fun("qEob3o53s2M", _ZNSt8numpunctIcED1Ev);
////    fun("Fr7j8dMsy4s", _ZNKSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewPK2tmcc);
////    fun("5OpjqFs8yv8", drem);
////    fun("DNbsDRZ-ntI", _ZSt25_Rethrow_future_exceptionSt13exception_ptr);
////    fun("-VVn74ZyhEs", difftime);
////    fun("gjLRZgfb3i0", _ZNSt4_PadD2Ev);
////    fun("oths6jEyBqo", getopt_long_only);
////    fun("01FSgNK1wwA", _ZTIPf);
////    fun("b54DvYZEHj4", __ffsti2);
////    fun("kTlQY47fo88", _ZNKSt13bad_exception8_DoraiseEv);
////    fun("UzmR8lOfyqU", _ZNKSt7_MpunctIwE13do_neg_formatEv);
////    fun("gZvNGjQsmf8", _ZTISt5ctypeIcE);
////    fun("uKwYpqeUkzo", _Unwind_GetBSP);
////    fun("wUCRGap1j0U",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERe);
////    fun("leSFwTZZuE4", _ZNKSt8numpunctIwE13decimal_pointEv);
////    fun("NVCBWomXpcw", _ZTIPKDh);
////    fun("pP76ElRLm78", _ZNSt12placeholders3_17E);
////    fun("4ZnE1sEyX3g", _ZGVNSt8messagesIcE2idE);
////    fun("Gvp-ypl9t5E", _ZTVSt13bad_exception);
////    fun("G7yOZJObV+4", qsort_s);
////    fun("zXCi78bYrEI", _Log);
////    fun("klWxQ2nKAHY", _ZNKSt8numpunctIcE13decimal_pointEv);
////    fun("qUxH+Damft4", _ZTIw);
////    fun("yDnwZsMnX0Q", __srget);
////    fun("Cv+zC4EjGMA", _ZNSt5ctypeIcE2idE);
////    fun("7ZmeGHyM6ew", _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED2Ev);
////    fun("6NCOqr3cD74", _init_tls);
////    fun("sS5fF+fht2c", _ZNSt10moneypunctIcLb0EED0Ev);
////    fun("z8GPiQwaAEY", _malloc_init);
////    fun("F+AmVDFUyqM",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERx);
////    fun("H0pVDvSuAVQ", sce_getchar_unlocked);
////    fun("tFgzEdfmEjI", __mulvsi3);
////    fun("Pq4PkG0x1fk",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERm);
////    fun("iXVrhA51z0M", _ZNKSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_bRSt8ios_basece);
////    fun("gI9un1H-fZk", _Fetch_or_seq_cst_4);
////    fun("7EirbE7st4E", _ZTVN10__cxxabiv117__pbase_type_infoE);
////    fun("wTjDJ6In3Cg", lcong48);
////    fun("7glioH0t9HM", _ZNKSt5ctypeIcE10do_toupperEPcPKc);
////    fun("8UeuxGKjQr8", _ZNSt7_MpunctIwED0Ev);
////    fun("zWSNYg14Uag", _ZNSt7_MpunctIcEC2Emb);
////    fun("N2VV+vnEYlw", _ZTIN10__cxxabiv120__si_class_type_infoE);
////    fun("1QJWxoB6pCo", fputwc);
////    fun("zqJhBxAKfsc", __stdoutp);
////    fun("rvNWRuHY6AQ", _Loctab);
////    fun("dnaeGXbjP6E", exp2);
////    fun("v95AEAzqm+0", sce_putchar_unlocked);
////    fun("T7uyNqP7vQA", tan);
////    fun("Fncgcl1tnXg", erand48);
////    fun("nWfZplDjbxQ", _ZNKSt22_Future_error_category7messageEi);
////    fun("BEFy1ZFv8Fw", copysign);
////    fun("QxqK-IdpumU", _Getpmbstate);
////    fun("DSnq6xesUo8", _ZGVNSt20_Future_error_objectIiE14_Future_objectE);
////    fun("-LFO7jhD5CE", ungetc);
////    fun("OviE3yVSuTU", _FEps);
////    fun("Cv-8x++GS9A", _Lock_spin_lock);
////    fun("G4XM-SS1wxE", _ZTVN10__cxxabiv123__fundamental_type_infoE);
////    fun("bEk+WGOU90I", _Setgloballocale);
////    fun("AqpZU2Njrmk", remquof);
////    fun("eMnBe5mZFLw", _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE7_GetcatEPPKNSt6locale5facetEPKS5_);
////    fun("N1VqUWz2OEI",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERj);
////    fun("sDOFamOKWBI", _Atomic_compare_exchange_weak_2);
////    fun("MFe91s8apQk", log1pf);
////    fun("utLW7uXm3Ss", feenableexcept);
////    fun("47RvLSo2HN8", _ZNSt8bad_castD1Ev);
////    fun("q05IXuNA2NE", _ZTv0_n24_NSt13basic_istreamIwSt11char_traitsIwEED1Ev);
////    fun("UbuTnKIXyCk", _ZNSt10moneypunctIcLb0EE4intlE);
////    fun("G1kDk+5L6dU", _Call_onceEx);
////    fun("z0OhwgG3Bik", __unordsf2);
////    fun("olsoiZsezkk", _ZNSt15basic_streambufIwSt11char_traitsIwEE9showmanycEv);
////    fun("LYo3GhIlB38", sceLibcMspaceCalloc);
////    fun("nqpARwWZmjI", _ZTIPs);
////    fun("n8onIBR4Qdk", clock_1700);
////    fun("3+VwUA8-QPI", _ZNKSt8numpunctIcE9falsenameEv);
////    fun("QhORYaNkS+U", _Atomic_is_lock_free_2);
////    fun("lA+PfiZ-S5A", _ZNSt8messagesIcE2idE);
////    fun("cmIyU0BNYeo", _Wcsxfrmx);
////    fun("OkYxbdkrv64", _ZTIPDs);
////    fun("n-3HFZvDwBw", _ZNKSt7collateIwE7compareEPKwS2_S2_S2_);
////    fun("DQbtGaBKlaw", strnlen_s);
////    fun("vZMcAfsA31I", setbuf);
////    fun("u14Y1HFh0uY", acoshl);
////    fun("5ZkEP3Rq7As", cbrt);
////    fun("5i8mTQeo9hs", __atomic_exchange);
////    fun("czcFqaa4PUU", __libunwind_Unwind_GetGR);
////    fun("rsS5cBMihAM", _ZNSt13basic_filebufIcSt11char_traitsIcEE7seekposESt4fposI9_MbstatetENSt5_IosbIiE9_OpenmodeE);
////    fun("5DFeXjP+Plg", _ZNSt10moneypunctIcLb0EEC2Em);
////    fun("h-a7+0UuK7Q", _ZN10__cxxabiv121__vmi_class_type_infoD1Ev);
////    fun("aW9KhGC4cOo", putwchar);
////    fun("LG6O0oW9bQU", posix_spawn_file_actions_adddup2);
////    fun("KwJ5V3D0v3A",
////        _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE11get_weekdayES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("EOLQfNZ9HpE", c16rtomb);
////    fun("tIhsqj0qsFE", free);
////    fun("YeBP0Rja7vc", _ZTSSt12future_error);
////    fun("zCnSJWp-Qj8", optind);
////    fun("hbiV9vHqTgo", __lesf2);
////    fun("uXj-oWD2334", _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC1Em);
////    fun("Ou7GV51-ng4",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERx);
////    fun("QTgRx1NTp6o", _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED1Ev);
////    fun("+my9jdHCMIQ", atol);
////    fun("zBHGQsN5Yfw", _ZNSt9_Num_base14min_exponent10E);
////    fun("ej+44l1PjjY", _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC1ERKSt8_Locinfom);
////    fun("6bRANWNYID0", llrintl);
////    fun("AeZTCCm1Qqc", fegetround);
////    fun("jJtoPFrxG-I", _ZTSPc);
////    fun("zzubCm+nDzc",
////        _ZNKSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERe);
////    fun("Q0VsWTapQ4M", _ZNKSt22_Future_error_category4nameEv);
////    fun("Ml1z3dYEVPM", _ZTIPKa);
////    fun("QWCTbYI14dA", _FInf);
////    fun("WIg11rA+MRY", drand48);
////    fun("weDug8QD-lE", atanf);
////    fun("hDuyUWUBrDU", _WFwprep);
////    fun("Ezzq78ZgHPs", wcschr);
////    fun("DYamMikEv2M", _Wctrans);
////    fun("-QgqOT5u2Vk", _Assert);
////    fun("Ujf3KzMvRmI", memalign);
////    fun("Wn6I3wVATUE", daemon);
////    fun("xZqiZvmcp9k", _ZNSt4_Pad7_LaunchEPP7pthread);
////    fun("upwSZWmYwqE", _ZN10__cxxabiv116__enum_type_infoD1Ev);
////    fun("LTov9gMEqCU", _ZNSt8ios_base7_AddstdEPS_);
////    fun("nvSsAW7tcX8", _ZNKSt8numpunctIwE11do_truenameEv);
////    fun("QO6Q+6WPgy0", _ZNSt14numeric_limitsIsE6digitsE);
////    fun("zG0BNJOZdm4", optarg);
////    fun("ihfGsjLjmOM", _Thrd_start_with_name);
////    fun("SkRR6WxCTcE", _Thrd_create);
////    fun("aNfpdhcsMWI", _ZNSt10moneypunctIcLb0EEC2EPKcm);
////    fun("5sfbtNAdt-M", _ZNKSt12future_error4whatEv);
////    fun("4EkPKYzOjPc", _ZNSt8ios_base6_IndexE);
////    fun("31g+YJf1fHk", __fixxfdi);
////    fun("K+w0ofCSsAY", _ZTSN10__cxxabiv129__pointer_to_member_type_infoE);
////    fun("wEImmi0YYQM", __atomic_fetch_add_2);
////    fun("6azovDgjxt0", _ZTSPKj);
////    fun("dCzeFfg9WWI", _ZTVSt9exception);
////    fun("KBBVmt8Td7c", _ZTIy);
////    fun("Av3zjWi64Kw", strftime);
////    fun("Vla-Z+eXlxo", sceLibcMspaceFree);
////    fun("DDxNvs1a9jM", __mulosi4);
////    fun("AwdlDnuQ6c0", _ZNSt14numeric_limitsIdE12min_exponentE);
////    fun("HI4N2S6ZWpE", scalb);
////    fun("1kduKXMqx7k", feholdexcept);
////    fun("ERYMucecNws", _ZNSt14numeric_limitsIeE12min_exponentE);
////    fun("4CVc6G8JrvQ", _Atomic_flag_clear);
////    fun("bleKr8lOLr8", _ZNSt6locale7_LocimpD0Ev);
////    fun("6htjEH2Gi-w", _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC1Em);
////    fun("Re3Lpk8mwEo", _ZN10__cxxabiv129__pointer_to_member_type_infoD2Ev);
////    fun("59oywaaZbJk", _ZTVSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("JjPXy-HX5dY", vfscanf_s);
////    fun("LxcEU+ICu8U", feof);
////    fun("yc-vi84-2aE", _ZN6Dinkum7codecvt10_Cvt_checkEmm);
////    fun("3wcYIMz8LUo", nrand48);
////    fun("2eQpqTjJ5Y4", asinh);
////    fun("rpl4rhpUhfg", _Atomic_fetch_xor_2);
////    fun("xLZr4GJRMLo",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERe);
////    fun("eNW5jsFxS6k", _ZTSSt12out_of_range);
////    fun("R72NCZqMX58", _ZTSSt13basic_ostreamIwSt11char_traitsIwEE);
////    fun("cbyLM5qrvHs", __subvti3);
////    fun("0WY1SH7eoIs", _ZNSt6_Winit9_Init_cntE);
////    fun("L1SBTkC+Cvw", abort);
////    fun("nlxVZWbqzsU", _ZNSt14numeric_limitsIcE6digitsE);
////    fun("XO9ihAZCBcY", _ZTIa);
////    fun("QW-f9vYgI7g", __negvti2);
////    fun("SZrEVfvcHuA", _Atomic_compare_exchange_strong_8);
////    fun("0il9qdo6fhs", _ZNSt7_MpunctIcEC2EPKcmbb);
////    fun("Gav1Xt1Ce+c", _ZSt7_MP_MulPyyy);
////    fun("FHJlhz0wVts", _WStoflt);
////    fun("e5Hwcntvd8c", _ZNSt7codecvtIcc9_MbstatetEC2Em);
////    fun("pkYiKw09PRA", fseeko);
////    fun("KaZ3xf5c9Vc", _ZN10__cxxabiv129__pointer_to_member_type_infoD1Ev);
////    fun("k9kErpz2Sv8", _ZTSl);
////    fun("zugltxeIXM0", _ZSt15_sceLibcLocinfoPKc);
////    fun("v6rXYSx-WGA", _Strcollx);
////    fun("43u-nm1hQc8", _Ldbl);
////    fun("yYIymfQGl2E", _ZN10__cxxabiv121__vmi_class_type_infoD2Ev);
////    fun("yhxKO9LYc8A", sigvec);
////    fun("XwLA5cTHjt4", __gxx_personality_v0);
////    fun("3uW2ESAzsKo", _FXbig);
////    fun("-3ZujD7JX9c", _Atomic_is_lock_free_8);
////    fun("XJp2C-b0tRU", acoshf);
////    fun("NEemVJeMwd0", _ZNKSt8bad_cast8_DoraiseEv);
////    fun("Ou6QdDy1f7g", _Atomic_flag_test_and_set);
////    fun("CLT04GjI7UE", _ZNKSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8_GetmfldERS3_S5_bRSt8ios_basePc);
////    fun("jfq92K8E1Vc", _ZZNSt13basic_filebufIcSt11char_traitsIcEE5_InitEP7__sFILENS2_7_InitflEE7_Stinit);
////    fun("j7Jk3yd3yC8", vscanf);
////    fun("cWaCDW+Dc9U", _ZNKSt7collateIwE7do_hashEPKwS2_);
////    fun("0x4NT++LU9s", _ZTSSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("q9rMtHuXvZ8", _ZNSt17bad_function_callD0Ev);
////    fun("nVS8UHz1bx0", _WGetstr);
////    fun("HSkPyRyFFHQ", _ZTv0_n24_NSt13basic_ostreamIwSt11char_traitsIwEED1Ev);
////    fun("g-fUPD4HznU", _ZTIh);
////    fun("4hiQK82QuLc",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERx);
////    fun("kzfj-YSkW7w", _ZTIPKDs);
////    fun("9kSWQ8RGtVw", _Atomic_fetch_add_2);
////    fun("x8dc5Y8zFgc", cosl);
////    fun("mQCm5NmJORg", _ZTSPDs);
////    fun("bqcStLRGIXw", _Logpoly);
////    fun("mAwXCpkWaYc",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERd);
////    fun("xvVR0CBPFAM", _ZNKSt23_Generic_error_category4nameEv);
////    fun("uQFv8aNF8Jc", _ZNSt10moneypunctIcLb0EEC2ERKSt8_Locinfomb);
////    fun("cJvOg1KV8uc", sigsetmask);
////    fun("RePA3bDBJqo", erff);
////    fun("eF26YAKQWKA", _ZNSt10moneypunctIcLb0EE2idE);
////    fun("aLYyS4Kx6rQ", _malloc_prefork);
////    fun("jaLGUrwYX84", _ZGVNSt7collateIwE2idE);
////    fun("kU7PvJJKUng",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERt);
////    fun("VsJCpXqMPJU", _ZNKSt8numpunctIwE8groupingEv);
////    fun("1kZFcktOm+s", _ZTVSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("0gS2-9PEbBM", _Ux86_64_regname);
////    fun("n3y8Rn9hXJo", _ZNSt12length_errorD1Ev);
////    fun("UZJnC81pUCw", putwc);
////    fun("007PjrBCaUM", _ZTISt8messagesIwE);
////    fun("6WR6sFxcd40", _Atomic_store_2);
////    fun("y1dYQsc67ys", _ZNSt14overflow_errorD0Ev);
////    fun("xMksIr3nXug", __atomic_fetch_and_8);
////    fun("VpwymQiS4ck", _ZNSt15basic_streambufIcSt11char_traitsIcEE6xsgetnEPci);
////    fun("lku-VgKK0RE", __atomic_compare_exchange_4);
////    fun("IAEl1Ta7yVU", _ZNSt15basic_streambufIcSt11char_traitsIcEE9showmanycEv);
////    fun("dYJJbxnyb74", _Fgpos);
////    fun("PBbZjsL6nfc", _ZNSt4_Pad7_LaunchEPKcPP7pthread);
////    fun("eT2UsmTewbU", _ZSt11_Xbad_allocv);
////    fun("GSAXi4F1SlM", _ZNKSt7collateIcE7do_hashEPKcS2_);
////    fun("gDsvnPIkLIE", _ZNSt11regex_errorD1Ev);
////    fun("Nz7J62MvgQs", ungetwc);
////    fun("AnTtuRUE1cE", _Makestab);
////    fun("ddLNBT9ks2I", _ZTISt8numpunctIcE);
////    fun("G+P6ACwtwVk", __libunwind_Unwind_GetTextRelBase);
////    fun("TQPr-IeNIS0", _Quad);
////    fun("v1WebHtIa24", _ZTISt7codecvtIDsc9_MbstatetE);
////    fun("mgNGxmJltOY", _Closreg);
////    fun("20cUk0qX9zo", _malloc_init_lv2);
////    fun("YEDrb1pSx2Y", _ZNSt17bad_function_callD1Ev);
////    fun("dW3xsu3EgFI", sce_rand_r);
////    fun("tcVi5SivF7Q", sprintf);
////    fun("KOy7MeQ7OAU", __opendir2);
////    fun("J2xO4cttypo", _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC1Em);
////    fun("n2mWDsholo8", getwchar);
////    fun("V+c0E+Uqcww", _ZNKSt5ctypeIwE9do_narrowEwc);
////    fun("r-JSsJQFUsY",
////        _ZNKSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE8_PutmfldES3_bRSt8ios_basewbSbIwS2_SaIwEEw);
////    fun("ZaOkYNQyQ6g", _ZNSt14numeric_limitsIDsE9is_signedE);
////    fun("QL+3q43NfEA", tanl);
////    fun("Kcb+MNSzZcc", _ZNSt12out_of_rangeD1Ev);
////    fun("bRujIheWlB0", _ZSt14_Throw_C_errori);
////    fun("9tnIVFbvOrw", __floatundisf);
////    fun("cbvW20xPgyc", _ZTISt14error_category);
////    fun("frx6Ge5+Uco", _Atomic_copy);
////    fun("yYk819F9TyU", _ZSt8_XLgammad);
////    fun("7GgGIxmwA6I", _FXp_ldexpx);
////    fun("gCf7+aGEhnU", __clzdi2);
////    fun("14xKj+SV72o", _ZNKSt7codecvtIcc9_MbstatetE3outERS0_PKcS4_RS4_PcS6_RS6_);
////    fun("DCY9coLQcVI", _ZTISt12future_error);
////    fun("-g26XITGVgE", strerrorlen_s);
////    fun("7O-vjsHecbY", _LXp_ldexpx);
////    fun("OXmauLdQ8kY", atan);
////    fun("Eva2i4D5J6I", _ZSt14set_unexpectedPFvvE);
////    fun("DEQmHCl-EGU", __atomic_store_8);
////    fun("bzQExy189ZI", _init_env);
////    fun("KeOZ19X8-Ug", _Cosh);
////    fun("smeljzleGRQ", _ZTIc);
////    fun("W6SiVSiCDtI", sceLibcMspaceDestroy);
////    fun("J+hjiBreZr4", _ZTSSt10moneypunctIwLb1EE);
////    fun("MwiKdf6QFvI", __atomic_compare_exchange_2);
////    fun("u0yN6tzBors", _ZSt14_Raise_handler);
////    fun("j6qwOi2Nb7k", _ZTVSt16nested_exception);
////    fun("CZNm+oNmB-I", optreset);
////    fun("yaFXXViLWPw", opterr);
////    fun("StJaKYTRdUE", _ZNSt13basic_istreamIwSt11char_traitsIwEED0Ev);
////    fun("7P1Wm-5KgAY", _ZNSt12domain_errorD1Ev);
////    fun("wpuIiVoCWcM", _Cnd_unregister_at_thread_exit);
////    fun("JZ9gGlJ22hg", _WStofx);
////    fun("I3+xmBWGPGo", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecl);
////    fun("JWplGh2O0Rs", _ZNKSt8numpunctIcE8groupingEv);
////    fun("a7ToDPsIQrc", __inet_aton);
////    fun("JwPU+6+T20M", _ZNSt15_Num_float_base9is_iec559E);
////    fun("1ZMchlkvNNQ", _ZN10__cxxabiv117__array_type_infoD1Ev);
////    fun("kr5ph+wVAtU", _ZSt6ignore);
////    fun("lWGF8NHv880", _DefaultRuneLocale);
////    fun("jfr41GGp2jA", _ZNSt13basic_filebufIcSt11char_traitsIcEE5uflowEv);
////    fun("W6qgdoww-3k", _ZNSt14numeric_limitsIfE6digitsE);
////    fun("0uAUs3hYuG4", ctime);
////    fun("d-MOtyu8GAk", _ZNSt7collateIwEC1Em);
////    fun("G1zcPUEvY7U", _ZNKSt7codecvtIwc9_MbstatetE9do_lengthERS0_PKcS4_m);
////    fun("A+Y3xfrWLLo", _Fspos);
////    fun("vI85k3GQcz8", _ZSt15future_categoryv);
////    fun("+iy+BecyFVw", __atomic_load);
////    fun("oVkZ8W8-Q8A", strtok);
////    fun("urrv9v-Ge6g", _LErf_one);
////    fun("Jo9ON-AX9eU", _Fltrounds);
////    fun("htdTOnMxDbQ", sceLibcPafMspaceIsHeapEmpty);
////    fun("Do3zPpsXj1o", _seekdir);
////    fun("AHxyhN96dy4", ferror);
////    fun("tcN0ngcXegg", j0);
////    fun("9SSHrlIamto", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewx);
////    fun("tYBLm0BoQdQ", _ZNSt10moneypunctIcLb0EEC1Em);
////    fun("BdfPxmlM9bs", _ZTSSt7collateIwE);
////    fun("fSdpGoYfYs8", _ZNSt9_Num_base17has_signaling_NaNE);
////    fun("cIA-+pV07Us", __libunwind_Unwind_ForcedUnwind);
////    fun("-Zfr0ZQheg4", _Atomic_fetch_sub_1);
////    fun("FOt55ZNaVJk", _ZdaPvm);
////    fun("GslDM6l8E7U", _Unwind_DeleteException);
////    fun("zhAIFVIN1Ds", __multi3);
////    fun("niPixjs0l2w", _LLogpoly);
////    fun("z-L6coXk6yo", _ZGVNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE2idE);
////    fun("yK3VESxclT0", __libunwind_Unwind_RaiseException);
////    fun("aHUFijEWlxQ", _Unlock_spin_lock);
////    fun("ryUxD-60bKM", _ZnwmRKSt9nothrow_t);
////    fun("XhdnPX5bosc", _ZNSt10moneypunctIcLb1EE4intlE);
////    fun("krshE4JAE6M", _ZN10__cxxabiv117__pbase_type_infoD2Ev);
////    fun("u5a7Ofymqlg", fesettrapenable);
////    fun("4fPIrH+z+E4", _ZNKSt7codecvtIwc9_MbstatetE11do_encodingEv);
////    fun("VpwhOe58wsM", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewl);
////    fun("Zqc++JB04Qs", _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED2Ev);
////    fun("4-Fllbzfh2k",
////        _ZZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8_GetffldEPcRS3_S6_RSt8ios_basePiE4_Src);
////    fun("wggIIjWSt-E", _ZNSt7collateIwEC1ERKSt8_Locinfom);
////    fun("s+MeMHbB1Ro", _Scanf);
////    fun("odPHnVL-rFg", _FTgamma);
////    fun("BNZq-mRvDS8",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERPv);
////    fun("XOEdRCackI4", _ZNSt12placeholders3_16E);
////    fun("Azw9C8cy7FY", _ZTVSt14overflow_error);
////    fun("fXUuZEw7C24", _ZNKSt8numpunctIcE8truenameEv);
////    fun("50aDlGVFt5I", _ZTIPKDi);
////    fun("IfWUkB7Snkc", _ZTSSt15basic_streambufIcSt11char_traitsIcEE);
////    fun("hew0fReI2H0", mblen);
////    fun("eLdDw6l0-bU", snprintf);
////    fun("TbuLWpWuJmc", _Atomic_exchange_2);
////    fun("OzMC6yz6Row", _ZTSm);
////    fun("QHSbn+zesvk", _ULx86_64_dwarf_search_unwind_table);
////    fun("xFva4yxsVW8", _ZNSt8numpunctIcED2Ev);
////    fun("gobJundphD0", _LRteps);
////    fun("chzmnjxxVtk", _Fac_tidy);
////    fun("ptL8XWgpGS4", __clzsi2);
////    fun("ElKI+ReiehU", _ZNSt8numpunctIwEC2Em);
////    fun("qPe7-h5Jnuc", asctime_s);
////    fun("e3shgCIZxRc", _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC2ERKSt8_Locinfom);
////    fun("mhoxSElvH0E", _ZNSt10moneypunctIwLb0EE2idE);
////    fun("myQDQapYJdw", fegetexceptflag);
////    fun("ArZF2KISb5k", _LDunscale);
////    fun("QCqMm6zIRvI", _ZNK10__cxxabiv117__class_type_info11can_cast_toEPKS0_);
////    fun("F2uQDOc7fMo", _ZNSt14numeric_limitsIyE9is_signedE);
////    fun("2yOarodWACE", _ZNSt6_WinitD2Ev);
////    fun("4r--aFJyPpI", _ZTSPw);
////    fun("PDX01ziUz+4", _ZSt13_Syserror_mapi);
////    fun("B2ZbqV9geCM", tgammaf);
////    fun("3Z8jD-HTKr8", _LPow);
////    fun("gfP0im5Z3g0", ldiv);
////    fun("5jX3QET-Jhw", _ZTSSt9time_base);
////    fun("ShlQcYrzRF8", _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE10date_orderEv);
////    fun("1t1-JoZ0sZQ", sinhf);
////    fun("mOnfZ5aNDQE", _Stollx);
////    fun("ZRRBeuLmHjc", _ZNKSbIwSt11char_traitsIwESaIwEE5_XlenEv);
////    fun("cJLTwtKGXJk", nearbyint);
////    fun("x8LHSvl5N6I", _ZTISt11logic_error);
////    fun("awr1A2VAVZQ", _ZSt5wclog);
////    fun("H0a2QXvgHOk", _ZNSt10moneypunctIwLb1EEC2ERKSt8_Locinfomb);
////    fun("r1W-NtOi6E8", _ZNKSt5ctypeIwE10do_toupperEPwPKw);
////    fun("9aODPZAKOmA", err_set_exit);
////    fun("8Wc+t3BCF-k", _ZTINSt6locale7_LocimpE);
////    fun("ItC2hTrYvHw", verrx);
////    fun("ZkP0sDpHLLg", _ZGVNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE2idE);
////    fun("D8JBAR3RiZQ", wscanf);
////    fun("w-BvXF4O6xo", llround);
////    fun("a6yvHMSqsV0", _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED1Ev);
////    fun("KQTHP-ij0yo", _ZNSt14numeric_limitsIaE6digitsE);
////    fun("Awj5m1LfcXQ", _ZGVNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE2idE);
////    fun("xROUuk7ItMM", _Fetch_and_seq_cst_2);
////    fun("bNFLV9DJxdc", _Atomic_compare_exchange_weak_8);
////    fun("KqYTqtSfGos", islower);
////    fun("j+XjoRSIvwU", fdopendir);
////    fun("NQ81EZ7CL6w", _ZNKSt7codecvtIDic9_MbstatetE16do_always_noconvEv);
////    fun("H0NwmJX8SOA", __libunwind_Unwind_DeleteException);
////    fun("6S8jzWWGcWo", _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED2Ev);
////    fun("wRyKNdtYYEY", _ZTSSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("--fMWwCvo+c", _ZTSSt8bad_cast);
////    fun("fiiNDnNBKVY", localtime_s);
////    fun("0Pd-K5jGcgM", _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC2ERKSt8_Locinfom);
////    fun("j4ViWNHEgww", strlen);
////    fun("77uWF3Z2q90", _Restore_state);
////    fun("XAqAE803zMg", _Atomic_load_1);
////    fun("8tL6yJaX1Ro", _ZNSt8ios_base5_SyncE);
////    fun("VxObo0uiafo", _ZTSSt10money_base);
////    fun("wpNJwmDDtxw", _Unwind_RaiseException);
////    fun("9xUnIQ53Ao4", __paritysi2);
////    fun("F7CUCpiasec", _ZNSt10moneypunctIcLb1EED2Ev);
////    fun("sV6ry-Fd-TM", __atomic_store);
////    fun("SPlcBQ4pIZ0", _ZNSt14numeric_limitsIfE12min_exponentE);
////    fun("RvsFE8j3C38", y0);
////    fun("DwcWtj3tSPA", putw);
////    fun("rdht7pwpNfM", __floatsisf);
////    fun("uMeLdbwheag", fmal);
////    fun("X7A21ChFXPQ", __floatsidf);
////    fun("7TIcrP513IM", _Tls_setup__Mbcurmax);
////    fun("LV2FB+f1MJE", _ZNSt14_Num_ldbl_base9is_signedE);
////    fun("+hjXHfvy1Mg",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERd);
////    fun("4f+Q5Ka3Ex0", __negsf2);
////    fun("ARZszYKvDWg", _ZNKSt7_MpunctIcE16do_positive_signEv);
////    fun("2BmJdX269kI", _ZNKSt8numpunctIwE11do_groupingEv);
////    fun("mXlxhmLNMPg", strtol);
////    fun("2mb8FYgER+E",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERf);
////    fun("++ge3jYlHW8", _ZNKSt11logic_error4whatEv);
////    fun("b8ySy0pHgSQ", _ZNSt12placeholders3_10E);
////    fun("YLEc5sPn1Rg", _Recip);
////    fun("39HHkIWrWNo", vswscanf_s);
////    fun("4llda2Y+Q+4", _ZNSt14numeric_limitsIlE9is_signedE);
////    fun("0uuqgRz9qfo", _Cnd_signal);
////    fun("Xr-8XNv8wr4", _ZNK10__cxxabiv121__vmi_class_type_info11can_cast_toEPKNS_17__class_type_infoE);
////    fun("xeYO4u7uyJ0", fopen);
////    fun("+wj27DzRPpo", __umoddi3);
////    fun("V23qt24VPVs", _ZSt17iostream_categoryv);
////    fun("W8tdMlttt3o", _Rteps);
////    fun("0mi6NtGz04Y", _ZNSt14numeric_limitsIbE9is_signedE);
////    fun("7tTpzMt-PzY", _ZTIN10__cxxabiv129__pointer_to_member_type_infoE);
////    fun("XDm4jTtoEbo", _ZNSt8numpunctIwED2Ev);
////    fun("+TtUFzALoDc", _ZTISt7codecvtIDic9_MbstatetE);
////    fun("dep6W2Ix35s", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecy);
////    fun("QZb07KKwTU0", _ZNSt8ios_base4Init9_Init_cntE);
////    fun("W5VYncHdreo", _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8_GetffldEPcRS3_S6_RSt8ios_basePi);
////    fun("1eVdDzPtzD4", _ZNSt8numpunctIcEC2Em);
////    fun("6gc1Q7uu244", __mulvti3);
////    fun("dSifrMdPVQ4", _ZTSPKa);
////    fun("ptwhA0BQVeE", _ZNSt6locale6globalERKS_);
////    fun("wYWYC8xNFOI", _ZTSSt9basic_iosIwSt11char_traitsIwEE);
////    fun("5tM252Rs2fc", sce_getc_unlocked);
////    fun("f06wGEmo5Pk", _ZNSt14_Num_ldbl_base12has_infinityE);
////    fun("dhK16CKwhQg", __isfinite);
////    fun("1KngsM7trps", _ZNSt15_Num_float_base5radixE);
////    fun("Ok8KPy3nFls", iswlower);
////    fun("+SeQg8c1WC0", _Flt);
////    fun("n+aUKkC-3sI", _ZTVSt12out_of_range);
////    fun("-UKRka-33sM", _ZNSt20bad_array_new_lengthD0Ev);
////    fun("XOgjMgZ3fjo", _ZNSt8numpunctIwEC1Em);
////    fun("UgZ7Rhk60cQ", imaxabs);
////    fun("3pF7bUSIH8o", isnanf);
////    fun("f+1EaDVL5C4", _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC2Em);
////    fun("MZO7FXyAPU8", remove);
////    fun("YrDCRdbOfOQ", _ZN9pathscale29set_use_thread_local_handlersEb);
////    fun("GE4I2XAd4G4", __sync_fetch_and_or_16);
////    fun("fsF-tGtGsD4", _ZNKSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_bRSt8ios_basewe);
////    fun("c41UEHVtiEA", _Stod);
////    fun("9iXtwvGVFRI", _ZNSt8numpunctIcE2idE);
////    fun("FX+eS2YsEtY", _ZSt10_GetloctxtIcSt19istreambuf_iteratorIwSt11char_traitsIwEEEiRT0_S5_mPKT_);
////    fun("+CnX+ZDO8qg", _ZNSt12placeholders2_1E);
////    fun("UuVHsmfVOHU", _ZTVSt23_Generic_error_category);
////    fun("h8nbSvw0s+M", __fixunsdfdi);
////    fun("8xXiEPby8h8", _Getptimes);
////    fun("nxdioQgDF2E", _ZNSt15_Num_float_base13has_quiet_NaNE);
////    fun("jyXTVnmlJD4", _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED0Ev);
////    fun("p6LrHjIQMdk", _ZNSt8_LocinfoD1Ev);
////    fun("F78ECICRxho", __ledf2);
////    fun("alNWe8glQH4", _LDint);
////    fun("efhK-YSUYYQ", localtime);
////    fun("CpiD2ZXrhNo", _Strxfrmx);
////    fun("oK6C1fys3dU", _Wctob);
////    fun("piJabTDQRVs", _ZNSbIwSt11char_traitsIwESaIwEE6assignERKS2_mm);
////    fun("318GifOvcOY", __cxa_demangle_gnu3);
////    fun("uhYTnarXhZM", _Tls_setup__Errno);
////    fun("TuKJRIKcceA", _ZTSSt9basic_iosIcSt11char_traitsIcEE);
////    fun("rZwUkaQ02J4", _ZTVSt7collateIwE);
////    fun("GlelR9EEeck", cbrtf);
////    fun("88Fre+wfuT0", _ZTSSt23_Generic_error_category);
////    fun("gZWsDrmeBsg", __mulxc3);
////    fun("fuyXHeERajE", _ZNSt12future_errorD1Ev);
////    fun("gC0DGo+7PVc", _ZGVNSt7collateIcE2idE);
////    fun("sJUU2ZW-yxU", _ZTINSt6locale5facetE);
////    fun("bZ0oEGQUKO8", _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE9_GetffldxEPcRS3_S6_RSt8ios_basePi);
////    fun("Df1BO64nU-k", _Xtime_diff_to_ts);
////    fun("I2UzwkwwEPs",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERl);
////    fun("HUbZmOnT-Dg", atan2);
////    fun("j-gWL6wDros", getifaddrs);
////    fun("pSpDCDyxkaY", _Stoullx);
////    fun("ZuCHPDq-dPw",
////        _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE16do_get_monthnameES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("8pXCeme0FC4", _ZNSt7collateIcED1Ev);
////    fun("1+5ojo5J2xU", _ZTIPl);
////    fun("u6UfGT9+HEA", _ZNKSt19istreambuf_iteratorIcSt11char_traitsIcEE5equalERKS2_);
////    fun("7DzM2fl46gU", _ZTISt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("3BytPOQgVKc", snprintf_s);
////    fun("dsJKehuajH4", _ZNSt6locale7_LocimpC2ERKS0_);
////    fun("Zxe-nQMgxHM", _ZNKSt7collateIwE12do_transformEPKwS2_);
////    fun("SqcnaljoFBw", __atomic_fetch_add_8);
////    fun("5IpoNfxu84U", lrand48);
////    fun("aWc+LyHD1vk", __atomic_fetch_xor_1);
////    fun("ogPDBoLmCcA", mbrtoc16);
////    fun("BYcXjG3Lw-o", _WGenld);
////    fun("NXKsxT-x76M", _ZTSSt12codecvt_base);
////    fun("Slmz4HMpNGs", wcsncpy_s);
////    fun("Ouz5Q8+SUq4", _Stoxflt);
////    fun("d+MxL9XXIRM", _ZNK10__cxxabiv120__si_class_type_info7cast_toEPvPKNS_17__class_type_infoE);
////    fun("W-T-amhWrkA", _LPlsw);
////    fun("fmT2cjPoWBs", fabsf);
////    fun("EtpM9Qdy8D4", __floattidf);
////    fun("nlAk46weq1w", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecm);
////    fun("Ovb2dSJOAuE", strcmp);
////    fun("x9uumWcxhXU", wcsspn);
////    fun("UOz27kgch8k", __atomic_exchange_8);
////    fun("TuIEPzIwWcI", _ZGVNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE2idE);
////    fun("UVft3+rc06o", _ZTSN10__cxxabiv119__pointer_type_infoE);
////    fun("wUa+oPQvFZ0", _Hypot);
////    fun("p6lrRW8-MLY", sceLibcMspaceReallocalign);
////    fun("FMU7jRhYCRU", _Dscale);
////    fun("owKuegZU4ew", logb);
////    fun("lahbB4B2ugY", _Xp_setn);
////    fun("kDZvoVssCgQ", strpbrk);
////    fun("k0vARyJi9oU", __modsi3);
////    fun("+xU0WKT8mDc", isalpha);
////    fun("tyHd3P7oDrU", _ZNKSt9exception8_DoraiseEv);
////    fun("0OjBJZd9VeM", _ZNSt10moneypunctIwLb0EE7_GetcatEPPKNSt6locale5facetEPKS1_);
////    fun("1HYEoANqZ1w", _Unlock_shared_ptr_spin_lock);
////    fun("kxXCvcat1cM", _ZNSt8ios_base4InitD1Ev);
////    fun("m0uSPrCsVdk", _Xp_sqrtx);
////    fun("I5jcbATyIWo", _ZNSt8ios_baseD0Ev);
////    fun("dKjhNUf9FBc", _ZTISt12out_of_range);
////    fun("qO4MLGs1o58", _ZNSt9_Num_base9is_moduloE);
////    fun("St4apgcBNfo", _ZTIi);
////    fun("Q+xU11-h0xQ", sqrtf);
////    fun("VVgqI3B2bfk", _FNan);
////    fun("5PfqUBaQf4g", _ZSt4cout);
////    fun("Utj8Sh5L0jE", _ZNKSt8numpunctIwE16do_thousands_sepEv);
////    fun("y3V0bIq38NE", wctomb_s);
////    fun("h+xQETZ+6Yo", _ZTSPKy);
////    fun("5Xa2ACNECdo", strcpy_s);
////    fun("uCRed7SvX5E", __cxa_get_globals_fast);
////    fun("EiMkgQsOfU0", __powisf2);
////    fun("RnqlvEmvkdw", _malloc_finalize_lv2);
////    fun("1TaQLyPDJEY", _ZNSt6locale16_SetgloballocaleEPv);
////    fun("hBeW7FhedsY", _ZTISt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("ieNeByYxFgA", _ZGVNSt14_Error_objectsIiE16_Iostream_objectE);
////    fun("apPZ6HKZWaQ", _ZNKSt9exception4whatEv);
////    fun("bsohl1ZrRXE", _ZSt10_GetloctxtIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEiRT0_S5_mPKT_);
////    fun("RalOJcOXJJo", _ZNKSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8_GetmfldERS3_S5_bRSt8ios_basePw);
////    fun("GHsPYRKjheE", _ZNSt12placeholders2_2E);
////    fun("HWBJOsgJBT8", __atomic_fetch_sub_1);
////    fun("DAbZ-Vfu6lQ", wcstoull);
////    fun("KUYSaO1qv0Y", posix_spawnattr_getschedpolicy);
////    fun("tJU-ttrsXsk", _ZNSiD1Ev);
////    fun("bNQpG-eKogg",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERd);
////    fun("rm2zzbZ6HjM", _ZN9pathscale13set_terminateEPFvvE);
////    fun("jVDuvE3s5Bs", _Fofree);
////    fun("D0gqPsqeZac", _ZNSt10moneypunctIwLb0EE4intlE);
////    fun("+5OuLYpRB28", _Dclass);
////    fun("41SqJvOe8lA", _FDunscale);
////    fun("EgSIYe3IYso", _ZNSt9_Num_base10is_integerE);
////    fun("GthClwqQAZs", _ZNKSt14error_category10equivalentEiRKSt15error_condition);
////    fun("we-vQBAugV8", _CStrxfrm);
////    fun("2KNnG2Z9zJA", _ZNSt6_MutexC1Ev);
////    fun("h3PbnNnZ-gI", _ZNKSt5ctypeIwE8do_widenEc);
////    fun("F81hKe2k2tg", fgetws);
////    fun("wElyE0OmoRw", _ZTSSt7collateIcE);
////    fun("IuFImJ5+kTk", _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC2ERKSt8_Locinfom);
////    fun("ryl2DYMxlZ0", _ZNSt13basic_filebufIwSt11char_traitsIwEE7_UnlockEv);
////    fun("fmHLr9P3dOk", _Snan);
////    fun("or0CNRlAEeE", _ZNSt12placeholders3_11E);
////    fun("t39dKpPEuVA",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERd);
////    fun("COSADmn1ROg", _Denorm);
////    fun("BIALMFTZ75I", devname_r);
////    fun("lP9zfrhtpBc", _FRecip);
////    fun("Xnrfb2-WhVw", strnstr);
////    fun("Uc+-Sx0UZ3U", _ZNKSt7codecvtIcc9_MbstatetE6do_outERS0_PKcS4_RS4_PcS6_RS6_);
////    fun("Jk+LgZzCsi8", _ZTIPa);
////    fun("kzWL2iOsv0E", _ZN10__cxxabiv123__fundamental_type_infoD1Ev);
////    fun("9biiDDejX3Q", _ZNSt13basic_filebufIcSt11char_traitsIcEE5_LockEv);
    fun("fJnpuVVBbKk", _Znwm);
////    fun("Q-bLp+b-RVY", fesetenv);
////    fun("i-ifjh3SLBU", lgammaf);
////    fun("96Ev+CE1luE", _ZNSt8messagesIcE7_GetcatEPPKNSt6locale5facetEPKS1_);
////    fun("hrv1BgM68kU", _Pmsw);
////    fun("NLHWcHpvMss", _ZNSt14numeric_limitsIeE6digitsE);
////    fun("ISTLytNGT0c", inet_aton);
////    fun("ANz-ClqTX+4", _ULx86_64_step);
////    fun("bzmM0dI80jM", _ZNSt14numeric_limitsIeE12max_exponentE);
////    fun("cPGyc5FGjy0", atanhf);
////    fun("yit-Idli5gU", __atomic_exchange_4);
////    fun("KHJflcH9s84", _Atomic_exchange_1);
////    fun("sYz-SxY8H6M", _Locsum);
////    fun("l3fh3QW0Tss", scalbnl);
////    fun("1rs4-h7Fq9U", __divmoddi4);
////    fun("QLqM1r9nPow", _ZTISt22_System_error_category);
////    fun("9rAeANT2tyE", __cxa_guard_release);
////    fun("J-oDqE1N8R4", _Tolotab);
////    fun("HmcMLz3cPkM", _ZNSt10moneypunctIwLb1EE7_GetcatEPPKNSt6locale5facetEPKS1_);
////    fun("MUjC4lbHrK4", fflush);
////    fun("h05OHOMZNMw", ftrylockfile);
////    fun("3eV8yB0VROo", _Ux86_64_get_elf_image);
////    fun("kUcinoWwBr8", _WLdtob);
////    fun("qOkciFIHghY", _ZNSt14numeric_limitsIbE9is_moduloE);
////    fun("Qb86Y5QldaE", __atomic_fetch_add_1);
////    fun("zxecOOffO68", vsyslog);
////    fun("wWIsjOqfcSc",
////        _ZNKSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERSbIwS2_SaIwEE);
////    fun("aNJaYyn0Ujo", warnx);
////    fun("IdWhZ0SM7JA", _FPmsw);
////    fun("8zTFvBIAIN8", memset);
////    fun("tWI4Ej9k9BY", __negdf2);
////    fun("+GxvfSLhoAo", _Litob);
////    fun("b-xTWRgI1qw", _Dtest);
////    fun("2B+V3qCqz4s", _Cnd_init_with_name);
////    fun("CiD6-BPDZrA", _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED1Ev);
////    fun("7xnsvjuqtZQ", __negvsi2);
////    fun("amHyU7v8f-A", _Tss_create);
////    fun("hMAe+TWS9mQ", __dynamic_cast);
////    fun("LN2bC6QtGQE", _ZNSt14numeric_limitsIxE6digitsE);
////    fun("qTgw+f54K34", _ZTIPKy);
////    fun("pDBDcY6uLSA", vfprintf);
////    fun("33t+tvosxCI", _ZTISt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("KZ++filsCL4", _ZNKSt23_Generic_error_category7messageEi);
////    fun("bEbjy6yceWo", _ZTSPj);
////    fun("AzBnOt1DouU", _ZNKSs5_XranEv);
////    fun("2ZqL1jnL8so", _ZTSSt8numpunctIcE);
////    fun("O4L+0oCN9zA", _FSinh);
////    fun("entYnoIu+fc", _ZNKSt7collateIwE4hashEPKwS2_);
////    fun("mzRlAVX65hQ", _ZNKSt8numpunctIwE16do_decimal_pointEv);
////    fun("xXM1q-ayw2Y", _ZN10__cxxabiv117__class_type_infoD2Ev);
////    fun("rLuypv9iADw", __fixunsdfti);
////    fun("oYMRgkQHoJM", _ZNSt13basic_filebufIcSt11char_traitsIcEE8overflowEi);
////    fun("c6Lyc6xOp4o", _ZNKSt7codecvtIDic9_MbstatetE10do_unshiftERS0_PcS3_RS3_);
////    fun("XFh0C66aEms", _ZNSt12future_errorD2Ev);
////    fun("rSADYzp-RTU", _ZTVSt13basic_filebufIcSt11char_traitsIcEE);
////    fun("mhR3IufA7fE", _ZSt10defer_lock);
////    fun("P0383AW3Y9A", _ZNSt5ctypeIwED1Ev);
////    fun("20qj+7O69XY", isnan);
////    fun("McUBYCqjLMg", _ZNSt10moneypunctIwLb0EEC1Em);
////    fun("EXW20cJ3oNA", _ZNSt14numeric_limitsIdE8digits10E);
////    fun("USLhWp7sZoU", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewl);
////    fun("TNexGlwiVEQ", _ZNKSt7codecvtIcc9_MbstatetE2inERS0_PKcS4_RS4_PcS6_RS6_);
////    fun("VhyjwJugIe8", _ZNKSt7_MpunctIcE14do_curr_symbolEv);
////    fun("sTaUDDnGOpE", _ZNSt10moneypunctIwLb1EEC2Em);
////    fun("g-McpZfseZo", strtouq);
////    fun("7pASQ1hhH00", posix_spawnattr_getsigdefault);
////    fun("9s3P+LCvWP8", _Frprep);
////    fun("x-04iOzl1xs", copysignf);
////    fun("PKJcFUfhKtw", sceLibcPafMspaceMemalign);
////    fun("usQDRS-1HZ8", __fixxfti);
////    fun("Tx5Y+BQJrzs", _ZTVSt13basic_filebufIwSt11char_traitsIwEE);
////    fun("4SnCJmLL27U", _ZNKSt5ctypeIcE9do_narrowEcc);
////    fun("H-TDszhsYuY", _ZTVSt7codecvtIwc9_MbstatetE);
////    fun("dSfKN47p6ac",
////        _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE11do_get_yearES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("QGSIlqfIU2c", _ZNKSt8numpunctIcE13thousands_sepEv);
////    fun("NQW6QjEPUak",
////        _ZZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8_GetifldEPcRS3_S6_NSt5_IosbIiE9_FmtflagsERKSt6localeE4_Src);
////    fun("udTM6Nxx-Ng", _ZTVSt11logic_error);
////    fun("9-TRy4p-YTM", _ZTISt11regex_error);
////    fun("9H2BStEAAMg", _ZTVSt7codecvtIDic9_MbstatetE);
////    fun("7ruwcMCJVGI", __kernel_cosdf);
////    fun("NU-T4QowTNA", _ZSt18_Xinvalid_argumentPKc);
////    fun("CkY0AVa-frg", _ZSt15get_new_handlerv);
////    fun("zck+6bVj5pA", nan);
////    fun("EeOtHxoUkvM", _ZTIN10__cxxabiv117__class_type_infoE);
////    fun("nlaojL9hDtA", round);
////    fun("UCjpTas5O3E", _FSnan);
////    fun("Ea+awuQ5Bm8", _ZNKSt8messagesIwE3getEiiiRKSbIwSt11char_traitsIwESaIwEE);
////    fun("2WVBaSdGIds", _ZSt3cin);
////    fun("mKhVDmYciWA", floorf);
////    fun("u32UXVridxQ", sceLibcPafMspaceRealloc);
////    fun("lynApfiP6Lw", _ZNKSt7codecvtIDsc9_MbstatetE9do_lengthERS0_PKcS4_m);
////    fun("rtBENmz8Iwc", __divmodsi4);
////    fun("zTX7LL+w12Q", _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE7_GetcatEPPKNSt6locale5facetEPKS5_);
////    fun("G4Pw4hv6NKc", _ZNSt14_Num_ldbl_base10is_boundedE);
////    fun("fqLrWSWcGHw", devname);
////    fun("VKdXFE7ualw",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERPv);
////    fun("dHs7ndrQBiI", _ZNSt10moneypunctIwLb0EEC1ERKSt8_Locinfomb);
////    fun("as8Od-tH1BI", __stderrp);
////    fun("uukWbYS6Bn4",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERe);
////    fun("MxZ4Lc35Rig", _Readloc);
////    fun("iXChH4Elf7M", _ZTISt13basic_istreamIwSt11char_traitsIwEE);
////    fun("tOicWgmk4ZI", daylight);
////    fun("splBMMcF3yk", _ZNSt7collateIcEC1EPKcm);
////    fun("MvdnffYU3jg", remainderl);
////    fun("QqBWUNEfIAo", _ZNSs6appendEmc);
////    fun("hXA24GbAPBk", __gedf2);
////    fun("yzcKSTTCz1M", _ZGVNSt10moneypunctIcLb1EE2idE);
////    fun("7FjitE7KKm4", _Gamma_big);
////    fun("AFRS4-8aOSo", __atomic_fetch_or_4);
////    fun("QuF2rZGE-v8", vwprintf);
////    fun("zCB24JBovnQ", _ZNSt8numpunctIcE7_GetcatEPPKNSt6locale5facetEPKS1_);
////    fun("64180GwMVro", _ZN10__cxxabiv119__pointer_type_infoD0Ev);
////    fun("xcc6DTcL8QA", __cxa_bad_typeid);
////    fun("Ecwid6wJMhY", _Stolx);
////    fun("DQXJraCc1rA", alarm);
////    fun("Q3-0HGD3Y48", __atomic_fetch_and_1);
////    fun("QJ5xVfKkni0", wmemcmp);
////    fun("zGpCWBtVC0A", _ZTSc);
////    fun("6NH0xVj6p7M", _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC2Em);
////    fun("DDHG1a6+3q0", roundf);
////    fun("2e96MkSXo3U", _ZTISt10moneypunctIcLb0EE);
////    fun("gpc4YXZffkQ", _Unwind_GetLanguageSpecificData);
////    fun("RQXLbdT2lc4", logf);
////    fun("DQ9mChn0nnE", _ZTIPj);
////    fun("7iFNNuNyXxw", _Getfloat);
////    fun("cPNeOAYgB0A", _ZSt22_Get_future_error_whati);
////    fun("W5k0jlyBpgM", _ZN10__cxxabiv121__vmi_class_type_infoD0Ev);
////    fun("PPdIvXwUQwA", __udivmodsi4);
////    fun("sij3OtJpHFc", _Mbtowc);
////    fun("SmtBNDda5qU",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERb);
////    fun("NG1phipELJE", _ZSt13set_terminatePFvvE);
////    fun("vzh0qoLIEuo", _ZNSt10bad_typeidD1Ev);
////    fun("4GbIwW5u5us", _ZNSt13basic_ostreamIwSt11char_traitsIwEE6sentryC2ERS2_);
////    fun("lckWSkHDBrY", vfscanf);
////    fun("kA-TdiOCsaY", frexp);
////    fun("bgAcsbcEznc", __stdinp);
////    fun("-svZFUiG3T4", _LGamma_big);
////    fun("Mniutm2JL2M", _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC2ERKSt8_Locinfom);
////    fun("UXS8VgAnIP4", _ZTSw);
////    fun("95MaQlRbfC8", _ZNSt10moneypunctIwLb0EED0Ev);
////    fun("iHZb2839dBc", _ZGVNSt8numpunctIcE2idE);
////    fun("g3dK2qlzwnM", _LSincos);
////    fun("57mMrw0l-40", _ZNSt6_WinitC2Ev);
////    fun("SK0Syya+scs", _ZTIDn);
////    fun("wuAQt-j+p4o", exp2f);
////    fun("OJPn-YR1bow", _ZTIPv);
////    fun("2HED9ow7Zjc", __absvsi2);
////    fun("bUQLE6uEu10", _ZNSt9_Num_base8is_exactE);
////    fun("z83caOn94fM", _ZNSt6chrono12steady_clock12is_monotonicE);
////    fun("MHyK+d+72V0", _LErf_small);
////    fun("EBkab3s8Jto", _FFpcomp);
////    fun("RxJnJ-HoySc", _ZNSt10moneypunctIcLb1EEC2EPKcm);
////    fun("Q-GfRQNi66I", posix_spawnattr_getpgroup);
////    fun("lO01m-JcDqM", cbrtl);
////    fun("2g5wco7AAHg", seed48);
////    fun("7iTp7O6VOXQ", inet_ntoa);
////    fun("eeeT3NKKQZ0", _ZNSt12placeholders3_13E);
////    fun("wazw2x2m3DQ", isspace);
////    fun("0NioBryjzSc", __libunwind_Unwind_Backtrace);
////    fun("ymXfiwv59Z0", _ZTISt15basic_streambufIcSt11char_traitsIcEE);
////    fun("Vo8rvWtZw3g", truncf);
////    fun("keXoyW-rV-0", _ZTVSt16invalid_argument);
////    fun("18rLbEV-NQs", _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC1Em);
////    fun("gLGmR9aan4c", __isinfl);
////    fun("MLWl90SFWNE", _ZdaPv);
////    fun("ICY0Px6zjjo", __gtsf2);
////    fun("JbQw6W62UwI", _Fetch_add_8);
////    fun("DZhZwYkJDCE", _ZTIN10__cxxabiv120__function_type_infoE);
////    fun("0hlfW1O4Aa4", localeconv);
////    fun("Yc2gZRtDeNQ", _ZNSt13basic_filebufIcSt11char_traitsIcEED1Ev);
////    fun("eul2MC3gaYs", _Ctype);
////    fun("g7zzzLDYGw0", strdup);
////    fun("4zhc1xNSIno", _ZTIPKj);
////    fun("H2QD+kNpa+U", __inet_ntoa_r);
////    fun("HCzNCcPxu+w",
////        _ZZNKSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8_GetmfldERS3_S5_bRSt8ios_basePwE4_Src);
////    fun("JrwFIMzKNr0", ldexp);
////    fun("2iW5Fv+kFxs", _ZNKSt13runtime_error4whatEv);
////    fun("sBCTjFk7Gi4", _ZTINSt8ios_base7failureE);
////    fun("E14mW8pVpoE", _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE2idE);
////    fun("Uyfpss5cZDE", __mulvdi3);
////    fun("8Ijx3Srynh0", _ZTIx);
////    fun("UTrpOVLcoOA", vsscanf);
////    fun("ql6hz7ZOZTs", _ZTSSt17bad_function_call);
////    fun("log6zy2C9iQ", _ZNSt6_MutexC2Ev);
////    fun("ickyvjtMLm0", _ZTSPb);
////    fun("wCehaWNuStY", _U_dyn_info_list_addr);
////    fun("k5BACNzcm-c", _U_dyn_register);
////    fun("+WvmZi3216M", _ZNSt13basic_filebufIwSt11char_traitsIwEE4syncEv);
////    fun("aeeMZ0XrNsY", err);
////    fun("vCQLavj-3CM", _Dtento);
////    fun("SmYrO79NzeI", abort_handler_s);
////    fun("rHRr+131ATY", llabs);
////    fun("3P+CcdakSi0",
////        _ZZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE9_GetffldxEPcRS3_S6_RSt8ios_basePiE4_Src);
////    fun("Sj7y+JO5PcM", posix_spawn_file_actions_addopen);
////    fun("lPYpsOb9s-I", lgammal);
////    fun("ig4VDIRc21Q", _ZNSt7collateIwED0Ev);
////    fun("0ITXuJOqrSk", _Save_state);
////    fun("388LcMWHRCA", fabs);
////    fun("t3RFHn0bTPg", index);
////    fun("XO3N4SBvCy0", _ZNSt20bad_array_new_lengthD1Ev);
////    fun("VClCrMDyydE", _ZNSt7collateIcEC2EPKcm);
////    fun("TDhjx3nyaoU", _ZNSt7_MpunctIwEC2EPKcmbb);
////    fun("H8AprKeZtNg", _Stderr);
////    fun("XU4yLKvcDh0", __divti3);
////    fun("ClfCoK1Zeb4", __atomic_compare_exchange);
////    fun("KPsQA0pis8o", llrintf);
////    fun("rYLrGFoqfi4",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERy);
////    fun("AjkxQBlsOOY", _ZNKSt8messagesIwE8do_closeEi);
////    fun("L0nMzhz-axs", _ZNSt14numeric_limitsIsE9is_signedE);
////    fun("i1N28hWcD-4", _Getctyptab);
////    fun("Xr4pio56GGQ", _Unwind_SetGR);
////    fun("uVXcivvVHzU", j1f);
////    fun("Zwn1Rlbirio", _ZNSt14_Num_ldbl_base10is_integerE);
////    fun("o0WexTj82pU", _ZNSt14numeric_limitsIhE9is_signedE);
////    fun("Y3hBU5FYmhM",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERj);
////    fun("-P6FNMzk2Kc", cosf);
////    fun("BKidCxmLC5w", _Stoldx);
////    fun("2H51caHZU0Y", _ZTVN10__cxxabiv129__pointer_to_member_type_infoE);
////    fun("6i5aLrxRhG0", __inet_ntoa);
////    fun("ER8-AFoFDfM", _ZTSPKm);
////    fun("81uX7PzrtG8", _ZNKSt7collateIwE9transformEPKwS2_);
////    fun("YHfG3-K23CY", _ZTVSt22_Future_error_category);
////    fun("Cp9ksNOeun8", _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED1Ev);
////    fun("rjak2Xm+4mE", scalbf);
////    fun("D+fkILS7EK0", _Fail_s);
////    fun("XX+xiPXAN8I", _ZNKSt7_MpunctIwE16do_thousands_sepEv);
////    fun("kQelMBYgkK0", _thread_autoinit_dummy_decl);
////    fun("yDPuV0SXp7g", __ctzdi2);
////    fun("U0FdtOUjUPg", _ZNSt9_Num_base15tinyness_beforeE);
////    fun("a3BNqojL4LM", atanhl);
////    fun("-nvxBWa0iDs", gethostname);
////    fun("0AgCOypbQ90", _Atomic_compare_exchange_weak_4);
////    fun("fiOgmWkP+Xc", fmax);
////    fun("HpCeP12cuNY", _ZNSoD1Ev);
////    fun("XmAquprnaGM", __sync_fetch_and_and_16);
////    fun("GUuiOcxL-r0", _WStof);
////    fun("UlJSnyS473g", _Tls_setup__Touptab);
////    fun("zOPA5qnbW2U", _ZNSt5ctypeIwED0Ev);
////    fun("Ks2FIQJ2eDc", _ZTISt10moneypunctIcLb1EE);
////    fun("vBP4ytNRXm0", __parityti2);
////    fun("vyLotuB6AS4", _Cnd_do_broadcast_at_thread_exit);
////    fun("Cj+Fw5q1tUo", _Xtime_get_ticks);
////    fun("hdcGjNpcr4w", _LDclass);
////    fun("0Hu7rUmhqJM", _Tls_setup__Times);
////    fun("qB5nGjWa-bk", sceLibcPafMspaceDestroy);
////    fun("TJFQFm+W3wg", finite);
////    fun("mMPu4-jx9oI", _ZNSt15_Num_float_base5trapsE);
////    fun("QNyUWGXmXNc", strtoumax);
////    fun("6RGkooTERsE",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERf);
////    fun("0nV21JjYCH8", wcsncpy);
////    fun("QGkJzBs3WmU", _ZTVNSt6locale7_LocimpE);
////    fun("uuga3RipCKQ", _ZNSt6locale7_Locimp7_AddfacEPNS_5facetEm);
////    fun("OGybVuPAhAY", reallocalign);
////    fun("ElU3kEY8q+I", _Locvar);
////    fun("AnE9WWbyWkM", _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED1Ev);
////    fun("Kbe+LHOer9o", _ZNKSt5ctypeIwE11do_scan_notEsPKwS2_);
////    fun("2HnmKiLmV6s", _Atomic_fetch_sub_4);
////    fun("EL+4ceAj+UU", _ZNSt14numeric_limitsIDsE6digitsE);
////    fun("KNgcEteI72I", _ZTSe);
////    fun("G84okRnyJJg",
////        _ZNKSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERe);
////    fun("Ani6e+T-y6Q", posix_spawnattr_setsigdefault);
////    fun("s62MgBhosjU", _Unwind_Backtrace);
////    fun("OVqW4uElSrc", _FXp_invx);
////    fun("z8lecpCHpqU", __atomic_exchange_1);
////    fun("WDpobjImAb4", wcsstr);
////    fun("DQ7K6s8euWY", fminl);
////    fun("9jFUM0UppvE", _Ux86_64_flush_cache);
////    fun("PdnFCFqKGqA", _FCosh);
////    fun("D0lUMKU+ELI", _ZNKSt5ctypeIwE5do_isEPKwS2_Ps);
////    fun("riets0BFHMY", _LXp_mulh);
////    fun("FXrK1DiAosQ", _ZNSt14numeric_limitsImE9is_signedE);
////    fun("YBRHNH4+dDo", __isnanl);
////    fun("bwVJf3kat9c", _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED2Ev);
////    fun("HIhqigNaOns", _Inf);
////    fun("k04jLXu3+Ic", sceLibcMspaceMallocStatsFast);
////    fun("woQta4WRpk0", setstate);
////    fun("lJnP-cn0cvQ", _ZTVSt8numpunctIcE);
////    fun("ovtwh8IO3HE", _Atomic_fetch_sub_2);
////    fun("DuW5ZqZv-70", _ZNKSt9exception6_RaiseEv);
////    fun("R0-hvihVoy0", nextafterl);
////    fun("VGhcd0QwhhY", _Divide);
////    fun("nJz16JE1txM", swprintf);
////    fun("TPq0HfoACeI", _ZNKSt8messagesIwE4openERKSsRKSt6locale);
////    fun("mO8NB8whKy8", sceLibcPafMspaceMallocStats);
////    fun("BJCgW9-OxLA", _ZTISt8ios_base);
////    fun("JOV1XY47eQA", _Thrd_abort);
////    fun("cRYyxdZo1YQ", _Atomic_is_lock_free_4);
////    fun("FCuvlxsgg0w", _ZTISt24_Iostream_error_category);
////    fun("gozsp4urvq8",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERt);
////    fun("Jh5qUcwiSEk", _ZnamRKSt9nothrow_t);
////    fun("Q1BL70XVV0o", _ZSt18uncaught_exceptionv);
////    fun("2veyNsXFZuw", _ZTSPKDn);
////    fun("Dw3ieb2rMmU", __atomic_fetch_xor_8);
////    fun("DbEnA+MnVIw", _Towctrans);
////    fun("TSMc8vgtvHI", _ZTSPDi);
////    fun("MxGclWMtl4g", _ZTSSt10moneypunctIwLb0EE);
////    fun("GKWcAz6-G7k", _ZTVSt5ctypeIcE);
////    fun("gTuXQwP9rrs", _Mtx_unlock);
////    fun("DVZmEd0ipSg", _FXp_movx);
////    fun("24m4Z4bUaoY", sscanf_s);
////    fun("zs817D8+hPA", _Unwind_ForcedUnwind);
////    fun("-lZdT34nAAE", sceLibcPafMspaceCalloc);
////    fun("YKbL5KR6RDI", fma);
////    fun("QNwdOK7HfJU", vwscanf);
////    fun("GMpvxPFW924", vprintf);
////    fun("o27+xO5NBZ8", _ZN6Dinkum7threads17_Throw_lock_errorEv);
////    fun("4F11tHMpJa0", _Strerror);
////    fun("w-AryX51ObA", fabsl);
////    fun("2NvhgiBTcVE", __ctzsi2);
////    fun("J6FoFNydpFI", malloc_finalize);
////    fun("+qKS53qzWdA", _ZNSt16nested_exceptionD1Ev);
////    fun("N2f485TmJms", _ZNSt8ios_base7failureD1Ev);
////    fun("6b3KIjPD33k", _ZNSt6locale7_LocimpC1Eb);
////    fun("zomvAQ5RFdA",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERPv);
////    fun("WkAsdy5CUAo", _ZNSt8_Locinfo8_AddcatsEiPKc);
////    fun("pVjisbvtQKU", fetestexcept);
////    fun("YsNLENNlNao", __libunwind_Unwind_FindEnclosingFunction);
////    fun("96xdSFbiR7Q", _ZTIPe);
////    fun("w178uGYbIJs", _Xp_subx);
////    fun("3rJJb81CDM4", __cxa_get_globals);
////    fun("DMWwdFKaCDw", __libunwind_Unwind_GetIPInfo);
////    fun("VHBnRBxBg5E", _ZNSt8numpunctIwED0Ev);
////    fun("RV7X3FrWfTI", wscanf_s);
////    fun("OmG3YPCBLJs", sceLibcPafMspaceMallocStatsFast);
////    fun("gkWgn0p1AfU", freopen);
////    fun("o4DiZqXId90", _ZTSSt10moneypunctIcLb1EE);
////    fun("0FKwlv9iH1c", _ZNKSt7codecvtIDsc9_MbstatetE6do_outERS0_PKDsS4_RS4_PcS6_RS6_);
////    fun("cO0ldEk3Uko", _Atomic_fetch_add_1);
////    fun("43PYQ2fMT8k", _WStopfx);
////    fun("C6gWCWJKM+U", lroundf);
////    fun("TP6INgQ6N4o", __swbuf);
////    fun("SQGxZCv3aYk", signalcontext);
////    fun("cjmSjRlnMAs", iswxdigit);
////    fun("fjHAU8OSaW8", _ZNSt7collateIwEC1EPKcm);
////    fun("45E7omS0vvc", iswctype);
////    fun("y1Ch2nXs4IQ", fpurge);
////    fun("aQURHgjHo-w", _Erfc);
////    fun("H0FQnSWp1es", _Deletegloballocale);
////    fun("gQFVRFgFi48", __moddi3);
////    fun("3PDS2cyBkCw", _ZN9pathscale14set_unexpectedEPFvvE);
////    fun("aTjYlKCxPGo", _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC2ERKSt8_Locinfom);
////    fun("pZ9WXcClPO8", _ZTVN10__cxxabiv120__si_class_type_infoE);
////    fun("hSUcSStZEHM", _ZNSbIwSt11char_traitsIwESaIwEE6appendERKS2_mm);
////    fun("cVSk9y8URbc", posix_memalign);
////    fun("QVsk3fWNbp0", _Atomic_fetch_add_8);
////    fun("ouXHPXjKUL4", _ZTVN6Dinkum7threads21thread_resource_errorE);
////    fun("seyrqIc4ovc", wctrans);
////    fun("ryykbHJ04Cw", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewx);
////    fun("jPywoVsPVR8", __clzti2);
////    fun("lX+4FNUklF0", __cxa_end_catch);
////    fun("Ss3108pBuZY", _Nnl);
////    fun("QvWOlLyuQ2o", _ZTIPDn);
////    fun("jRLwj8TLcQY", _ZTVSt10bad_typeid);
////    fun("hf2Ljaf19Fs", _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE2idE);
////    fun("Zmeuhg40yNI", _ZSt10adopt_lock);
////    fun("2MK5Lr9pgQc", _ZNSt8bad_castD0Ev);
////    fun("YNzNkJzYqEg", strncpy_s);
////    fun("BuxsERsopss", _ZNSt10moneypunctIcLb1EE7_GetcatEPPKNSt6locale5facetEPKS1_);
////    fun("mhIInD5nz8I", __addsf3);
////    fun("aMucxariNg8", _Btowc);
////    fun("VPcTGA-LwSo", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basece);
////    fun("-egu08GJ0lw", _Fetch_or_seq_cst_2);
////    fun("HU3yzCPz3GQ", _ZNSt15_Num_float_base9is_moduloE);
////    fun("+qso2nVwQzg", _ZTIPb);
////    fun("XyJPhPqpzMw", _ZNSt4_PadD1Ev);
////    fun("PlDgAP2AS7M", _Wctype);
////    fun("3eGXiXpFLt0", sce_stdout);
////    fun("hsi9drzHR2k", log2f);
////    fun("IBtzswgYU3A", _ZTISt10money_base);
////    fun("TEd4egxRmdE", mbrtoc32);
////    fun("Cb1hI+w9nyU", _ZNSt8numpunctIcEC2ERKSt8_Locinfomb);
////    fun("TMhLRjcQMw8", _PathLocale);
////    fun("YCVxOE0lHOI", _LXp_subx);
////    fun("Gr+ih5ipgNk", _ZTIPKl);
////    fun("JWBr5N8zyNE", isdigit);
////    fun("U52BlHBvYvE", _Getmbcurmax);
////    fun("ZlsoRa7pcuI", _Daysto);
////    fun("HMRMLOwOFIQ", _Atomic_store_4);
////    fun("dOKh96qQFd0", _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED2Ev);
////    fun("y7aMFEkj4PE", _ZSt15_Xruntime_errorPKc);
////    fun("2SSK3UFPqgQ", __floatdisf);
////    fun("QfPuSqhub7o", _ZSt7_MP_AddPyy);
////    fun("6IM2up2+a-A", fstatvfs);
////    fun("1kvQkOSaaVo", _ZNSt10moneypunctIwLb0EEC2ERKSt8_Locinfomb);
////    fun("c-EfVOIbo8M", _ZTVSt11regex_error);
////    fun("DR029KeWsHw", _ZNKSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_bRSt8ios_basecRKSs);
////    fun("7eNus40aGuk", wcscspn);
////    fun("pA9N3VIgEZ4", wcsncat);
////    fun("kmzNbhlkddA", _ZNSt13basic_filebufIwSt11char_traitsIwEE5imbueERKSt6locale);
////    fun("V02oFv+-JzA", __isinf);
////    fun("6DBUo0dty1k", _ZNSt8messagesIcEC2Em);
////    fun("fRWufXAccuI", _Lock_shared_ptr_spin_lock);
////    fun("U7IhU4VEB-0", iswprint);
////    fun("K+k1HlhjyuA", __atomic_fetch_sub_8);
////    fun("E3RYvWbYLgk", gammaf_r);
////    fun("VUtibKJCt1o", getopt);
////    fun("-syPONaWjqw", _ZNKSt12future_error8_DoraiseEv);
////    fun("Vw03kdKZUN0",
////        _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmcc);
////    fun("JJ-mkOhdook", _ZNSt13basic_filebufIwSt11char_traitsIwEED0Ev);
////    fun("x7vtyar1sEY", _ZNSt8ios_base7failureD0Ev);
////    fun("IvP-B8lC89k", _Wcstate);
////    fun("ikBt0lqkxPc", _ZNKSt7codecvtIcc9_MbstatetE6lengthERS0_PKcS4_m);
////    fun("1C2-2WB9NN4", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewb);
////    fun("I6Z-684E2C4", ispunct);
////    fun("pMQUQSfX6ZE", _ZTSd);
////    fun("Az3tTyAy380", significand);
////    fun("iBrS+wbpuT0", _ZNSt14numeric_limitsIaE9is_signedE);
////    fun("y9OoA+P5cjk", wcrtomb);
////    fun("cjZEuzHkgng", _Atomic_load_4);
////    fun("+tfKv1vt0QQ", y0f);
////    fun("qlWiRfOJx1A", __fpclassifyd);
////    fun("cfAXurvfl5o", __cxa_allocate_exception);
////    fun("JM4EBvWT9rc", tanh);
////    fun("vW-nnV62ea4",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERb);
////    fun("rf0BfDQG1KU", _ZTSSt7codecvtIcc9_MbstatetE);
////    fun("5rD2lCo4688", _ZTSPKs);
////    fun("GLNDoAYNlLQ", __kernel_rem_pio2);
////    fun("OdzLUcBLhb4", __floatuntixf);
////    fun("GdwuPYbVpP4", __atomic_store_n);
////    fun("2emaaluWzUw", __cxa_guard_abort);
////    fun("A-cEjaZBDwA", _LZero);
////    fun("sZLrjx-yEx4", wcstombs_s);
    fun("z+P+xCnWLBk", _ZdlPv);
////    fun("oNAnn5cOxfs", _ZTISt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("yofHspnD9us", _ZNSt13basic_filebufIcSt11char_traitsIcEE7_UnlockEv);
////    fun("JFiji2DpvXQ", _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE2idE);
////    fun("Tt3ZSp9XD4E", _ZTSSt7codecvtIDic9_MbstatetE);
////    fun("ZvahxWPLKm0", _ZNSt14numeric_limitsIiE6digitsE);
////    fun("5-3wdp3FVtc", _Unwind_GetIPInfo);
////    fun("CC-BLMBu9-I", malloc_stats);
////    fun("4PL4SkJXTos", _ZNSt14_Num_ldbl_base8is_exactE);
////    fun("McaImWKXong", _Cnd_timedwait);
////    fun("VRX+Ul1oSgE", _Atomic_store_1);
////    fun("eTP9Mz4KkY4", __divdc3);
////    fun("pbolHsxA72U", _ULx86_64_init_local);
////    fun("MIKN--3fORE", _ZNSt15_Num_float_base12has_infinityE);
////    fun("cqcY17uV3dI", _Fetch_xor_seq_cst_4);
////    fun("wrR3T5i7gpY", _ZNSt10moneypunctIcLb0EEC1ERKSt8_Locinfomb);
////    fun("7Pc0nOTw8po", verrc);
////    fun("YFoOw5GkkK0", hypot);
////    fun("ij-yZhH9YjY", _ZNKSt7_MpunctIwE14do_frac_digitsEv);
////    fun("F+m2tOMgeTo", __atomic_load_4);
////    fun("G9LB1YD5-xc",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8_GetifldEPcRS3_S6_NSt5_IosbIiE9_FmtflagsERKSt6locale);
////    fun("Tlfgn9TIWkA", _ZNSt14numeric_limitsIwE6digitsE);
////    fun("A+1YXWOGpuo", _LDtest);
////    fun("c9S0tXDhMBQ", _Xp_ldexpx);
////    fun("ouo2obDE6yU", _ZTSSt22_Future_error_category);
////    fun("GAUuLKGhsCw", ceilf);
////    fun("8Vs1AjNm2mE", _ZTSN10__cxxabiv117__class_type_infoE);
////    fun("fffwELXNVFA", fprintf);
////    fun("rcQCUr0EaRU", _Getptoupper);
////    fun("Bo5wtXSj4kc", fwscanf_s);
////    fun("fQYpcUzy3zo", vscanf_s);
////    fun("nRf0VQ++OEw", _ZNKSt8numpunctIcE11do_truenameEv);
////    fun("Bc4ozvHb4Kg", _ZGVNSt10moneypunctIcLb0EE2idE);
////    fun("YZk9sHO0yNg", frexpl);
////    fun("6sAaleB7Zgk", _ZNSt8messagesIwED1Ev);
////    fun("AoZRvn-vaq4", _ZZNSt13basic_filebufIwSt11char_traitsIwEE5_InitEP7__sFILENS2_7_InitflEE7_Stinit);
////    fun("E64hr8yXoXw", _ZNKSt7_MpunctIcE13do_pos_formatEv);
////    fun("XX9KWzJvRf0", vfwprintf_s);
////    fun("3YCLxZqgIdo", _ZNSt7codecvtIcc9_MbstatetEC1ERKSt8_Locinfom);
////    fun("SFlW4kqPgU8", posix_spawnattr_setschedparam);
////    fun("s0V1HJcZWEs", _ZNSt12placeholders3_14E);
////    fun("enqPGLfmVNU", strtok_r);
////    fun("9yDWMxEFdJU", strrchr);
////    fun("VbczgfwgScA", _WFrprep);
////    fun("2wz4rthdiy8", _ZNSt7collateIwE7_GetcatEPPKNSt6locale5facetEPKS1_);
////    fun("G8z7rz17xYM", _ZTSSt13bad_exception);
////    fun("M-KRaPj9tQQ", _ZNSt9_Num_base8digits10E);
////    fun("6hdfGRKHefs", sceLibcPafMspaceCreate);
////    fun("fQ+SWrQUQBg", _FSincos);
////    fun("lhJWkEh-HXM", _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE7_GetintERS3_S5_iiRiRKSt5ctypeIwE);
////    fun("9ByRMdo7ywg", _ZTVN10__cxxabiv121__vmi_class_type_infoE);
////    fun("xX6s+z0q6oo", _ZTISt9type_info);
////    fun("kFYQ4d6jVls", _ZSt11try_to_lock);
////    fun("Sb26PiOiFtE", _Isdst);
////    fun("OdvMJCV7Oxo", __floatunsidf);
////    fun("2bUUbbcqHUo",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERj);
////    fun("9Vyfb-I-9xw", _ZNKSt7codecvtIDsc9_MbstatetE16do_always_noconvEv);
////    fun("stv1S3BKfgw", _Wctombx);
////    fun("mdGgLADsq8A", __divdf3);
////    fun("7WoI+lVawlc", seekdir);
////    fun("1e-q5iIukH8", wcstoumax);
////    fun("LmXIpdHppBM", __eqsf2);
////    fun("5KOPB+1eEfs", _ZTVSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("Fwow0yyW0nI", remquol);
////    fun("Cj81LPErPCc", __signbitl);
////    fun("D2njLPpEt1E",
////        _ZNKSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERSs);
////    fun("iz2shAGFIxc", hypotf);
////    fun("n7iD5r9+4Eo", _ZTISt11_Facet_base);
////    fun("CyXs2l-1kNA", _Iswctype);
////    fun("-hn1tcVHq5Q", sceLibcMspaceCreate);
////    fun("aOtpC3onyJo", _Xp_mulh);
////    fun("u2tMGOLaqnE", _ZSt14_Atomic_assertPKcS0_);
////    fun("owT6zLJxrTs", malloc_initialize);
////    fun("jUQ+FlOMEHk", _ZTSSt5ctypeIcE);
////    fun("wvqDod5pVZg", posix_spawnattr_getsigmask);
////    fun("mM4OblD9xWM", _LTan);
////    fun("Fj7VTFzlI3k", _ZTISt5ctypeIwE);
////    fun("inwDBwEvw18", xtime_get);
////    fun("Kkgg8mWU2WE", _WPuttxt);
////    fun("M4YYbSFfJ8g", setenv);
////    fun("Im55VJ-Bekc", swprintf_s);
////    fun("sVUkO0TTpM8", _ZTIPKi);
////    fun("vU9svJtEnWc", _ZSt4setwi);
////    fun("tQIo+GIPklo", _ZSt14_Xlength_errorPKc);
////    fun("ogi5ZolMUs4", _ZTVSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("EDvkw0WaiOw", __unorddf2);
////    fun("qdHsu+gIxRo", _ZTVSt5ctypeIwE);
////    fun("KdP-nULpuGw", fgets);
////    fun("jatbHyxH3ek", _Xp_mulx);
////    fun("ddr7Ie4u5Nw", _ZNSt16invalid_argumentD2Ev);
////    fun("Ej0X1EwApwM", _ZNKSt5ctypeIwE10do_tolowerEPwPKw);
////    fun("2MfMa3456FI", _Getcloc);
////    fun("K-5mtupQZ4g", _ZNSt6locale5emptyEv);
////    fun("jbz9I9vkqkk", vsprintf);
////    fun("LEbYWl9rBc8", wcstold);
////    fun("KY+yxjxFBSY", _ZTSSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("myTyhGbuDBw", _Clocale);
////    fun("AQlqO860Ztc", _ZTSN10__cxxabiv120__si_class_type_infoE);
////    fun("8e2KBTO08Po", _Nan);
////    fun("hnGhTkIDFHg", _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED1Ev);
////    fun("XYqrcE4cVMM", _ZTSSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("6OYWLisfrB8", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewe);
////    fun("3Fd+8Pk6fgE", _ZTSPKi);
////    fun("BDteGj1gqBY", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewPKv);
////    fun("vu0B+BGlol4", _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED2Ev);
////    fun("OQorbmM+NbA", _ZNSt14numeric_limitsItE8digits10E);
////    fun("B8c4P1vCixQ",
////        _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmPKwSE_);
////    fun("SNdBm+sNfM4", __divxc3);
////    fun("NL836gOLANs", fopen_s);
////    fun("27Z-Cx1jbkU", _ZNSt24_Iostream_error_categoryD1Ev);
////    fun("H-QeERgWuTM", sce_putc_unlocked);
////    fun("oYliMCqNYQg", _ZTISt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("NtqD4Q0vUUI", _ZN10__cxxabiv120__function_type_infoD2Ev);
////    fun("6yplvTHbxpE", _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED0Ev);
////    fun("HeBwePMtuFs", _ZNKSt8messagesIcE7do_openERKSsRKSt6locale);
////    fun("259y57ZdZ3I", __ltsf2);
////    fun("sOz2j1Lxl48", _ZTIPKx);
////    fun("Ky+C-qbKcX0", _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC1ERKSt8_Locinfom);
////    fun("v9HHsaa42qE", _ZNSt9_Num_base10is_boundedE);
////    fun("djNkrJKTb6Q", _ZNSt7codecvtIcc9_MbstatetED1Ev);
////    fun("lybyyKtP54c", readdir);
////    fun("A2TTRMAe6Sw", _ZTISt8numpunctIwE);
////    fun("D4Hf-0ik5xU", __muldc3);
////    fun("44hlATrd47U", posix_spawnattr_init);
////    fun("XAk7W3NcpTY", _ZN10__cxxabiv117__class_type_infoD0Ev);
////    fun("Z7NWh8jD+Nw", _ZTISt13bad_exception);
////    fun("DmUIy7m0cyE", _WStoxflt);
////    fun("pzfFqaTMsFc", _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE2idE);
////    fun("Zb+hMspRR-o", _ZSt13get_terminatev);
////    fun("R2QKo3hBLkw", _rtld_atfork_post);
////    fun("9mMuuhXMwqQ", sceLibcPafMspaceFree);
////    fun("zwV79ZJ9qAU", __fpclassifyl);
////    fun("fMfCVl0JvfE", _ZNSt14_Error_objectsIiE15_Generic_objectE);
////    fun("wO1-omboFjo", _Stoflt);
////    fun("zEhcQGEiPik", _ZTSSt12length_error);
////    fun("8zsu04XNsZ4", expf);
////    fun("HHVZLHmCfI4", _ZTSPh);
////    fun("QQsnQ2bWkdM", _ZTISt4_Pad);
////    fun("mNk6FfI8T7I", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE5_FputES3_RSt8ios_basecPKcmmmm);
////    fun("VxG0990tP3E", wcsncat_s);
////    fun("P8F2oavZXtY", _ZNSt8ios_baseD2Ev);
////    fun("7PxmvOEX3oc", wctomb);
////    fun("Q8pvJimUWis", __isfinitef);
////    fun("yIn4l8OO1zA", _ZNSt8numpunctIcEC2EPKcmb);
////    fun("EzuahjKzeGQ", _ZNSt15_Num_float_base15tinyness_beforeE);
////    fun("YrYO5bTIPqI", _ZTSSt7codecvtIwc9_MbstatetE);
////    fun("6CPwoi-cFZM", _ZNKSt8bad_cast4whatEv);
////    fun("-vXEQdRADLI", strtok_s);
////    fun("fV2xHER+bKE", wcscoll);
////    fun("mY9FWooxqJY", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewb);
////    fun("dtYRVCJF4LY", _U_dyn_cancel);
////    fun("+X-5yNFPbDw", __muloti4);
////    fun("NFAhHKyMnCg", _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE2idE);
////    fun("DS03EjPDtFo", _FHypot);
////    fun("J7d2Fq6Mb0k", _ZNSt14numeric_limitsIfE8digits10E);
////    fun("ASUJmlcHSLo", _ZNSt11regex_errorD0Ev);
////    fun("PgiTG7nVxXE", _ZNSt10moneypunctIcLb1EE2idE);
////    fun("hM7qvmxBTx8", _Tls_setup__Tolotab);
////    fun("XI0YDgH8x1c", remquo);
////    fun("GZWjF-YIFFk", asinf);
////    fun("j84nSG4V0B0", copysignl);
////    fun("Q3YIaCcEeOM", _ZTSSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("P7l9+yBL5VU", _ZTSSt12bad_weak_ptr);
////    fun("oQDS9nX05Qg", _ZNSt16invalid_argumentD1Ev);
////    fun("6zU++1tayjA", __extendsfdf2);
////    fun("bRN9BzEkm4o", _Gentime);
////    fun("M6nCy6H8Hs4", _Atqexit);
////    fun("kCKHi6JYtmM", rewinddir);
////    fun("MXRNWnosNlM", sqrt);
////    fun("vsK6LzRtRLI", basename_r);
////    fun("Y7aJ1uydPMo", realloc);
////    fun("Uw3OTZFPNt4", _ZNSt6_WinitD1Ev);
////    fun("7f4Nl2VS0gw", _ZTISt9basic_iosIcSt11char_traitsIcEE);
////    fun("iI6kGxgXzcU", _getprogname);
////    fun("I-e7Dxo087A", _ZNSt8messagesIwED2Ev);
////    fun("Hq9-2AMG+ks", posix_spawnattr_destroy);
////    fun("XRxuwvN++2w", _ZTSSt12system_error);
////    fun("u5yK3bGG1+w", _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC1ERKSt8_Locinfom);
////    fun("0ZdjsAWtbG8", _ZNSt9_Num_base9is_iec559E);
////    fun("islhay8zGWo", _LDsign);
////    fun("uO6YxonQkJI", _ZTSSt5_IosbIiE);
////    fun("apdxz6cLMh8", truncl);
////    fun("Q4rRL34CEeE", sinf);
////    fun("-ZjhROPju4Q", _ULx86_64_create_addr_space);
////    fun("CfOrGjBj-RY", log2l);
////    fun("HmdaOhdCr88", _Erf_one);
////    fun("tgioGpKtmbE", _Mtx_init_with_name);
////    fun("cG5hQhjFGog",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERx);
////    fun("bF4eWOM5ouo", _Getpcostate);
////    fun("FUvnVyCDhjg", _ZTSDn);
////    fun("hcuQgD53UxM", printf);
////    fun("jAO1IJKMhE4", _ZNSt12bad_weak_ptrD1Ev);
////    fun("QaTrhMKUT18", nexttowardf);
////    fun("DOBCPW6DL3w", _ZTIPDi);
////    fun("cNGg-Y7JQQw", _FGamma_big);
////    fun("L7f7zYwBvZA", _Thrd_detach);
////    fun("cV6KpJiF0Ck", _ZNSt13basic_filebufIwSt11char_traitsIwEE9_EndwriteEv);
////    fun("QI-x0SL8jhw", acosf);
////    fun("ikHTMeh60B0", _FXp_setn);
////    fun("PJ2UDX9Tvwg", _ZNKSt7codecvtIDic9_MbstatetE5do_inERS0_PKcS4_RS4_PDiS6_RS6_);
////    fun("5Lf51jvohTQ", _Mtx_destroy);
////    fun("hzX87C+zDAY", bsearch_s);
////    fun("g7gjCDEedJA", _ZNSt13basic_filebufIwSt11char_traitsIwEE7seekoffElNSt5_IosbIiE8_SeekdirENS4_9_OpenmodeE);
////    fun("1jUJO+TZm5k", _ZTSSt5ctypeIwE);
////    fun("VlDpPYOXL58", __floattisf);
////    fun("EnMjfRlO5f0", _ZTISt10moneypunctIwLb0EE);
////    fun("jHo78LGEtmU", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewm);
////    fun("srzSVSbKn7M", isxdigit);
////    fun("4SvlEtd0j40", sceLibcPafMspaceReallocalign);
////    fun("31lJOpD3GGk", _ZNSt14numeric_limitsIlE6digitsE);
////    fun("wjG0GyCyaP0", iswgraph);
////    fun("lb+HLLZkbbw", gets_s);
////    fun("OmDPJeJXkBM", _WStoll);
////    fun("8OSy0MMQ7eI", _ZTSPt);
////    fun("-Q6FYBO4sn0", nexttoward);
////    fun("W+lrIwAQVUk", _FXp_mulh);
////    fun("2UFh+YKfuzk", ctime_s);
////    fun("apHKv46QaCw", _ZTVSt12bad_weak_ptr);
////    fun("LcHsLn97kcE", _WGetfloat);
////    fun("kHg45qPC6f0", strncat);
////    fun("PqF+kHW-2WQ", tolower);
////    fun("0JidN6q9yGo", _Thrd_start);
////    fun("1PvImz6yb4M", __gtdf2);
////    fun("wZi5ly2guNw", _Exit);
////    fun("BvRS0cGTd6I", _ZNKSt7codecvtIDsc9_MbstatetE13do_max_lengthEv);
////    fun("5zWUVRtR8xg", _FXp_setw);
////    fun("9pPbNXw5N9E", _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC1EPKcm);
////    fun("MHA0XR2YHoQ", _ZNSt10bad_typeidD0Ev);
////    fun("GGqIV4cjzzI",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8_GetifldEPcRS3_S6_NSt5_IosbIiE9_FmtflagsERKSt6locale);
////    fun("Z2I0BWPANGY", __sync_fetch_and_sub_16);
////    fun("m4kEqv7eGVY", _ZNSt8numpunctIwEC2EPKcmb);
////    fun("4Srtnk+NpC4", _ZNSt8messagesIwEC1EPKcm);
////    fun("9FF+T5Xks9E", _ZNSt6locale7_Locimp8_ClocptrE);
////    fun("izmoTISVoF8", _ZTv0_n24_NSt13basic_istreamIwSt11char_traitsIwEED0Ev);
////    fun("6cfcRTPD2zU", _ZTSPl);
////    fun("8SDojuZyQaY", _ZNKSt14error_category23default_error_conditionEi);
////    fun("R5X1i1zcapI", _Atomic_fetch_or_4);
////    fun("1eEXfeW6wrI", _ZNKSt7collateIcE10do_compareEPKcS2_S2_S2_);
////    fun("UG6HJeH5GNI", _ZTSSt11regex_error);
////    fun("dGYo9mE8K2A", _ZNSt4_PadC2Ev);
////    fun("FHErahnajkw", _Atan);
////    fun("c6Qa0P3XKYQ", _LPoly);
////    fun("iTODM3uXS2s", _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC1ERKSt8_Locinfom);
////    fun("5AN3vhTZ7f8", _FPoly);
////    fun("o-gc5R8f50M",
////        _ZZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8_GetffldEPcRS3_S6_RSt8ios_basePiE4_Src);
////    fun("q0F6yS-rCms", strcspn);
////    fun("GST3YemNZD8", _ZNSt14numeric_limitsIiE9is_signedE);
////    fun("gacfOmO8hNs", ceil);
////    fun("q1OvUam0BJU", __isthreaded);
////    fun("XXiGcYa5wtg", _ZNSt9_Num_base11round_styleE);
////    fun("fPxypibz2MY", atoi);
////    fun("8gG-+co6LfM", __addvdi3);
////    fun("ijAqq39g4dI", _Puttxt);
////    fun("v7S7LhP2OJc", wcstombs);
////    fun("749AEdSd4Go", _ZTVSt9type_info);
////    fun("MQFPAqQPt1s", __cxa_decrement_exception_refcount);
////    fun("4+oswXtp7PQ", _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED2Ev);
////    fun("LLssoYjMOow", _ZNSt9type_infoD1Ev);
////    fun("iS4aWbUonl0", _Mtx_lock);
////    fun("T1YYqsPgrn0", _ZNSt14numeric_limitsIhE6digitsE);
////    fun("7PkSz+qnTto", fsetpos);
////    fun("hPzYSd5Nasc", _Mtx_timedlock);
////    fun("FOsY+JAyXow", _ZNKSt12codecvt_base13do_max_lengthEv);
////    fun("aJKn6X+40Z8", ceill);
////    fun("Xk7IZcfHDD8", _ZNKSt7codecvtIcc9_MbstatetE9do_lengthERS0_PKcS4_m);
////    fun("UQPicLg8Sx8", _ZNSt9basic_iosIcSt11char_traitsIcEE4initEPSt15basic_streambufIcS1_Eb);
////    fun("Pr1yLzUe230", _ZNSt7_MpunctIwEC2Emb);
////    fun("gguxDbgbG74", _Costate);
////    fun("go1LaO7f15k", _Unwind_FindEnclosingFunction);
////    fun("P+q1OLiErP0", _ZNKSt7codecvtIcc9_MbstatetE5do_inERS0_PKcS4_RS4_PcS6_RS6_);
////    fun("k-l0Jth-Go8", mrand48);
////    fun("ig6SRr1GCU4",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERm);
////    fun("XQFE8Y58WZM", _warn);
////    fun("F8b+Wb-YQVs", wmemmove_s);
////    fun("a54t8+k7KpY", _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE2idE);
////    fun("cMwgSSmpE5o", _Mtxunlock);
////    fun("uqLGWOX7-YE", _ZNSt9basic_iosIwSt11char_traitsIwEE4initEPSt15basic_streambufIwS1_Eb);
////    fun("M1xC101lsIU", _Geterrno);
////    fun("Dl4hxL59YF4", _ZNSt9_Num_base14max_exponent10E);
////    fun("moDSeLQGJFQ", _LCosh);
////    fun("XilOsTdCZuM", _ZNSt14overflow_errorD1Ev);
////    fun("7p7kTAJcuGg", __inet_addr);
////    fun("3m2ro+Di+Ck", nextafterf);
////    fun("tPsGA6EzNKA", _ZNSt5ctypeIcED1Ev);
////    fun("BEKIcbCV-MU", towctrans);
////    fun("-l+ODHZ96LI", _ZTSSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("dIxG0L1esAI", _ZTSPd);
////    fun("fXCPTDS0tD0", _LQuad);
////    fun("WMw8eIs0kjM", _ZNKSt10bad_typeid8_DoraiseEv);
////    fun("vhtcIgZG-Lk", realpath);
////    fun("iCl-Z-g-uuA", asinhl);
////    fun("yAobGI2Whrg", _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC2ERKSt8_Locinfom);
////    fun("HWEOv0+n7cU", mergesort);
////    fun("fjni7nkqJ4M", _ZTVN10__cxxabiv116__enum_type_infoE);
////    fun("QEr-PxGUoic", _LXp_addh);
////    fun("jCX3CPIVB8I", _ZNSt6chrono12system_clock12is_monotonicE);
////    fun("TsGewdW9Rto", _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE2idE);
////    fun("-vyIrREaQ0g", _Dint);
////    fun("rsqqpUwcsQY", __libunwind_Unwind_Resume_or_Rethrow);
////    fun("NlgA2fMtxl4", _ZTIe);
////    fun("I4y33AOamns", _ZTIPKc);
////    fun("QossXdwWltI",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERl);
////    fun("DN0xDLRXD2Y", _ZTSPf);
////    fun("H2e8t5ScQGc", __cxa_finalize);
////    fun("tX8ED4uIAsQ", __udivdi3);
////    fun("1h8hFQghR7w", _ZSt12setprecisioni);
////    fun("jy9urODH0Wo", _ZNSt8messagesIcED0Ev);
////    fun("MqeMaVUiyU8", wcscat_s);
////    fun("5TjaJwkLWxE", bcmp);
////    fun("+Le0VsFb9mE", _ZTISt23_Generic_error_category);
////    fun("Qazy8LmXTvw", ftell);
////    fun("hEQ2Yi4PJXA", _ZNSt6locale16_GetgloballocaleEv);
////    fun("Vbeoft607aI", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecy);
////    fun("jr5yQE9WYdk", _Tls_setup__Locale);
////    fun("8KwflkOtvZ8", __atomic_load_8);
////    fun("PjH5dZGfQHQ", _ZNSt13basic_filebufIwSt11char_traitsIwEE8overflowEi);
////    fun("MCtJ9D7B5Cs",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERe);
////    fun("UV4m0bznVtU", posix_spawnattr_setflags);
////    fun("2nqzJ87zsB8", finitef);
////    fun("OwfBD-2nhJQ", _ZTVSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("z7aCCd9hMsI", __fpclassifyf);
////    fun("aYVETR3B8wk", _Atomic_load_2);
////    fun("vj2WUi2LrfE", fscanf_s);
////    fun("RrTMGyPhYU4", _ZNSt8messagesIwEC1ERKSt8_Locinfom);
////    fun("fLcU5G6Qrjs", atoll);
////    fun("6WYrZgAyjuE", _ZTSN10__cxxabiv123__fundamental_type_infoE);
////    fun("uBlwRfRb-CM", _ZNSt13runtime_errorD1Ev);
////    fun("fGYLBr2COwA", __mb_sb_limit);
////    fun("hGljHZEfF0U", flockfile);
////    fun("H61hE9pLBmU", _ZTSSt9exception);
////    fun("pzUa7KEoydw", sceLibcMspaceIsHeapEmpty);
////    fun("10VcrHqHAlw", _ZNSt13basic_filebufIwSt11char_traitsIwEE7seekposESt4fposI9_MbstatetENSt5_IosbIiE9_OpenmodeE);
////    fun("MSSvHmcbs3g",
////        _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE7_GetcatEPPKNSt6locale5facetEPKS5_);
////    fun("2uKxXHAKynI", _Atomic_store_8);
////    fun("AEuF3F2f8TA", fgetc);
////    fun("gYlF567r3-A", _ZNKSt7collateIcE12do_transformEPKcS2_);
////    fun("zF2GfKzBgqU", _ZNKSt7_MpunctIwE13do_pos_formatEv);
////    fun("++In3PHBZfw", _Atomic_fetch_or_8);
////    fun("j6OnScWpu7k", mbrlen);
////    fun("MOBxtefPZUg", _ZNSt9exceptionD2Ev);
////    fun("sQL8D-jio7U", _Fopen);
////    fun("bTQcNwRc8hE", _ZNSt8ios_base4InitC2Ev);
////    fun("ckD5sIxo+Co", _ZNKSt7_MpunctIwE11do_groupingEv);
////    fun("-e-F9HjUFp8", fprintf_s);
////    fun("yb9iUBPkSS0", fdimf);
////    fun("TLvAYmLtdVw", _FRint);
////    fun("hjDoZ6qbkmQ", _LXp_setn);
////    fun("xM5re58mxj8", _ZNSt7codecvtIDsc9_MbstatetE2idE);
////    fun("D-qDARDb1aM", iswalpha);
////    fun("mnufPlYbnN0", _FDtest);
////    fun("zpy7LnTL5p0", __kernel_sin);
////    fun("tdcAqgCS+uI", mbstowcs_s);
////    fun("hzsdjKbFD7g", _Getpwcostate);
////    fun("Ntg7gSs99PY", _ZNSt13_Num_int_base10is_boundedE);
////    fun("M1jmeNsWVK8", _ZTIPc);
////    fun("y-bbIiLALd4", _ZTSN6Dinkum7threads10lock_errorE);
////    fun("TtYifKtVkYA",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERy);
////    fun("djHbPE+TFIo", _ZNSt6_MutexD1Ev);
////    fun("ZRAcn3dPVmA", fwprintf);
////    fun("FModQzwn1-4", _Printf);
////    fun("lYdqBvDgeHU", sinhl);
////    fun("m6rjfL4aMcA", _ZNKSt7codecvtIDic9_MbstatetE9do_lengthERS0_PKcS4_m);
////    fun("pMWnITHysPc", _ZNSt6localeD1Ev);
////    fun("ywJpNe675zo",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERj);
////    fun("8F52nf7VDS8", __eqdf2);
////    fun("yCdGspbNHZ8", sys_siglist);
////    fun("DwH3gdbYfZo", _ZTISt9bad_alloc);
////    fun("lYDzBVE5mZs", _ZdlPvm);
////    fun("aPDKI3J8PqI", posix_spawnattr_setpgroup);
////    fun("n4+3hznhkU4", _ZNSt8messagesIwEC1Em);
////    fun("90T0XESrYzU", _ZNSt13_Num_int_base10is_integerE);
////    fun("+CUrIMpVuaM", sce_stderr);
////    fun("sZ93QMbGRJY", gammaf);
////    fun("DJXyKhVrAD8", _Erf_small);
////    fun("RYTHR81Y-Mc", _ZNKSt7codecvtIDsc9_MbstatetE10do_unshiftERS0_PcS3_RS3_);
////    fun("XaSxLBnqcWE", _ZNKSt7collateIcE9transformEPKcS2_);
////    fun("0ISumvb2U5o", _WPutfld);
////    fun("YvmY5Jf0VYU", _Thrd_join);
////    fun("gSveh4yZbi0", __libunwind_Unwind_GetCFA);
////    fun("C0IYaaVSC1w", _ZTISt11range_error);
////    fun("thDTXTikSmc", _ZTSPKh);
////    fun("csNIBfF6cyI", _ZNSt14numeric_limitsIjE8digits10E);
////    fun("bPUMNZBqRqQ", _ZTSN10__cxxabiv117__pbase_type_infoE);
////    fun("8VVXJxB5nlk", getopt_long);
////    fun("rcVv5ivMhY0", lrintf);
////    fun("yhbF6MbVuYc", _Stopfx);
////    fun("iQiT26+ZGnA", _ZN10__cxxabiv116__enum_type_infoD2Ev);
////    fun("EWWEBA+Ldw8", __udivmoddi4);
////    fun("qtpzdwMMCPc", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewm);
////    fun("waexoHL0Bf4", _WStoldx);
////    fun("GcFKlTJEMkI", isupper);
////    fun("OY5mqEBxP+8",
////        _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8get_dateES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("YgZ6qvFH3QI", vwscanf_s);
////    fun("q+9E0X3aWpU", _Stoull);
////    fun("KKgUiHSYGRE", fgetln);
////    fun("BnV7WrHdPLU", _Thrd_equal);
////    fun("37CMzzbbHn8", _ZTISt13basic_filebufIwSt11char_traitsIwEE);
////    fun("7e3DrnZea-Q", _ZNSt10moneypunctIcLb1EEC2ERKSt8_Locinfomb);
////    fun("PnfhEsZ-5uk", _Atomic_fetch_and_2);
////    fun("WYWf+rJuDVU", _ZTSSt13basic_filebufIcSt11char_traitsIcEE);
////    fun("Nl6si1CfINw", _ZNSt13basic_filebufIcSt11char_traitsIcEE9underflowEv);
////    fun("5bBacGLyLOs", gmtime_s);
////    fun("lt0mLhNwjs0", _ZTISt14overflow_error);
////    fun("JBcgYuW8lPU", acos);
////    fun("WDKzMM-uuLE", _ZNSt22_Future_error_categoryD0Ev);
////    fun("Q3VBxCXhUHs", memcpy);
////    fun("KDgQWX1eDeo", _ZNSs6insertEmmc);
////    fun("fquAdjX4Sws", rand_s);
////    fun("EjL+gY1G2lk", lgamma_r);
////    fun("nk+0yTWvoRE", _ZNSo6sentryD2Ev);
////    fun("Fot-m6M2oKE", _Putstr);
////    fun("XGNIEdRyYPo", __cleanup);
////    fun("JruBeQgsAaU",
////        _ZNKSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_bRSt8ios_basewRKSbIwS2_SaIwEE);
////    fun("3n3wCJGFP7o", _ZTVSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("0ND8MZiuTR8", _ZGVZNSt13basic_filebufIcSt11char_traitsIcEE5_InitEP7__sFILENS2_7_InitflEE7_Stinit);
////    fun("XUs40umcJLQ", _ZNSt10moneypunctIwLb1EE2idE);
////    fun("6gCBQs1mIi4", _ZNSt8messagesIcEC1Em);
////    fun("jIvWFH24Bjw", _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC2Em);
////    fun("LvLuiirFk8U", __atomic_fetch_or_1);
////    fun("au+YxKwehQM", _ZTSN10__cxxabiv116__enum_type_infoE);
////    fun("qWESlyXMI3E", sceLibcMspacePosixMemalign);
////    fun("ByfjUZsWiyg", strlcat);
////    fun("Csx50UU9b18", _Tzoff);
////    fun("chlN6g6UbGo", _CurrentRuneLocale);
////    fun("j9SGTLclro8", _Sincos);
////    fun("1Pk0qZQGeWo", sscanf);
////    fun("JhVR7D4Ax6Y", _WStoul);
////    fun("k6pGNMwJB08", _Mtx_trylock);
////    fun("NdeDffcZ-30", _ZN10__cxxabiv129__pointer_to_member_type_infoD0Ev);
////    fun("ZSnX-xZBGCg", stpcpy);
////    fun("S3nFV6TR1Dw", __isnormall);
////    fun("Yy5yMiZHBIc", rintl);
////    fun("IHiK3lL7CvI", _Atthreadexit);
////    fun("CClObiVHzDY", _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED2Ev);
////    fun("Heh4KJwyoX8", posix_spawn_file_actions_addclose);
////    fun("8NVUcufbklM", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecPKv);
////    fun("T+zVxpVaaTo", _ZSt14_Cl_wcharnames);
////    fun("PWFePkVdv9w", _ZNKSt7_MpunctIwE16do_positive_signEv);
////    fun("gRw4XrztJ4Q", _rtld_thread_init);
////    fun("vakoyx9nkqo", _Genld);
////    fun("U8pDVMfBDUY", __atomic_fetch_add_4);
////    fun("KLfd8g4xp+c", __floatuntisf);
////    fun("qdlHjTa9hQ4", fdopen);
////    fun("BNbWdC9Jg+4", j1);
////    fun("wgDImKoGKCM", _ZNSt11logic_errorD2Ev);
////    fun("1NFvAuzw8dA", mbsrtowcs_s);
////    fun("G3qjOUu7KnM", flsl);
////    fun("ALcclvT4W3Q", _ZTSSt10bad_typeid);
////    fun("Zhtj6WalERg", _ZNSt14numeric_limitsIDiE6digitsE);
////    fun("wEU8oFtBXT8",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERb);
////    fun("zqASRvZg6d0", _LNan);
////    fun("NUvrURzyxJE", _Ux86_64_strerror);
////    fun("n1Y6pGR-8AU", _ZNSt8messagesIwE2idE);
////    fun("O6LEoHo2qSQ", _Atomic_fetch_and_8);
////    fun("-L+-8F0+gBc", _ZTVSt13runtime_error);
////    fun("9fs1btfLoUs", scalbnf);
////    fun("qTwVlzGoViY", _ZTVSt22_System_error_category);
////    fun("qM0gUepGWT0", _ZNSt13basic_filebufIcSt11char_traitsIcEE5imbueERKSt6locale);
////    fun("JjTc4SCuILE", _ZTSNSt6locale7_LocimpE);
////    fun("us3bDnDzd70", _FAtan);
////    fun("BxPeH9TTcs4", __cxa_current_exception_type);
////    fun("lX9M5u0e48k", _ZNSt13bad_exceptionD0Ev);
////    fun("3BbBNPjfkYI", _Ldtob);
////    fun("DZU+K1wozGI", nanf);
////    fun("CL7VJxznu6g", wctob);
////    fun("79s2tnYQI6I", wcsrtombs_s);
////    fun("coVkgLzNtaw", _ZTSSt13basic_filebufIwSt11char_traitsIwEE);
////    fun("tG8pGyxdLEs", modfl);
////    fun("mpcTgMzhUY8", floor);
////    fun("18E1gOH7cmk", __lshrdi3);
////    fun("cYNk9M+7YkY", sce_clearerr_unlocked);
////    fun("NqMgmxSA1rc", _ZNSt17bad_function_callD2Ev);
////    fun("7Ly52zaL44Q", asin);
////    fun("xRycekLYXdc", _Vacopy);
////    fun("yDdbQr1oLOc", _ZNSt8messagesIcED2Ev);
////    fun("7oio2Gs1GNk", _ZNSt13basic_filebufIcSt11char_traitsIcEE7seekoffElNSt5_IosbIiE8_SeekdirENS4_9_OpenmodeE);
////    fun("4MdGVqrsl7s", _ZNSt14numeric_limitsIlE8digits10E);
////    fun("rg5JEBlKCuo", basename);
////    fun("iUhx-JN27uI", _ZTSSt8messagesIcE);
////    fun("OR-4zyIi2aE", _ZNKSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_bRSt8ios_basecRKSs);
////    fun("koazg-62JMk", _ZGVNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE2idE);
////    fun("i2yN6xBwooo", _Getstr);
////    fun("1mecP7RgI2A", gmtime);
////    fun("BE7U+QsixQA", _ZNSt12placeholders3_19E);
////    fun("E2YhT7m79kM", _Fetch_xor_seq_cst_1);
////    fun("5BIbzIuDxTQ", _ZTISt12domain_error);
////    fun("CjzjU2nFUWw", _ZN10__cxxabiv116__enum_type_infoD0Ev);
////    fun("IUfBO5UIZNc", posix_spawnp);
////    fun("qVHpv0PxouI", mbrtowc);
////    fun("3qWXO9GTUYU", _ZNSt12system_errorD1Ev);
////    fun("e1y7KVAySrY", _FXp_getw);
////    fun("l0qC0BR1F44", __fixunsxfti);
////    fun("+P6FRGH4LfA", memmove);
////    fun("q5MWYCDfu3c", strtoimax);
////    fun("FEtOJURNey0", wprintf_s);
////    fun("KM0b6O8show", _ZNKSt5ctypeIwE8do_widenEPKcS2_Pw);
////    fun("uVOxy7dQTFc", _ZNSt10moneypunctIwLb1EED2Ev);
////    fun("8hygs6D9KBY", mbsrtowcs);
////    fun("xQtNieUQLVg", _ZNSt14numeric_limitsIdE6digitsE);
////    fun("TRn3cMU4mjY", _ZNSt10moneypunctIcLb1EEC1ERKSt8_Locinfomb);
////    fun("2GWRrgT8o20", _ZTISt10ctype_base);
////    fun("IPjwywTNR8w", _LXp_sqrtx);
////    fun("x5yAFCJRz8I", _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC2Em);
////    fun("DWMcG8yogkY", _ZSt8_XLgammaf);
////    fun("8G2LB+A3rzg", atexit);
////    fun("fjnxuk9ucsE", _ZNSt12placeholders2_4E);
////    fun("ibs6jIR0Bw0", __floatuntidf);
////    fun("7yMFgcS8EPA", _Cnd_destroy);
////    fun("mbGmSOLXgN0", _ZNSt10moneypunctIcLb0EED2Ev);
////    fun("6sJWiWSRuqk", strncpy);
////    fun("n0kT+8Eeizs", iswdigit);
////    fun("QdPk9cbJrOY", _ZTSPKl);
////    fun("sETNbyWsEHs", _Unwind_GetIP);
////    fun("henpJH+XpjE", _ULx86_64_get_proc_info_by_ip);
////    fun("+KSnjvZ0NMc", srand48);
////    fun("uUDq10y4Raw", _ZNKSt7_MpunctIcE13do_neg_formatEv);
////    fun("iy1lPjADRUs", _ZTSSt14overflow_error);
////    fun("wRs5S54zjm0", llroundl);
////    fun("GS1AvxBwVgY", _ZNSt10moneypunctIwLb1EEC2EPKcm);
////    fun("F27xQUBtNdU", _ZNKSt8ios_base7failure8_DoraiseEv);
////    fun("ea-rVHyM3es", _Atomic_load_8);
////    fun("zkUC74aJxpE", _ZNKSt7_MpunctIwE16do_negative_signEv);
////    fun("txJTOe0Db6M", logbl);
////    fun("Ls4tzzhimqQ", strcat);
////    fun("KUW22XiVxvQ", _ZTIPt);
////    fun("Y16fu+FC+3Q", sys_signame);
////    fun("0UPU3kvxWb0", _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC1ERKSt8_Locinfom);
////    fun("KJutwrVUFUo", _ZTSSt16invalid_argument);
////    fun("-u3XfqNumMU", _readdir_unlocked);
////    fun("FM5NPnLqBc8", wcscpy);
////    fun("4rrOHCHAV1w", _ZSt7setbasei);
////    fun("ww3UUl317Ng", _ZNKSt22_System_error_category23default_error_conditionEi);
////    fun("Ud8CbISKRGM", posix_spawn_file_actions_destroy);
////    fun("d57FDzON1h0", _ZNKSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE8_PutmfldES3_bRSt8ios_basecbSsc);
////    fun("fGPRa6T+Cu8", __isnormal);
////    fun("McsGnqV6yRE", _ZdlPvRKSt9nothrow_t);
////    fun("MwAySqTo+-M", _ZNKSt12codecvt_base11do_encodingEv);
////    fun("lbB+UlZqVG0", fread);
////    fun("Ok+SYcoL19Y", gets);
////    fun("wAcnCK2HCeI", _ZNSt13basic_filebufIwSt11char_traitsIwEE6setbufEPwi);
////    fun("kAay0hfgDJs", _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED1Ev);
////    fun("8ArIPXBlkgM", _ZGVNSt8numpunctIwE2idE);
////    fun("K+v+cnmGoH4", wcsnlen_s);
////    fun("iH+oMJn8YPk", vwarnx);
////    fun("6aEXAPYpaEA", _CWcsxfrm);
////    fun("6XcQYYO2YMY", _ZTSPs);
////    fun("ibyFt0bPyTk", _Tss_set);
////    fun("GhTZtaodo7o", vfprintf_s);
////    fun("K8CzKJ7h1-8", _ZTSSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("Ti86LmOKvr0", _ZNSbIwSt11char_traitsIwESaIwEE5_CopyEmm);
////    fun("khbdMADH4cQ", _ZNSt9bad_allocD1Ev);
////    fun("fzgkSILqRHE", _WScanf);
////    fun("nufufTB4jcI", __divsf3);
////    fun("A0PftWMfrhk",
////        _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE11get_weekdayES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("0xgFRKf0Lc4", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecPKv);
////    fun("RIkUZRadZgc", sqrtl);
////    fun("Z+vcX3rnECg", _ZTVSt20bad_array_new_length);
////    fun("dHw0YAjyIV4", _ZTIN10__cxxabiv123__fundamental_type_infoE);
////    fun("1uJgoVq3bQU", _Getptolower);
////    fun("byV+FWlAnB4", _ZTVN10__cxxabiv117__class_type_infoE);
////    fun("lFKA8eMU5PA", _ZTSPKDi);
////    fun("xSxPHmpcNzY", _ZNSs6assignEPKcm);
////    fun("zogPrkd46DY", strxfrm);
////    fun("vyqQpWI+O48", _ZNSt14numeric_limitsItE9is_signedE);
////    fun("NApYynEzlco", _Dnorm);
////    fun("N84qS6rJuGI", _ZTSPe);
////    fun("fJmOr59K8QY", _LRint);
////    fun("L71JAnoQees", _ZNSt7collateIcEC2ERKSt8_Locinfom);
////    fun("y9DXbdhSQ-4", __libunwind_Unwind_GetBSP);
////    fun("+WLgzxv5xYA", __sync_fetch_and_add_16);
////    fun("2Lvc7KWtErs", __kernel_sindf);
////    fun("NFxDIuqfmgw", scalblnl);
////    fun("3aZN32UTqqk", _ZTISt22_Future_error_category);
////    fun("y8PXwxTZ9Hc", _ZNSt14_Num_ldbl_base10has_denormE);
////    fun("3qQmz11yFaA", __fixsfsi);
////    fun("p3BVovMPEE4", _Unwind_GetRegionStart);
////    fun("2PoQu-K2qXk", _ZNSt7collateIwEC2ERKSt8_Locinfom);
////    fun("oDoV9tyHTbA", vswprintf_s);
////    fun("cyy-9ntjWT8", _ZNSt12system_errorD0Ev);
////    fun("YXVCU6PdgZk", _ZNSt8_LocinfoD2Ev);
////    fun("1D0H2KNjshE", powf);
////    fun("9XL3Tlgx6lc", _ZTSSt7codecvtIDsc9_MbstatetE);
////    fun("TzxDRcns9e4", _Rint);
////    fun("cfpRP3h9F6o", _FPlsw);
////    fun("QXJCcrXoqpU", _ZNSt8ios_base5clearENSt5_IosbIiE8_IostateEb);
////    fun("Zs8Xq-ce3rY", _Xtime_to_ts);
////    fun("2WE3BTYVwKM", cos);
////    fun("BamOsNbUcn4", _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE2idE);
////    fun("hbU5HOTy1HM", _ZTISt7codecvtIwc9_MbstatetE);
////    fun("1hGmBh83dL8", _Plsw);
////    fun("Ozk+Z6QnlTY", _ZSt7_MP_RemPyy);
////    fun("ltRLAWAeSaM", _ZTIDh);
////    fun("Mo6K-pUyNhI", _ZNKSt7codecvtIDsc9_MbstatetE11do_encodingEv);
////    fun("j7e7EQBD6ZA", _ZNSt6_MutexD2Ev);
////    fun("GGoH7e6SZSY", _ZNKSt8messagesIwE5closeEi);
////    fun("c33GAGjd7Is", _ZTISt17bad_function_call);
////    fun("QklPP5q5JUY", backtrace);
////    fun("Cs3DBACRSY8", _ZTISt7codecvtIcc9_MbstatetE);
////    fun("j9LU8GsuEGw", _ZNKSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecPK2tmPKcSB_);
////    fun("Fm-dmyywH9Q", fileno);
////    fun("88EyUEoBX-E", _ZNSt6chrono12system_clock9is_steadyE);
////    fun("b4aGU8znnCo", _ULx86_64_resume);
////    fun("ZjtRqSMJwdw", sinh);
////    fun("p4vYrlsVpDE", __umodsi3);
////    fun("n+NFkoa0VD0", _ZNSt14numeric_limitsIfE14min_exponent10E);
////    fun("h1Eewgzowes", _ZTIN10__cxxabiv116__enum_type_infoE);
////    fun("N2OjwJJGjeQ", freeifaddrs);
////    fun("oXgaqGVnW5o", erf);
////    fun("vqtytrxgLMs", iswspace);
////    fun("kiZSXIWd9vg", strcpy);
////    fun("CbRhl+RoYEM", _LQuadph);
////    fun("VenLJSDuDXY", _ZTSs);
////    fun("MELi-cKqWq0", _ZSt19_Xbad_function_callv);
////    fun("bl0DPP6kFBk", _ZSt8_XLgammae);
////    fun("DaYYQBc+SY8", _ZTISt7_MpunctIwE);
////    fun("dJvVWc2jOP4", __floattixf);
////    fun("5qtcuXWt5Xc", _Randseed);
////    fun("zb3436295XU", _ZNSt9type_infoD2Ev);
////    fun("4Wt5uzHO98o", _Dunscale);
////    fun("J-0I2PtiZc4", _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE9_GetffldxEPcRS3_S6_RSt8ios_basePi);
////    fun("iH4YAIRcecA", acosl);
////    fun("GvYZax3i-Qk", _ZNKSbIwSt11char_traitsIwESaIwEE5_XranEv);
////    fun("XwRd4IpNEss", _FRteps);
////    fun("XK2R46yx0jc", coshl);
////    fun("H4MCONF+Gps", wcspbrk);
////    fun("q5WzucyVSkM", rintf);
////    fun("qXkZo1LGnfk", _Atomic_compare_exchange_strong_2);
////    fun("RC3VBr2l94o", __floatunsisf);
////    fun("S6LHwvK4h8c", scalblnf);
////    fun("JYZigPvvB6c", _ZNSt14numeric_limitsIeE8digits10E);
////    fun("Z9gbzf7fkMU", _Atomic_fetch_xor_1);
////    fun("z7STeF6abuU", _Mtxinit);
////    fun("YQ0navp+YIc", puts);
////    fun("94ZLp2+AOq0",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERm);
////    fun("XghI4vmw8mU", _ZNSt8messagesIwE7_GetcatEPPKNSt6locale5facetEPKS1_);
////    fun("OM0FnA7Tldk", _ZNSt10moneypunctIwLb1EED1Ev);
////    fun("aZK8lNei-Qw", fputc);
////    fun("PJW+-O4pj6I", _thread_init);
////    fun("jJC7x18ge8k", hypotl);
////    fun("UM6rGQxnEMg", _ZNKSt8messagesIwE6do_getEiiiRKSbIwSt11char_traitsIwESaIwEE);
////    fun("xGT4Mc55ViQ", _Fofind);
////    fun("AUYdq63RG3U", sceLibcPafMspaceTrim);
////    fun("1uf1SQsj5go", towupper);
////    fun("qcaIknDQLwE", _ZTIN6Dinkum7threads21thread_resource_errorE);
////    fun("OypvNf3Uq3c", _ZNSt14overflow_errorD2Ev);
////    fun("TgEb5a+nOnk", _ZNSbIwSt11char_traitsIwESaIwEE5eraseEmm);
////    fun("E4wZaG1zSFc", dirname);
////    fun("7XEv6NnznWw", scanf);
////    fun("W0j6vCxh9Pc", _ZSt16_Throw_Cpp_errori);
////    fun("aSNAf0kxC+Y", __atomic_fetch_or_2);
////    fun("m7iLTaO9RMs", cosh);
////    fun("uVRcM2yFdP4", fminf);
////    fun("9VeY8wiqf8M", atan2l);
////    fun("0Ir3jiT4V6Q", _ZNSs5eraseEmm);
////    fun("+9ypoH8eJko", _Times);
////    fun("j97CjKJNtQI", _ZTIs);
////    fun("St9nbxSoezk", clearerr);
////    fun("b7J3q7-UABY", tgamma);
////    fun("Zm2LLWgxWu8", _Xp_movx);
////    fun("jbj2wBoiCyg", snwprintf_s);
////    fun("VET8UnnaQKo", _ZNSt13_Num_int_base8is_exactE);
////    fun("06QaR1Cpn-k", floorl);
////    fun("O7GtbAGB85A", __libunwind_Unwind_GetDataRelBase);
////    fun("LAEVU8cBSh4", _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC1Em);
////    fun("Hf8hPlDoVsw", __muldi3);
////    fun("SAd0Z3wKwLA", tanhf);
////    fun("hbY3mFi8XY0", _Tls_setup__Costate);
////    fun("9rMML086SEE", _ZNSt6locale5_InitEv);
////    fun("a6CYO8YOzfw", fwscanf);
////    fun("aC9OWBGjvxA", _ZNSt13basic_filebufIwSt11char_traitsIwEED2Ev);
////    fun("vIJPARS8imE", _ZN10__cxxabiv117__pbase_type_infoD1Ev);
////    fun("pPdYDr1KDsI", __atomic_fetch_xor_4);
////    fun("SD403oMc1pQ", _ZNSt8messagesIcEC1ERKSt8_Locinfom);
////    fun("GYTma8zq0NU", _ZNSt13basic_filebufIwSt11char_traitsIwEE5_LockEv);
////    fun("dsQ5Xwhl9no", _ZN10__cxxabiv120__function_type_infoD1Ev);
////    fun("RY8mQlhg7mI", __cxa_current_primary_exception);
////    fun("r8003V6UwZg",
////        _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE7_GetfmtES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmPKc);
////    fun("tTDqwhYbUUo", _Makeloc);
////    fun("1QcrrL9UDRQ", iswupper);
////    fun("1gYJIrfHxkQ", _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC2Em);
////    fun("8PIh8BFpNYQ", _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE13do_date_orderEv);
////    fun("PEU-SAfo5+0", _LDtento);
////    fun("OWZ3ZLkgye8", __nesf2);
////    fun("+RuThw5axA4",
////        _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmcc);
////    fun("2TYdayAO39E", _ZNSt7codecvtIcc9_MbstatetED0Ev);
////    fun("QJJ-4Dgm8YQ", _ZTv0_n24_NSoD0Ev);
////    fun("NdvAi34vV3g", freopen_s);
////    fun("dO7-MxIPfsw", _ZTISt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("HQAa3rCj8ho", _ZGVNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE2idE);
////    fun("ixWEOmOBavk", _Fetch_or_8);
////    fun("DDQjbwNC31E", _ZNSt8messagesIwEC2ERKSt8_Locinfom);
////    fun("2pJJ0dl-aPQ",
////        _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE11do_get_timeES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("4iFgTDd9NFs", _LLog);
////    fun("ufqiLmjiBeM", __atomic_load_2);
////    fun("w3gRFGRjpZ4", _WStold);
////    fun("tfNbpqL3D0M", vsscanf_s);
////    fun("MQJQCxbLfM0", _ZNSt8numpunctIwEC2ERKSt8_Locinfomb);
////    fun("FuxaUZsWTok", fesetexceptflag);
////    fun("y4WlO8qzHqI", fedisableexcept);
////    fun("-mUC21i8WBQ", __atomic_fetch_sub_4);
////    fun("dP5zwQ2Yc8g", _ZNSt7collateIcED2Ev);
////    fun("-Oet9AHzwtU", timezone);
////    fun("ongs2C6YZgA", _ZNSt13_Num_int_base5radixE);
////    fun("O2wxIdbMcMQ", _ZGVZNSt13basic_filebufIwSt11char_traitsIwEE5_InitEP7__sFILENS2_7_InitflEE7_Stinit);
////    fun("ozMAr28BwSY", _ZSt14_Xout_of_rangePKc);
////    fun("xd7O9oMO+nI", _ZNSt14_Num_ldbl_base13has_quiet_NaNE);
////    fun("0AeC+qCELEA", _ZNSt7_MpunctIcED1Ev);
////    fun("Bq8m04PN1zw", _ZTVSt12system_error);
////    fun("ycMCyFmWJnU", _Xp_addx);
////    fun("NuydofHcR1w", sce_feof_unlocked);
////    fun("hGlkh5YpcKw",
////        _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE14do_get_weekdayES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("sqWytnhYdEg", _ZNSt8ios_base4InitC1Ev);
////    fun("DDnr3lDwW8I", _ZNKSt7codecvtIDic9_MbstatetE11do_encodingEv);
////    fun("-9SIhUr4Iuo", _Mbtowcx);
////    fun("QuZzFJD5Hrw", sceLibcPafMspaceMalloc);
////    fun("Pcq7UoYAcFE", sceLibcPafMspaceCheckMemoryBounds);
////    fun("Fg4w+h9wAMA", _ZN10__cxxabiv120__si_class_type_infoD0Ev);
////    fun("aOK5ucXO-5g", _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED0Ev);
////    fun("dP14OHWe4nI", _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE13do_date_orderEv);
////    fun("vEaqE-7IZYc", _Cnd_wait);
////    fun("8ngzWNZzFJU", wcstok);
////    fun("CVcmmf8VL40", _ZTISt9time_base);
////    fun("iLSavTYoxx0", _ZTISo);
////    fun("QS7CASjt4FU", _ZNSt12length_errorD0Ev);
////    fun("tsiBm2NZQfo", _ZNSt14_Num_ldbl_base9is_iec559E);
////    fun("TsrS8nGDQok", siginterrupt);
////    fun("VsP3daJgmVA", _Cnd_broadcast);
////    fun("kALvdgEv5ME", _Locksyslock);
////    fun("YBrp9BlADaA", _ZNSt23_Generic_error_categoryD0Ev);
////    fun("3CtP20nk8fs", _ZNSt14_Error_objectsIiE14_System_objectE);
////    fun("saNCRNfjeeg", __fixdfsi);
////    fun("6n23e0gIJ9s", nearbyintl);
////    fun("cnNz2ftNwEU", _ZNKSt8numpunctIcE11do_groupingEv);
////    fun("ryyn6-WJm6U", nexttowardl);
////    fun("CVoT4wFYleE", __fe_dfl_env);
////    fun("UWyL6KoR96U", _ZSt13_Xregex_errorNSt15regex_constants10error_typeE);
////    fun("0G36SAiYUhQ", _ZTIPKd);
////    fun("HQbgeUdQyyw", _ZNSt7collateIwEC2Em);
////    fun("H8ya2H00jbI", sin);
////    fun("FihG2CudUNs", err_set_file);
////    fun("3o0PDVnn1qA", _ZTIPd);
////    fun("B73zpTXiw2s", _ULx86_64_get_reg);
////    fun("A4zXSpJuqvk", _ULx86_64_set_caching_policy);
////    fun("Rqu9OmkKY+M", _ZNKSt7_MpunctIcE16do_negative_signEv);
////    fun("wDmL2EH0CBs", iswalnum);
////    fun("kImHEIWZ58Q", _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE2idE);
////    fun("aINUE2xbhZ4", _Poly);
////    fun("h9C+J68WriE", _ZTISt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("AqUBdZqHZi4", warnc);
////    fun("aotaAaQK6yc", _ZSt15system_categoryv);
////    fun("0FnzR6qum90", sceLibcPafMspaceReportMemoryBlocks);
////    fun("hb77EGA39U4", _ULx86_64_get_proc_name);
////    fun("zvl6nrvd0sE", _Dsign);
////    fun("jerxcj2Xnbg", _ZNSt13basic_filebufIcSt11char_traitsIcEE9pbackfailEi);
////    fun("SdXFaufpLIs", _ZNSt8numpunctIwE7_GetcatEPPKNSt6locale5facetEPKS1_);
////    fun("5r801ZWiJJI", _ZNSt6locale7_Locimp8_MakelocERKSt8_LocinfoiPS0_PKS_);
////    fun("ZT4ODD2Ts9o", Need_sceLibcInternal);
////    fun("rnxaQ+2hSMI", _Putfld);
////    fun("bZ+lKHGvOr8",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERt);
////    fun("lhpd6Wk6ccs", log10f);
////    fun("gsqXCd6skKs", _Thrd_start_with_attr);
////    fun("M+AMxjxwWlA", _ZNSt14numeric_limitsIcE9is_signedE);
////    fun("gjLRFhKCMNE", _ZTIN10__cxxabiv121__vmi_class_type_infoE);
////    fun("u4FNPlIIAtw", _Errno);
////    fun("cj+ge8YLU7s", _ZTSPDn);
////    fun("OAE3YU396YQ", _FLogpoly);
////    fun("-3XZEzt2lqM", _Ux86_64_setcontext);
////    fun("YXQ4gXamCrY", feupdateenv);
////    fun("OCbJ96N1utA", _Thrd_lt);
////    fun("sb2vivqtLS0", _ZNKSt24_Iostream_error_category4nameEv);
////    fun("2q5PPh7HsKE", isinf);
////    fun("uROsAczW6OU", _ZNSt10moneypunctIwLb1EEC1ERKSt8_Locinfomb);
////    fun("34nH7v2xvNQ", wcstoll);
////    fun("iWNC2tkDgxw", _ZNSt13bad_exceptionD2Ev);
////    fun("NjJfVHJL2Gg", _ZNSt12length_errorD2Ev);
////    fun("46haDPRVtPo", _ZTSSi);
////    fun("W0w8TGzAu0Q", _ZNSt8messagesIcEC1EPKcm);
////    fun("zYHryd8vd0w", _ZNSt7codecvtIDsc9_MbstatetED0Ev);
////    fun("7dlAxeH-htg", _Stofx);
////    fun("qkl3Siab04M", _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED0Ev);
////    fun("GipcbdDM5cI", makecontext);
////    fun("b1LciG4lUUk", _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC2ERKSt8_Locinfom);
////    fun("O7zxyNnSDDA", _LDenorm);
////    fun("m-fSo3EbxNA", _ZdaPvRKSt9nothrow_t);
////    fun("yvhjR7PTRgc", __atomic_fetch_sub_2);
////    fun("pCWh67X1PHU", _Mbcurmax);
////    fun("R8xUpEJwAA8", _ZNSt14numeric_limitsIfE14max_exponent10E);
////    fun("DlDsyX+XsoA", _ZNSt8messagesIwEC2EPKcm);
////    fun("bSgY14j4Ov4", _Wctomb);
////    fun("SYCwZXKZQ08",
////        _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE11do_get_dateES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("2sWzhYqFH4E", _Stdout);
////    fun("OwcpepSk5lg", _ZNSt14numeric_limitsIxE8digits10E);
////    //fun("2X5agFjKxMc", calloc);
////    fun("xvRvFtnUk3E", _ZNKSt9bad_alloc4whatEv);
////    fun("oi3kpQPqpMY", _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC1ERKSt8_Locinfom);
////    fun("szUft0jERdo", _Tan);
////    fun("dWz3HtMMpPg", _WGetint);
////    fun("RYwqCx0hU5c", _Tls_setup__Wctrans);
////    fun("bIfFaqUwy3I", _Xp_setw);
////    fun("pKwslsMUmSk", fmod);
////    fun("E-XEmpL9i1A", __atomic_fetch_and_4);
////    fun("0MzJAexrlr4",
////        _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmcc);
////    fun("gYFKAMoNEfo", tzset);
////    fun("FnEnECMJGag", _ZTVSt8messagesIcE);
////    fun("IHq2IaY4UGg", __fixsfti);
////    fun("8Ce6O0B-KpA", _ZTSPKe);
////    fun("eoW60zcLT8Q", _ZNKSt7codecvtIDic9_MbstatetE6do_outERS0_PKDiS4_RS4_PcS6_RS6_);
////    fun("sX3o6Zmihw0", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewd);
////    fun("qjBlw2cVMAM", vasprintf);
////    fun("lzK3uL1rWJY", _ZNSt8numpunctIwED1Ev);
////    fun("V7aIsVIsIIA", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewd);
////    fun("5G2ONUzRgjY", memrchr);
////    fun("6f5f-qx4ucA", wcscpy_s);
////    fun("FtPFMdiURuM", _ZTISt16nested_exception);
////    fun("Nx-F5v0-qU8", asinl);
////    fun("iCoD0EOIbTM", _ZNSt7_MpunctIwE5_InitERKSt8_Locinfob);
////    fun("g3ShSirD50I", wcsrchr);
////    fun("aoTkxU86Mr4", verr);
////    fun("qKQiNX91IGo", __cxa_rethrow_primary_exception);
////    fun("KNNNbyRieqQ", _new_setup);
////    fun("APrAh-3-ICg", _ZTVSt10moneypunctIwLb1EE);
////    fun("Dc4ZMWmPMl8", _ZNSt22_System_error_categoryD0Ev);
////    fun("eS+MVq+Lltw", remainderf);
////    fun("ocyIiJnJW24", __nedf2);
////    fun("dST+OsSWbno", _FErf_one);
////    fun("S5WbPO54nD0",
////        _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmPKcSE_);
////    fun("Y9C9GeKyZ3A", _ZTVSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("fvgYbBEhXnc", _ZTISt7collateIcE);
////    fun("9TfGnN6xq-U",
////        _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8get_timeES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("I1Ru2fZJDoE", _ZTSN10__cxxabiv121__vmi_class_type_infoE);
////    fun("-+RKa3As0gE", _ZNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC2Em);
////    fun("-431A-YBAks", llrint);
////    fun("Y5DhuDKGlnQ", log2);
////    fun("xfOSCbCiY44", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewPKv);
////    fun("AEJdIVZTEmo", qsort);
////    fun("s67G-KeDKOo", vwarn);
////    fun("YGPopdkhujM", _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC1Em);
////    fun("jjjRS7l1MPM", _FLog);
////    fun("OJjm-QOIHlI", sceLibcMspaceMalloc);
////    fun("-1G1iE3KyGI", _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED0Ev);
////    fun("RBPhCcRhyGI", _Atomic_is_lock_free_1);
////    fun("it6DDrqKGvo", _ZNSt12system_errorD2Ev);
////    fun("jlNI3SSF41o", _ZTVSt7codecvtIDsc9_MbstatetE);
////    fun("ay3uROQAc5A", opendir);
////    fun("6BYT26CFh58", _ZTSN10__cxxabiv117__array_type_infoE);
////    fun("LrXu7E+GLDY", _FZero);
////    fun("qMp2fTDCyMo", expl);
////    fun("CHrhwd8QSBs", _ZNSt6thread20hardware_concurrencyEv);
////    fun("exNzzCAQuWM", _Thrd_yield);
////    fun("WV94zKqwgxY", _ZTIPKb);
////    fun("5OqszGpy7Mg", strtoull);
////    fun("h+iBEkE50Zs", _ZTSSt10moneypunctIcLb0EE);
////    fun("bF2uVCqVhBY", _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE7_GetcatEPPKNSt6locale5facetEPKS5_);
////    fun("6aFwTNpqTP8", _ZNKSt7_MpunctIcE16do_thousands_sepEv);
////    fun("77qd0ksTwdI", log1pl);
////    fun("p1WMhxL4Wds", _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE7_GetcatEPPKNSt6locale5facetEPKS5_);
////    fun("p07Yvdjjoo4", _ZTSPa);
////    fun("n2-b3O8qvqk", _Thrd_current);
////    fun("TnublTBYXTI", _LXp_getw);
////    fun("EYZJsnX58DE", _ZNSt13basic_ostreamIwSt11char_traitsIwEED1Ev);
////    fun("2+6K-2tWaok", _Fetch_or_seq_cst_1);
////    fun("RNmYVYlZvv8", _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC2Em);
////    fun("WuMbPBKN1TU", log10);
////    fun("NcZqFTG-RBs", __fixunssfsi);
////    fun("Oeo9tUbzW7s", _ZNSt7codecvtIDsc9_MbstatetED1Ev);
////    fun("5FoE+V3Aj0c", _rtld_addr_phdr);
////    fun("Z7+siBC690w", _ZTSDs);
////    fun("NeiFvKblpZM", _ZNSt13basic_filebufIwSt11char_traitsIwEE9pbackfailEi);
////    fun("-EgDt569OVo", _Atomic_exchange_4);
////    fun("7Psx1DlAyE4", getprogname);
////    fun("bU3S1OS1sc0", _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC2ERKSt8_Locinfom);
////    fun("ac102y6Rjjc", _Hugeval);
////    fun("XrXTtRA7U08", _LRecip);
////    fun("agAYSUes238", _ZGVNSt7codecvtIcc9_MbstatetE2idE);
////    fun("DYivN1nO-JQ", getcwd);
////    fun("J0kng1yac3M", readdir_r);
////    fun("rBbtKToRRq4", _Atomic_compare_exchange_weak_1);
////    fun("PG4HVe4X+Oc", _LErfc);
////    fun("7-a7sBHeUQ8", wcstod);
////    fun("8o+oBXdeQPk", _ZGVNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE2idE);
////    fun("ijc7yCXzXsc", _Xp_addh);
////    fun("c5-Jw-LTekM", _ZTIf);
////    fun("SYFNsz9K2rs", _ZNSt13basic_filebufIcSt11char_traitsIcEE6setbufEPci);
////    fun("mdcx6KcRIkE", _ZNSt14numeric_limitsIwE8digits10E);
////    fun("oly3wSwEJ2A", _ZNSt12system_errorC2ESt10error_codePKc);
////    fun("YVacrIa4L0c", _ZNSt14numeric_limitsIwE9is_signedE);
////    fun("dmUgzUX3nXU", _thread_autoinit_dummy_decl_stub);
////    fun("vYWK2Pz8vGE", _ZSt7_FiopenPKcNSt5_IosbIiE9_OpenmodeEi);
////    fun("rRmMX83UL1E", _ZNKSt8messagesIcE8do_closeEi);
////    fun("1vo6qqqa9F4", _ZdlPvS_);
////    fun("5ZKavcBG7eM", __atomic_fetch_or_8);
////    fun("YUdPel2w8as", _Tls_setup__WCostate);
////    fun("dSBshTZ8JcA", _ZTIN10__cxxabiv117__pbase_type_infoE);
////    fun("9HILqEoh24E", _ZNSs5_CopyEmm);
////    fun("aaDMGGkXFxo", frexpf);
////    fun("kVvGL9aF5gg", _ZN10__cxxabiv120__function_type_infoD0Ev);
////    fun("3CAYAjL-BLs", __adddf3);
////    fun("eWSt5lscApo", yn);
////    fun("ZrFcJ-Ab0vw", _ZTVSt15underflow_error);
////    fun("SoNnx4Ejxw8", _FDtento);
////    fun("Uea1kfRJ7Oc", _ZTSSt15underflow_error);
////    fun("9BI3oYkCTCU", _ZNSt7codecvtIwc9_MbstatetED0Ev);
////    fun("jQuruQuMlyo", _Fetch_and_seq_cst_4);
////    fun("fL3O02ypZFE", wmemcpy);
////    fun("NVadfnzQhHQ", exp);
////    fun("T8lH8xXEwIw", _Atomic_fetch_sub_8);
////    fun("rDMyAf1Jhug", __isinff);
////    fun("B59+zQQCcbU", memmove_s);
////    fun("P9XP5U7AfXs", _ZNSt14numeric_limitsIjE9is_signedE);
////    fun("RlB3+37KJaE", _ZNSt12placeholders2_9E);
////    fun("vCLVhOcdQMY", _ZN10__cxxabiv119__pointer_type_infoD2Ev);
////    fun("ayTeobcoGj8", statvfs);
////    fun("uX0nKsUo8gc", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_RSt8ios_basewy);
////    fun("AoLA2MRWJvc", getw);
////    fun("L29QQz-6+X8", _ZNSt14numeric_limitsIfE12max_exponentE);
////    fun("sBBuXmJ5Kjk", inet_addr);
////    fun("XbEM58kMYFc", _ZNK10__cxxabiv117__class_type_info7cast_toEPvPKS0_);
////    fun("MU25eqxSDTw", _Sinh);
////    fun("qiloU7D8MBM", _ZTSSt15basic_streambufIwSt11char_traitsIwEE);
////    fun("eQhBFnTOp40", llroundf);
////    fun("y8j-jP6bHW4", __subvsi3);
////    fun("a2MOZf++Wjg", srandomdev);
////    fun("VOKOgR7L-2Y", lrint);
////    fun("MVPtIf3MtL8", __floatdixf);
////    fun("JG1MkIFKnT8", _FDenorm);
////    fun("raLgIUi3xmk", _ZNSt7collateIcEC1ERKSt8_Locinfom);
////    fun("22g2xONdXV4", _ZTSSt9bad_alloc);
////    fun("r-Wr8J0lqlI", _ZNK10__cxxabiv121__vmi_class_type_info7cast_toEPvPKNS_17__class_type_infoE);
////    fun("21KFhEQDJ3s", _malloc_fini);
////    fun("btueF8F0fQE", _ZNSt14numeric_limitsIaE8digits10E);
////    fun("WXOoCK+kqwY", _ZNKSt5ctypeIcE10do_tolowerEc);
////    fun("VmqsS6auJzo", _ZNSt5ctypeIwE2idE);
////    fun("cOYia2dE0Ik", asprintf);
////    fun("3cUUypQzMiI", __cxa_begin_catch);
////    fun("Jcu0Wl1-XbE", _Getdst);
////    fun("L7Vnk06zC2c", _ZSt11setiosflagsNSt5_IosbIiE9_FmtflagsE);
////    fun("E4SYYdwWV28", _Eps);
////    fun("Nmtr628eA3A", _ZSt14_Random_devicev);
////    fun("XhwSbwsBdx0", _ZNSt7codecvtIcc9_MbstatetEC1Em);
////    fun("lOF5jrFNZUA", _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC2Em);
////    fun("RBcs3uut1TA", strerror_r);
////    fun("xENtRue8dpI", strtof);
////    fun("xy0MR+OOZI8",
////        _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE13get_monthnameES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("vZkmJmvqueY", _Lockfilelock);
////    fun("IRUFZywbTT0", _LHypot);
////    fun("gOxGOQmSVU0", _ZNSt13basic_filebufIcSt11char_traitsIcEED2Ev);
////    fun("tQNolUV1q5A", swscanf_s);
////    fun("gjbmYpP-XJQ", strcoll);
////    fun("CRoMIoZkYhU", _ZTVSt4_Pad);
////    fun("GLxD5v2uMHw", _ZN10__cxxabiv117__pbase_type_infoD0Ev);
////    fun("qOD-ksTkE08", _ZTISt8bad_cast);
////    fun("jp2e+RSrcow", lrintl);
////    fun("SreZybSRWpU", _Cnd_init);
////    fun("VfsML+n9cDM", log1p);
////    fun("LDbKkgI-TZg", _ZNKSt8numpunctIwE9falsenameEv);
////    fun("X1C-YhubpGY", _ZNSt12placeholders2_3E);
////    fun("8u8lPzUEq+U", memchr);
////    fun("hdm0YfMa7TQ", _Znam);
////    fun("X69UlAXF-Oc", _ZNSt10moneypunctIwLb1EEC1Em);
////    fun("aD-iqbVlHmQ", _ZNSt6locale7_LocimpD1Ev);
////    fun("y0hzUSFrkng", _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC1ERKSt8_Locinfom);
////    fun("-jpk31lZR6E", _ZNSt14numeric_limitsIjE6digitsE);
////    fun("4qHwSTPt-t8", _ZNSt7codecvtIcc9_MbstatetEC2ERKSt8_Locinfom);
////    fun("mdYczJb+bb0", _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC1ERKSt8_Locinfom);
////    fun("liR+QkhejDk", _ZTIPKDn);
////    fun("iWMhoHS8gqk", _ZTSPKt);
////    fun("u5zp3yXW5wA", _ZTSSt14error_category);
////    fun("s2zG12AYKTg", _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED0Ev);
////    fun("irGo1yaJ-vM", _ZNSt7collateIwE2idE);
////    fun("BgZcGDh7o9g", _ZNSt9exceptionD1Ev);
////    fun("wKsLxRq5+fg", _Mbstate);
////    fun("7Jp3g-qTgZw", scalbln);
////    fun("U54NBtdj6UY", _ZNSt6_Mutex5_LockEv);
////    fun("b5JSEuAHuDo", _ZTIl);
////    fun("EMNG6cHitlQ", _ZTVSt9bad_alloc);
////    fun("2G1UznxkcgU", _ZGVNSt14_Error_objectsIiE14_System_objectE);
////    fun("6ddOFPDvuCo", _ZTISt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("cCXMypoz4Vs", _ZNSt12out_of_rangeD2Ev);
////    fun("LEvm25Sxi7I", btowc);
////    fun("VYQwFs4CC4Y", _Mtx_current_owns);
////    fun("+Uuj++A+I14", _ZTv0_n24_NSiD1Ev);
////    fun("BuP7bDPGEcQ", _LXp_addx);
////    fun("K49mqeyzLSk", _Atomic_fetch_or_1);
////    fun("MpiTv3MErEQ", _ZTIj);
////    fun("xdHqQoggdfo", _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC1Em);
////    fun("M2bGWSqt764", vfwprintf);
////    fun("H0aqk25W6BI", _ZTISt10bad_typeid);
////    fun("WkkeywLJcgU", wcslen);
////    fun("Lzj4ws7DlhQ", _ZNSt7_MpunctIcED0Ev);
////    fun("nftirmo6hBg", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecb);
////    fun("YxwfcCH5Q0I", _ZSt16generic_categoryv);
////    fun("pN02FS5SPgg", _ZNSt8_LocinfoC2EPKc);
////    fun("Uq5K8tl8I9U", _ZNSt6locale7classicEv);
////    fun("H+T2VJ91dds", _ZNSt8numpunctIwEC1EPKcmb);
////    fun("h64u7Gu3-TM", _ZTSPKw);
////    fun("Gbr7tQZ4A1A", __libunwind_Unwind_Resume);
////    fun("s1EM2NdPf0Y", _ZNSt8numpunctIwEC1ERKSt8_Locinfomb);
////    fun("YYG-8VURgXA", _Tls_setup__Mbstate);
////    fun("TVfbf1sXt0A", _ZSt4cerr);
////    fun("dsXnVxORFdc", wcstok_s);
////    fun("tiOgRYEeYgw", _ULx86_64_local_addr_space);
////    fun("w9fCz0pbHdw",
////        _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE7_GetcatEPPKNSt6locale5facetEPKS5_);
////    fun("LSp+r7-JWwc", _FDclass);
////    fun("eVFYZnYNDo0", _ZNSt7codecvtIcc9_MbstatetE2idE);
////    fun("bzbQ5zQ2Y3g", fgetwc);
////    fun("R5nRbLQT3oI", _ZN10__cxxabiv117__array_type_infoD0Ev);
////    fun("4ZXlZy7iRWI", _ZTSN10__cxxabiv120__function_type_infoE);
////    fun("Sopthb9ztZI", _malloc_thread_cleanup);
////    fun("-B76wP6IeVA", scanf_s);
////    fun("3cW6MrkCKt0", _ZNSt10moneypunctIcLb0EED1Ev);
////    fun("88Vv-AzHVj8", fmodf);
////    fun("hNAh1l09UpA", _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE7_GetcatEPPKNSt6locale5facetEPKS5_);
////    fun("E4gWXl6V2J0", _ZNSt9_Num_base12max_exponentE);
////    fun("o+ok6Y+DtgY", strerror_s);
////    fun("2d5UH9BnfVY", _Tls_setup__Wcstate);
////    fun("-mLzBSk-VGs", _ZNSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE2idE);
////    fun("zQQIrnpCoFA", _Fetch_and_seq_cst_1);
////    fun("0WMHDb5Dt94", modf);
////    fun("VmOyIzWFNKs", _ZNSt14numeric_limitsIdE14max_exponent10E);
////    fun("dFhgrqyzqhI", _ZNSt12placeholders3_20E);
////    fun("muIOyDB+DP8", _ZTISt15basic_streambufIwSt11char_traitsIwEE);
////    fun("6F1JfiING18", _ZNSt10moneypunctIwLb0EED2Ev);
////    fun("wozVkExRax4", _ZGVNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE2idE);
////    fun("sEca1nUOueA", _ZTVSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("X9D8WWSG3As", _ZNSt8ios_baseD1Ev);
////    fun("f5zmgYKSpIY", _ZTSt);
////    fun("wmkXFgerLnM", _Wcscollx);
////    fun("6-WU6MmYG0c", _ULx86_64_is_signal_frame);
////    fun("RYlvfQvnOzo", _ZTISt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("qwR1gtp-WS0", _FErfc);
////    fun("E8wCoUEbfzk", wcsncmp);
////    fun("7gj0BXUP3dc", _ZTIPw);
////    fun("qjyK90UVVCM", _ZTVSo);
////    fun("f41T4clGlzY", _Xp_invx);
////    fun("l7OtvplI42U", _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC2ERKSt8_Locinfom);
////    fun("rme+Po9yI5M", _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC1Em);
////    fun("iwIUndpU5ZI", _ZTSSt22_System_error_category);
////    fun("rF07weLXJu8", _ZNSt8bad_castD2Ev);
////    fun("IMt+UO5YoQI", fdiml);
////    fun("KfcTPbeaOqg", _ZTVSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("ZE6RNL+eLbk", tanf);
////    fun("0x7rx8TKy2Y", _Unlockfilelock);
////    fun("ruZtIwbCFjk", _ZTVSt7collateIcE);
////    fun("Fh1GjwqvCpE", _WPutstr);
////    fun("RLdcWoBjmT4", _Getpwctrtab);
////    fun("TYE4irxSmko", toupper);
////    fun("WVB9rXLAUFs", _ZNSt12placeholders3_18E);
////    fun("nEuTkSQAQFw", _ZTIb);
////    fun("eD+mC6biMFI", _ZTIN10__cxxabiv117__array_type_infoE);
////    fun("BMVIEbwpP+8", __floatdidf);
////    fun("F7fNJZaRFnU", __libunwind_Unwind_GetRegionStart);
////    fun("SfQIZcqvvms", strlcpy);
////    fun("v8P1X84ytFY", _ZNKSt7_MpunctIwE16do_decimal_pointEv);
////    fun("tsvEmnenz48", __cxa_atexit);
////    fun("QalEczzSNqc", _Makewct);
////    fun("vCIFGeI6adI", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewe);
////    fun("DoGS21ugIfI", _ZTIm);
////    fun("rhJg5tjs83Q", _ZNSt15_Num_float_base15has_denorm_lossE);
////    fun("ldbrWsQk+2A", _LDnorm);
////    fun("+kvyBGa+5VI", __subvdi3);
////    fun("qEvDssa4tOE", _FErf_small);
////    fun("5gOOC0kzW0c", sce_signgam);
////    fun("s+LfDF7LKxM", _Atomic_compare_exchange_strong_4);
////    fun("FAah-AY8+vY", _ZTSSt11_Facet_base);
////    fun("sUP1hBaouOw", _Getpctype);
////    fun("hqi8yMOCmG0", _ZNSt8_LocinfoC1EPKc);
////    fun("bPtdppw1+7I", _Zero);
////    fun("dRu2RLn4SKM",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERt);
////    fun("HC8vbJStYVY", at_quick_exit);
////    fun("qF3mHeMAHVk", _ZNSt8messagesIcEC2EPKcm);
////    fun("vwMx2NhWdLw", _ZNSt7codecvtIDic9_MbstatetED0Ev);
////    fun("PjwbqtUehPU", _ZSt19_Throw_future_errorRKSt10error_code);
////    fun("PZoM-Yn6g2Q", __atomic_fetch_xor_2);
////    fun("1GhiIeIpkms", _ZNSt16invalid_argumentD0Ev);
////    fun("rQFVBXp-Cxg", fseek);
////    fun("VVK0w0uxDLE", _ZNSt14numeric_limitsIcE8digits10E);
////    fun("sPC7XE6hfFY", srandom);
////    fun("BWAtsvaXToA", _Unwind_SetIP);
////    fun("4F9pQjbh8R8", _Fwprep);
////    fun("DhXTD5eM7LQ",
////        _ZNKSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_bRSt8ios_baseRNSt5_IosbIiE8_IostateERSbIwS2_SaIwEE);
////    fun("s-Ml8NxBKf4", _LDscale);
////    fun("nOIEswYD4Ig", __cxa_free_exception);
////    fun("BfAsxVvQVTQ", vwarnc);
////    fun("kHVXc-AWbMU", _LXp_setw);
////    fun("NDOLSTFT1ns", __feenableexcept);
////    fun("RVDooP5gZ4s", _ZTSNSt6locale5facetE);
////    fun("7OO0uCJWILQ", _ZTIPDh);
////    fun("6ei1eQn2WIc", _FDsign);
////    fun("oQpl5qCT014", _Unwind_GetGR);
////    fun("yZmHOKICuxg", _ZTIN6Dinkum7threads10lock_errorE);
////    fun("3M20pLo9Gdk", _ZNKSt8numpunctIwE8truenameEv);
////    fun("cRSJysDpVl4",
////        _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE11do_get_yearES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("TEtyeXjcZ0w", _ZNSt8numpunctIcEC1Em);
////    fun("9O1Xdko-wSo", exp2l);
////    fun("XKenFBsoh1c", _Atomic_fetch_xor_8);
////    fun("94dk1V7XfYw", _ZNSt13runtime_errorD0Ev);
////    fun("-UzbeNWVAE4", __libunwind_Unwind_GetLanguageSpecificData);
////    fun("DTH1zTBrOO8",
////        _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE7_GetcatEPPKNSt6locale5facetEPKS5_);
////    fun("U4JP-R+-70c", _ZNSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED2Ev);
////    fun("mU88GYCirhI", _ZNSt10moneypunctIcLb0EE7_GetcatEPPKNSt6locale5facetEPKS1_);
////    fun("HNnWdT43ues", swscanf);
////    fun("zlfEH8FmyUA", _Stoul);
////    fun("x8PFBjJhH7E", _ZNSt8messagesIwEC2Em);
////    fun("f2YbMj0gBf8", erfcl);
////    fun("RMo7j0iTPfA", bcopy);
////    fun("-nCVE3kBjjA", _ZNKSt5ctypeIcE9do_narrowEPKcS2_cPc);
////    fun("8Q60JLJ6Rv4", getc);
////    fun("LxGIYYKwKYc", rint);
////    fun("0dQrYWd7g94", ilogbf);
////    fun("2vDqwBlpF-o", strtod);
////    fun("XVu4--EWzcM", _ZNKSt17bad_function_call4whatEv);
////    fun("rtV7-jWC6Yg", log);
////    fun("tVHE+C8vGXk", _ZTVSt8bad_cast);
////    fun("jSquWN7i7lc", _ZSt4clog);
////    fun("jVwxMhFRw8Q", _ZNSt24_Iostream_error_categoryD0Ev);
////    fun("eZfFLyWCkvg",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERy);
////    fun("iF1iQHzxBJU", sceLibcMspaceMemalign);
////    fun("SVIiJg5eppY", _Atomic_fetch_or_2);
////    fun("gVTWlvyBSIc", _ZNSo6sentryC2ERSo);
////    fun("RWqyr1OKuw4", logbf);
////    fun("PDQbEFcV4h0", _FQuadph);
////    fun("FwzVaZ8Vnus", optopt);
////    fun("KDFy-aPxHVE", _Shared_ptr_flag);
////    fun("5i8mYqsM8z0", _ULx86_64_destroy_addr_space);
////    fun("QNAIWEkBocY", _ZTISt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("iBrTJkDlQv8", _FTan);
////    fun("aeHxLWwq0gQ", _ZTVN10__cxxabiv119__pointer_type_infoE);
////    fun("qMXslRdZVj4", _ZSt13resetiosflagsNSt5_IosbIiE9_FmtflagsE);
////    fun("MuACiCSA8-s", _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED2Ev);
////    fun("kwp-0uidHpw",
////        _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8get_dateES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("Oo1r8jKGZQ4", _ZNSt7collateIcEC1Em);
////    fun("4nRn+exUJAM", _WStod);
////    fun("qiR-4jx1abE", _ZNSs6appendERKSsmm);
////    fun("smbQukfxYJM", getenv);
////    fun("uhnb6dnXOnc", _ZNSt4_PadC2EPKc);
////    fun("fNH4tsl7rB8", erfl);
////    fun("i726T0BHbOU", _ZNSt11logic_errorD1Ev);
////    fun("F+ehGYUe36Y", _ZNSt14_Num_ldbl_base15has_denorm_lossE);
////    fun("wm9QKozFM+s", _ZTSPKc);
////    fun("zqOGToT2dH8", _ZTIPKs);
////    fun("z1EfxK6E0ts", _Feraise);
////    fun("ENLfKJEZTjE", set_constraint_handler_s);
////    fun("papHVcWkO5A", _ZTSi);
////    fun("0TADyPWrobI", _ZNSt7_MpunctIwED1Ev);
////    fun("5BQIjX7Y5YU", _ZNKSt7codecvtIwc9_MbstatetE13do_max_lengthEv);
////    fun("bLPn1gfqSW8", _ZTISt13runtime_error);
////    fun("aqyjhIx7jaY", _ZSt7_FiopenPKwNSt5_IosbIiE9_OpenmodeEi);
////    fun("fEoW6BJsPt4", sceLibcMspaceMallocUsableSize);
////    fun("W5OtP+WC800", _ZNKSt5ctypeIcE8do_widenEPKcS2_Pc);
////    fun("0r8rbw2SFqk", _ZSt17_Future_error_mapi);
////    fun("Nd2Y2X6oR18", _WCostate);
////    fun("2ud1bFeR0h8", _ZTSSt12domain_error);
////    fun("kBpWlgVZLm4", _Atomic_compare_exchange_strong);
////    fun("D5m73fSIxAU", _ZNSt14_Error_objectsIiE16_Iostream_objectE);
////    fun("3Nr9caNHhyg", posix_spawn);
////    fun("J3J1T9fjUik", towlower);
////    fun("pXvbDfchu6k", strncasecmp);
////    fun("aesyjrHVWy4", strncmp);
////    fun("dplyQ6+xatg", _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE2idE);
////    fun("13xzrgS8N4o", _ZNSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC1Em);
////    fun("c27lOSHxPA4", _ZNSt14_Num_ldbl_base9is_moduloE);
////    fun("qQ4c52GZlYw", _ZTSPKDs);
////    fun("u2MAta5SS84", _ZNSt7codecvtIDic9_MbstatetE2idE);
////    fun("RqeErO3cFHU", _ZSt15set_new_handlerPFvvE);
////    fun("kfuINXyHayQ", _ZTSy);
////    fun("1kIpmZX1jw8", _thread_init_stub);
////    fun("fjG5plxblj8", _ZNSt8ios_base7failureD2Ev);
////    fun("l1wz5R6cIxE", __popcountti2);
////    fun("b9QSruV4nnc", _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED0Ev);
////    fun("dcaiFCKtoDg", __divsc3);
////    fun("969Euioo12Q", _ZNSt8messagesIcEC2ERKSt8_Locinfom);
////    fun("oqYAk3zpC64", _ZGVNSt8messagesIwE2idE);
////    fun("rZ5sEWyLqa4", _ZNSt13_Num_int_base9is_moduloE);
////    fun("HfKQ6ZD53sM", _Atomic_signal_fence);
////    fun("1nZ4Xfnyp38", _sceLibcGetMallocParam);
////    fun("rQ-70s51wrc", _LSnan);
////    fun("KmfpnwO2lkA", _Touptab);
////    fun("3A9RVSwG8B0", __floatundixf);
////    fun("XZNi3XtbWQ4", _ZNSt8numpunctIwE2idE);
////    fun("VOBg+iNwB-4", strtoll);
////    fun("gBZnTFMk6N0", _ZTISt10moneypunctIwLb1EE);
////    fun("lrQSsTRMMr4", _ZSt7_MP_GetPy);
////    fun("3+5qZWL6APo", _ZNSt14numeric_limitsIeE14min_exponent10E);
////    fun("jna5sqISK4s", _ZNSt10moneypunctIwLb0EEC1EPKcm);
////    fun("cJWGxiQPmDQ", strsep);
////    fun("2bfL3yIBi5k",
////        _ZNKSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERm);
////    fun("XZzWt0ygWdw", _ZTISt16invalid_argument);
////    fun("7brRfHVVAlI", _ZNSt7collateIcE2idE);
////    fun("kBxt5LwtLA4", __cxa_free_dependent_exception);
////    fun("lKfKm6INOpQ", _ZSt10unexpectedv);
////    fun("VPbJwTCgME0", srand);
////    fun("-BwLPxElT7U", _LFpcomp);
////    fun("MzsycG6RYto", c32rtomb);
////    fun("npLpPTaSuHg", fscanf);
////    fun("H+8UBOwfScI", __powidf2);
////    fun("--ll-AgiK2M", _ULx86_64_set_reg);
////    fun("0fkFA3za2N8", _ZNSt7codecvtIwc9_MbstatetED1Ev);
////    fun("5L5Aft+9nZU",
////        _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE8get_yearES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("Suc8W0QPxjw", _ZdaPvS_);
////    fun("Gtsl8PUl40U", _ZTVSt8numpunctIwE);
////    fun("L1Ze94yof2I", _ZNSt8_LocinfoC1EiPKc);
////    fun("iOLTktXe6a0", _ZTSa);
////    fun("qkuA-unH7PU", _ZNKSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecPK2tmcc);
////    fun("QW2jL1J5rwY", _ZNSt6locale5facet9_RegisterEv);
////    fun("EeBQ7253LkE", _ZNKSt8messagesIcE5closeEi);
////    fun("eDciML+moZs", _ZTISt12system_error);
////    fun("Q2V+iqvjgC0", vsnprintf);
////    fun("vbgCuYKySLI", _ZNKSt8messagesIcE6do_getEiiiRKSs);
////    fun("3QIPIh-GDjw", rewind);
////    fun("YTM5eyFGh2c", _ZNKSt11logic_error8_DoraiseEv);
////    fun("w2GyuoXCnkw", _ZNSbIwSt11char_traitsIwESaIwEE6insertEmmw);
////    fun("wXs12eD3uvA", ilogbl);
////    fun("e-QultjK7Q0", _ULx86_64_get_fpreg);
////    fun("M86y4bmx+WA", _ZTSb);
////    fun("cpCOXWMgha0", rand);
////    fun("AgxEl+HeWRQ", _ZNSt6locale5facet7_DecrefEv);
////    fun("-3pU5y1utmI", _FExp);
////    fun("zdCex1HjCCM", _ZTVSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("d5Q-h2wF+-E", __sync_fetch_and_xor_16);
////    fun("bn3sb2SwGk8", _ZTSSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("gigoVHZvVPE", sceLibcMspaceRealloc);
////    fun("JZWEhLSIMoQ", __atomic_is_lock_free);
////    fun("WJU9B1OjRbA", _ZTVN6Dinkum7threads10lock_errorE);
////    fun("roztnFEs5Es", _ZNKSt7collateIwE10do_compareEPKwS2_S2_S2_);
////    fun("7lCihI18N9I", _ZdaPvmRKSt9nothrow_t);
////    fun("mh9Jro0tcjg", _ZTSSt7_MpunctIwE);
////    fun("kxm0z4T5mMI", sce_fileno_unlocked);
////    fun("OGVdXU3E-xg", wprintf);
////    fun("N0EHkukBz6Q", _ZTSSt13messages_base);
////    fun("RGc3P3UScjY", _Wcsftime);
////    fun("6eU9xX9oEdQ", mbtowc);
////    fun("BNC7IeJelZ0", _ZTSh);
////    fun("q+AdV-EHiKc", gamma_r);
////    fun("U2t+WvMQj8A", _ZNKSt8messagesIcE4openERKSsRKSt6locale);
////    fun("oAlR5z2iiCA", wcrtomb_s);
////    fun("7NQGsY7VY3c", _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEEC1ERKSt8_Locinfom);
////    fun("ec8jeC2LMOc", _ZNSt14_Num_ldbl_base17has_signaling_NaNE);
////    fun("4r9P8foa6QQ", _ZNSt14numeric_limitsItE6digitsE);
////    fun("lcNk3Ar5rUQ", __udivmodti4);
////    fun("6JcY5RDA4jY", sceLibcPafMspaceMallocUsableSize);
////    fun("vvA7HtdtWnY",
////        _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE13get_monthnameES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("hqVKCQr0vU8", _ZNSt14numeric_limitsIdE12max_digits10E);
////    fun("0g+zCGZ7dgQ", _ZTIPKv);
////    fun("wdPaII721tY", ynf);
////    fun("DghzFjzLqaE", _ZTSDi);
////    fun("lA94ZgT+vMM", __isnanf);
////    fun("94OiPulKcao", _ZNSt12placeholders3_15E);
////    fun("r1k-y+1yDcQ", _ZNSt14numeric_limitsIDiE8digits10E);
////    fun("Wvm90I-TGl0", vfwscanf_s);
////    fun("gNQ1V2vfXDE", sce_setjmp);
////    fun("FQ9NFbBHb5Y", _ZSt7_BADOFF);
////    fun("RDeUB6JGi1U", __paritydi2);
////    fun("ys1W6EwuVw4", __absvdi2);
////    fun("BAYjQubSZT8", tzname);
////    fun("C4j57iQD4I8", _ZTISt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("SPiW3NTO8I0", _ZTIPm);
////    fun("ZO3a6HfALTQ", _ZNSt7collateIwED1Ev);
////    fun("uodLYyUip20", fclose);
////    fun("wVY5DpvU6PU", _ZNKSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_bRSt8ios_basewe);
////    fun("DfivPArhucg", memcmp);
////    fun("RjWhdj0ztTs", _ZTISt9basic_iosIwSt11char_traitsIwEE);
////    fun("6ZDv6ZusiFg", _ZNSiD0Ev);
////    fun("9opd1ucwDqI", _ZTIPx);
////    fun("tfmEv+TVGFU", _ZGVNSt10moneypunctIwLb0EE2idE);
////    fun("wLlFkwG9UcQ", time);
////    fun("0eoyU-FoNyk", __fixsfdi);
////    fun("XtP9KKwyK9Q", _ZTISt12bad_weak_ptr);
////    fun("ZwapHUAcijE", __atomic_compare_exchange_1);
////    fun("qYhnoevd9bI", _ZSt9terminatev);
////    fun("ckFrsyD2830", _ZN10__cxxabiv117__array_type_infoD2Ev);
////    fun("rG8xXX-L7NU", _Ux86_64_is_fpreg);
////    fun("pNtJdE3x49E", wcscmp);
////    fun("7VPIYFpwU2A", _ZdlPvmRKSt9nothrow_t);
////    fun("OzMS0BqVUGQ", _ZNKSt12bad_weak_ptr4whatEv);
////    fun("KZm8HUIX2Rw", wcscat);
////    fun("aMQhMoYipk4", _ZTVN10__cxxabiv117__array_type_infoE);
////    fun("iU0z6SdUNbI", fmin);
////    fun("VuMMb5CfpEw", wcsxfrm);
////    fun("pphEhnnuXKA", _ZTISt7collateIwE);
////    fun("rh7L-TliPoc", _ZNKSt5ctypeIwE5do_isEsw);
////    fun("ffnhh0HcxJ4", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecl);
////    fun("IJKVjsmxxWI", __cxa_allocate_dependent_exception);
////    fun("CX3WC8qekJE", _ZTSSt13runtime_error);
////    fun("98w+P+GuFMU", _ZNSt9_Num_base12has_infinityE);
////    fun("uMei1W9uyNo", exit);
////    fun("H4fcpQOpc08", _ZNSt6locale2id7_Id_cntE);
////    fun("iCWgjeqMHvg", _ZNKSt7codecvtIcc9_MbstatetE10do_unshiftERS0_PcS3_RS3_);
////    fun("tkqNipin1EI", _ZNSt7collateIcEC2Em);
////    fun("ZL9FV4mJXxo", __cxa_rethrow);
////    fun("WK24j1F3rCU", _ZNSt8numpunctIcEC1EPKcmb);
////    fun("g8ozp2Zrsj8", _Gettime);
////    fun("Gy2iRxp3LGk",
////        _ZNKSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE6do_getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateERf);
////    fun("VNHXByo1yuY", _ZTSSt11logic_error);
////    fun("jfRI3snge3o", _Thrd_sleep);
////    fun("ke36E2bqNmI", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecb);
////    fun("pyBabUesXN4", _ZNSt10moneypunctIwLb1EEC1EPKcm);
////    fun("9nf8joUTSaQ", _Unlocksyslock);
////    fun("cxqzgvGm1GI", _ZTISt12length_error);
////    fun("PSAw7g1DD24", _ZNSt7collateIwEC2EPKcm);
////    fun("WAPkmrXx2o8", _ZNKSt7codecvtIwc9_MbstatetE5do_inERS0_PKcS4_RS4_PwS6_RS6_);
////    fun("kn0yiYeExgA", ldexpf);
////    fun("0cLFYdr1AGc", _ZTIPKm);
////    fun("HDnBZ+mkyjg", _ZGVNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE2idE);
////    fun("-GVEj2QODEI", _Atomic_fetch_xor_4);
////    fun("t1ytXodWUH0", putenv);
////    fun("ob5xAW4ln-0", strchr);
////    fun("6hV3y21d59k", _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED2Ev);
////    fun("vHy+a4gLBfs", _ZNSt6chrono12steady_clock9is_steadyE);
////    fun("1TDo-ImqkJc", _Stdin);
////    fun("JCmHsYVc2eo", tanhl);
////    fun("NLwJ3q+64bY", _ZSt7nothrow);
////    fun("qvTpLUKwq7Q", _Atomic_exchange);
////    fun("gqwPsSmdh+U", _ZTSSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE);
////    fun("96Bg8h09w+o", _ZNSt9_Num_base15has_denorm_lossE);
////    fun("XBmd6G-HoYI", _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC2EPKcm);
////    fun("h+J60RRlfnk", nextafter);
////    fun("a9KMkfXXUsE", _ZTIPy);
////    fun("2gbcltk3swE", div);
////    fun("zr094EQ39Ww", __cxa_pure_virtual);
////    fun("fIskTFX9p68", __srefill);
////    fun("mDIHE-aaRaI", _ZSt22_Random_device_entropyv);
////    fun("nW9JRkciRk4", strtold);
////    fun("w1NxRBQqfmQ", printf_s);
////    fun("YMZO9ChZb0E", reallocf);
////    fun("WViwxtEKxHk", _ZNSt6locale7_LocimpC1ERKS0_);
////    fun("qR6GVq1IplU", _ZTSSt4_Pad);
////    fun("4TTbo2SxxvA", _Ttotm);
////    fun("4ifo9QGrO5w", _Locterm);
////    fun("oCH4efUlxZ0", __atomic_exchange_n);
////    fun("XbFyGCk3G2s", _ZTVSt10moneypunctIcLb0EE);
////    fun("gF5aGNmzWSg", _ZNSt9_Num_base13has_quiet_NaNE);
////    fun("Qa6HUR3h1k4", __fixunssfdi);
////    fun("saSUnPWLq9E", _ZSt16_Xoverflow_errorPKc);
////    fun("ECUHmdEfhic", __ashlti3);
////    fun("iZrCfFRsE3Y", _ZTId);
////    fun("omQZ36ESr98", sce_stdin);
////    fun("3THnS7v0D+M", _ZTSPKx);
////    fun("r3tNGoVJ2YA", __ffsdi2);
////    fun("se3uU7lRMHY", _LExp);
////    fun("zMCYAqNRllc", __cxa_demangle);
////    fun("XGnuIBmEmyk", strndup);
////    fun("9LCjpWyQ5Zc", pow);
////    fun("Krt-A7EnHHs",
////        _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE8get_yearES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("ZUvemFIkkhQ", nanl);
////    fun("fuzzBVdpRG0", _FDivide);
////    fun("XPrliF5n-ww", vwprintf_s);
////    fun("Zb70g9IUs98", _Xp_getw);
////    fun("77pL1FoD4I4", sce__mb_cur_max);
////    fun("h6pVBKjcLiU", ilogb);
////    fun("9mKjVppFsL0", __longjmp);
////    fun("STNAj1oxtpk", _ZTISt13basic_filebufIcSt11char_traitsIcEE);
////    fun("EiHf-aLDImI", logl);
////    fun("8vEBRx0O1fc", _ZNSt10moneypunctIwLb1EE4intlE);
////    fun("bhfgrK+MZdk", _ZN10__cxxabiv119__pointer_type_infoD1Ev);
////    fun("aX8H2+BBlWE", ldexpl);
////    fun("9f-LMAJYGCY", _ZNSt12placeholders2_8E);
////    fun("0j1jspKbuFk", _ZTv0_n24_NSt13basic_ostreamIwSt11char_traitsIwEED0Ev);
////    fun("hBvqSQD5yNk", _ZTSSt20bad_array_new_length);
////    fun("zS94yyJRSUs", _Getpwcstate);
////    fun("9kOFELic7Pk", fwprintf_s);
////    fun("iVhJZvAO2aQ", lldiv);
////    fun("A71XWS1kKqA", __atomic_fetch_and_2);
////    fun("x1vTXM-GLCE", _ZNSt14numeric_limitsIbE6digitsE);
////    fun("o4kt51-uO48", _ZTVSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("n2kx+OmFUis", _ZTISt9exception);
////    fun("cWgvLiSJSOQ", __atomic_load_1);
////    fun("ECh+p-LRG6Y", _Pow);
////    fun("1b+IhPTX0nk", _FXp_addx);
////    fun("ReK9U6EUWuU", _ZNSt8_LocinfoC2ERKSs);
////    fun("SZk+FxWXdAs", __ucmpdi2);
////    fun("NFLs+dRJGNg", memcpy_s);
////    fun("sIvK5xl5pzw", _ZTISt5_IosbIiE);
////    fun("MfyPz2J5E0I", _ZTVSt10moneypunctIcLb1EE);
////    fun("usKbuvy2hQg", __cxa_call_unexpected);
////    fun("mmrSzkWDrgA", _ZNSt14numeric_limitsIxE9is_signedE);
////    fun("jbgqYhmVEGY", posix_spawnattr_getschedparam);
////    fun("9oiX1kyeedA", bzero);
////    fun("MFqdrWyu9Ls", _ZNSt14numeric_limitsIfE12max_digits10E);
////    fun("psx0YiAKm7k", fegetenv);
////    fun("2aSk2ruCP0E", _ZNSt8_LocinfoC1ERKSs);
////    fun("RV6sGVpYa-o", _ZNSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC1ERKSt8_Locinfom);
////    fun("MYCRRmc7cDA", _ZNSt13basic_filebufIcSt11char_traitsIcEED0Ev);
////    fun("f1ZGLUnQGgo", _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC2Em);
////    fun("ozLi0i4r6ds", _ZNKSt8numpunctIcE12do_falsenameEv);
////    fun("MmTshn7gIgU", _Unwind_GetCFA);
////    fun("nF8-CM+tro4", _ZNSbIwSt11char_traitsIwESaIwEE6appendEmw);
////    fun("9hB8AwIqQfs", _ZNKSt14error_category10equivalentERKSt10error_codei);
////    fun("FjZCPmK0SbA", _ZNSt7codecvtIwc9_MbstatetE2idE);
////    fun("TqvziWHetnw", _ZNSt12out_of_rangeD0Ev);
////    fun("WkYnBHFsmW4", __isnormalf);
////    fun("BXmn6hA5o0M", __mulsf3);
////    fun("PWmDp8ZTS9k", _Getint);
////    fun("65cvm2NDLmU", _ZNKSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_bRSt8ios_basece);
////    fun("IBn9qjWnXIw", __popcountsi2);
////    fun("t6egllDqQ2M", _ZNSt13bad_exceptionD1Ev);
////    fun("5AYcEn7aoro", wcstoul);
////    fun("0H5TVprQSkA", fmaxl);
////    fun("QmSKE-bAvvs", unw_backtrace);
////    fun("ufZdCzu8nME", __sync_lock_test_and_set_16);
////    fun("4pQ3B1BTMgo", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE5_IputES3_RSt8ios_basewPcm);
////    fun("fh54IRxGBUQ", __negvdi2);
////    fun("u0XOsuOmOzc", vswprintf);
////    fun("4uJJNi+C9wk", isalnum);
////    fun("qdGFBoLVNKI", quick_exit);
////    fun("tUo2aRfWs5I", _ZNSt14numeric_limitsIeE14max_exponent10E);
////    fun("RpTR+VY15ss", fmaf);
////    fun("w1o05aHJT4c", random);
////    fun("JHvEnCQLPQI", _Locale);
////    fun("vkuuLfhnSZI", __cxa_throw);
////    fun("sXaxl1QGorg", _ZNSt15basic_streambufIcSt11char_traitsIcEE6xsputnEPKci);
////    fun("XepdqehVYe4", closedir);
////    fun("7VRfkz22vPk", _ZNSt13basic_ostreamIwSt11char_traitsIwEED0Ev);
////    fun("to7GggwECZU", _ZNSt7codecvtIcc9_MbstatetED2Ev);
////    fun("fjI2ddUGZZs", _ZNSt14numeric_limitsIdE12max_exponentE);
////    fun("4tDF0D+qdWk", __atomic_store_4);
////    fun("uF8hDs1CqWI", _Getpwctytab);
////    fun("1IB0U3rUtBw", __fedisableexcept);
////    fun("gMwkpZNI9Us", _ZNSt8messagesIwED0Ev);
////    fun("N7z+Dnk1cS0", _ZNKSt7codecvtIcc9_MbstatetE7unshiftERS0_PcS3_RS3_);
////    fun("p--TkNVsXjA", posix_spawn_file_actions_init);
////    fun("lmb3oBpMNPU", _ZNKSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE3putES3_RSt8ios_basewy);
////    fun("gso5X06CFkI", _LPmsw);
////    fun("abCQEKstw7g", _ULx86_64_get_save_loc);
////    fun("fnUEjBCNRVU", wmemchr);
////    fun("R1ITHuTUMEI",
////        _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE16do_get_monthnameES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("cDHRgSXYdqA", _ZNSt9_Num_base10has_denormE);
////    fun("8H3yBUytv-0", _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED0Ev);
////    fun("lZVehk7yFok", _ZNSt15basic_streambufIwSt11char_traitsIwEE6xsgetnEPwi);
////    fun("1nylaCTiH08", _ZTSj);
////    fun("7uX6IsXWwak", _ZTIPKe);
////    fun("UNS2V4S097M", syslog);
////    fun("p-SW25yE-Q8", _ZNSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEEC2Em);
////    fun("3+UPM-9E6xY", modff);
////    fun("qyB3FpjlDSY", _ULx86_64_handle_signal_frame);
////    fun("8SKVXgeK1wY", vsnwprintf_s);
////    fun("LaPaA6mYA38", _Mtxdst);
////    fun("Qi5fpNt5+T0", _ZNSt9money_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEEC1Em);
////    fun("Cqpti4y-D3E", _PJP_CPP_Copyright);
////    fun("0jk9oqKd2Gw", _ZN10__cxxabiv123__fundamental_type_infoD2Ev);
////    fun("hXsvfky362s", _ZNSt13basic_filebufIwSt11char_traitsIwEE9underflowEv);
////    fun("N5nZ3wQw+Vc", _ZNSt9type_infoD0Ev);
////    fun("Eubj+4g8dWA", _ZNSt14numeric_limitsIyE8digits10E);
////    fun("+3PtYiUxl-U", wctype);
////    fun("5ViZYJRew6g", _ZTSSt8messagesIwE);
////    fun("XcuCO1YXaRs", _ZNSt13basic_filebufIwSt11char_traitsIwEED1Ev);
////    fun("CvJ3HUPlMIY", _ZSt5wcerr);
////    fun("za50kXyi3SA", _ZNSt16nested_exceptionD0Ev);
////    fun("1RNxpXpVWs4", __floatundidf);
////    fun("VecRKuKdXdo", _Quadph);
////    fun("kvEP5-KOG1U", _WPrintf);
////    fun("f1zwJ3jAI2k", _Unwind_Resume);
////    fun("ejBz8a8TCWU", _ZNSt9_Num_base6digitsE);
////    fun("olBDzD1rX2Y", __ctzti2);
////    fun("WF4fBmip+38", vfwscanf);
////    fun("kPELiw9L-gw", _ZNSt10moneypunctIcLb1EEC2Em);
////    fun("cBSM-YB7JVY", fcloseall);
////    fun("mxv24Oqmp0E", _ZNSt15_Num_float_base10is_integerE);
////    fun("Q-8lQCGMj4g", _ZNKSt7collateIcE7compareEPKcS2_S2_S2_);
////    fun("802pFCwC9w0", __udivti3);
////    fun("Wz9CvcF5jn4", _Getzone);
////    fun("dtMu9zCDn3s", __fixunsxfsi);
////    fun("qG50MWOiS-Q", _Files);
////    fun("UFsKD1fd1-w", _ZTISt8messagesIcE);
////    fun("y+E+IUZYVmU", __mulsc3);
////    fun("5qP1iVQkdck", ftello);
////    fun("84wIPnwBGiU", _ZNSt7collateIwED2Ev);
////    fun("hVQgfGhJz3U", _ZNSt22_System_error_categoryD1Ev);
////    fun("L-jLYJFP9Mc", errc);
////    fun("lF66NEAqanc", _ZTVSt14error_category);
////    fun("V0X-mrfdM9E", imaxdiv);
////    fun("h+c9OSfCAEg", _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED1Ev);
////    fun("OvbYtSGnzFk", __cmpdi2);
////    fun("fkFGlPdquqI", _ZNSt6locale7_Locimp9_MakexlocERKSt8_LocinfoiPS0_PKS_);
////    fun("9iCP-hTL5z8", _WStoull);
////    fun("P38JvXuK-uE", fegettrapenable);
////    fun("2FezsYwelgk", _ZTVSt8messagesIwE);
////    fun("mnq3tbhCs34", _ZNKSt5ctypeIcE10do_toupperEc);
////    fun("i180MNC9p4c", _ZNSt8_LocinfoC2EiPKc);
////    fun("yxbGzBQC5xA", sce_ferror_unlocked);
////    fun("2M9VZGYPHLI", __truncdfsf2);
////    fun("PxP1PFdu9OQ", __udivsi3);
////    fun("VJL9W+nOv1U", _ZTSPy);
////    fun("7hOUKGcT6jM", sceLibcPafMspacePosixMemalign);
////    fun("DSI7bz2Jt-I", __powixf2);
////    fun("OXkzGA9WqVw", _ZTSPm);
////    fun("kvqg376KsJo", _ZTv0_n24_NSoD1Ev);
////    fun("2R2j1QezUGM", _ZNSt12bad_weak_ptrD2Ev);
////    fun("CWiqHSTO5hk", rindex);
////    fun("+i81FtUCarA", _ZNKSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE6do_putES3_RSt8ios_basecPK2tmcc);
////    fun("K+-VjJdCYVc", _ZGVNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE2idE);
////    fun("Bin7e2UR+a0", _ZNSt9exception18_Set_raise_handlerEPFvRKS_E);
////    fun("9AcX4Qk47+o", _ZNSt15_Num_float_base11round_styleE);
////    fun("p6DbM0OAHNo", iswblank);
////    fun("z-OrNOmb6kk", _Tgamma);
////    fun("XbVXpf5WF28", wcsftime);
    fun("gQX+4GDQjpM", malloc);
////    fun("5DdJdPeXCHE", _ZTSSt7num_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE);
////    fun("+xoGf-x7nJA", _Atomic_exchange_8);
////    fun("XkT6YSShQcE", _WLitob);
////    fun("lJPCM50sdTc", _ZTVSt24_Iostream_error_category);
////    fun("xuEUMolGMwU", _ZTSSt8numpunctIwE);
////    fun("fBne7gcou0s", posix_spawnattr_setschedpolicy);
////    fun("JTwt9OTgk1k", _ZNSt13basic_filebufIcSt11char_traitsIcEE9_EndwriteEv);
////    fun("T85u2sPrKOo",
////        _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE11do_get_dateES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tm);
////    fun("QrZZdJ8XsX0", fputs);
////    fun("rWSuTWY2JN0", vsnprintf_s);
////    fun("hmHH6DsLWgA", _ZTSN6Dinkum7threads21thread_resource_errorE);
////    fun("iPBqs+YUUFw", _Atomic_fetch_add_4);
////    fun("tcdvTUlPnL0", _ZNSt10moneypunctIcLb1EED0Ev);
////    fun("RCQAffkEh9A", coshf);
////    fun("1nTKA7pN1jw", utime);
////    fun("DjLpZIMEkks", _ZGVNSt14_Error_objectsIiE15_Generic_objectE);
////    fun("vhPKxN6zs+A", _Fpcomp);
////    fun("pDtTdJ2sIz0", _ZNKSs5_XlenEv);
////    fun("tjuEJo1obls", psignal);
////    fun("p8-44cVeO84", _ZNSt5ctypeIcED0Ev);
////    fun("rX58aCQCMS4", _ZNSt5ctypeIcE10table_sizeE);
////    fun("ota-3+co4Jk", _ZTIPh);
////    fun("Rj4qy44yYUw", __negdi2);
////    fun("QxmSHBCuKTk", strtoul);
////    fun("64pqofAwJEg",
////        _ZNKSt8time_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE3getES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmcc);
////    fun("LfMY9H6d5mI", _ZTSSt7_MpunctIcE);
////    fun("95V3PF0kUEA", powl);
////    fun("cqt8emEH3kQ", feclearexcept);
////    fun("h9RyP3R30HI", _ZNSt14numeric_limitsImE8digits10E);
////    fun("nbTAoMwiO38", _ZNSt10moneypunctIcLb1EEC1Em);
////    fun("a4gLGspPEDM", trunc);
////    fun("NJtKruu9qOs", _ZNKSt7num_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEE3putES3_RSt8ios_basecx);
////    fun("8raXTYQ11cg", _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE7_GetintERS3_S5_iiRiRKSt5ctypeIcE);
////    fun("NC4MSB+BRQg", strncat_s);
////    fun("xUsJSLsdv9I", _Unwind_Resume_or_Rethrow);
////    fun("jq4Srfnz9K8", _LTgamma);
////    fun("Vf68JAD5rbM", _ZNKSt5ctypeIwE9do_narrowEPKwS2_cPc);
////    fun("yAZ5vOpmBus", __vfprintf);
////    fun("fgXJvOSrqfg", _Fetch_xor_seq_cst_2);
////    fun("SRI6S9B+-a4", atof);
////    fun("FeyelHfQPzo", __subsf3);
////    fun("oNRAB0Zs2+0", _ZTISt15underflow_error);
////    fun("VtRkfsD292M", fegetexcept);
////    fun("8oO55jndPRg", _ZNSbIwSt11char_traitsIwESaIwEE6assignEmw);
////    fun("cFsD9R4iY50", _Thrd_exit);
////    fun("AdYYKgtPlqg", _rtld_get_stack_prot);
////    fun("UVDWssRNEPM", _Atomic_fetch_and_1);
////    fun("+qitMEbkSWk", vsprintf_s);
////    fun("dLmvQfG8am4", __ucmpti2);
////    fun("bJhmjmsPRTM", _ZNK10__cxxabiv120__si_class_type_info11can_cast_toEPKNS_17__class_type_infoE);
////    fun("nvMOoFdQYk0", __libunwind_Unwind_SetIP);
////    fun("WY7615THqKM", _ZTIPKt);
////    fun("6-LMlTS1nno", _ZTVSt12future_error);
////    fun("CjQROLB88a4", __signbitf);
////    fun("BO1r8DPhmyg", _ZNSt12placeholders3_12E);
////    fun("xiqd+QkuYXc", _ZNSt15underflow_errorD2Ev);
////    fun("AV6ipCNa4Rw", strcasecmp);
////    fun("iNbtyJKM0iQ", _Stold);
////    fun("pDFe-IgbTPg", _ZSt6_ThrowRKSt9exception);
////    fun("5FD0gWEuuTs", _ZGVNSt7num_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEE2idE);
////    fun("-7c7thUsi1c", _ZTSPKd);
////    fun("FOe7cAuBjh8", _Atomic_compare_exchange_weak);
////    fun("g8Jw7V6mn8k", _ZNSt14error_categoryD2Ev);
////    fun("goLVqD-eiIY", _ZN10__cxxabiv117__class_type_infoD1Ev);
////    fun("u2kPEkUHfsg", __cmpti2);
////    fun("gsnW-FWQqZo", __addvsi3);
////    fun("5jNubw4vlAA", strnlen);
////    fun("JsZjB3TnZ8A", _ZNKSt5ctypeIwE10do_toupperEw);
////    fun("zkCx9c2QKBc", _ZNSt9money_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEED1Ev);
////    fun("+Nc8JGdVLQs", _ZNSt9exceptionD0Ev);
////    fun("NOaB7a1PZl8", _ZN6Dinkum7threads21thread_resource_errorD1Ev);
////    fun("ugJXRBTIcdY", _ULx86_64_set_fpreg);
////    fun("34mi8lteNTs", _ZNSt8messagesIcED1Ev);
////    fun("KqdclsYd24w", _ZNSt9_Num_base12min_exponentE);
////    fun("jxlpClEsfJQ", _ZNSt12placeholders2_5E);
////    fun("q9SHp+5SOOQ", __fixdfdi);
////    fun("RlGUiqyKf9I", lgammaf_r);
////    fun("RmE3aE8WHuY", j0f);
////    fun("DzkYNChIvmw", _LEps);
////    fun("i-7v8+UGglc", _rtld_atfork_pre);
////    fun("KuOuD58hqn4", malloc_stats_fast);
////    fun("3Rhy2gXDhwc", warn);
////    fun("yPPtp1RMihw", asinhf);
////    fun("AJsqpbcCiwY", _ZTVSt8ios_base);
////    fun("+uPwCGfmJHs", _ZNKSt7codecvtIDsc9_MbstatetE5do_inERS0_PKcS4_RS4_PDsS6_RS6_);
////    fun("WiUy3dEtCOQ", _ZNSt7num_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEED1Ev);
////    fun("weALTw0uesc", _ZTISt7_MpunctIcE);
////    fun("RB3ratfpZDc", _ZNSt9money_getIwSt19istreambuf_iteratorIwSt11char_traitsIwEEED0Ev);
////    fun("WQQlL0n2SpU", _ZNSt8time_putIcSt19ostreambuf_iteratorIcSt11char_traitsIcEEED0Ev);
////    fun("PG-2ZeVVWZE", _ZN6Dinkum7threads10lock_errorD0Ev);
////    fun("vh9aGR3ALP0", y1);
////    fun("qSiIrmgy1D8", _ZTSPKb);
////    fun("vEdl5Er9THU", _ZNSt14numeric_limitsIDsE8digits10E);
////    fun("WG7lrmFxyKY", _ZTSSt9type_info);
////    fun("7+0ouwmGDww", __ashrti3);
////    fun("A98W3Iad6xE", _FPow);
////    fun("U1CBVMD42HA", _ZTISi);
////    fun("ItmiNlkXVkQ", _ZTSSt24_Iostream_error_category);
////    fun("-amctzWbEtw", _ZNKSt8numpunctIwE12do_falsenameEv);
////    fun("kv4kgdjswN0", sceLibcPafMspaceGetFooterValue);
////    fun("pxFnS1okTFE", _Fetch_and_8);
////    fun("EMutwaQ34Jo", perror);
////    fun("C3LC9A6SrVE", _ZNKSt7_MpunctIcE14do_frac_digitsEv);
////    fun("X3DrtC2AZCI", _ZNSt8time_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEEC1Em);
////    fun("7OCTkL2oWyg", _ZNSt6_Mutex7_UnlockEv);
////    fun("A5EX+eJmQI8",
////        _ZZNKSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_bRSt8ios_basewRKSbIwS2_SaIwEEE4_Src);
////    fun("8hyOiMUD36c", _ZNSt14_Num_ldbl_base14is_specializedE);
////    fun("3GPpjQdAMTw", __cxa_guard_acquire);
////    fun("VNaqectsZNs", _ZNSt13basic_filebufIcSt11char_traitsIcEE4syncEv);
////    fun("-7vr7t-uto8", _Atomic_thread_fence);
////    fun("yLE5H3058Ao", _ZTVNSt8ios_base7failureE);
////    fun("YstfcFbhwvQ", _ZTIPi);
////    fun("WiH8rbVv5s4", _ZNSt9bad_allocD2Ev);
////    fun("V94pLruduLg", _malloc_postfork);
////    fun("RP7ijkGGx50", _ZNSt13basic_istreamIwSt11char_traitsIwEED1Ev);
////    fun("YaHc3GS7y7g", _Mtx_init);
////    fun("DG8dDx9ZV70", __fixunsxfdi);
////    fun("NnNaWa16OvE", _ZNSt11range_errorD1Ev);
////    fun("oDtGxrzLXMU", _ZNKSt7codecvtIwc9_MbstatetE10do_unshiftERS0_PcS3_RS3_);
////    fun("arIKLlen2sg", erfc);
////    fun("K+gcnFFJKVc", strcat_s);
////    fun("JXzQGOtumdM", _ZNKSt8numpunctIcE16do_decimal_pointEv);
////    fun("GDiCYtaiUyM",
////        _ZNKSt9money_putIwSt19ostreambuf_iteratorIwSt11char_traitsIwEEE6do_putES3_bRSt8ios_basewRKSbIwS2_SaIwEE);
////    fun("ODGONXcSmz4", ignore_handler_s);
////    fun("1iRAqEqEL0Y", __lshrti3);
////    fun("KGotca3AjYw", vswscanf);
////    fun("u16WKNmQUNg", _ZNSt14numeric_limitsIeE12max_digits10E);
////    fun("wlEdSSqaz+E", _LXp_movx);
////    fun("ZF6hpsTZ2m8", __atomic_store_1);
////    fun("E7UermPZVcw",
////        _ZNKSt8time_getIcSt19istreambuf_iteratorIcSt11char_traitsIcEEE7_GetfmtES3_S3_RSt8ios_baseRNSt5_IosbIiE8_IostateEP2tmPKc);
////    fun("0bGGr4zLE3w", _ZTv0_n24_NSiD0Ev);
////    fun("e+hi-tOrDZU", _Dbl);
////    fun("viiwFMaNamA", strstr);
////    fun("M7KmRg9CERk", jrand48);
////    fun("qb6A7pSgAeY", _ZNSt9bad_allocD0Ev);
////    fun("uTiJLq4hayE", _ZNSt14numeric_limitsIhE8digits10E);
////    fun("XbD-A2MEsS4", _ZNSt15_Num_float_base10is_boundedE);
////    fun("n9-NJEULZ-0", _ZNKSt24_Iostream_error_category7messageEi);
////    fun("-Bl9-SZ2noc", _ZNSt6_WinitC1Ev);
////    fun("SSvY6pTRAgk", _FDscale);
////    fun("2h3jY75zMkI", _LInf);
////    fun("6aEkwkEOGRg", _ZN10__cxxabiv120__si_class_type_infoD1Ev);
////    fun("wVbBBrqhwdw", __mulodi4);
////    fun("WFTOZxDfpbQ", _ZNSt13_Num_int_base14is_specializedE);
////    fun("msxwgUAPy-Y", _ZTSSt11range_error);
////    fun("ypq5jFNRIJA", _ZNKSt7_MpunctIwE14do_curr_symbolEv);
////    fun("cY4yCWdcTXE", __fixdfti);
////    fun("NWtTN10cJzE", sceLibcHeapGetTraceInfo);
////
////    //fun("YrL-1y6vfyo", ); // libSceLibcInternal
////    //fun("bGuDd3kWVKQ", ); // libSceLibcInternal
////    //fun("wUqJ0psUjDo", ); // libSceLibcInternal
////    fun("h8jq9ee4h5c", sceLibcInternalMemoryMutexEnable);
////    //fun("GG6441JdYkA", ); // libSceLibcInternal
////    //fun("f9LVyN8Ky8g", ); // libSceLibcInternal
////    fun("vhETq-NiqJA", _SceLibcDebugOut);
#undef fun
}
} // namespace libSceLibcInternal
