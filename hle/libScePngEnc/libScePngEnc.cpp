#include "libScePngEnc.h"

namespace libScePngEnc {

PS4API int scePngEncCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePngEncDelete() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePngEncEncode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePngEncQueryMemorySize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("xgDjJKpcyHo", scePngEncEncode);
    fun("7aGTPfrqT9s", scePngEncCreate);
    fun("9030RnBDoh4", scePngEncQueryMemorySize);
    fun("RUrWdwTWZy8", scePngEncDelete);
#undef fun
}
}
