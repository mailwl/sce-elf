#include "libSceUsbd.h"

namespace libSceUsbd {

#define SCE_USBD_ERROR_IO 0x80240001

PS4API int sceUsbdAllocTransfer() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdAttachKernelDriver() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdBulkTransfer() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdCancelTransfer() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdCheckConnected() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdClaimInterface() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdClearHalt() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdClose() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdControlTransfer() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdControlTransferGetData() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdControlTransferGetSetup() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdDetachKernelDriver() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdEventHandlerActive() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdEventHandlingOk() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdExit() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdFillBulkTransfer() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdFillControlSetup() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdFillControlTransfer() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdFillInterruptTransfer() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdFillIsoTransfer() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdFreeConfigDescriptor() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdFreeDeviceList() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdFreeTransfer() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetActiveConfigDescriptor() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetBusNumber() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetConfigDescriptor() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetConfigDescriptorByValue() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetConfiguration() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetDescriptor() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetDevice() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetDeviceAddress() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetDeviceDescriptor() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetDeviceList() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetDeviceSpeed() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetIsoPacketBuffer() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetMaxIsoPacketSize() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetMaxPacketSize() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetStringDescriptor() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdGetStringDescriptorAscii() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdHandleEvents() {
    //LOG_DEBUG("%s()\n", __FUNCTION__);
    return 0;//SCE_USBD_ERROR_IO;
}
PS4API int sceUsbdHandleEventsLocked() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdHandleEventsTimeout() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdInit() {
    LOG_DEBUG("%s()\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdInterruptTransfer() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdKernelDriverActive() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdLockEvents() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdLockEventWaiters() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdOpen() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdOpenDeviceWithVidPid() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdRefDevice() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdReleaseInterface() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdResetDevice() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdSetConfiguration() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdSetInterfaceAltSetting() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdSetIsoPacketLengths() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdSubmitTransfer() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdTryLockEvents() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdUnlockEvents() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdUnlockEventWaiters() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdUnrefDevice() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
PS4API int sceUsbdWaitForEvent() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("Fq6+0Fm55xU", sceUsbdExit);
    fun("hvMn0QJXj5g", sceUsbdResetDevice);
    fun("oHCade-0qQ0", sceUsbdFillBulkTransfer);
    fun("rt-WeUGibfg", sceUsbdHandleEventsLocked);
    fun("OULgIo1zAsA", sceUsbdUnrefDevice);
    fun("RRKFcKQ1Ka4", sceUsbdControlTransfer);
    fun("REfUTmTchMw", sceUsbdReleaseInterface);
    fun("SEdQo8CFmus", sceUsbdControlTransferGetSetup);
    fun("Dkm5qe8j3XE", sceUsbdGetConfigDescriptor);
    fun("1DkGvUQYFKI", sceUsbdUnlockEventWaiters);
    fun("DVCQW9o+ki0", sceUsbdSetInterfaceAltSetting);
    fun("t3J5pXxhJlI", sceUsbdFillInterruptTransfer);
    fun("EkqGLxWC-S0", sceUsbdHandleEvents);
    fun("8KrqbaaPkE0", sceUsbdFillControlSetup);
    fun("RLf56F-WjKQ", sceUsbdKernelDriverActive);
    fun("-KNh1VFIzlM", sceUsbdCancelTransfer);
    fun("e1UWb8cWPJM", sceUsbdGetDeviceSpeed);
    fun("U1t1SoJvV-A", sceUsbdRefDevice);
    fun("MlW6deWfPp0", sceUsbdCheckConnected);
    fun("3tPPMo4QRdY", sceUsbdClearHalt);
    fun("TcXVGc-LPbQ", sceUsbdTryLockEvents);
    fun("t7WE9mb1TB8", sceUsbdGetBusNumber);
    fun("bhomgbiQgeo", sceUsbdGetDeviceDescriptor);
    fun("8qB9Ar4P5nc", sceUsbdGetDeviceList);
    fun("e7gp1xhu6RI", sceUsbdEventHandlingOk);
    fun("u9yKks02-rA", sceUsbdLockEvents);
    fun("+wU6CGuZcWk", sceUsbdHandleEventsTimeout);
    fun("Vw8Hg1CN028", sceUsbdEventHandlerActive);
    fun("-JBoEtvTxvA", sceUsbdGetDescriptor);
    fun("BKMEGvfCPyU", sceUsbdAttachKernelDriver);
    fun("0ktE1PhzGFU", sceUsbdAllocTransfer);
    fun("XUWtxI31YEY", sceUsbdControlTransferGetData);
    fun("vokkJ0aDf54", sceUsbdGetIsoPacketBuffer);
    fun("VJ6oMq-Di2U", sceUsbdOpen);
    fun("GjlCrU4GcIY", sceUsbdGetDeviceAddress);
    fun("fotb7DzeHYw", sceUsbdBulkTransfer);
    fun("t4gUfGsjk+g", sceUsbdGetStringDescriptorAscii);
    fun("TOhg7P6kTH4", sceUsbdInit);
    fun("g2oYm1DitDg", sceUsbdGetStringDescriptor);
    fun("Hvd3S--n25w", sceUsbdFreeConfigDescriptor);
    fun("vrQXYRo1Gwk", sceUsbdOpenDeviceWithVidPid);
    fun("dJxro8Nzcjk", sceUsbdSetIsoPacketLengths);
    fun("RA2D9rFH-Uw", sceUsbdUnlockEvents);
    fun("rsl9KQ-agyA", sceUsbdGetDevice);
    fun("HarYYlaFGJY", sceUsbdClose);
    fun("ys2e9VRBPrY", sceUsbdWaitForEvent);
    fun("YJ0cMAlLuxQ", sceUsbdGetMaxPacketSize);
    fun("EQ6SCLMqzkM", sceUsbdFreeDeviceList);
    fun("-sgi7EeLSO8", sceUsbdFreeTransfer);
    fun("xqmkjHCEOSY", sceUsbdFillIsoTransfer);
    fun("S1o1C6yOt5g", sceUsbdGetActiveConfigDescriptor);
    fun("Y5go+ha6eDs", sceUsbdDetachKernelDriver);
    fun("7VGfMerK6m0", sceUsbdFillControlTransfer);
    fun("GQsAVJuy8gM", sceUsbdGetConfigDescriptorByValue);
    fun("AeGaY8JrAV4", sceUsbdLockEventWaiters);
    fun("FhU9oYrbXoA", sceUsbdSetConfiguration);
    fun("nuIRlpbxauM", sceUsbdGetMaxIsoPacketSize);
    fun("L7FoTZp3bZs", sceUsbdGetConfiguration);
    fun("AE+mHBHneyk", sceUsbdClaimInterface);
    fun("rxi1nCOKWc8", sceUsbdInterruptTransfer);
    fun("L0EHgZZNVas", sceUsbdSubmitTransfer);
#undef fun
}
} // namespace libSceUsbd
