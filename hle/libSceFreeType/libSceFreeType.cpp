#include "libSceFreeType.h"

namespace libSceFreeType {
PS4API int FT_Stream_GetChar() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int FT_New_Memory() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int FT_Face_GetCharsOfVariant() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int FT_GlyphLoader_Add() {
    UNIMPLEMENTED_FUNC();
    return 0;
}

PS4API int autofit_module_class() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int winfnt_driver_class() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int pcf_driver_class() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int pshinter_module_class() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int ft_raster1_renderer_class() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int bdf_driver_class() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int ft_raster5_renderer_class() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("2zKnvQlTfsw", autofit_module_class);
    fun("5IJlKspehi8", winfnt_driver_class);
    fun("XQbd3lyks0c", pcf_driver_class);
    fun("BRAPUfVBWfk", pshinter_module_class);
    fun("ocFN4P1KsWQ", ft_raster1_renderer_class);
    fun("mKA4m9Tc6-4", bdf_driver_class);
    fun("t7sxUY1f9+E", ft_raster5_renderer_class);

//    fun("jl0VxYZb8-o", FT_Stream_GetChar);
//    fun("tCMhTh7h8D4", FT_New_Memory);
//    fun("tl72PmqhXww", FT_Face_GetCharsOfVariant);
//    fun("UD7IFu7ML2U", FT_GlyphLoader_Add);
//    fun("Uw+BZJpflxk", FT_Alloc);
//    fun("OgFczHko0w8", FT_Done_Face);
//    fun("fHMsRluEMfI", FT_Free);
//    fun("obnSzeb-KXk", FT_Get_Char_Index);
//    fun("LPtlB241JSg", FT_Get_Kerning);
//    fun("omDz0x8lyHk", FT_Get_Sfnt_Table);
//    fun("M156RuYoUbU", FT_Load_Glyph);
//    fun("OeNH41MA2Vs", FT_Open_Face);
//    fun("hz-J3hohXW8", FT_QAlloc);
//    fun("xuY99Qcb108", FT_Set_Char_Size);
//    fun("iruPsrXCN7U", FT_Stream_Open);
//    fun("VVfxa65sWF0", FTA_Export_Module_smooth);
//    fun("fp-jO+c8Rfk", );
//    fun("774ZHX0Z3Cc", FT_MulFix);
//    fun("BgUMBGvhATU", FT_Outline_EmboldenXY);
//    fun("oM0zMNCfXKM", );
//    fun("1XCz6Gk0Lt4", );
//    fun("fPrLu1YGOaw", );
//    fun("8bkQ+YRnpOE", );
//    fun("gVf7xRLdWm8", );
//    fun("C-PpdS5zris", );
//    fun("g8eLxL9VAhU", );
//    fun("dlbnv6Wass8", );
//    fun("sylevnx-fqk", );
//    fun("0RSuGozMl9o", );
//    fun("J3HnvW8FJV8", );
//    fun("YpxR8CqGMIM", );
//    fun("83qtpn8+XnA", );
//    fun("JuzrPZClLew", );
//    fun("xqjAbn8VNGo", );
//    fun("g2p7fkJpuC0", );
//    fun("upeKra1FaQk", );
//    fun("Yu7mmGCjSYM", );
//    fun("2D3H+1nhwqQ", );
//    fun("4to3tThESn0", );
//    fun("sylevnx-fqk", );
//    fun("Yu7mmGCjSYM", );
//    fun("0RSuGozMl9o", );
//    fun("g2p7fkJpuC0", );
//    fun("J3HnvW8FJV8", );
//    fun("YpxR8CqGMIM", );
//    fun("83qtpn8+XnA", );
//    fun("4to3tThESn0", );
//    fun("JuzrPZClLew", );
//    fun("xqjAbn8VNGo", );
//    fun("upeKra1FaQk", );
//    fun("2D3H+1nhwqQ", );
#undef fun
}
} // namespace libSceFreeType