#pragma  once

#include "common.h"

typedef struct _SceFontLibrarySelection {
    char pad[0x20];
} * SceFontLibrarySelection;

typedef struct SceFontRendererSelectionStruct_  {
    char pad[0x20];
} *SceFontRendererSelection;

typedef struct SceFontRendererStruct_   {
    char pad[0x20];
} *SceFontRenderer;

namespace libSceFontFt {
    void load(export_funcs& exp);
}
