#include "libSceFontFt.h"

namespace libSceFontFt {

static _SceFontLibrarySelection fls;
static SceFontRendererSelectionStruct_ frs;

PS4API const SceFontLibrarySelection sceFontSelectLibraryFt(int value) {
    LOG_DEBUG("%d", value);
    return &fls;
}
PS4API const SceFontRendererSelection sceFontSelectRendererFt(int value) {
    LOG_DEBUG("%d", value);
    return &frs;
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
//    fun("oM+XCzVG3oM", sceFontSelectLibraryFt);
//    fun("Xx974EW-QFY", sceFontSelectRendererFt);
#undef fun
}
} // namespace libSceFontFt
