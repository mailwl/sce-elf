#include <cstring>
#include <ctime>
#include "libScePad.h"

namespace libScePad {

typedef enum ScePadButtonDataOffset {
    SCE_PAD_BUTTON_L3 = 0x00000002,
    SCE_PAD_BUTTON_R3 = 0x00000004,
    SCE_PAD_BUTTON_OPTIONS = 0x00000008,
    SCE_PAD_BUTTON_UP = 0x00000010,
    SCE_PAD_BUTTON_RIGHT = 0x00000020,
    SCE_PAD_BUTTON_DOWN = 0x00000040,
    SCE_PAD_BUTTON_LEFT = 0x00000080,
    SCE_PAD_BUTTON_L2 = 0x00000100,
    SCE_PAD_BUTTON_R2 = 0x00000200,
    SCE_PAD_BUTTON_L1 = 0x00000400,
    SCE_PAD_BUTTON_R1 = 0x00000800,
    SCE_PAD_BUTTON_TRIANGLE = 0x00001000,
    SCE_PAD_BUTTON_CIRCLE = 0x00002000,
    SCE_PAD_BUTTON_CROSS = 0x00004000,
    SCE_PAD_BUTTON_SQUARE = 0x00008000,
    SCE_PAD_BUTTON_TOUCH_PAD = 0x00100000,
    SCE_PAD_BUTTON_INTERCEPTED = 0x80000000,
} ScePadButtonDataOffset;

typedef struct ScePadOpenParam {
    uint8_t reserve[8];
} ScePadOpenParam;

typedef struct ScePadAnalogStick {
    uint8_t x;
    uint8_t y;
} ScePadAnalogStick;

typedef struct ScePadAnalogButtons {
    uint8_t l2;
    uint8_t r2;
    uint8_t padding[2];
} ScePadAnalogButtons;

typedef struct SceFQuaternion {
    float x, y, z, w;
} SceFQuaternion;

typedef struct SceFVector3 {
    float x, y, z;
} SceFVector3;

typedef struct ScePadTouch {
    uint16_t x;
    uint16_t y;
    uint8_t id;
    uint8_t reserve[3];
} ScePadTouch;

#define SCE_PAD_MAX_TOUCH_NUM 2

typedef struct ScePadTouchData {
    uint8_t touchNum;
    uint8_t reserve[3];
    uint8_t reserve1;
    ScePadTouch touch[SCE_PAD_MAX_TOUCH_NUM];
} ScePadTouchData;

typedef struct ScePadExtensionUnitData {
    uint32_t extensionUnitId;
    uint8_t reserve[1];
    uint8_t dataLength;
    uint8_t data[10];
} ScePadExtensionUnitData;

#define SCE_PAD_MAX_DEVICE_UNIQUE_DATA_SIZE 12
typedef struct ScePadData {
    uint32_t buttons;
    ScePadAnalogStick leftStick;
    ScePadAnalogStick rightStick;
    ScePadAnalogButtons analogButtons;
    SceFQuaternion orientation;
    SceFVector3 acceleration;
    SceFVector3 angularVelocity;
    ScePadTouchData touchData;
    bool connected;
    uint64_t timestamp;
    ScePadExtensionUnitData extensionUnitData;
    uint8_t connectedCount;
    uint8_t reserve[2];
    uint8_t deviceUniqueDataLen;
    uint8_t deviceUniqueData[SCE_PAD_MAX_DEVICE_UNIQUE_DATA_SIZE];
} ScePadData;

typedef struct ScePadTouchPadInformation {
    float pixelDensity;
    struct {
        uint16_t x;
        uint16_t y;
    } resolution;
} ScePadTouchPadInformation;

typedef struct ScePadStickInformation {
    uint8_t deadZoneLeft;
    uint8_t deadZoneRight;
} ScePadStickInformation;

typedef enum {
    SCE_PAD_DEVICE_CLASS_INVALID = -1,
    SCE_PAD_DEVICE_CLASS_STANDARD = 0,
    SCE_PAD_DEVICE_CLASS_GUITAR = 1,
    SCE_PAD_DEVICE_CLASS_DRUM = 2,
    SCE_PAD_DEVICE_CLASS_DJ_TURNTABLE = 3,
    SCE_PAD_DEVICE_CLASS_DANCEMAT = 4,
    SCE_PAD_DEVICE_CLASS_NAVIGATION = 5,
    SCE_PAD_DEVICE_CLASS_STEERING_WHEEL = 6,
    SCE_PAD_DEVICE_CLASS_STICK = 7,
    SCE_PAD_DEVICE_CLASS_FLIGHT_STICK = 8,
    SCE_PAD_DEVICE_CLASS_GUN = 9,
} ScePadDeviceClass;

typedef struct ScePadControllerInformation {
    ScePadTouchPadInformation touchPadInfo;
    ScePadStickInformation stickInfo;
    uint8_t connectionType;
    uint8_t connectedCount;
    bool connected;
    ScePadDeviceClass deviceClass;
    uint8_t reserve[8];
} ScePadControllerInformation;

typedef struct ScePadLightBarParam {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t reserve[1];
} ScePadLightBarParam;

PS4API int scePadClose() {
    LOG_DEBUG("%s()", __func__);
    return 0;
}
PS4API int scePadConnectPort() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadDeviceClassGetExtendedInformation() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadDeviceClassParseData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadDeviceOpen() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadDisableVibration() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadDisconnectDevice() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadDisconnectPort() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadEnableAutoDetect() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadEnableUsbConnection() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadGetCapability() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadGetControllerInformation(int32_t handle, ScePadControllerInformation* pInfo) {
    LOG_DEBUG("%s(%d, %p)", __FUNCTION__, handle, pInfo);
    pInfo->connected = true;
    pInfo->connectionType = 0;
    pInfo->connectedCount = 1;
    return 0;
}
PS4API int scePadGetDataInternal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadGetDeviceInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadGetHandle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadGetVersionInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadInit() {
    LOG_DEBUG("%s()", __FUNCTION__);
    return 0;
}
PS4API int scePadIsLightBarBaseBrightnessControllable() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadMbusInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadOpen(SceUserServiceUserId userId, int32_t type, int32_t index, const ScePadOpenParam* pParam) {
    LOG_DEBUG("%s(%d, %d, %d, %p)", __FUNCTION__, userId, type, index, pParam);
    return 1;
}
PS4API int scePadRead(int32_t handle, ScePadData* pData, int32_t num) {
    LOG_DEBUG("%s(%d, %p, %d)", __FUNCTION__, handle, pData, num);
    return 0;
}
PS4API int scePadReadState(int32_t handle, ScePadData* pData) {
    LOG_DEBUG("%s(%d, %p)", __FUNCTION__, handle, pData);
    ::memset(pData, 0, sizeof(ScePadData));
    pData->connected = true;
    pData->connectedCount = 1;
    // pData->buttons = SCE_PAD_BUTTON_CROSS;
    pData->timestamp = std::time(nullptr);
    return 0;
}
PS4API int scePadResetLightBar() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadResetOrientation() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadSetAngularVelocityDeadbandState() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadSetAutoPowerOffCount() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadSetButtonRemappingInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadSetConnection() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadSetForceIntercepted() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadSetLightBar(int32_t handle, const ScePadLightBarParam* pParam) {
    LOG_DEBUG("%d, %p", handle, pParam);
    return 0;
}
PS4API int scePadSetLightBarBaseBrightness() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadSetLightBarBlinking() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadSetMotionSensorState(int32_t handle, bool bEnable) {
    LOG_DEBUG("%s(%d, %d)", __FUNCTION__, handle, bEnable);
    return 0;
}
PS4API int scePadSetTiltCorrectionState() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadSetVibration() {
    // UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadShareOutputData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int scePadSwitchConnection() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("q1cHNfGycLI", scePadRead);
    fun("Uq6LgTJEmQs", scePadGetDataInternal);
    fun("Oi7FzRWFr0Y", scePadSwitchConnection);
    fun("MLA06oNfF+4", scePadSetConnection);
    fun("d2Qk-i8wGak", scePadIsLightBarBaseBrightnessControllable);
    fun("clVvL4ZDntw", scePadSetMotionSensorState);
    fun("ew647HuKi2Y", scePadSetAutoPowerOffCount);
    fun("hv1luiJrqQM", scePadInit);
    fun("0aziJjRZxqQ", scePadDisableVibration);
    fun("MbTt1EHYCTg", scePadSetButtonRemappingInfo);
    fun("kazv1NzSB8c", scePadConnectPort);
    fun("u1GRHp+oWoY", scePadGetHandle);
    fun("d7bXuEBycDI", scePadDeviceOpen);
    fun("DscD1i9HX1w", scePadResetLightBar);
    fun("IHPqcbc0zCA", scePadDeviceClassParseData);
    fun("6ncge5+l5Qs", scePadClose);
    fun("DD-KiRLBqkQ", scePadEnableUsbConnection);
    fun("r44mAxdSG+U", scePadSetAngularVelocityDeadbandState);
    fun("YndgXqQVV7c", scePadReadState);
    fun("gjP9-KQzoUk", scePadGetControllerInformation);
    fun("77ooWxGOIVs", scePadEnableAutoDetect);
    fun("etaQhgPHDRY", scePadSetLightBarBlinking);
    fun("zFJ35q3RVnY", scePadShareOutputData);
    fun("yFVnOdGxvZY", scePadSetVibration);
    fun("xk0AcarP3V4", scePadOpen);
    fun("CfwUlQtCFi4", scePadMbusInit);
    fun("AcslpN1jHR8", scePadDeviceClassGetExtendedInformation);
    fun("9ez71nWSvD0", scePadDisconnectPort);
    fun("pnZXireDoeI", scePadDisconnectDevice);
    fun("dhQXEvmrVNQ", scePadSetLightBarBaseBrightness);
    fun("vDLMoJLde8I", scePadSetTiltCorrectionState);
    fun("rIZnR6eSpvk", scePadResetOrientation);
    fun("QuOaoOcSOw0", scePadGetVersionInfo);
    fun("RR4novUEENY", scePadSetLightBar);
    fun("qtasqbvwgV4", scePadGetCapability);
    fun("4rS5zG7RFaM", scePadGetDeviceInfo);
    fun("lrjFx4xWnY8", scePadSetForceIntercepted);
#undef fun
}
} // namespace libScePad
