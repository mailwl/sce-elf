#include "libSceCompanionHttpd.h"

namespace libSceCompanionHttpd {

PS4API int sceCompanionHttpdAddHeader() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdGet2ndScreenStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdGetEvent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdGetUserId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdInitialize2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdOptParamInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdRegisterRequestBodyReceptionCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdRegisterRequestCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdRegisterRequestCallback2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdSetBody() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdSetStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdStart() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdStop() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdTerminate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdUnregisterRequestBodyReceptionCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionHttpdUnregisterRequestCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("B-QBMeFdNgY", sceCompanionHttpdGet2ndScreenStatus);
    fun("xweOi2QT-BE", sceCompanionHttpdUnregisterRequestCallback);
    fun("0SCgzfVQHpo", sceCompanionHttpdStop);
    fun("-0c9TCTwnGs", sceCompanionHttpdRegisterRequestCallback2);
    fun("k7F0FcDM-Xc", sceCompanionHttpdStart);
    fun("OA6FbORefbo", sceCompanionHttpdInitialize2);
    fun("8pWltDG7h6A", sceCompanionHttpdAddHeader);
    fun("r-2-a0c7Kfc", sceCompanionHttpdOptParamInitialize);
    fun("fHNmij7kAUM", sceCompanionHttpdRegisterRequestBodyReceptionCallback);
    fun("h3OvVxzX4qM", sceCompanionHttpdSetBody);
    fun("ykNpWs3ktLY", sceCompanionHttpdInitialize);
    fun("w7oz0AWHpT4", sceCompanionHttpdSetStatus);
    fun("Vku4big+IYM", sceCompanionHttpdGetEvent);
    fun("+-du9tWgE9s", sceCompanionHttpdTerminate);
    fun("OaWw+IVEdbI", sceCompanionHttpdRegisterRequestCallback);
    fun("ZSHiUfYK+QI", sceCompanionHttpdUnregisterRequestBodyReceptionCallback);
    fun("0SySxcuVNG0", sceCompanionHttpdGetUserId);
#undef fun
}
}
