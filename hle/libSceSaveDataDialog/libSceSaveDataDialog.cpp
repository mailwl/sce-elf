#include "libSceSaveDataDialog.h"

namespace libSceSaveDataDialog {

PS4API int sceSaveDataDialogClose() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataDialogGetResult() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataDialogGetStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataDialogInitialize() {
    LOG_DEBUG("");
    return 0;
}
PS4API int sceSaveDataDialogIsReadyToDisplay() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataDialogOpen() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataDialogProgressBarInc() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataDialogProgressBarSetValue() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataDialogTerminate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceSaveDataDialogUpdateStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("KK3Bdg1RWK0", sceSaveDataDialogUpdateStatus);
    fun("ERKzksauAJA", sceSaveDataDialogGetStatus);
    fun("fH46Lag88XY", sceSaveDataDialogClose);
    fun("V-uEeFKARJU", sceSaveDataDialogProgressBarInc);
    fun("4tPhsP6FpDI", sceSaveDataDialogOpen);
    fun("s9e3+YpRnzw", sceSaveDataDialogInitialize);
    fun("en7gNVnh878", sceSaveDataDialogIsReadyToDisplay);
    fun("YuH2FA7azqQ", sceSaveDataDialogTerminate);
    fun("yEiJ-qqr6Cg", sceSaveDataDialogGetResult);
    fun("hay1CfTmLyA", sceSaveDataDialogProgressBarSetValue);
#undef fun
}
}
