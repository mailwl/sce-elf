#include "libSceSysUtil.h"

namespace libSceSysUtil {

PS4API int sceSysUtilSendAddressingSystemNotification() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendAddressingSystemNotificationWithDeviceId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendAddressingSystemNotificationWithUserId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendAddressingSystemNotificationWithUserName() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendNotificationRequest() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendNpDebugNotificationRequest() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendSystemNotification() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendSystemNotification2() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendSystemNotificationWithAppId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendSystemNotificationWithAppInfo() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendSystemNotificationWithAppName() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendSystemNotificationWithAppNameRelatedToUser() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendSystemNotificationWithDeviceId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendSystemNotificationWithDeviceIdRelatedToUser() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendSystemNotificationWithErrorCode() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendSystemNotificationWithParams() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendSystemNotificationWithText() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendSystemNotificationWithTextRelatedToUser() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendSystemNotificationWithUserId() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendSystemNotificationWithUserName() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendSystemNotificationWithUserNameInfo() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
PS4API int sceSysUtilSendTrcCheckNotificationRequest() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}

PS4API int __7YODtoVLM() {
    LOG_DEBUG("Unimplemented %s\n",__FUNCTION__);
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("VHGBWkTe0Jg", sceSysUtilSendAddressingSystemNotificationWithUserName);
    fun("3vHcGnZpiN8", sceSysUtilSendSystemNotificationWithAppInfo);
    fun("SxYUo6o6mjo", sceSysUtilSendSystemNotificationWithDeviceId);
    fun("yKfATNkQZdc", sceSysUtilSendAddressingSystemNotificationWithDeviceId);
    fun("iSlI1LKy7Bg", sceSysUtilSendSystemNotificationWithErrorCode);
    fun("jTNAhVZ-5Nc", sceSysUtilSendSystemNotification);
    fun("AodT6gE5Qpk", sceSysUtilSendSystemNotificationWithParams);
    fun("KQxuDpbldZs", sceSysUtilSendSystemNotificationWithAppId);
    fun("Eu7QNB9x1r0", sceSysUtilSendNpDebugNotificationRequest);
    fun("+RZFlwV4k3I", sceSysUtilSendSystemNotificationWithAppNameRelatedToUser);
    fun("HJyZ8CZGVoY", sceSysUtilSendSystemNotificationWithTextRelatedToUser);
    fun("Btgtlzfk9EQ", sceSysUtilSendSystemNotificationWithText);
    fun("DC2NSz1IDyk", sceSysUtilSendSystemNotificationWithUserName);
    fun("TnvxGzkFmbo", sceSysUtilSendAddressingSystemNotification);
    fun("Kl9aRhwtXBM", sceSysUtilSendSystemNotificationWithUserNameInfo);
    fun("ISeIA7PBwyc", sceSysUtilSendSystemNotificationWithDeviceIdRelatedToUser);
    fun("AAk6w60xP3A", sceSysUtilSendNotificationRequest);
    fun("YbAfwT8fO7Q", sceSysUtilSendSystemNotificationWithUserId);
    fun("BgOJY0UvOQ4", sceSysUtilSendAddressingSystemNotificationWithUserId);
    fun("f21DEbadT98", sceSysUtilSendSystemNotification2);
    fun("N4AToF3po08", sceSysUtilSendSystemNotificationWithAppName);
    fun("-0FdPC78x7U", sceSysUtilSendTrcCheckNotificationRequest);
    fun("-+7YODtoVLM", __7YODtoVLM); // libSceSysUtil
#undef fun
}
}
