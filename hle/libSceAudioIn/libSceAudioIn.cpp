#include "libSceAudioIn.h"

namespace libSceAudioIn {

PS4API int sceAudioInChangeAppModuleState() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInClose() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInGetGain() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInGetRerouteCount() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInGetSilentState() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInHqOpen() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInHqOpenEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInInput() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInInputs() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInOpen() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInOpenEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInSetConnections() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInSetDevConnection() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInSetPortConnections() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInSetPortStatuses() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInSetSparkParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInSetSparkSideTone() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceAudioInSetUserMute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("hWMCAPpqzDo", sceAudioInSetSparkSideTone);
    fun("IQtWgnrw6v8", sceAudioInChangeAppModuleState);
    fun("5NE8Sjc7VC8", sceAudioInOpen);
    fun("+DY07NwJb0s", sceAudioInOpenEx);
    fun("NUWqWguYcNQ", sceAudioInSetPortStatuses);
    fun("Jh6WbHhnI68", sceAudioInClose);
    fun("rmgXsZ-2Tyk", sceAudioInInputs);
    fun("S-rDUfQk9sg", sceAudioInGetGain);
    fun("YeBSNVAELe4", sceAudioInSetConnections);
    fun("rcgv2ciDrtc", sceAudioInSetDevConnection);
    fun("CTh72m+IYbU", sceAudioInHqOpenEx);
    fun("nya-R5gDYhM", sceAudioInHqOpen);
    fun("tQpOPpYwv7o", sceAudioInSetPortConnections);
    fun("U0ivfdKFZbA", sceAudioInSetSparkParam);
    fun("SxQprgjttKE", sceAudioInInit);
    fun("3shKmTrTw6c", sceAudioInGetRerouteCount);
    fun("LozEOU8+anM", sceAudioInInput);
    fun("BohEAQ7DlUE", sceAudioInGetSilentState);
    fun("arJp991xk5k", sceAudioInSetUserMute);
#undef fun
}
}
