#include "libSceCompanionUtil.h"

namespace libSceCompanionUtil {

PS4API int sceCompanionUtilGetEvent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionUtilGetRemoteOskEvent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionUtilInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionUtilOptParamInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceCompanionUtilTerminate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("xb1xlIhf0QY", sceCompanionUtilInitialize);
    fun("H1fYQd5lFAI", sceCompanionUtilTerminate);
    fun("cE5Msy11WhU", sceCompanionUtilGetEvent);
    fun("MaVrz79mT5o", sceCompanionUtilGetRemoteOskEvent);
    fun("IPN-FRSrafk", sceCompanionUtilOptParamInitialize);
#undef fun
}
}
