#include "libSceWebBrowserDialog.h"

namespace libSceWebBrowserDialog {

PS4API int sceWebBrowserDialogClose() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceWebBrowserDialogGetResult() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceWebBrowserDialogGetStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceWebBrowserDialogInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceWebBrowserDialogOpen() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceWebBrowserDialogOpenForPredeterminedContent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceWebBrowserDialogResetCookie() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceWebBrowserDialogSetCookie() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceWebBrowserDialogTerminate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceWebBrowserDialogUpdateStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
    void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
        fun("PSK+Eik919Q", sceWebBrowserDialogClose);
        fun("TZnDVkP91Rg", sceWebBrowserDialogSetCookie);
        fun("FraP7debcdg", sceWebBrowserDialogOpen);
        fun("jqb7HntFQFc", sceWebBrowserDialogInitialize);
        fun("Cya+jvTtPqg", sceWebBrowserDialogResetCookie);
        fun("O7dIZQrwVFY", sceWebBrowserDialogOpenForPredeterminedContent);
        fun("ocHtyBwHfys", sceWebBrowserDialogTerminate);
        fun("CFTG6a8TjOU", sceWebBrowserDialogGetStatus);
        fun("vCaW0fgVQmc", sceWebBrowserDialogGetResult);
        fun("h1dR-t5ISgg", sceWebBrowserDialogUpdateStatus);
#undef fun
    }
}
