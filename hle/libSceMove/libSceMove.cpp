#include "libSceMove.h"


namespace libSceMove {
    PS4API void sceMoveOpen() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceMoveGetDeviceInfo() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceMoveReadStateRecent() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceMoveTerm() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceMoveInit() {
        UNIMPLEMENTED_FUNC();
    }

    void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
        fun("HzC60MfjJxU", sceMoveOpen);
        fun("GWXTyxs4QbE", sceMoveGetDeviceInfo);
        fun("f2bcpK6kJfg", sceMoveReadStateRecent);
        fun("tsZi60H4ypY", sceMoveTerm);
        fun("j1ITE-EoJmE", sceMoveInit);
#undef fun
    }
}
