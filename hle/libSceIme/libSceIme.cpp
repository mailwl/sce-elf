#include "libSceIme.h"

namespace libSceIme {
PS4API void sceImeKeyboardClose() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceImeKeyboardOpen() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceImeKeyboardGetResourceId() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceImeUpdate() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceImeClose() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceImeOpen() {
    UNIMPLEMENTED_FUNC();
}
PS4API void WmYDzdC4EHI() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceImeParamInit() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceImeSetCaret() {
    UNIMPLEMENTED_FUNC();
}
PS4API void sceImeSetText() {
    UNIMPLEMENTED_FUNC();
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("PMVehSlfZ94", sceImeKeyboardClose);         // libSceIme
    fun("eaFXjfJv3xs", sceImeKeyboardOpen);          // libSceIme
    fun("dKadqZFgKKQ", sceImeKeyboardGetResourceId); // libSceIme
    fun("-4GCfYdNF1s", sceImeUpdate);                // libSceIme
    fun("TmVP8LzcFcY", sceImeClose);                 // libSceIme
    fun("RPydv-Jr1bc", sceImeOpen);                  // libSceIme
    fun("WmYDzdC4EHI", sceImeParamInit);             // libSceIme
    fun("WLxUN2WMim8", sceImeSetCaret);              // libSceIme
    fun("ieCNrVrzKd4", sceImeSetText);               // libSceIme
#undef fun
}
} // namespace libSceIme