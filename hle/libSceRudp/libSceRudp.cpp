#include "libSceRudp.h"

namespace libSceRudp {

PS4API int sceRudpAccept() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpActivate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpBind() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpCreateContext() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpEnableInternalIOThread() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpEnableInternalIOThread2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpEnd() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpFlush() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpGetContextStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpGetLocalInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpGetMaxSegmentSize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpGetNumberOfPacketsToRead() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpGetOption() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpGetRemoteInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpGetSizeReadable() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpGetSizeWritable() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpGetStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpInit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpInitiate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpListen() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpNetFlush() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpNetReceived() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpPollCancel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpPollControl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpPollCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpPollDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpPollWait() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpProcessEvents() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpRead() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpSetEventHandler() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpSetMaxSegmentSize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpSetOption() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpTerminate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceRudpWrite() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("sAZqO2+5Qqo", sceRudpGetSizeReadable);
    fun("MVbmLASjn5M", sceRudpPollCreate);
    fun("3hBvwqEwqj8", sceRudpEnd);
    fun("+BJ9svDmjYs", sceRudpNetFlush);
    fun("CAbbX6BuQZ0", sceRudpCreateContext);
    fun("9U9m1YH0ScQ", sceRudpProcessEvents);
    fun("tYVWcWDnctE", sceRudpListen);
    fun("LjwbHpEeW0A", sceRudpPollDestroy);
    fun("vPzJldDSxXc", sceRudpNetReceived);
    fun("KaPL3fbTLCA", sceRudpWrite);
    fun("2G7-vVz9SIg", sceRudpGetLocalInfo);
    fun("Qignjmfgha0", sceRudpGetRemoteInfo);
    fun("fJ51weR1WAI", sceRudpEnableInternalIOThread2);
    fun("J-6d0WTjzMc", sceRudpActivate);
    fun("szEVu+edXV4", sceRudpInitiate);
    fun("vfrL8gPlm2Y", sceRudpGetMaxSegmentSize);
    fun("i3STzxuwPx0", sceRudpGetStatus);
    fun("6PBNpsgyaxw", sceRudpEnableInternalIOThread);
    fun("wIJsiqY+BMk", sceRudpGetContextStatus);
    fun("uQiK7fjU6y8", sceRudpAccept);
    fun("yzeXuww-UWg", sceRudpPollCancel);
    fun("beAsSTVWVPQ", sceRudpSetMaxSegmentSize);
    fun("M6ggviwXpLs", sceRudpPollWait);
    fun("Ms0cLK8sTtE", sceRudpFlush);
    fun("l4SLBpKUDK4", sceRudpBind);
    fun("fRc1ahQppR4", sceRudpGetSizeWritable);
    fun("rZqWV3eXgOA", sceRudpRead);
    fun("0yzYdZf0IwE", sceRudpSetOption);
    fun("haMpc7TFx0A", sceRudpPollControl);
    fun("OMYRTU0uc4w", sceRudpTerminate);
    fun("amuBfI-AQc4", sceRudpInit);
    fun("mCQIhSmCP6o", sceRudpGetOption);
    fun("Px0miD2LuW0", sceRudpGetNumberOfPacketsToRead);
    fun("SUEVes8gvmw", sceRudpSetEventHandler);
#undef fun
}
}
