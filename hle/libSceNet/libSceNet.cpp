#include "libSceNet.h"

namespace libSceNet {

PS4API int in6addr_any() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int in6addr_loopback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetAccept() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetAllocateAllRouteInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetBandwidthControlGetDefaultParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetBandwidthControlGetIfParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetBandwidthControlGetPolicy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetBandwidthControlSetDefaultParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetBandwidthControlSetIfParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetBandwidthControlSetPolicy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetBind() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetClearDnsCache() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigAddArp() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigAddIfaddr() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigAddMRoute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigAddRoute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigAddRoute6() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigCleanUpAllInterfaces() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigDelArp() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigDelDefaultRoute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigDelDefaultRoute6() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigDelIfaddr() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigDelIfaddr6() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigDelMRoute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigDelRoute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigDelRoute6() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigDownInterface() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigEtherGetLinkMode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigEtherPostPlugInOutEvent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigEtherSetLinkMode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigFlushRoute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigGetDefaultRoute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigGetDefaultRoute6() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigGetIfaddr() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigGetIfaddr6() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigRoutingShowRoutingConfig() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigRoutingShowtCtlVar() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigRoutingStart() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigRoutingStop() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigSetDefaultRoute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigSetDefaultRoute6() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigSetDefaultScope() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigSetIfaddr() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigSetIfaddr6() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigSetIfmtu() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigUpInterface() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanAdhocClearWakeOnWlan() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanAdhocCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanAdhocGetWakeOnWlanInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanAdhocJoin() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanAdhocLeave() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanAdhocPspEmuClearWakeOnWlan() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanAdhocPspEmuGetWakeOnWlanInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanAdhocPspEmuSetWakeOnWlan() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanAdhocScanJoin() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanAdhocSetExtInfoElement() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanAdhocSetWakeOnWlan() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanApStart() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanApStop() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanBackgroundScanQuery() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanBackgroundScanStart() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanBackgroundScanStop() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanDiagGetDeviceInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanDiagSetAntenna() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanDiagSetTxFixedRate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanGetDeviceConfig() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanInfraGetRssiInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanInfraLeave() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanInfraScanJoin() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanScan() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConfigWlanSetDeviceConfig() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetConnect() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetControl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetDhcpdStart() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetDhcpdStop() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetDhcpGetAutoipInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetDhcpGetInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetDhcpGetInfoEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetDhcpStart() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetDhcpStop() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetDumpAbort() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetDumpCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetDumpDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetDumpRead() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetDuplicateIpStart() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetEpollAbort() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetEpollControl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetEpollCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetEpollDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetEpollWait() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetErrnoLoc() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetEtherNtostr() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetEtherStrton() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetEventCallbackCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetEventCallbackDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetEventCallbackGetError() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetEventCallbackWaitCB() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetFreeAllRouteInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetArpInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetDnsInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetIfList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetIfListOnce() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetIfName() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetIfnameNumList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetMacAddress() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetMemoryPoolStats() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetNameToIndex() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetpeername() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetRandom() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetRouteInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetSockInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetSockInfo6() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetsockname() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetsockopt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetStatisticsInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetGetSystemTime() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetHtonl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetHtonll() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetHtons() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetInetNtop() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetInetPton() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetInetPtonEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetInit() {
    LOG_DEBUG("%s()\n", __func__);
    return 0;
}
PS4API int sceNetIoctl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetListen() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetMemoryAllocate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetMemoryFree() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetNtohl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetNtohll() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetNtohs() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetPoolCreate(const char* name, int size, int flags) {
    LOG_DEBUG("%s('%s', %d, %d)\n", __func__, name, size, flags);
    return 0;
}
PS4API int sceNetPoolDestroy() {
    // UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetPppoeStart() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetPppoeStop() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetRecv() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetRecvfrom() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetRecvmsg() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetResolverAbort() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetResolverCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetResolverDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetResolverGetError() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetResolverStartAton() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetResolverStartAton6() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetResolverStartNtoa() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetResolverStartNtoa6() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetResolverStartNtoaMultipleRecords() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetResolverStartNtoaMultipleRecordsEx() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetSend() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetSendmsg() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetSendto() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetSetDnsInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetSetDnsInfoToKernel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetSetsockopt() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetShowIfconfig() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetShowIfconfigWithMemory() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetShowNetstat() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetShowNetstatWithMemory() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetShowPolicy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetShowPolicyWithMemory() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetShowRoute() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetShowRoute6() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetShowRouteWithMemory() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetShutdown() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetSocket() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetSocketAbort() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetSocketClose() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetSyncCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetSyncDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetSyncGet() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetSyncSignal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetSyncWait() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetSysctl() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetTerm() {
    // UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetThreadCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetThreadExit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetThreadJoin() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNetUsleep() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
//    fun("6uYcvVjH7Ms", sceNetConfigWlanApStop);
//    fun("sT4nBQKUPqM", sceNetResolverStartNtoaMultipleRecordsEx);
//    fun("3zRdT3O2Kxo", sceNetSysctl);
//    fun("DrZuCQDnm3w", sceNetDhcpGetInfoEx);
//    fun("-Mi5hNiWC4c", sceNetConfigWlanScan);
//    fun("cEMX1VcPpQ8", sceNetShowRoute);
//    fun("toi8xxcSfJ0", sceNetConfigRoutingStop);
//    fun("7xYdUWg1WdY", sceNetConfigWlanAdhocScanJoin);
//    fun("ZLdJyQJUMkM", sceNetConfigWlanAdhocCreate);
//    fun("atGfzCaXMak", sceNetSyncDestroy);
//    fun("mOUkgTaSkJU", sceNetConfigEtherGetLinkMode);
//    fun("w21YgGGNtBk", sceNetEpollAbort);
//    fun("ZVw46bsasAk", sceNetEpollControl);
//    fun("5Yl1uuh5i-A", sceNetConfigDelMRoute);
//    fun("Xma8yHmV+TQ", sceNetConfigWlanAdhocJoin);
//    fun("304ooNZxWDY", sceNetRecvfrom);
//    fun("QGOqGPnk5a4", sceNetPppoeStart);
//    fun("EAl7xvi7nXg", sceNetConfigSetDefaultRoute);
//    fun("+S-2-jlpaBo", sceNetGetNameToIndex);
//    fun("Inp1lfL+Jdw", sceNetEpollDestroy);
//    fun("0239JNsI6PE", sceNetConfigDelIfaddr);
//    fun("vbZLomImmEE", sceNetConfigRoutingShowRoutingConfig);
//    fun("PDkapOwggRw", sceNetBandwidthControlGetPolicy);
//    fun("ge7g15Sqhks", sceNetConfigAddIfaddr);
//    fun("WxislcDAW5I", sceNetShowNetstat);
//    fun("lFJb+BlPK1c", sceNetConfigGetDefaultRoute);
//    fun("Xn2TA2QhxHc", sceNetInetPtonEx);
//    fun("2mKX2Spso7I", sceNetSetsockopt);
//    fun("5isaotjMWlA", sceNetEventCallbackWaitCB);
//    fun("FU6NK4RHQVE", sceNetConfigGetIfaddr6);
//    fun("cMA8f6jI6s0", sceNetConfigWlanBackgroundScanStart);
//    fun("Rbvt+5Y2iEw", sceNetNtohs);
//    fun("RuVwHEW6dM4", sceNetConfigDelDefaultRoute);
//    fun("G3O2j9f5z00", sceNetGetRandom);
//    fun("0-XSSp1kEFM", sceNetShowRouteWithMemory);
//    fun("9vA2aW+CHuA", sceNetInetNtop);
//    fun("rMyh97BU5pY", sceNetGetMemoryPoolStats);
//    fun("fCa7-ihdRdc", sceNetShowRoute6);
//    fun("KirVfZbqniw", sceNetThreadExit);
//    fun("8Kh+1eidI3c", sceNetConfigSetIfaddr);
//    fun("bjrzRLFali0", sceNetUsleep);
//    fun("c8IRpl4L74I", sceNetBandwidthControlGetDefaultParam);
//    fun("Q4qBuN-c0ZM", sceNetSocket);
//    fun("SF47kB2MNTo", sceNetEpollCreate);
//    fun("YWTpt45PxbI", sceNetDumpRead);
//    fun("Apb4YDxKsRI", sceNetResolverStartAton);
//    fun("g4DKkzV2qC4", sceNetBandwidthControlSetIfParam);
//    fun("9wO9XrMsNhc", sceNetRecv);
//    fun("dgJBaeJnGpo", sceNetPoolCreate);
//    fun("4wDGvfhmkmk", sceNetConfigDelRoute6);
//    fun("Z-8Jda650Vk", sceNetSyncSignal);
//    fun("cTGkc6-TBlI", sceNetTerm);
//    fun("J5i3hiLJMPk", sceNetResolverGetError);
//    fun("0MT2l3uIX7c", sceNetGetIfName);
//    fun("hvCXMwd45oc", sceNetConfigDelIfaddr6);
//    fun("3CHi1K1wsCQ", sceNetHtonll);
//    fun("Nd91WaWmG2w", sceNetResolverStartNtoa);
//    fun("QO7+2E3cD-U", sceNetConfigDelRoute);
//    fun("ix4LWXd12F0", sceNetDhcpGetInfo);
//    fun("jzP0MoZpYnI", sceNetEventCallbackDestroy);
//    fun("P4zZXE7bpsA", sceNetBandwidthControlSetDefaultParam);
//    fun("wvuUDv0jrMI", sceNetRecvmsg);
//    fun("TCkRD0DWNLg", sceNetGetpeername);
//    fun("k1V1djYpk7k", sceNetShowIfconfig);
//    fun("Q6T-zIblNqk", sceNetDhcpdStart);
//    fun("eyLyLJrdEOU", sceNetClearDnsCache);
//    fun("TwjkDIPdZ1Q", sceNetDuplicateIpStart);
//    fun("FIV95WE1EuE", sceNetPppoeStop);
//    fun("eszLdtIMfQE", sceNetConfigRoutingStart);
//    fun("pQGpHYopAIY", sceNetNtohl);
//    fun("K4o48GTNbSc", sceNetConfigWlanAdhocLeave);
//    fun("mCLdiNIKtW0", sceNetConfigGetDefaultRoute6);
//    fun("b-bFZvNV59I", sceNetEtherStrton);
//    fun("UMlVCy7RX1s", sceNetConfigDelDefaultRoute6);
//    fun("Nlev7Lg8k3A", sceNetInit);
//    fun("9oiOWQ5FMws", sceNetConfigWlanDiagSetAntenna);
//    fun("bghgkeLKq1Q", sceNetDumpCreate);
//    fun("HoV-GJyx7YY", sceNetGetIfList);
//    fun("MDbg-oAj8Aw", sceNetConfigWlanBackgroundScanQuery);
//    fun("18KNgSvYx+Y", sceNetConfigFlushRoute);
//    fun("lDTIbqNs0ps", sceNetControl);
//    fun("273-I-zD8+8", sceNetConfigWlanInfraScanJoin);
//    fun("2eKbgcboJso", sceNetSendmsg);
//    fun("C-+JPjaEhdA", sceNetConfigWlanAdhocPspEmuSetWakeOnWlan);
//    fun("ejwa0hWWhDs", sceNetConfigGetIfaddr);
//    fun("1j4DZ5dXbeQ", sceNetConfigWlanAdhocPspEmuGetWakeOnWlanInfo);
//    fun("OXXX4mUk3uk", sceNetConnect);
//    fun("C4UgDHHPvdw", sceNetResolverCreate);
//    fun("kOj1HiAGE54", sceNetListen);
//    fun("7Z1hhsEmkQU", sceNetBandwidthControlSetPolicy);
//    fun("Cyjl1yzi1qY", sceNetConfigAddRoute);
//    fun("oGEBX0eXGFs", sceNetConfigUpInterface);
//    fun("xZ54Il-u1vs", sceNetDumpDestroy);
//    fun("GA5ZDaLtUBE", sceNetGetStatisticsInfo);
//    fun("XCuA-GqjA-k", in6addr_loopback);
//    fun("45ggEzakPJQ", sceNetSocketClose);
//    fun("zvzWA5IZMsg", sceNetResolverStartAton6);
//    fun("+ezgWao0wo8", sceNetDumpAbort);
//    fun("3WzWV86AJ3w", sceNetConfigDownInterface);
//    fun("HJt+4x-CnY0", sceNetConfigDelArp);
//    fun("b9Ft65tqvLk", sceNetBandwidthControlGetIfParam);
//    fun("6A6EweB3Dto", sceNetConfigWlanAdhocClearWakeOnWlan);
//    fun("tB3BB8AsrjU", sceNetEventCallbackGetError);
//    fun("bErx49PgxyY", sceNetBind);
//    fun("Pkx0lwWVzmQ", sceNetConfigWlanInfraGetRssiInfo);
//    fun("3T5aIe-7L84", sceNetConfigWlanBackgroundScanStop);
//    fun("U1q6DrPbY6k", sceNetConfigWlanSetDeviceConfig);
//    fun("tOrRi-v3AOM", sceNetNtohll);
//    fun("sAleh-BoxLA", sceNetSyncGet);
//    fun("Yr3UeApLWTY", sceNetConfigWlanAdhocGetWakeOnWlanInfo);
//    fun("cWGGXoeZUzA", sceNetEventCallbackCreate);
//    fun("v6M4txecCuo", sceNetEtherNtostr);
//    fun("ZvKgNrrLCCQ", sceNetConfigWlanAdhocPspEmuClearWakeOnWlan);
//    fun("p2vxsE2U3RQ", sceNetGetSystemTime);
//    fun("hoOAofhhRvE", sceNetGetsockname);
//    fun("RCCY01Xd+58", sceNetResolverStartNtoaMultipleRecords);
//    fun("Ea2NaVMQNO8", sceNetConfigAddArp);
//    fun("pRbEzaV30qI", sceNetThreadJoin);
//    fun("2ee14ktE1lw", sceNetFreeAllRouteInfo);
//    fun("ghqRRVQxqKo", sceNetIoctl);
//    fun("IkBCxG+o4Nk", sceNetConfigWlanInfraLeave);
//    fun("E8dTcvQw3hg", sceNetShowIfconfigWithMemory);
//    fun("NP5gxDeYhIM", sceNetSyncWait);
//    fun("6Nx1hIQL9h8", sceNetGetRouteInfo);
//    fun("a6sS6iSE0IA", sceNetConfigRoutingShowtCtlVar);
//    fun("6Oc0bLsIYe0", sceNetGetMacAddress);
//    fun("Wzv6dngR-DQ", sceNetDhcpStart);
//    fun("zl35YNs9jnI", sceNetResolverStartNtoa6);
//    fun("tk0p0JmiBkM", sceNetShowPolicy);
//    fun("beRjXBn-z+o", sceNetSend);
//    fun("ahiOMqoYYMc", sceNetGetIfListOnce);
//    fun("j-Op3ibRJaQ", sceNetThreadCreate);
//    fun("HKIa-WH0AZ4", sceNetMemoryAllocate);
//    fun("q8j9OSdnN1Y", sceNetGetArpInfo);
//    fun("B-M6KjO8-+w", sceNetSetDnsInfo);
//    fun("MzA1YrRE6rA", sceNetConfigCleanUpAllInterfaces);
//    fun("221fvqVs+sQ", sceNetMemoryFree);
//    fun("PNDDxnqqtk4", sceNetConfigWlanGetDeviceConfig);
//    fun("Q7ee2Uav5f8", sceNetConfigWlanAdhocSetExtInfoElement);
//    fun("+3KMyS93TOs", sceNetConfigWlanDiagGetDeviceInfo);
//    fun("xaOTiuxIQNY", sceNetConfigWlanAdhocSetWakeOnWlan);
//    fun("6AN7OlSMWk0", sceNetDhcpStop);
//    fun("Bu+L5r1lKRg", sceNetConfigAddRoute6);
//    fun("H5WHYRfDkR0", sceNetShowNetstatWithMemory);
//    fun("gvD1greCu0A", sceNetSendto);
//    fun("QltDK6wWqF0", sceNetConfigEtherSetLinkMode);
//    fun("KhQxhlEslo0", sceNetDhcpGetAutoipInfo);
//    fun("xphrZusl78E", sceNetGetsockopt);
//    fun("xwWm8jzrpeM", sceNetDhcpdStop);
//    fun("K7RlrTkI-mw", sceNetPoolDestroy);
//    fun("HQOwnfMGipQ", sceNetErrnoLoc);
//    fun("zJGf8xjFnQE", sceNetSocketAbort);
//    fun("ZRAJo-A-ukc", in6addr_any);
//    fun("iWQWrwiSt8A", sceNetHtons);
//    fun("drjIbDbA7UQ", sceNetEpollWait);
//    fun("PIWqhn9oSxc", sceNetAccept);
//    fun("PcdLABhYga4", sceNetAllocateAllRouteInfo);
//    fun("dbrSNEuZfXI", sceNetShowPolicyWithMemory);
//    fun("8s+T0bJeyLQ", sceNetSetDnsInfoToKernel);
//    fun("Cidi9Y65mP8", sceNetGetSockInfo6);
//    fun("4zLOHbt3UFk", sceNetConfigSetDefaultRoute6);
//    fun("fHr45B97n0U", sceNetConfigWlanDiagSetTxFixedRate);
//    fun("nCL0NyZsd5A", sceNetGetDnsInfo);
//    fun("TSM6whtekok", sceNetShutdown);
//    fun("6AJE2jKg-c0", sceNetSyncCreate);
//    fun("5lrSEHdqyos", sceNetGetIfnameNumList);
//    fun("yaVAdLDxUj0", sceNetConfigSetDefaultScope);
//    fun("AzqoBha7js4", sceNetResolverAbort);
//    fun("9T2pDF2Ryqg", sceNetHtonl);
//    fun("hLuXdjHnhiI", sceNetGetSockInfo);
//    fun("QlRJWya+dtE", sceNetConfigWlanApStart);
//    fun("8Kcp5d-q1Uo", sceNetInetPton);
//    fun("FDHr4Iz7dQU", sceNetConfigAddMRoute);
//    fun("pF3Vy1iZ5bs", sceNetConfigEtherPostPlugInOutEvent);
//    fun("kJlYH5uMAWI", sceNetResolverDestroy);
//    fun("s31rYkpIMMQ", sceNetConfigSetIfmtu);
//    fun("QJbV3vfBQ8Q", sceNetConfigSetIfaddr6);
#undef fun
}
} // namespace libSceNet
