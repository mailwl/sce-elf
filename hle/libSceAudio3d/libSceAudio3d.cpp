
#include "libSceAudio3d.h"

namespace libSceAudio3d {
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
#undef fun
}
}
