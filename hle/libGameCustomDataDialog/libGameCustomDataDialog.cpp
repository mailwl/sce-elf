#include "libGameCustomDataDialog.h"

namespace libGameCustomDataDialog {
PS4API void sceGameCustomDataDialogInitialize() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
}

PS4API void sceGameCustomDataDialogOpen() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
}

PS4API void sceGameCustomDataDialogTerminate() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
}

PS4API void sceGameCustomDataDialogUpdateStatus() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
}

PS4API void sceGameCustomDataDialogGetResult() {
    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
}

void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("xtbb-2f703A", sceGameCustomDataDialogInitialize);   // libGameCustomDataDialog
    fun("5TvttyRuU84", sceGameCustomDataDialogOpen);         // libGameCustomDataDialog
    fun("HwEtHFCpU5U", sceGameCustomDataDialogTerminate);    // libGameCustomDataDialog
    fun("PkdLsRA4ON0", sceGameCustomDataDialogUpdateStatus); // libGameCustomDataDialog
    fun("sJptZwvs1is", sceGameCustomDataDialogGetResult);    // libGameCustomDataDialog
#undef fun
}
} // namespace libGameCustomDataDialog
