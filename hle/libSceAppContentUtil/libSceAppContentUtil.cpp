#include "libSceAppContentUtil.h"

namespace libSceAppContentUtil {
typedef struct SceAppContentInitParam {
    char reserved[32];
} SceAppContentInitParam;

typedef uint32_t SceAppContentMediaType;
typedef uint32_t SceAppContentBootAttribute;

typedef struct SceAppContentBootParam {
    char reserved1[4];
    SceAppContentBootAttribute attr;
    char reserved2[32];
} SceAppContentBootParam;

//PS4API int _Z5dummyv() {
//    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentAddcontDelete() {
//    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentAddcontEnqueueDownload() {
//    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentAddcontMount() {
//    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentAddcontUnmount() {
//    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentAppParamGetInt() {
//    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentAppParamGetString() {
//    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentDownloadDataFormat() {
//    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentDownloadDataGetAvailableSpaceKb() {
//    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentGetAddcontDownloadProgress() {
//    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentGetAddcontInfo() {
//    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentGetAddcontInfoList() {
//    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentGetEntitlementKey() {
//    LOG_DEBUG("Unimplemented %s\n", __FUNCTION__);
//    return 0;
//}
PS4API int sceAppContentInitialize(const SceAppContentInitParam* initParam, SceAppContentBootParam* bootParam) {
    LOG_DEBUG("(%p, %p)", initParam, bootParam);
    return 0;
}
//PS4API int sceAppContentTemporaryDataFormat() {
//    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentTemporaryDataGetAvailableSpaceKb() {
//    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentTemporaryDataMount() {
//    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentTemporaryDataMount2() {
//    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
//    return 0;
//}
//PS4API int sceAppContentTemporaryDataUnmount() {
//    LOG_DEBUG("Unimplemented %s", __FUNCTION__);
//    return 0;
//}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
//    fun("XTWR0UXvcgs", sceAppContentGetEntitlementKey);
//    fun("CN7EbEV7MFU", sceAppContentDownloadDataFormat);
//    fun("xnd8BJzAxmk", sceAppContentGetAddcontInfoList);
//    fun("a5N7lAG0y2Q", sceAppContentTemporaryDataFormat);
//    fun("7gxh+5QubhY", sceAppContentAddcontEnqueueDownload);
//    fun("bcolXMmp6qQ", sceAppContentTemporaryDataUnmount);
//    fun("3rHWaV-1KC4", sceAppContentAddcontUnmount);
//    fun("SaKib2Ug0yI", sceAppContentTemporaryDataGetAvailableSpaceKb);
//    fun("7bOLX66Iz-U", sceAppContentTemporaryDataMount);
//    fun("AS45QoYHjc4", _Z5dummyv);
//    fun("+OlXCu8qxUk", sceAppContentAppParamGetString);
//    fun("ZiATpP9gEkA", sceAppContentAddcontDelete);
//    fun("VANhIWcqYak", sceAppContentAddcontMount);
//    fun("5bvvbUSiFs4", sceAppContentGetAddcontDownloadProgress);
//    fun("Gl6w5i0JokY", sceAppContentDownloadDataGetAvailableSpaceKb);
//    fun("m47juOmH0VE", sceAppContentGetAddcontInfo);
//    fun("99b82IKXpH4", sceAppContentAppParamGetInt);
    fun("R9lA82OraNs", sceAppContentInitialize);
//    fun("buYbeLOGWmA", sceAppContentTemporaryDataMount2);
#undef fun
}
} // namespace libSceAppContentUtil
