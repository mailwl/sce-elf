#include "libSceUlt.h"

namespace libSceUlt {

PS4API int _sceUltConditionVariableCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltConditionVariableOptParamInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltMutexCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltMutexOptParamInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltQueueCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltQueueDataResourcePoolCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltQueueDataResourcePoolOptParamInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltQueueOptParamInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltReaderWriterLockCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltReaderWriterLockOptParamInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltSemaphoreCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltSemaphoreOptParamInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltUlthreadCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltUlthreadOptParamInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltUlthreadRuntimeCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltUlthreadRuntimeOptParamInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltWaitingQueueResourcePoolCreate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int _sceUltWaitingQueueResourcePoolOptParamInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltConditionVariableDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltConditionVariableSignal() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltConditionVariableSignalAll() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltConditionVariableWait() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltFinalize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltGetConditionVariableInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltGetMutexInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltGetQueueDataResourcePoolInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltGetQueueInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltGetReaderWriterLockInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltGetSemaphoreInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltGetUlthreadInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltGetUlthreadRuntimeInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltGetWaitingQueueResourcePoolInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltInitialize() {
    LOG_DEBUG("");
    return 0;
}
PS4API int sceUltMutexDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltMutexLock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltMutexTryLock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltMutexUnlock() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltQueueDataResourcePoolDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltQueueDataResourcePoolGetWorkAreaSize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltQueueDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltQueuePop() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltQueuePush() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltQueueTryPop() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltQueueTryPush() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltReaderWriterLockDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltReaderWriterLockLockRead() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltReaderWriterLockLockWrite() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltReaderWriterLockTryLockRead() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltReaderWriterLockTryLockWrite() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltReaderWriterLockUnlockRead() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltReaderWriterLockUnlockWrite() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltSemaphoreAcquire() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltSemaphoreDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltSemaphoreRelease() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltSemaphoreTryAcquire() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltUlthreadExit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltUlthreadGetSelf() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltUlthreadJoin() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltUlthreadRuntimeDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API uint64_t sceUltUlthreadRuntimeGetWorkAreaSize(uint32_t maxNumUlthread, uint32_t numWorkerThread) {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltUlthreadTryJoin() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltUlthreadYield() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltWaitingQueueResourcePoolDestroy() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUltWaitingQueueResourcePoolGetWorkAreaSize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("h0XebKiMBtk", sceUltMutexUnlock);
    fun("byiceqcMvV0", sceUltConditionVariableSignalAll);
    fun("dfVvFVZgr68", sceUltGetUlthreadRuntimeInfo);
    fun("JTw1cAVkuc0", sceUltConditionVariableSignal);
    fun("WIWV1Qd7PFU", sceUltWaitingQueueResourcePoolGetWorkAreaSize);
    fun("uZz3ci7XYqc", sceUltQueueTryPop);
    fun("wps6M1FgHE4", sceUltGetConditionVariableInfo);
    fun("g9FncxNrnmw", sceUltGetQueueInfo);
    fun("LuLTRt0rfTw", _sceUltWaitingQueueResourcePoolOptParamInitialize);
    fun("V2u3WLrwh64", _sceUltUlthreadRuntimeOptParamInitialize);
    fun("RgKmNey20Ns", sceUltReaderWriterLockLockWrite);
    fun("TFHm6-N6vks", _sceUltQueueDataResourcePoolCreate);
    fun("J7Xs-UluzGk", sceUltReaderWriterLockTryLockRead);
    fun("Gw7yn0CEmv8", _sceUltReaderWriterLockOptParamInitialize);
    fun("E5Koh2tNPtY", sceUltGetSemaphoreInfo);
    fun("gCA-D2wiiD0", sceUltReaderWriterLockDestroy);
    fun("-aVO3GCDUAs", sceUltGetQueueDataResourcePoolInfo);
    fun("NPRRPNKDBN0", _sceUltSemaphoreOptParamInitialize);
    fun("1+8t9aHLiz8", _sceUltMutexOptParamInitialize);
    fun("9Y5keOvb6ok", _sceUltQueueCreate);
    fun("YiHujOG9vXY", _sceUltWaitingQueueResourcePoolCreate);
    fun("5xGAHCxA8M0", sceUltConditionVariableWait);
    fun("jW+HnafeS3Y", sceUltMutexDestroy);
    fun("RVSq2tsm2yw", sceUltQueuePop);
    //fun("jw9FkZBXo-g", _sceUltUlthreadRuntimeCreate);
    fun("RVmEia0vXMI", _sceUltConditionVariableOptParamInitialize);
    fun("xrmmI832R4U", sceUltConditionVariableDestroy);
    fun("9Sh0Kk7Xf4w", sceUltReaderWriterLockTryLockWrite);
    fun("-gxcs521SvA", sceUltUlthreadRuntimeDestroy);
    fun("vIuen2qlRmo", _sceUltUlthreadOptParamInitialize);
    fun("evj9YPkS8s4", sceUltQueueDataResourcePoolGetWorkAreaSize);
    fun("h5QlIYj+Ro8", _sceUltSemaphoreCreate);
    fun("8hEGkR1pfr8", sceUltMutexLock);
    fun("d-kSG2fLrvI", sceUltFinalize);
    fun("DsW+3FTXL0Q", sceUltUlthreadTryJoin);
    fun("kF--s3xZjco", sceUltUlthreadExit);
    fun("jNMdItooJzc", sceUltGetUlthreadInfo);
    fun("HFd-lpjGxJA", sceUltUlthreadYield);
    fun("znI3q8S7KQ4", _sceUltUlthreadCreate);
    fun("TkASc9I-xX0", _sceUltQueueOptParamInitialize);
    fun("PP9nZxpSKLY", sceUltQueueDestroy);
    fun("or55417wcDk", sceUltWaitingQueueResourcePoolDestroy);
    fun("izXyehpoZGo", sceUltSemaphoreDestroy);
    fun("iIfTXvh1hiM", _sceUltReaderWriterLockCreate);
    fun("jnKaHGkrxZ4", _sceUltConditionVariableCreate);
    fun("gCeAI57LGgI", sceUltUlthreadJoin);
    fun("SB7YXgR8WSE", sceUltGetReaderWriterLockInfo);
    fun("xOQ9oACobA4", sceUltUlthreadGetSelf);
    fun("gcFCn5J5DXY", sceUltReaderWriterLockUnlockWrite);
    fun("dh11uAUWNyM", sceUltQueueDataResourcePoolDestroy);
    fun("QAH1ofI97vU", sceUltSemaphoreAcquire);
    fun("6Mc2Xs7pI1I", sceUltQueueTryPush);
    fun("HA1Ldbi3lPY", sceUltSemaphoreTryAcquire);
    fun("mmt8Sa6tL6c", _sceUltMutexCreate);
    //fun("grs2pbc2awM", sceUltUlthreadRuntimeGetWorkAreaSize);
    fun("8Ssk4OU38vw", sceUltReaderWriterLockUnlockRead);
    fun("Hb9HWFKo9F4", sceUltReaderWriterLockLockRead);
    fun("dUwpX3e5NDE", sceUltQueuePush);
    fun("lbtk5X1mecw", sceUltSemaphoreRelease);
    fun("6gYjd50q0CE", _sceUltQueueDataResourcePoolOptParamInitialize);
    fun("XSjr+FjsU4s", sceUltGetWaitingQueueResourcePoolInfo);
    fun("jOsUG0BJI-Y", sceUltMutexTryLock);
    //fun("hZIg1EWGsHM", sceUltInitialize);
    fun("xBd3WoSt2PA", sceUltGetMutexInfo);
#undef fun
}
}
