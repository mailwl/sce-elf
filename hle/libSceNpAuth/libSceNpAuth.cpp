#include "libSceNpAuth.h"

namespace libSceNpAuth {
    PS4API void sceNpAuthAbortRequest() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceNpAuthCreateRequest() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceNpAuthGetAuthorizationCode() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceNpAuthDeleteRequest() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceNpAuthCreateAsyncRequest() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceNpAuthPollAsync() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void sceNpAuthWaitAsync() {
        UNIMPLEMENTED_FUNC();
    }

    void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
        fun("cE7wIsqXdZ8", sceNpAuthAbortRequest); // libSceNpAuth
        fun("6bwFkosYRQg", sceNpAuthCreateRequest); // libSceNpAuth
        fun("KxGkOrQJTqY", sceNpAuthGetAuthorizationCode); // libSceNpAuth
        fun("H8wG9Bk-nPc", sceNpAuthDeleteRequest); // libSceNpAuth
        fun("N+mr7GjTvr8", sceNpAuthCreateAsyncRequest); // libSceNpAuth
        fun("gjSyfzSsDcE", sceNpAuthPollAsync); // libSceNpAuth
        fun("SK-S7daqJSE", sceNpAuthWaitAsync); // libSceNpAuth
#undef fun
    }
}
