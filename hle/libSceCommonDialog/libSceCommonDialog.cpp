#include "libSceCommonDialog.h"

namespace libSceCommonDialog {

static bool is_initialized{false};

PS4API int sceCommonDialogInitialize() {
    LOG_DEBUG("");
    is_initialized = true;
    return 0;
}
PS4API int sceCommonDialogIsUsed() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("uoUpLGNkygk", sceCommonDialogInitialize);
    fun("BQ3tey0JmQM", sceCommonDialogIsUsed);
//    fun("2RdicdHhtGA", );
//    fun("I+tdxsCap08", );
//    fun("reTFEla4NQw", );
//    fun("p9TTq4bLdFU", );
//    fun("QXFsLON5QWw", );
//    fun("+UyKxWAnqIU", );
//    fun("xZtXq554Lbg", );
//    fun("afLdI6i0lQw", );
//    fun("v4+gzuTkv6k", );
//    fun("CwCzG0nnLg8", );
//    fun("yxjgDvqUbGQ", );
//    fun("C-EZ3PkhibQ", );
//    fun("70niEKUAnZ0", );
//    fun("87GekE1nowg", );
//    fun("6ljeTSi+fjs", );
//    fun("I+tdxsCap08", );
//    fun("p9TTq4bLdFU", );
#undef fun
}
}
