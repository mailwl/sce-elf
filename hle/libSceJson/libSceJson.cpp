#include "libSceJson.h"

namespace libSceJson {
    PS4API void _ZN3sce4Json12MemAllocatorC2Ev() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void _ZN3sce4Json11InitializerC1Ev() {
        UNIMPLEMENTED_FUNC();
    }
    PS4API void _ZN3sce4Json11Initializer10initializeEPKNS0_13InitParameterE() {
        UNIMPLEMENTED_FUNC();
    }
    void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
//        fun("-hJRce8wn1U", _ZN3sce4Json12MemAllocatorC2Ev);
//        fun("cK6bYHf-Q5E", _ZN3sce4Json11InitializerC1Ev);
//        fun("Cxwy7wHq4J0", _ZN3sce4Json11Initializer10initializeEPKNS0_13InitParameterE);
#undef fun
    }
}
