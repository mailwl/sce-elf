#include "libSceNpFriendListDialog.h"

namespace libSceNpFriendListDialog {

PS4API int sceNpFriendListDialogClose() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpFriendListDialogGetResult() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpFriendListDialogGetResultA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpFriendListDialogGetStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpFriendListDialogInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpFriendListDialogOpen() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpFriendListDialogOpenA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpFriendListDialogTerminate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpFriendListDialogUpdateStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("Z4JJhXCnIvY", sceNpFriendListDialogGetResult);
    fun("8BVP56JIZuA", sceNpFriendListDialogGetStatus);
    fun("2L-W-ZYn2Qo", sceNpFriendListDialogInitialize);
    fun("frwz3eyuA6w", sceNpFriendListDialogUpdateStatus);
    fun("vN9sTP63WT8", sceNpFriendListDialogOpenA);
    fun("hK0CCljzJmY", sceNpFriendListDialogGetResultA);
    fun("zUM-RG5Hmyc", sceNpFriendListDialogOpen);
    fun("ECEzk+K9L2k", sceNpFriendListDialogTerminate);
    fun("+Y1acHGomGQ", sceNpFriendListDialogClose);
#undef fun
}
}
