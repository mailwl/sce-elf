#include "libSceNpWebApi.h"

namespace libSceNpWebApi {

PS4API int sceNpWebApiAbortHandle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiAbortRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiAddHttpRequestHeader() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiAddMultipartPart() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiCheckTimeout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiClearAllUnusedConnection() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiClearUnusedConnection() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiCreateContext(int32_t libCtxId,
                                    SceUserServiceUserId userId) {
    LOG_DEBUG("dummy");
    return 0;
}
PS4API int sceNpWebApiCreateContextA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiCreateExtdPushEventFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiCreateHandle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiCreateMultipartRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiCreatePushEventFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiCreateRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiCreateServicePushEventFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiDeleteContext() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiDeleteExtdPushEventFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiDeleteHandle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiDeletePushEventFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiDeleteRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiDeleteServicePushEventFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiGetConnectionStats() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiGetErrorCode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiGetHttpResponseHeaderValue() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiGetHttpResponseHeaderValueLength() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiGetHttpStatusCode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiGetMemoryPoolStats() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiInitialize(int libHttpCtxId, size_t poolSize) {
    //UNIMPLEMENTED_FUNC(); TODO: call native
    return 0;
}
PS4API int sceNpWebApiIntCreateCtxIndExtdPushEventFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiIntCreateServicePushEventFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiIntInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiReadData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiRegisterExtdPushEventCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiRegisterExtdPushEventCallbackA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiRegisterNotificationCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiRegisterPushEventCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiRegisterServicePushEventCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiSendMultipartRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiSendMultipartRequest2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiSendRequest() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiSendRequest2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiSetHandleTimeout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiSetMaxConnection() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiSetMultipartContentType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiSetRequestTimeout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiTerminate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiUnregisterExtdPushEventCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiUnregisterNotificationCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiUnregisterPushEventCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiUnregisterServicePushEventCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceNpWebApiUtilityParseNpId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("vrM02A5Gy1M", sceNpWebApiRegisterExtdPushEventCallback);
    fun("CQtPRSF6Ds8", sceNpWebApiReadData);
    fun("zk6c65xoyO0", sceNpWebApiCreateContextA);
    fun("sIFx734+xys", sceNpWebApiCreateServicePushEventFilter);
    fun("gVNNyxf-1Sg", sceNpWebApiCheckTimeout);
    fun("8Vjplhyyc44", sceNpWebApiIntInitialize);
    fun("KQIkDGf80PQ", sceNpWebApiClearAllUnusedConnection);
    fun("rdgs5Z1MyFw", sceNpWebApiCreateRequest);
    fun("VwJ5L0Higg0", sceNpWebApiGetHttpResponseHeaderValue);
    fun("2qSZ0DgwTsc", sceNpWebApiGetErrorCode);
    fun("19KgfJXgM+U", sceNpWebApiAddMultipartPart);
    fun("k210oKgP80Y", sceNpWebApiGetHttpStatusCode);
    fun("kJQJE0uKm5w", sceNpWebApiRegisterServicePushEventCallback);
    fun("TZSep4xB4EY", sceNpWebApiIntCreateServicePushEventFilter);
    fun("c1pKoztonB8", sceNpWebApiIntCreateCtxIndExtdPushEventFilter);
    fun("KCItz6QkeGs", sceNpWebApiSendMultipartRequest);
    fun("asz3TtIqGF8", sceNpWebApiTerminate);
    fun("pfaJtb7SQ80", sceNpWebApiDeleteExtdPushEventFilter);
    fun("noQgleu+KLE", sceNpWebApiDeleteRequest);
    fun("JzhYTP2fG18", sceNpWebApiAbortRequest);
    fun("qK4o2656W4w", sceNpWebApiUnregisterPushEventCallback);
    fun("5Mn7TYwpl30", sceNpWebApiDeleteHandle);
    fun("KBxgeNpoRIQ", sceNpWebApiCreateMultipartRequest);
    fun("i0dr6grIZyc", sceNpWebApiSetMultipartContentType);
    fun("PfQ+f6ws764", sceNpWebApiDeleteServicePushEventFilter);
    fun("jhXKGQJ4egI", sceNpWebApiRegisterExtdPushEventCallbackA);
    fun("PqCY25FMzPs", sceNpWebApiUnregisterExtdPushEventCallback);
    fun("WKcm4PeyJww", sceNpWebApiAbortHandle);
    fun("y5Ta5JCzQHY", sceNpWebApiCreatePushEventFilter);
    fun("2edrkr0c-wg", sceNpWebApiUnregisterServicePushEventCallback);
    fun("KjNeZ-29ysQ", sceNpWebApiSendRequest2);
    fun("3OnubUs02UM", sceNpWebApiGetMemoryPoolStats);
    fun("743ZzEBzlV8", sceNpWebApiGetHttpResponseHeaderValueLength);
    fun("XUjdsSTTZ3U", sceNpWebApiDeleteContext);
    fun("wjYEvo4xbcA", sceNpWebApiUnregisterNotificationCallback);
    fun("79M-JqvvGo0", sceNpWebApiCreateHandle);
    fun("G3AnLNdRBjE", sceNpWebApiInitialize);
    fun("joRjtRXTFoc", sceNpWebApiAddHttpRequestHeader);
    fun("x1Y7yiYSk7c", sceNpWebApiCreateContext);
    fun("or0e885BlXo", sceNpWebApiUtilityParseNpId);
    fun("f-pgaNSd1zc", sceNpWebApiClearUnusedConnection);
    fun("DsPOTEvSe7M", sceNpWebApiSendMultipartRequest2);
    fun("kVbL4hL3K7w", sceNpWebApiSendRequest);
    fun("qWcbJkBj1Lg", sceNpWebApiSetRequestTimeout);
    fun("UJ8H+7kVQUE", sceNpWebApiGetConnectionStats);
    fun("M2BUB+DNEGE", sceNpWebApiCreateExtdPushEventFilter);
    fun("gRiilVCvfAI", sceNpWebApiSetMaxConnection);
    fun("zE+R6Rcx3W0", sceNpWebApiDeletePushEventFilter);
    fun("PfSTDCgNMgc", sceNpWebApiRegisterPushEventCallback);
    fun("HVgWmGIOKdk", sceNpWebApiRegisterNotificationCallback);
    fun("6g6q-g1i4XU", sceNpWebApiSetHandleTimeout);
#undef fun
}
}
