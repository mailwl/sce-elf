#include "libSceMsgDialog.h"

namespace libSceMsgDialog {

enum SceCommonDialogStatus {
    SCE_COMMON_DIALOG_STATUS_NONE = 0,  // The feature dialog is not running
    SCE_COMMON_DIALOG_STATUS_INITIALIZED = 1, // The feature dialog is initialized
    SCE_COMMON_DIALOG_STATUS_RUNNING = 2, // The feature dialog is being displayed
    SCE_COMMON_DIALOG_STATUS_FINISHED = 3, // The feature dialog is closed
};

PS4API int sceMsgDialogClose() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceMsgDialogGetResult() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API SceCommonDialogStatus sceMsgDialogGetStatus() {
    LOG_DEBUG("dummy");
    return SCE_COMMON_DIALOG_STATUS_INITIALIZED;
}
PS4API int sceMsgDialogInitialize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceMsgDialogOpen() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceMsgDialogProgressBarInc() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceMsgDialogProgressBarSetMsg() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceMsgDialogProgressBarSetValue() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceMsgDialogTerminate() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceMsgDialogUpdateStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("Gc5k1qcK4fs", sceMsgDialogProgressBarInc);
    fun("Lr8ovHH9l6A", sceMsgDialogGetResult);
    fun("CWVW78Qc3fI", sceMsgDialogGetStatus);
    fun("wTpfglkmv34", sceMsgDialogProgressBarSetValue);
    fun("ePw-kqZmelo", sceMsgDialogTerminate);
    fun("6fIC3XKt2k0", sceMsgDialogUpdateStatus);
    fun("HTrcDKlFKuM", sceMsgDialogClose);
    fun("lDqxaY1UbEo", sceMsgDialogInitialize);
    fun("6H-71OdrpXM", sceMsgDialogProgressBarSetMsg);
    fun("b06Hh0DPEaE", sceMsgDialogOpen);
#undef fun
}
}
