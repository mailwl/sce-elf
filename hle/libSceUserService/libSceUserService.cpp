#include <cstring>
#include "libSceUserService.h"

namespace libSceUserService {
typedef struct SceUserServiceInitializeParams {
    int32_t priority;
} SceUserServiceInitializeParams;

typedef enum SceUserServiceEventType {
    SCE_USER_SERVICE_EVENT_TYPE_LOGIN = 0,
    SCE_USER_SERVICE_EVENT_TYPE_LOGOUT = 1,
} SceUserServiceEventType;

typedef struct SceUserServiceEvent {
    SceUserServiceEventType event;
    SceUserServiceUserId userId;
} SceUserServiceEvent;

typedef enum SceUserServiceUserColor {
    SCE_USER_SERVICE_USER_COLOR_BLUE = 0, // Blue
    SCE_USER_SERVICE_USER_COLOR_RED = 1, //  Red
    SCE_USER_SERVICE_USER_COLOR_GREEN = 2, // Green
    SCE_USER_SERVICE_USER_COLOR_PINK = 3, // Pink
} SceUserServiceUserColor;

#define SCE_USER_SERVICE_USER_ID_INVALID		(-1)

#define SCE_USER_SERVICE_MAX_LOGIN_USERS (4)

    typedef struct SceUserServiceLoginUserIdList {
        SceUserServiceUserId userId[SCE_USER_SERVICE_MAX_LOGIN_USERS];
    } SceUserServiceLoginUserIdList;

#define SCE_USER_SERVICE_ERROR_NO_EVENT 0x80960007


    PS4API int sceUserServiceDestroyUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetAccessibilityKeyremapData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetAccessibilityKeyremapEnable() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetAccessibilityZoom() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetAccountRemarks() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetAgeVerified() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetAppearOfflineSetting() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetAppSortOrder() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetAutoLoginEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetCreatedVersion() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetCurrentUserGroupIndex() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetDefaultNewUserGroupName() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetDeletedUserInfo() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetDiscPlayerFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetEvent(SceUserServiceEvent *event) {
    LOG_DEBUG("%s(%p)",__FUNCTION__, event);
    return SCE_USER_SERVICE_ERROR_NO_EVENT;
}
PS4API int sceUserServiceGetEventSortEvent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetEventSortTitle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetEventUiFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetFaceRecognitionDeleteCount() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetFaceRecognitionRegisterCount() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetFileBrowserFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetFileBrowserSortContent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetFileBrowserSortTitle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetFileSelectorFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetFileSelectorSortContent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetFileSelectorSortTitle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetForegroundUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetFriendFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsAccessTokenNiconicoLive() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsAccessTokenTwitch() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsAccessTokenUstream() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsAnonymousUserId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsBcTags() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsBcTitle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsBroadcastChannel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsBroadcastersComment() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsBroadcastersCommentColor() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsBroadcastService() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsBroadcastUiLayout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsCamCrop() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsCameraBgFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsCameraBrightness() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsCameraChromaKeyLevel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsCameraContrast() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsCameraDepthLevel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsCameraEdgeLevel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsCameraEffect() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsCameraEliminationLevel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsCameraPosition() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsCameraReflection() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsCameraSize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsCameraTransparency() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsFloatingMessage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsHintFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsInitSpectating() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsIsCameraHidden() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsIsFacebookEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsIsMuteEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsIsRecDisabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsIsRecievedMessageHidden() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsIsTwitterEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsLanguageFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsLfpsSortOrder() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsLiveQuality() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsLiveQuality2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsLiveQuality3() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsLiveQuality4() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsLiveQuality5() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsMessageFilterLevel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsTtsFlags() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsTtsPitch() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsTtsSpeed() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetGlsTtsVolume() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetHmuBrightness() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetHmuZoom() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetHomeDirectory() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetImeAutoCapitalEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetImeInitFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetImeInputType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetImeLastUnit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetImePointerMode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetImePredictiveTextEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetImeRunCount() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int32_t sceUserServiceGetInitialUser(
        SceUserServiceUserId *userId
) {
    *userId = 1;
    return 0;
}
PS4API int sceUserServiceGetIPDLeft() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetIPDRight() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetIsFakePlus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetIsQuickSignup() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetIsRemotePlayAllowed() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetJapaneseInputType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetKeyboardType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetKeyRepeatSpeed() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetKeyRepeatStartingTime() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetKratosPrimaryUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetLastLoginOrder() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetLightBarBaseBrightness() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetLoginFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetLoginUserIdList(SceUserServiceLoginUserIdList *userIdList) {
    LOG_DEBUG("%s(%p)",__FUNCTION__, userIdList);
    userIdList->userId[0] = 1;
    userIdList->userId[1] = SCE_USER_SERVICE_USER_ID_INVALID;
    userIdList->userId[2] = SCE_USER_SERVICE_USER_ID_INVALID;
    userIdList->userId[3] = SCE_USER_SERVICE_USER_ID_INVALID;
    return 0;
}
PS4API int sceUserServiceGetMicLevel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetMouseHandType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetMousePointerSpeed() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetNotificationSettings() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetNpAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetNpAuthErrorFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetNpLoginId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetPadSpeakerVolume() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetPartyMuteList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetPartyMuteListA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetPartySettingFlags() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetPasscode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetPlayTogetherFlags() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetPsnPasswordForDebug() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetRegisteredHomeUserIdList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetRegisteredUserIdList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetSaveDataAutoUpload() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetSaveDataSort() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetSecureHomeDirectory() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetShareButtonAssign() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetShareDailymotionAccessToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetShareDailymotionRefreshToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetSharePlayFlags() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetSharePlayFramerateHost() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetSharePlayResolutionHost() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetShareStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetShareStatus2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetSystemLoggerHashedAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetSystemLoggerHashedAccountIdParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetThemeEntitlementId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetThemeHomeShareOwner() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetThemeTextShadow() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetThemeWaveColor() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetTopMenuLimitItem() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetTopMenuNotificationFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetTopMenuTutorialFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetTraditionalChineseInputType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetUserColor(const SceUserServiceUserId userId,
                                      SceUserServiceUserColor *color) {
    LOG_DEBUG("dummy");
    *color = SCE_USER_SERVICE_USER_COLOR_RED;
    return 0;
}
PS4API int sceUserServiceGetUserGroupName() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetUserGroupNameList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetUserGroupNum() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetUserName(SceUserServiceUserId userId, char* name, const size_t size) {
    ::strncpy(name, "mailwl", size);
    return 0;
}
PS4API int sceUserServiceGetVibrationEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetVoiceRecognitionTutorialState() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetVolumeForController() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetVolumeForGenericUSB() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetVolumeForMorpheusSidetone() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceGetVolumeForSidetone() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceInitialize(const SceUserServiceInitializeParams *initParams) {
    LOG_DEBUG("%s(%p)",__FUNCTION__, initParams);
    return 0;
}
PS4API int sceUserServiceInitialize2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceIsGuestUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceIsKratosPrimaryUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceIsKratosUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceIsLoggedIn() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceIsSharePlayClientUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceIsUserStorageAccountBound() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceLogin() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceRegisterEventCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetAccessibilityKeyremapData() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetAccessibilityKeyremapEnable() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetAccessibilityZoom() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetAccountRemarks() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetAgeVerified() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetAppearOfflineSetting() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetAppSortOrder() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetAutoLoginEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetCreatedVersion() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetDiscPlayerFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetEventSortEvent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetEventSortTitle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetEventUiFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetFaceRecognitionDeleteCount() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetFaceRecognitionRegisterCount() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetFileBrowserFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetFileBrowserSortContent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetFileBrowserSortTitle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetFileSelectorFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetFileSelectorSortContent() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetFileSelectorSortTitle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetForegroundUser() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetFriendFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsAccessTokenNiconicoLive() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsAccessTokenTwitch() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsAccessTokenUstream() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsAnonymousUserId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsBcTags() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsBcTitle() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsBroadcastChannel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsBroadcastersComment() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsBroadcastersCommentColor() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsBroadcastService() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsBroadcastUiLayout() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsCamCrop() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsCameraBgFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsCameraBrightness() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsCameraChromaKeyLevel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsCameraContrast() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsCameraDepthLevel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsCameraEdgeLevel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsCameraEffect() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsCameraEliminationLevel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsCameraPosition() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsCameraReflection() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsCameraSize() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsCameraTransparency() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsFloatingMessage() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsHintFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsInitSpectating() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsIsCameraHidden() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsIsFacebookEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsIsMuteEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsIsRecDisabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsIsRecievedMessageHidden() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsIsTwitterEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsLanguageFilter() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsLfpsSortOrder() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsLiveQuality() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsLiveQuality2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsLiveQuality3() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsLiveQuality4() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsLiveQuality5() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsMessageFilterLevel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsTtsFlags() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsTtsPitch() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsTtsSpeed() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetGlsTtsVolume() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetHmuBrightness() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetHmuZoom() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetImeAutoCapitalEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetImeInitFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetImeInputType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetImeLastUnit() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetImePointerMode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetImePredictiveTextEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetImeRunCount() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetIPDLeft() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetIPDRight() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetIsFakePlus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetIsQuickSignup() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetIsRemotePlayAllowed() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetJapaneseInputType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetKeyboardType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetKeyRepeatSpeed() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetKeyRepeatStartingTime() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetLightBarBaseBrightness() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetLoginFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetMicLevel() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetMouseHandType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetMousePointerSpeed() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetNotificationSettings() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetNpAuthErrorFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetNpLoginId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetPadSpeakerVolume() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetPartyMuteList() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetPartyMuteListA() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetPartySettingFlags() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetPasscode() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetPlayTogetherFlags() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetPsnPasswordForDebug() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetSaveDataAutoUpload() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetSaveDataSort() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetShareButtonAssign() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetShareDailymotionAccessToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetShareDailymotionRefreshToken() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetSharePlayFlags() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetSharePlayFramerateHost() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetSharePlayResolutionHost() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetShareStatus() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetShareStatus2() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetSystemLoggerHashedAccountId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetSystemLoggerHashedAccountIdParam() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetThemeEntitlementId() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetThemeHomeShareOwner() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetThemeTextShadow() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetThemeWaveColor() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetTopMenuLimitItem() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetTopMenuNotificationFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetTopMenuTutorialFlag() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetTraditionalChineseInputType() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetUserGroupIndex() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetUserGroupName() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetUserName() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetVibrationEnabled() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetVoiceRecognitionTutorialState() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetVolumeForController() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetVolumeForGenericUSB() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetVolumeForMorpheusSidetone() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceSetVolumeForSidetone() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
PS4API int sceUserServiceTerminate() {
    LOG_DEBUG("");
    return 0;
}
PS4API int sceUserServiceUnregisterEventCallback() {
    UNIMPLEMENTED_FUNC();
    return 0;
}
void load(export_funcs& exp) {
#define fun(a, b) exp[a] = std::make_pair((export_func)b, nullptr)
    fun("B-WW6mNtp2s", sceUserServiceSetShareDailymotionAccessToken);
    fun("4nEjiZH1LKM", sceUserServiceSetVolumeForController);
    fun("eQrBbMmZ1Ss", sceUserServiceGetGlsTtsPitch);
    fun("aaC3005VtY4", sceUserServiceGetEventSortTitle);
    fun("PQlF4cjUz9U", sceUserServiceGetIPDLeft);
    fun("TerdSx+FXrc", sceUserServiceSetGlsAnonymousUserId);
    fun("iWpzXixD0UE", sceUserServiceGetKeyRepeatStartingTime);
    fun("OVdVBcejvmQ", sceUserServiceGetRegisteredHomeUserIdList);
    fun("HYMgE5B62QY", sceUserServiceSetGlsCamCrop);
    fun("1JNYgwRcANI", sceUserServiceSetVoiceRecognitionTutorialState);
    fun("3UZADLBXpiA", sceUserServiceGetVolumeForSidetone);
    fun("WGXOvoUwrOs", sceUserServiceGetCreatedVersion);
    fun("t1oU0+93b+s", sceUserServiceSetGlsFloatingMessage);
    fun("ygVuZ1Hb-nc", sceUserServiceGetEventSortEvent);
    fun("W5RgPUuv35Y", sceUserServiceGetNpLoginId);
    fun("OALd6SmF220", sceUserServiceSetGlsBroadcastChannel);
    fun("91kOKRnkrhE", sceUserServiceGetGlsCameraChromaKeyLevel);
    fun("64PEUYPuK98", sceUserServiceGetGlsAccessTokenNiconicoLive);
    fun("zBLxX8JRMoo", sceUserServiceGetGlsCameraSize);
    fun("yLNm3n7fgpw", sceUserServiceGetImeAutoCapitalEnabled);
    fun("XUT7ad-BUMc", sceUserServiceGetKeyRepeatSpeed);
    fun("UDx67PTzB20", sceUserServiceGetIPDRight);
    fun("FP4TKrdRXXM", sceUserServiceGetGlsBcTags);
    fun("CMl8mUJvSf8", sceUserServiceSetSharePlayFlags);
    fun("pVsEKLk5bIA", sceUserServiceGetSaveDataSort);
    fun("CdWp0oHWGr0", sceUserServiceGetInitialUser);
    fun("HkuBuYhYaPg", sceUserServiceSetThemeTextShadow);
    fun("Zgq19lM+u2U", sceUserServiceSetNpLoginId);
    fun("xzQVBcKYoI8", sceUserServiceGetSecureHomeDirectory);
    fun("4nUbGGBcGco", sceUserServiceGetLastLoginOrder);
    fun("uhwssTtt3yo", sceUserServiceSetEventSortEvent);
    fun("spW--yoLQ9o", sceUserServiceUnregisterEventCallback);
    fun("2-b8QbU+HNc", sceUserServiceGetImeLastUnit);
    fun("V7ZG7V+dd08", sceUserServiceGetGlsAccessTokenUstream);
    fun("Ta52bXx5Tek", sceUserServiceGetKeyboardType);
    fun("8PXQIdRsZIE", sceUserServiceSetGlsCameraDepthLevel);
    fun("56bliV+tc0Y", sceUserServiceSetGlsCameraEdgeLevel);
    fun("vC-uSETCFUY", sceUserServiceSetAccountRemarks);
    fun("GkcHilidQHk", sceUserServiceGetGlsCameraReflection);
    fun("om4jx+pJlQo", sceUserServiceSetNpAuthErrorFlag);
    fun("ksUJCL0Hq20", sceUserServiceSetSaveDataAutoUpload);
    fun("zNvCnHpkPmM", sceUserServiceGetPadSpeakerVolume);
    fun("z-lbCrpteB4", sceUserServiceSetIPDRight);
    fun("lsdxBeRnEes", sceUserServiceSetGlsIsTwitterEnabled);
    fun("bdJdX2bKo2E", sceUserServiceSetGlsHintFlag);
    fun("+29DSndZ9Oc", sceUserServiceGetGlsFloatingMessage);
    fun("1xxcMiGu2fo", sceUserServiceGetUserName);
    fun("EjxE+-VvuJ4", sceUserServiceSetGlsIsCameraHidden);
    fun("SBurFYk7M74", sceUserServiceGetGlsTtsVolume);
    fun("ZfUouUx2h8w", sceUserServiceSetTraditionalChineseInputType);
    fun("fCBpPJbELDk", sceUserServiceGetFileBrowserSortContent);
    fun("gnViUj0ab8U", sceUserServiceGetImeInitFlag);
    fun("lAR1nkEoMBo", sceUserServiceSetJapaneseInputType);
    fun("WvM21J1SI0U", sceUserServiceGetGlsBroadcastersCommentColor);
    fun("IKk3EGj+xRI", sceUserServiceGetIsFakePlus);
    fun("4ys00CRU6V8", sceUserServiceGetGlsLiveQuality5);
    fun("Xp9Px0V0tas", sceUserServiceGetGlsIsTwitterEnabled);
    fun("97ZkWubtMk0", sceUserServiceSetPartyMuteListA);
    fun("HwFpasG4+kM", sceUserServiceSetGlsLiveQuality4);
    fun("IiwhRynrDnQ", sceUserServiceSetPartySettingFlags);
    fun("PKHZK960qZE", sceUserServiceSetThemeWaveColor);
    fun("wuI7c7UNk0A", sceUserServiceRegisterEventCallback);
    fun("UYR9fcPXDUE", sceUserServiceGetFileBrowserSortTitle);
    fun("Z+dzNaClq7w", sceUserServiceGetGlsLiveQuality2);
    fun("dCdhOJIOtR4", sceUserServiceSetKeyboardType);
    fun("OANH5P9lV4I", sceUserServiceSetShareDailymotionRefreshToken);
    fun("-jRGLt2Dbe4", sceUserServiceGetVoiceRecognitionTutorialState);
    fun("gBLMGhB6B9E", sceUserServiceSetAgeVerified);
    fun("UxrSdH6jA3E", sceUserServiceGetSaveDataAutoUpload);
    fun("MG+ObGDYePw", sceUserServiceGetTopMenuTutorialFlag);
    fun("rriXMS0a7BM", sceUserServiceSetGlsCameraSize);
    fun("5EiQCnL2G1Y", sceUserServiceGetRegisteredUserIdList);
    fun("fPhymKNvK-A", sceUserServiceGetLoginUserIdList);
    fun("O1nURsxyYmk", sceUserServiceGetGlsCameraTransparency);
    fun("X5On-7hVCs0", sceUserServiceGetGlsLiveQuality3);
    fun("5G-MA1x5utw", sceUserServiceGetCurrentUserGroupIndex);
    fun("+NVJMeISrM4", sceUserServiceGetGlsTtsFlags);
    fun("GC18r56Bp7Y", sceUserServiceDestroyUser);
    fun("fbCC0yo2pVQ", sceUserServiceGetFaceRecognitionRegisterCount);
    fun("NiwMhCbg764", sceUserServiceSetImePredictiveTextEnabled);
    fun("yX-TpbFAYxo", sceUserServiceGetGlsBcTitle);
    fun("FfXgMSmZLfk", sceUserServiceSetKeyRepeatStartingTime);
    fun("0e0wzFADy0I", sceUserServiceSetGlsCameraTransparency);
    fun("jiMNYgxzT-4", sceUserServiceSetFaceRecognitionRegisterCount);
    fun("LIBEeNNfeQo", sceUserServiceSetGlsBroadcastService);
    fun("YnBnZpr3UJg", sceUserServiceSetGlsCameraEliminationLevel);
    fun("6Et3d4p1u8c", sceUserServiceGetFileSelectorSortTitle);
    fun("IWEla-izyTs", sceUserServiceGetImeRunCount);
    fun("Y5U66nk0bUc", sceUserServiceGetGlsCameraBgFilter);
    fun("MzVmbq2IVCo", sceUserServiceGetIsQuickSignup);
    fun("zs4i9SEHy0g", sceUserServiceSetKeyRepeatSpeed);
    fun("EYvRF1VUpUU", sceUserServiceSetShareStatus);
    fun("sukPd-xBDjM", sceUserServiceGetMouseHandType);
    fun("kUaJUV1b+PM", sceUserServiceGetEventUiFlag);
    fun("Llv693Nx+nU", sceUserServiceSetFileSelectorSortTitle);
    fun("WU5s+cPzO8Y", sceUserServiceSetHmuBrightness);
    fun("6dfDreosXGY", sceUserServiceGetNpAccountId);
    fun("lg2I8bETiZo", sceUserServiceSetMouseHandType);
    fun("NjhK36GfEGQ", sceUserServiceGetShareDailymotionAccessToken);
    fun("8no2rlDjl7o", sceUserServiceGetSystemLoggerHashedAccountId);
    fun("jIe8ZED06XI", sceUserServiceGetGlsCameraDepthLevel);
    fun("f5DDIXCTxww", sceUserServiceSetGlsBroadcastersCommentColor);
    fun("oJzfZxZchX4", sceUserServiceGetAgeVerified);
    fun("K8Nh6fhmYkc", sceUserServiceGetThemeHomeShareOwner);
    fun("hQ72M-YRb8g", sceUserServiceSetGlsLiveQuality2);
    fun("TEsQ0HWJ8R4", sceUserServiceGetVolumeForGenericUSB);
    fun("ZWAUCzgSQ2Q", sceUserServiceSetGlsLiveQuality3);
    fun("Ov8hs+c1GNY", sceUserServiceSetGlsLiveQuality5);
    fun("NB9-D-o3hN0", sceUserServiceSetGlsTtsPitch);
    fun("75cwn1y2ffk", sceUserServiceGetGlsMessageFilterLevel);
    fun("Ghu0khDguq8", sceUserServiceSetImeInputType);
    fun("8Y+aDvVGLiw", sceUserServiceGetGlsAccessTokenTwitch);
    fun("HxNRiCWfVFw", sceUserServiceGetGlsBroadcastService);
    fun("6ZQ4kfhM37c", sceUserServiceGetGlsBroadcastUiLayout);
    fun("g6ojqW3c8Z4", sceUserServiceGetAccessibilityKeyremapData);
    fun("pZL154KvMjU", sceUserServiceIsKratosUser);
    fun("nCfhbtuZbk8", sceUserServiceSetPsnPasswordForDebug);
    fun("7SE4sjhlOCI", sceUserServiceSetIsFakePlus);
    fun("hjlUn9UCgXg", sceUserServiceSetImeLastUnit);
    fun("uAPBw-7641s", sceUserServiceGetKratosPrimaryUser);
    fun("wBGmrRTUC14", sceUserServiceGetFriendFlag);
    fun("VSQR9qYpaCM", sceUserServiceGetPsnPasswordForDebug);
    fun("nlOWAiRyxkA", sceUserServiceGetNpAuthErrorFlag);
    fun("19uCF96mfos", sceUserServiceSetImePointerMode);
    fun("nNn8Gnn+E6Y", sceUserServiceSetIsQuickSignup);
    fun("lUoqwTQu4Go", sceUserServiceGetUserColor);
    fun("7IiUdURpH0k", sceUserServiceSetAppearOfflineSetting);
    fun("t-I2Lbj8a+0", sceUserServiceGetShareDailymotionRefreshToken);
    fun("HKu68cVzctg", sceUserServiceSetAccessibilityZoom);
    fun("LbQ-jU9jOsk", sceUserServiceGetGlsCameraBrightness);
    fun("j3YMu1MVNNo", sceUserServiceInitialize);
    fun("UdZhN1nVYfw", sceUserServiceSetGlsBcTags);
    fun("3fcBoTACkWY", sceUserServiceSetImeInitFlag);
    fun("ga2z3AAn8XI", sceUserServiceGetUserGroupNameList);
    fun("Fl52JeSLPyw", sceUserServiceSetFileSelectorSortContent);
    fun("+qAE4tRMrXk", sceUserServiceGetGlsLiveQuality4);
    fun("ki81gh1yZDM", sceUserServiceGetGlsHintFlag);
    fun("rB70KuquYxs", sceUserServiceSetSharePlayFramerateHost);
    fun("yARnQeWzhdM", sceUserServiceGetPartySettingFlags);
    fun("r2QuHIT8u9I", sceUserServiceGetVolumeForMorpheusSidetone);
    fun("uvVR70ZxFrQ", sceUserServiceLogin);
    fun("41kc2YhzZoU", sceUserServiceSetPadSpeakerVolume);
    fun("CokWh8qGANk", sceUserServiceSetVibrationEnabled);
    fun("YVzw4T1fnS4", sceUserServiceGetHmuBrightness);
    fun("LyXzCtzleAQ", sceUserServiceGetGlsLfpsSortOrder);
    fun("ld396XJQPgM", sceUserServiceGetVolumeForController);
    fun("YfDgKz5SolU", sceUserServiceGetMicLevel);
    fun("1+nxJ4awLH8", sceUserServiceGetUserGroupName);
    fun("gGbu3TZiXeU", sceUserServiceSetGlsCameraContrast);
    fun("X5rJZNDZ2Ss", sceUserServiceGetPasscode);
    fun("xrtki9sUopg", sceUserServiceGetAccessibilityKeyremapEnable);
    fun("CvwCMJtzp1I", sceUserServiceGetGlsLiveQuality);
    fun("1zDEFUmBdoo", sceUserServiceGetAccessibilityZoom);
    fun("MZxH8029+Wg", sceUserServiceIsLoggedIn);
    fun("x6m8P9DBPSc", sceUserServiceGetThemeEntitlementId);
    fun("wgVAwa31l0E", sceUserServiceSetGlsLanguageFilter);
    fun("QzeIQXyavtU", sceUserServiceSetGlsTtsVolume);
    fun("wWZzH-BwWuA", sceUserServiceSetGlsCameraPosition);
    fun("J-KEr4gUEvQ", sceUserServiceGetHomeDirectory);
    fun("-7XgCmEwKrs", sceUserServiceIsSharePlayClientUser);
    fun("AmJ3FJxT7r8", sceUserServiceSetGlsIsRecievedMessageHidden);
    fun("u-E+6d9PiP8", sceUserServiceSetAutoLoginEnabled);
    fun("SykFcJEGvz4", sceUserServiceGetTopMenuNotificationFlag);
    fun("WaHZGp0Vn2k", sceUserServiceGetThemeWaveColor);
    fun("QqZ1A3vukFM", sceUserServiceGetGlsAnonymousUserId);
    fun("az-0R6eviZ0", sceUserServiceInitialize2);
    fun("gQh8NaCbRqo", sceUserServiceSetHmuZoom);
    fun("8IqdtMmc5Uc", sceUserServiceGetGlsIsCameraHidden);
    fun("5cK+UC54Oz4", sceUserServiceSetFriendFlag);
    fun("IAB7wscPwio", sceUserServiceGetFileSelectorSortContent);
    fun("vRgpAhKJJ+M", sceUserServiceSetGlsInitSpectating);
    fun("Di05lHWmCLU", sceUserServiceSetGlsCameraChromaKeyLevel);
    fun("Izy+4XmTBB8", sceUserServiceSetIPDLeft);
    fun("eC88db1i-f8", sceUserServiceGetSharePlayFramerateHost);
    fun("hJ5gj+Pv3-M", sceUserServiceSetGlsBcTitle);
    fun("YUhBM-ASEcA", sceUserServiceGetImePredictiveTextEnabled);
    fun("IxCpDYsiTX0", sceUserServiceGetTopMenuLimitItem);
    fun("Lge4s3h8BFA", sceUserServiceSetGlsTtsFlags);
    fun("pfz4rzKJc6g", sceUserServiceSetSaveDataSort);
    fun("c9U2pk4Ao9w", sceUserServiceSetMicLevel);
    fun("bkQ7aNx62Qg", sceUserServiceSetVolumeForGenericUSB);
    fun("Lgi5A4fQwHc", sceUserServiceGetIsRemotePlayAllowed);
    fun("snOzH0NQyO0", sceUserServiceSetFaceRecognitionDeleteCount);
    fun("rLEw4n5yI40", sceUserServiceGetGlsCameraEffect);
    fun("bFzA3t6muvU", sceUserServiceSetShareButtonAssign);
    fun("wMtSHLNAVj0", sceUserServiceGetShareStatus2);
    fun("Y5zgw69ndoE", sceUserServiceGetMousePointerSpeed);
    fun("MgBIXUkGtpE", sceUserServiceSetForegroundUser);
    fun("5iqtUryI-hI", sceUserServiceGetNotificationSettings);
    fun("k-7kxXGr+r0", sceUserServiceGetFileBrowserFilter);
    fun("YmmFiEoegko", sceUserServiceGetGlsCamCrop);
    fun("ghjrbwjC0VE", sceUserServiceSetGlsCameraEffect);
    fun("IcM2f5EoRRA", sceUserServiceSetUserGroupIndex);
    fun("7EnjUtnAN+o", sceUserServiceSetVolumeForMorpheusSidetone);
    fun("TLrDgrPYTDo", sceUserServiceIsUserStorageAccountBound);
    fun("FsOBy3JfbrM", sceUserServiceGetFileSelectorFilter);
    fun("RdAvEmks-ZE", sceUserServiceSetGlsBroadcastUiLayout);
    fun("X9Jgur0QtLE", sceUserServiceSetNotificationSettings);
    fun("63t6w0MgG8I", sceUserServiceSetGlsIsMuteEnabled);
    fun("7zu3F7ykVeo", sceUserServiceGetAccountRemarks);
    fun("uMkqgm70thg", sceUserServiceGetGlsLanguageFilter);
    fun("W3neFYAvZss", sceUserServiceGetGlsIsMuteEnabled);
    fun("1U5cFdTdso0", sceUserServiceGetDefaultNewUserGroupName);
    fun("uRU0lQe+9xY", sceUserServiceGetFaceRecognitionDeleteCount);
    fun("AZFXXpZJEPI", sceUserServiceSetImeRunCount);
    fun("NNblpSGxrY8", sceUserServiceGetImePointerMode);
    fun("YnXM2saZkl4", sceUserServiceGetShareStatus);
    fun("0H51EFxR3mc", sceUserServiceGetGlsCameraEdgeLevel);
    fun("0D2xtHQYxII", sceUserServiceSetGlsAccessTokenTwitch);
    fun("nqDEnj7M0QE", sceUserServiceGetAutoLoginEnabled);
    fun("aq1jwlgyOV4", sceUserServiceGetPartyMuteListA);
    fun("QNk7qD4dlD4", sceUserServiceGetLoginFlag);
    fun("xzdhJrL3Hns", sceUserServiceGetUserGroupNum);
    fun("Ty9wanVDC9k", sceUserServiceSetEventUiFlag);
    fun("FnWkLNOmJXw", sceUserServiceIsGuestUser);
    fun("Xy4rq8gpYHU", sceUserServiceSetFileBrowserSortContent);
    fun("zru8Zhuy1UY", sceUserServiceGetImeInputType);
    fun("ALyjUuyowuI", sceUserServiceSetThemeEntitlementId);
    fun("2EWfAroUQE4", sceUserServiceSetGlsTtsSpeed);
    fun("8Q71i3u9lN0", sceUserServiceSetTopMenuTutorialFlag);
    fun("EgEPXDie5XQ", sceUserServiceGetThemeTextShadow);
    fun("zsJcWtE81Rk", sceUserServiceGetShareButtonAssign);
    fun("q+7UTGELzj4", sceUserServiceGetLightBarBaseBrightness);
    fun("lrPF-kNBPro", sceUserServiceGetSharePlayFlags);
    fun("Zr4h+Bbx0do", sceUserServiceGetSystemLoggerHashedAccountIdParam);
    fun("8TGeI5PAabg", sceUserServiceSetImeAutoCapitalEnabled);
    fun("HfQTiMSCHJk", sceUserServiceSetGlsIsFacebookEnabled);
    fun("UeIv6aNXlOw", sceUserServiceGetPartyMuteList);
    fun("VEUKQumI5B8", sceUserServiceSetGlsAccessTokenNiconicoLive);
    fun("qT8-eJKe+rI", sceUserServiceSetGlsLiveQuality);
    fun("AQ680L4Sr74", sceUserServiceSetIsRemotePlayAllowed);
    fun("4IXuUaBxzEg", sceUserServiceGetGlsIsRecDisabled);
    fun("6r4hDyrRUGg", sceUserServiceGetAppearOfflineSetting);
    fun("II+V6wXKS-E", sceUserServiceSetShareStatus2);
    fun("7LCq4lSlmw4", sceUserServiceSetPasscode);
    fun("vdBd3PMBFp4", sceUserServiceSetGlsAccessTokenUstream);
    fun("+Prbx5iagl0", sceUserServiceGetGlsCameraEliminationLevel);
    fun("mNnB2PWMSgw", sceUserServiceIsKratosPrimaryUser);
    fun("XEgdhGfqRpI", sceUserServiceSetEventSortTitle);
    fun("hyW5w855fk4", sceUserServiceGetGlsIsRecievedMessageHidden);
    fun("BCDA6jn4HVY", sceUserServiceGetGlsTtsSpeed);
    fun("NpEYVDOyjRk", sceUserServiceGetGlsBroadcastersComment);
    fun("BhRxR+R0NFA", sceUserServiceSetSharePlayResolutionHost);
    fun("oXVAQutr3Ns", sceUserServiceGetTraditionalChineseInputType);
    fun("ZP0ti1CRxNA", sceUserServiceSetAccessibilityKeyremapEnable);
    fun("O8ONJV3b8jg", sceUserServiceGetHmuZoom);
    fun("Zdd5gybtsi0", sceUserServiceSetLoginFlag);
    fun("hP2q9Eb5hf0", sceUserServiceSetFileSelectorFilter);
    fun("ZopdvNlYFHc", sceUserServiceSetGlsBroadcastersComment);
    fun("1ppzHkQhiNs", sceUserServiceGetGlsCameraContrast);
    fun("m8VtSd5I5og", sceUserServiceSetDiscPlayerFlag);
    fun("5jL7UM+AdbQ", sceUserServiceSetSystemLoggerHashedAccountId);
    fun("wN5zRLw4J6A", sceUserServiceSetFileBrowserSortTitle);
    fun("NiTGNLkBc-Q", sceUserServiceGetDeletedUserInfo);
    fun("yAWUqugjPvE", sceUserServiceGetPlayTogetherFlags);
    fun("M9noOXMhlGo", sceUserServiceSetFileBrowserFilter);
    fun("U07X36vgbA0", sceUserServiceSetSystemLoggerHashedAccountIdParam);
    fun("pnHR-aj9edo", sceUserServiceSetGlsCameraReflection);
    fun("QfYasZZPvoQ", sceUserServiceSetUserGroupName);
    fun("u-dCVE6fQAU", sceUserServiceGetJapaneseInputType);
    fun("eNb53LQJmIM", sceUserServiceGetForegroundUser);
    fun("Tib8zgDd+V0", sceUserServiceSetTopMenuNotificationFlag);
    fun("N-xzO5-livc", sceUserServiceSetGlsCameraBgFilter);
    fun("PhXZbj4wVhE", sceUserServiceGetAppSortOrder);
    fun("zR+J2PPJgSU", sceUserServiceGetGlsInitSpectating);
    fun("pmW5v9hORos", sceUserServiceSetPlayTogetherFlags);
    fun("feqktbQD1eo", sceUserServiceSetCreatedVersion);
    fun("RdpmnHZ3Q9M", sceUserServiceGetDiscPlayerFlag);
    fun("F0wuEvioQd4", sceUserServiceGetGlsCameraPosition);
    fun("ZsyQjvVFHnk", sceUserServiceSetPartyMuteList);
    fun("WQ-l-i2gJko", sceUserServiceSetVolumeForSidetone);
    fun("b5-tnLcyUQE", sceUserServiceSetAppSortOrder);
    fun("Jqu2XFr5UvA", sceUserServiceSetUserName);
    fun("6oZ3DZGzjIE", sceUserServiceSetGlsIsRecDisabled);
    fun("omf6BE2-FPo", sceUserServiceSetMousePointerSpeed);
    fun("f7VSHQHB6Ys", sceUserServiceSetTopMenuLimitItem);
    fun("bwFjS+bX9mA", sceUserServiceTerminate);
    fun("Mm4+PSflHbM", sceUserServiceGetGlsBroadcastChannel);
    fun("O0mtfoE5Cek", sceUserServiceGetVibrationEnabled);
    fun("GxqMYA60BII", sceUserServiceSetGlsCameraBrightness);
    fun("fm7XpsO++lk", sceUserServiceSetGlsMessageFilterLevel);
    fun("dlBQfiDOklQ", sceUserServiceSetLightBarBaseBrightness);
    fun("ttiSviAPLXI", sceUserServiceGetSharePlayResolutionHost);
    fun("yH17Q6NWtVg", sceUserServiceGetEvent);
    fun("f5lAVp0sFNo", sceUserServiceGetGlsIsFacebookEnabled);
    fun("jhy6fa5a4k4", sceUserServiceSetThemeHomeShareOwner);
    fun("SfGVfyEN8iw", sceUserServiceSetAccessibilityKeyremapData);
    fun("rDkflpHzrRE", sceUserServiceSetGlsLfpsSortOrder);
#undef fun
}
}
