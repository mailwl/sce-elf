#pragma once

#include <map>
#include <string>

#include "logger/logger.h"

#if !defined(__clang__)
#define PS4API __attribute__((sysv_abi))
#else
#define PS4API __attribute__((sysv_abi))
#endif

typedef void (*export_func)();
// nid -> hle/native
typedef std::map<std::string, std::pair<export_func, export_func> > export_funcs;
typedef std::map<std::string, export_funcs> exports;
typedef void(*load_hle_lib)(export_funcs&);
typedef std::map<std::string, load_hle_lib> hle_libs;

typedef struct params {
    uint64_t argc;
    const char* argv[1];
} params;

typedef PS4API void (*exit_proc)();
typedef PS4API int (*start_func)(params&, exit_proc);

typedef uint32_t SceUserServiceUserId;

typedef uint64_t sceoff_t;

#ifndef ssize_t
typedef int64_t ssize_t;
#endif // !ssize_t

namespace process {
    export_func get_export(const char* lib_name, const char* name, bool force_native);
}

template <typename... Args>
int call_native(const char* lib_name, const char* func_name, Args... a) {
    typedef PS4API int(*func)(Args...);
    func f = (func)process::get_export(lib_name, func_name, true);
    if (f) {
        return f(a...);
    }
    UNIMPLEMENTED_FUNC();
    return -1;
}

template <typename Ret, typename... Args>
Ret call_native2(const char* lib_name, const char* func_name, Args... a) {
    typedef PS4API Ret(*func)(Args...);
    func f = (func)process::get_export(lib_name, func_name, true);
    if (f) {
        Ret rc = f(a...);
        return rc;
    }
    UNIMPLEMENTED_FUNC();
    return Ret();
}

/* tepmlates for platforms 

#if defined(_WIN64) || defined(__CYGWIN__)
#else
#endif

#if defined(_WIN64) || defined(__CYGWIN__)
#elif __APPLE__
#else
#endif

LOG_DEBUG("STUB %s()\n", __FUNCTION__);
*/