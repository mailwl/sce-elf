
#include <iostream>
#include <string>
#define HAVE_STRUCT_TIMESPEC
#include <pthread.h>
#if defined(_WIN64)
#include <windows.h>
#include <direct.h>
#include <eh.h>
#include <exception>
#include <mutex>
#include <zydis/Zydis.h>
#include <cstring>
#include <vector>
#else
#include <unistd.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <emmintrin.h>
#endif

#include <SDL.h>

#include "process/process.h"

static char path[0x100];

params p{ 1, path };

void* ptr;

#if defined(_WIN64)

::LONG CALLBACK vectoredExceptionHandler( ::PEXCEPTION_POINTERS ep ) {
    //LOG_DEBUG("entering vectorized ex. handler 0x%lx\n", ep->ExceptionRecord->ExceptionCode);
    if (ep->ExceptionRecord->ExceptionCode == EXCEPTION_ACCESS_VIOLATION) {
        void* pExcptAddr = ep->ExceptionRecord->ExceptionAddress;
        //LOG_DEBUG("%p", pExcptAddr);
        ZydisDecodedInstruction instruction;
        ZydisDecoder decoder;
        ZydisDecoderInit(&decoder, ZYDIS_MACHINE_MODE_LONG_64, ZYDIS_ADDRESS_WIDTH_64);

        ZydisStatus status = ZydisDecoderDecodeBuffer(&decoder, pExcptAddr, 15, (ZydisU64)pExcptAddr, &instruction);
        if (!ZYDIS_SUCCESS(status)) {
            LOG_DEBUG("decode instruction failed at %p", pExcptAddr);
            return EXCEPTION_CONTINUE_SEARCH;
        }
        if (instruction.mnemonic != ZYDIS_MNEMONIC_MOV) {
            return EXCEPTION_CONTINUE_SEARCH;
        }
        if (instruction.operandCount != 2) {
            return EXCEPTION_CONTINUE_SEARCH;
        }
        if (instruction.operands[0].reg.value != ZYDIS_REGISTER_RAX ||
            instruction.operands[1].mem.segment != ZYDIS_REGISTER_FS) {
            return EXCEPTION_CONTINUE_SEARCH;
        }
        uint32_t instLen = instruction.length;
        int64_t fsOffset = instruction.raw.disp.value;
        if (fsOffset != 0) {
            return EXCEPTION_CONTINUE_SEARCH;
        }

        ep->ContextRecord->Rax = reinterpret_cast<uintptr_t>(process::read_fs_register(fsOffset));;
        ep->ContextRecord->Rip += instLen;

        return EXCEPTION_CONTINUE_EXECUTION;

    } else  if (EXCEPTION_ILLEGAL_INSTRUCTION == ep->ExceptionRecord->ExceptionCode) {
        PCONTEXT Context = ep->ContextRecord;
        auto code = (const uint8_t*)Context->Rip;
        if (code[2] == 0x79) { //insertq
            Context->Rip += 4;
        }
        else if (code[2] == 0x78) { // insertq
            Context->Rip += 6;
        }
        return EXCEPTION_CONTINUE_EXECUTION;

    }
    return EXCEPTION_CONTINUE_SEARCH;
}
#endif


void* game_thread(void* arg) {
    auto start = (start_func)arg;
    start(p, process::ps4_exit);
    return nullptr;
}

int main(int /*argc*/, char */*argv*/[]) {
    logger::init_logging();
#if defined(_WIN64)
    ::AddVectoredExceptionHandler( 1, vectoredExceptionHandler );
    //::SetUnhandledExceptionFilter( unhandledExecptionFilter );
#endif

    try {
#if defined(_WIN64)
        _getcwd(path, 0x100);
#else
        getcwd(path, 0x100);
#endif
        setbuf(stdout, nullptr);

        start_func start = process::load_app(path);
        if (start == nullptr) {
            throw std::runtime_error("Unable to load eboot.bin");
        }
        LOG_DEBUG("\n------------------------------------------------ Starting game...----------------------------------------------------\n");

        pthread_attr_t attribute;
        pthread_t thread;
        int rc;
        rc = pthread_attr_init(&attribute);
        if (rc)
            throw std::runtime_error("pthread_attr_init");
        rc = pthread_attr_setstacksize(&attribute,0x1000000);
        if (rc)
            throw std::runtime_error("pthread_attr_setstacksize");
        rc = pthread_create(&thread, &attribute, game_thread, (void*)start);
        if (rc)
            throw std::runtime_error("pthread_create");
        rc = pthread_join(thread, nullptr);
        if (rc)
            throw std::runtime_error("pthread_join");
    } catch (std::exception& exc) {
        std::cout << exc.what() << std::endl;
    }
    return 0;
}
